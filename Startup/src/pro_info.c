/**********************************************************************
*
*	文件名称：pro_info.c
*	功能说明：工程相关信息程序
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0   2019-04-23   yxh       实现基本功能
*
*   Copyright (C), 2019-2028, www.geniusmedica.com
*
**********************************************************************/

#ifndef __PRO_INFO_C__
#define __PRO_INFO_C__

#include "pro_type.h"
#include "pro_info.h"

//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1

#pragma import(__use_no_semihosting)
              
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       

void _sys_exit(int x) 
{ 
	x = x; 
} 

int fputc(int ch, FILE *f)
{      
	while ((DEBUG_USART->SR&0X40) == 0);  
    DEBUG_USART->DR = (u8) ch;      
	
	return ch;
}
#endif 

#ifdef PRO_DEBUG_ENABLE

/**********************************************************************
* 函数名称：pro_debug_usart_init
* 功能描述：调试串口初始化
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其他说明：
*         ：默认情况下，调试串口都使用串口1((115200-8-N-1))
**********************************************************************/
void pro_debug_usart_init(void)
{
	GPIO_InitTypeDef 		GPIO_InitStructure;
	USART_InitTypeDef 		USART_InitStructure;
	
//#ifdef PRO_SHELL_ENABLE		
//	NVIC_InitTypeDef        NVIC_InitStructure;
//#endif  // PRO_SHELL_ENABLE 

	/* 使能DEBUG_USART和GPIOA的时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO , ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 , ENABLE);
	
	/* 配置DEBUG_USART Rx (PC.11) 端口为输入浮空模式 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* 配置DEBUG_USART Tx (PC.10) 端口为复用推挽模式 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

//#ifdef PRO_SHELL_ENABLE	
//	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2 ;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			
//	NVIC_Init(&NVIC_InitStructure);
//#endif  // PRO_SHELL_ENABLE

	/* DEBUG_USART的相关配置如下:
		- 波特率 = 115200 baud  
		- 数据长度 = 8 Bits
		- 一个停止位
		- 无奇偶校验
		- 失能硬件控制流(RTS 和 CTS 信号)
		- 使能接收和发送功能
	*/
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(DEBUG_USART, &USART_InitStructure);

//#ifdef PRO_SHELL_ENABLE	
	USART_ITConfig(DEBUG_USART, USART_IT_RXNE, ENABLE);
//#endif  // PRO_SHELL_ENABLE

	USART_Cmd(DEBUG_USART, ENABLE);
	USART_ClearFlag(DEBUG_USART, USART_FLAG_TC);	
	USART_ClearFlag(DEBUG_USART, USART_FLAG_RXNE);	
	
}

/**********************************************************************
* 函数名称：pro_debug_init
* 功能描述：调试串口初始化
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其他说明：无
**********************************************************************/
void pro_debug_init(void)
{
	pro_debug_usart_init();
}

/**********************************************************************
* 函数名称：debug_send_string
* 功能描述：调试串口输出字符串
* 输入参数：
*         ：*pucBufferPtr：数据指针		ulLength：数据长度 	        
* 输出参数：无
* 返 回 值：无
* 其他说明：无
**********************************************************************/	
void debug_send_string(uint8_t *pucBufferPtr, uint32_t ulLength )
{
    while ( ulLength != 0 )
    {			
		//while (USART_GetFlagStatus(DEBUG_USART, USART_FLAG_TC) == RESET);
		USART_SendData(DEBUG_USART, (uint8_t) *pucBufferPtr++);
		ulLength--;
		while (USART_GetFlagStatus(DEBUG_USART, USART_FLAG_TC) == RESET);	
	}
}

/**********************************************************************
* 函数名称：debug_printf
* 功能描述：调试串口输出字符串
* 输入参数：
*         ：*iBufferPtr：数据指针		iLength：数据长度 
* 输出参数：无
* 返 回 值：无
* 其他说明：
*         ：最大打印长度由s_acLogBuf决定,该函数最大打印长度为128字节
**********************************************************************/	
void debug_printf(const char *pucFmt,...)
{
	int lLength = 0;
	static char s_acLogBuf[128];
	va_list stArgs;
	
	va_start(stArgs,pucFmt);
	lLength = vsnprintf(s_acLogBuf, sizeof(s_acLogBuf), pucFmt, stArgs);
	if (lLength)
	{
		debug_send_string( (uint8_t*)s_acLogBuf, lLength);
	}
	va_end(stArgs);
}

/**********************************************************************
* 函数名称：pro_show_version
* 功能描述：软件基本信息初始化
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其他说明：无
**********************************************************************/	
void pro_show_version(void)
{
	
#ifdef PRO_DEBUG_ENABLE	
	PRO_DEBUG(SHOW_VERSION,("-----------------------------------------"));
	PRO_DEBUG(SHOW_VERSION,("\r\n \\ | / \r\n"));
	PRO_DEBUG(SHOW_VERSION,("- VD02 -    U1 Drive Board by STM32F103VET6 \r\n"));
	PRO_DEBUG(SHOW_VERSION,(" / | \\      V%d.%d.%d.%d  build %s \r\n", (int)PRO_VERSION,
						   (int)PRO_SUBVERSION,(int)PRO_REVISION1,(int)PRO_REVISION2, __DATE__));
	PRO_DEBUG(SHOW_VERSION,("            StdPeriph Version : V%d.%d.%d \r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
							__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2));
	PRO_DEBUG(SHOW_VERSION,(" 2019 - 2028 Copyright by genrui team \r\n"));
	PRO_DEBUG(SHOW_VERSION,("----------------------------------------- \r\n"));
#endif	
	
}

#ifdef PRO_SHELL_ENABLE

////extern uint8_t g_ucShellRecvSem;
////extern uint8_t g_ucShellRecvStatus;
////extern uint8_t g_ucShellRecvBuffer[128];    

/**********************************************************************
* 函数名称：DEBUG_USART_IRQHandler
* 功能描述：Shell功能接受数据函数
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其他说明：
*         ：当检测到0x0D 0x0A(\r\n)后，则认为接收完一帧数据
**********************************************************************/		
////void DEBUG_USART_IRQHandler(void)                	
////{
////	uint8_t ucRes;

////	/* DEBUG_USART接收缓冲区非空 */
////	if (USART_GetITStatus(DEBUG_USART, USART_IT_RXNE) != RESET)  
////	{
////		/* 接收DEBUG_USART数据 */
////		ucRes = USART_ReceiveData(DEBUG_USART);
////		
////		/* 一帧数据还未接收完 */
////		if ( (g_ucShellRecvStatus & 0x80) == 0)
////		{
////			/* 上一个接收数据为0x0D */
////			if (g_ucShellRecvStatus & 0x40)
////			{
////				/* 本次接收数据不为Ox0A */
////				if (ucRes != 0x0a)
////				{
////					/* 保存数据 */
////					g_ucShellRecvBuffer[g_ucShellRecvStatus & 0X3F] = ucRes;
////					g_ucShellRecvStatus++;
////					
////					/* 清除上次接收到0x0D标志 */
////					g_ucShellRecvStatus &= 0X3F;					 
////				}
////				else 
////				{
////					/* 设置接收到0x0A标志 */
////					g_ucShellRecvStatus |= 0x80;	
////						
////					/* 设置接收完一帧数据标志 */
////					g_ucShellRecvSem = 1;
////				}
////			}
////			else 
////			{	
////				/* 本次接收到的数据为0x0D */
////				if (ucRes == 0x0d)
////				{
////					/* 设置接收到0x0D标志 */
////					g_ucShellRecvStatus |= 0x40;
////					
////					/* 保存数据 */
////					g_ucShellRecvBuffer[g_ucShellRecvStatus & 0X3F] = ucRes;
////					g_ucShellRecvStatus++;
////				}
////				else
////				{
////					/* 保存数据 */
////					g_ucShellRecvBuffer[g_ucShellRecvStatus & 0X3F] = ucRes ;
////					g_ucShellRecvStatus++;
////					
////					/* 如果数据长度大于64，则清0*/
////					if (g_ucShellRecvStatus > (64 - 1))
////					{
////						g_ucShellRecvStatus = 0;  
////					}
////				}		 
////			}
////		}
////		USART_ClearITPendingBit(DEBUG_USART, USART_IT_RXNE); 		 
////     }
////}
#endif  // PRO_SHELL_ENABLE

#endif  // PRO_DEBUG_ENABLE

#endif  // __PRO_INFO_C__
