
#ifndef __PRO_SYS_C__
#define __PRO_SYS_C__

#include "pro_sys.h"

void pro_sys_init(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
}

void pro_sys_int_enter(void)
{
    if (OSRunning == OS_TRUE) 
	{
        if (OSIntNesting < 255u) 
		{
            OSIntNesting++;                     
        }
    }
}

void pro_sys_int_exit(void)
{
#if OS_CRITICAL_METHOD == 3u                            
    OS_CPU_SR  cpu_sr = 0u;
#endif

    if (OSRunning == OS_TRUE) 
	{
        OS_ENTER_CRITICAL();
        if (OSIntNesting > 0u) 
		{                           
            OSIntNesting--;
        }
        OS_EXIT_CRITICAL();
    }
}

#endif
