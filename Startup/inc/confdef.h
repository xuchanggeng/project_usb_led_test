/******************** (C) COPYRIGHT 2018 XuChangGeng ******************************
* File Name          : COMMAND.h
* Author             : XuChangGeng
* Version            : V1.0
* Date               : 28/06/2018
* Description        : use firmware lib version: V2.0.1 06/13/2008
********************************************************************************
* 
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _CONFDEF_H
#define _CONFDEF_H
#include <includes.h>



/*! CAN ID编号 */
#define             CAN_TARGET_ID				(0x00000000)



/*! 单元编号 */
#define			PC_PART_UNIT_CAN_ID						    	0x00					//上位机单元ID号
#define			CMD_PROCESS_UNIT_CAN_ID							0x10					//IO控制模块单元ID号
#define			CMD_STEAM_UNIT_CAN_ID							0x11					//蒸汽产生模块单元ID号
#define			CMD_MID_PART_UNIT_CAN_ID						0x12					//路由单元ID号

/*! 电机单元编号 */
#define				SALINE_PERFUSION_MOTOR			   			0x20					//生理盐水灌注电机
#define				WATER_PERFUSION_MOTOR			        	0x21                    //无菌水注射电机       
       


/*! 数据帧控制字 */
#define CONTROL_C1C0_STATUS_REG_READ            											      	0x00    // 读状态寄存器控制字
#define CONTROL_C1C0_DATA_REG_READ             											        	0x01    // 读数据寄存器控制字
#define CONTROL_C1C0_DATA_REG_WRITE            											        	0x02    // 写数据寄存器控制字
#define CONTROL_C1C0_CMD_OPERATION               											        0x03    // 命令操作控制字

/*! 命令编号 */
#define CMD_PROCESS_DATA_READ            											        	    0x01    // IO板数据读取
#define CMD_PROCESS_DATA_WRITE                      											    0x02    // IO板数据写入
#define CMD_PROCESS_SALINE_MOTOR_OP            											      	    0x03    // 生理盐水电机操作指令
#define CMD_PROCESS_SALINE_MOTOR_RUN_SPEED            											    0x04    // 生理盐水电机指定速度运行指令
#define CMD_PROCESS_WATER_MOTOR_OP            											      	    0x05    // 无菌水电机操作指令
#define CMD_PROCESS_WATER_MOTOR_REJECT_VOL            											    0x06    // 采无菌水电机注射指定的液量指令
#define CMD_PROCESS_WATER_MOTOR_RUN_SPEED            											    0x07    // 无菌水电机指定速度运行指令
#define CMD_PROCESS_THREE_LED_OP            											            0x08    // 色灯控制指令
#define CMD_PROCESS_NEEDLE_ONE_PULSE_OP            											        0x09    // 针控制指令


#define SALINE_UL_TO_STEP       (4*8)    // 采样针注射器电机转换系数(ul -> step)(*8转换成微步)
#define WATER_UL_TO_STEP        (0.4*8)  // 缓冲液注射器电机转换系数(ul -> step)(*8转换成微步)

#define SALINE_PERFUSION_MOTOR_SPEES_LEVEL1        (0x01)    // 生理盐水电机灌注速度1,慢速
#define SALINE_PERFUSION_MOTOR_SPEES_LEVEL2        (0x02)    // 生理盐水电机灌注速度2,高速

/*! 状态还回参数 */
#define CMD_TYPE_WATER_SYRING_INSTALL_STATUS            									0x02    // 无菌水注射器是否安装正确
#define CMD_TYPE_SALINE_MOTOR_PERFUSION_STATUS                      						0x03    // 生理盐水电机灌注状态
#define CMD_TYPE_NEEDLE_STATUS            											        0x04    // 枪头针状态
#define CMD_TYPE_WATER_BTN_STATUS            											    0x05    // 无菌水蒸汽按钮状态




/*! 温控单元命令码 */
#define  CMD_TEMP_UNIT_SYSTEM            											        		0x00    //显微镜杆单元系统指令
#define  CMD_TEMP_UNIT_QUERY             											        		0x01    //显微镜杆单元查询指令
#define  CMD_TEMP_UNIT_CONFIG            											        		0x02    //显微镜杆单元配置指令


/*! 通信数据帧长度 */
#define 		MAX_PARA_NUM                            100
/*! 命令帧执行结果信息长度 */
#define 		MAX_RESULT_INFO_NUM                     10


/*! 电机错误信息码 */
#define   		ERR_CMD_NO            						        0x00               /*!< 没有错误 */
#define    		ERR_NONE                							0x00               //无错误 
#define    		ERR_MOTOR_ACT_EXE       							0x01               //电机正在执行动作 
#define    		ERR_MOTOR_ACT_YNOTSTART        						0x02               //电机运动时Y电机不在初始位
#define    		ERR_MOTOR_RESET_STEPOUT								0x03               //电机复位动作走完步数未到达初始位
#define    		ERR_MOTOR_RESETSTEP1_STEPOUT 						0x04               //电机复位动作走完步数未离开初始位
#define    		ERR_MOTOR_STOP      								0x05               //电机停止失败
#define    		ERR_MOTOR_ACT_TIMEOUT        						0x06               //电机动作执行超时
#define    		ERR_MOTOR_ACT       								0x07               //电机动作执行失败
#define    		ERR_MOTOR_AT_INTANGIBLY_POS     					0x08               //电机到指定位置时在不明位置
#define    		ERR_MOTOR_NOTHIS_POS			    				0x09               //电机指定位置不存在
#define    		ERR_MOTOR_RESETSTART_STEPOUT        				0x0A               //电机复位动作走完步数未到达指定位置
#define    		ERR_MOTOR_GOOVERSEPP_NOWIN_CUPPOS   				0x0B               //电机走完步数未到达指定位置
#define    		ERR_MOTOR_ABSORB_CHECK_NO_LIQUID   					0x0C               //电机吸液没有检测到液面
#define    		ERR_MOTOR_HIT   									0x0D               //电机撞针
#define    		ERR_MOTOR_X_RUN_MOTOR_Y_NO_RESET_POS				0x0E               //水平电机运动时，垂直电机不在初始位
#define			ERR_EEPROM_EXE										0x0F				// EEPROM读写错误
#define			ERR_FPGA_EXE										0x10				// FPGA读写错误
#define			ERR_GATHER_DATA_TIMEOUT								0x11				// FPGA采集数据超时
#define	   		ERR_CMD_PARAM							     	    0x12		       //命令参数错误
#define         ERR_CODE_SCANNER_TIMEOUT                            0x13               //扫码超时失败
#define         ERR_VALVE_NUM_OVER								    0x14               //超过协议阀编号
#define         ERR_VALVE_VALUE									    0x15               //记忆阀状态值错误
#define         ERR_DC_MOTOR_NUM_OVER							    0x16               //超过协议直流电机编号
#define         ERR_DC_MOTOR									    0x17               //直流电机错误
#define    		ERR_MOTOR_ACT_DILSYRNOTSTART        				0x18               //电机运动时稀释液注射器电机不在初始位
#define    		ERR_MOTOR_ACT_WASHNOTSTART        				    0x19               //电机运动时清洗电机不在初始位


/*! 单元错误信息码 */
#define    		ERR_UNIT_ACT_EXE       								0x26               //电机正在执行动作 
#define    		ERR_UNIT_NO_THIS_UNIT		       					0x27               //不是本单元的指令 

_EXT_ UINT8 set_debug;


#endif /*_CONFDEF_H*/

