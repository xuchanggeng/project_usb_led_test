/******************** (C) COPYRIGHT 2018 XuchangGeng ******************************
* File Name          : Define.c
* Author             : XuchangGeng
* Version            : V1.0
* Date               : 28/06/2018
* Description        : use firmware lib version: V3.5.1 06/13/2018
********************************************************************************
* 
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEFINE_H
#define __DEFINE_H

typedef enum{
	false = 0,
	true  = !false,
}bool;

typedef enum 
{
	DISENABLE = 0,
	ENABLED =!DISENABLE,
}state_t;



/*! 通信错误标识 */
#define    	ERR_CMD_START            				0x01             	// 起始码错误 
#define    	ERR_CMD_FUNCTION         				0x02             	// 控制码中的功能码错误 
#define    	ERR_CMD_DATA_LENGTH      				0x03             	// 数据长度错误 
#define    	ERR_CMD_ID               				0x04             	// 命令标识错误 
#define    	ERR_CMD_CHECK            				0x05             	// 校验码错误 
#define    	ERR_CMD_END              				0x06             	// 结束码错误 
#define    	ERR_CMD_MIX_FRAME              			0x07             	// 采样位帧错误 
#define    	ERR_CMD_SAMPLE_FRAME            		0x08             	// 混合位帧错误 

/*!>.该驱动版UART 接口	 		*/
#define EL180_DRIVER_BOARD_NULL  	((unsigned char)0x00)		/*>.空板			*/
#define EL180_DRIVER_BOARD_0  		((unsigned char)0x01)		/*>.主控板			*/
#define EL180_DRIVER_BOARD_1  		((unsigned char)0x02)		/*>.自动进样板  	*/
#define EL180_DRIVER_BOARD_2  		((unsigned char)0x03) 	    /*>.采样轴单元+注射器单元	*/
#define EL180_DRIVER_BOARD_3  		((unsigned char)0x04)		/*>.tip头仓盒单元+卡盒单元+载体台单元	*/
#define EL180_DRIVER_BOARD_4  		((unsigned char)0x05)		/*>.孵育盒拍照单元 显微镜单元1	*/
#define EL180_DRIVER_BOARD_5  		((unsigned char)0x06)		/*>.温控板				*/ 
#define EL180_DRIVER_BOARD_ALL  	((unsigned char)0x3F)		/*>.全部驱动		    */ 
#define EL180_DRIVER_BOARD_BACK_OFFSET  ((unsigned char)0x10)		/*>.还回帧ID偏移		    */ 


#define EL180_LOCAL_CAN_ID  	EL180_DRIVER_BOARD_5			/*>.驱动板CAN本地ID			*/
#define EL180_RADIO_CAN_ID  	EL180_DRIVER_BOARD_ALL			/*>.驱动板CAN本地ID			*/

/* 电机动作指令 */
enum
{
	MOTOR_STATE_IDLE = 0,
	MOTOR_STATE_RUN,
}; 

/* 电机速度等级 */
enum
{
	MOTOR_SPEED_LEVEL1 = 0,
	MOTOR_SPEED_LEVEL2    ,
	MOTOR_SPEED_LEVEL3    ,
	MOTOR_SPEED_LEVEL4    ,
	MOTOR_SPEED_LEVEL5    ,
	MOTOR_SPEED_LEVEL6    ,
	MOTOR_SPEED_LEVEL7    ,
	MOTOR_SPEED_LEVEL8    ,
	MOTOR_SPEED_LEVEL9    ,
	MOTOR_SPEED_LEVEL10    ,
	MOTOR_SPEED_LEVEL11    ,
	MOTOR_SPEED_LEVEL12    ,
	MOTOR_SPEED_LEVEL13    ,
	MOTOR_SPEED_LEVEL14    ,
	MOTOR_SPEED_LEVEL15    ,
	MOTOR_SPEED_LEVEL16    ,
	MOTOR_SPEED_LEVEL17    ,

};





/* 电机动作指令 */
typedef enum
{
	ACTION_MIX_NULL              =0, 
	
	ACTION_ALL_RESET             =1,      
	ACTION_ALL_RUN_ONEPOS	       =2, 
	ACTION_ALL_RESET_RUN         =3,    
  
	ACTION_MIX_RESET             =10,      
	ACTION_MIX_RUN	             =11,     
	ACTION_MIX_STOP              =12,      
	ACTION_MIX_STEPONEPOS        =13,      
	ACTION_MIX_COMPENSATE        =14,

}ACTIONCMD_MIX; //动作指令


/* 直流电机的开关控制 */
#define     DC_MOTOR_ON            1
#define     DC_MOTOR_OFF           0


/*! 延时10ms，以定时器0为时基 */
/*! 延时1ms */
#define DELAY_1MS          (1)

/*! 延时2ms */
#define DELAY_2MS          (2)

/*! 延时5ms */
#define DELAY_5MS          (5)

/*! 延时10ms */
#define DELAY_10MS         (10)

/*! 延时20ms */
#define DELAY_20MS         (2*(DELAY_10MS))

/*! 延时50ms */
#define DELAY_50MS         (5*(DELAY_10MS))

/*! 延时100ms */
#define DELAY_100MS        (10*(DELAY_10MS))

/*! 延时200ms */
#define DELAY_200MS        (20*(DELAY_10MS))

/*! 延时400ms */
#define DELAY_400MS        (40*(DELAY_10MS))

/*! 延时500ms */
#define DELAY_500MS        (50*(DELAY_10MS))

/*! 延时1000ms */
#define DELAY_1000MS       (100*(DELAY_10MS))


#define  MOTOR_MUM                          7
#define  SPEEDLEVEL_NUM                     10
#define  ACCU_NUM                           200

#endif /* __DEFINE_H */

