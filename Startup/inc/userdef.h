/**********************************************************************
*
*	文件名称：userdef.h
*	功能说明：用户条件编译相关宏定义
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2028, www.geniusmedica.com
*
**********************************************************************/

#ifndef __USERDEF_H__
#define __USERDEF_H__

#include "stm32f10x.h"

#define PRO_VERSION                      1L      		// 注册版本号 
#define PRO_SUBVERSION                   0L      		// 工程变更版本号 
#define PRO_REVISION1					 0L      		// 特殊版本号 
#define PRO_REVISION2					 21L      		// 自定义版本号


#define PRO_DEBUG_ENABLE                         		// 使能调试功能

#define SYSTEM_SUPPORT_OS				 1				// 定义系统文件夹是否支持UCOS
#define INIT_DEBUG						 1		 		// 开启所有初始化打印信息	
#define COMM_DEBUG                   	 1       		// 开启所有通信调试打印信息
#define ERR_DEBUG                        1       		// 开启所有错误打印信息
//#define VALVE_DEBUG						 0			// 开启电磁阀的调试打印信息
#define REAGENT_DEBUG                    0              // 开启试剂量调试打印信息
#define MOTOR_DEBUG                      1      		// 开启电机调试信息
#define INFO_DEBUG                       1      		// 开启提示信息
#define TEMP_DEBUG                       1      		// 开启提示信息


#define DEBUG_USART                        USART1     					//接受上位机串口
#define COM_USART                          UART4     					//接受上位机串口
#define SLAVE_USART                        UART4     					//接受从机串口
#define HOST_USART                         USART2     					//接受上位机串口
#define HOST_COM_IRQHandler                USART2_IRQHandler     		//主接受串口中断程序

#endif


