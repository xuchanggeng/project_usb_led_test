/**********************************************************************
*
*	文件名称：pro_info.h
*	功能说明：工程相关信息程序
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0   2019-04-23   yxh       实现基本功能
*
*   Copyright (C), 2019-2028, www.geniusmedica.com
*
**********************************************************************/

#ifndef __PRO_INFO_H__
#define __PRO_INFO_H__

#include <stdio.h>
#include <stdarg.h>
#include "userdef.h"

#define DEBUG_ON			0x01U
#define DEBUG_OFF			0x00U
#define DEBUG_ALL_ON    	0xFFU

#ifndef SHOW_VERSION_ENABLE
#define SHOW_VERSION		DEBUG_ON
#endif

#ifndef DEBUG_HALT_ENABLE
#define DEBUG_HALT			DEBUG_OFF
#else
#define DEBUG_HALT			DEBUG_ON
#endif

#ifdef PRO_DEBUG_ENABLE

void pro_debug_init(void);
void debug_printf(const char *pucFmt,...);
#define DEBUG_DIAG(message) do { debug_printf  message;} while(0)

#define PRO_DEBUG(debug,message) do {\
										if((debug) & DEBUG_ALL_ON){\
											DEBUG_DIAG(message);\
											if((debug) & DEBUG_HALT){\
												while(1);\
											}\
										}\
									}while(0)

#else	// PRO_DEBUG_ENABLE
										
#define PRO_DEBUG(debug,message) 
										
#endif	// PRO_DEBUG_ENABLE 							
	
void pro_show_version(void);
									
#endif	// __PRO_INFO_H__
