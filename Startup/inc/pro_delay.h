
#ifndef __PRO_DELAY_H__
#define __PRO_DELAY_H__ 			   

#include "includes.h" 
 
void pro_delay_init(void);
void pro_delay_ms(u16 nms);
void pro_delay_us(u32 nus);

#endif
