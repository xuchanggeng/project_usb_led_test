/**
  ******************************************************************************
  * @文件   bsp_tm1640.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "bsp_tm1640.h"
#include "pro_delay.h"
/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 --------------------------------------------------------------*/

/* 本地函数 ----------------------------------------------------------------*/

void TM1640_Init(void)
{		
		GPIO_InitTypeDef GPIO_InitStructure;
		RCC_APB2PeriphClockCmd(SCLK_GPIO_CLK|DIN_GPIO_CLK, ENABLE);
//		RCC_APB2PeriphClockCmd(EN_GPIO_CLK, ENABLE);
		
		GPIO_InitStructure.GPIO_Pin = SCLK_GPIO_PIN;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;     
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(SCLK_GPIO_PORT, &GPIO_InitStructure);   
		GPIO_SetBits(SCLK_GPIO_PORT, SCLK_GPIO_PIN);	//  SCLK
	
		GPIO_InitStructure.GPIO_Pin = DIN_GPIO_PIN;  
		GPIO_Init(DIN_GPIO_PORT, &GPIO_InitStructure);     
		GPIO_SetBits(DIN_GPIO_PORT, DIN_GPIO_PIN); //DIN
	
}

static void delay_us(uint16_t unCount) 
{ 
	uint8_t j,k;
	uint16_t i;

	for ( i = 0; i < unCount; i++ )
	{
		for ( j = 0; j < 8; j++ )
		{
			k++;
		}
	}

}


/****************************************  
起始函数
***************************************/  
void TM1640Start(void)  
{   
        TM1640SLK_HING;
		delay_us(2);
		TM1640DAT_HING;    
        delay_us(2) ;  
        TM1640DAT_LOW;
		delay_us(2);  
} 
/*************************************  
结束函数
***************************************/  
void TM1640Stop(void)  
{       
		TM1640SLK_LOW;  
        delay_us(2);
		TM1640DAT_LOW;  
        delay_us(2); 
        TM1640SLK_HING;  
        delay_us(2);
        TM1640DAT_HING;            
}  
/*************************************  
TM1640WriteByte  
写一字节数据  date 为所要写的数据
***************************************/  
void TM1640WriteByte(u8 date)  
{  
        u8 i;     
        for(i=0;i<8;i++)  
        {  
                TM1640SLK_LOW;  
                delay_us(2);  
                if(date & 0x01)  //先低位后高位
                {  
					TM1640DAT_HING;  
					delay_us(3);  
                }  
                else  
                {  
					TM1640DAT_LOW;  
					delay_us(3);  
                } 
				date = date>>1;		//数据右移一位						
                TM1640SLK_HING;
				delay_us(3);                
        }				
} 
/***************************************
发送数组
Addr1640：起始地址值
*a : 所要发送的数组
ValueLong:想要发送的数据长度
适用于地址自加1模式
**************************************/
void TM1640_SendData(u8 Addr1640,u8 *a,u8 ValueLong)  
{  
        u8 i;       
        TM1640Start();  
        TM1640WriteByte(Addr1640);   
          
        for(i=0;i<ValueLong;i++)  
        {  
              TM1640WriteByte(a[i]);   
        }        
        TM1640Stop();   
}

void tm1640_write_byte_inc_mode(uint8_t *Buffer, uint8_t ucLen)
{
	TM1640Start();
	TM1640WriteByte(0x40);
	TM1640Stop();
	TM1640Start();
	TM1640_SendData(0xc0,Buffer,ucLen);
	TM1640Stop();
	TM1640Start();
	TM1640WriteByte(0x8b);
	TM1640Stop();
	
}

void tm1640_write_byte_fixed_mode(uint8_t ucAddr, uint8_t ucData)
{
	
	TM1640Start();
	TM1640WriteByte(0x44);
	TM1640Stop();
	TM1640Start();
	TM1640WriteByte(0xC0+ucAddr);
	TM1640WriteByte(ucData);
	TM1640Stop();
}

//共阴极，数码管正常排列数字对应表
u8 Test_data[8] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07};//0,1 2 3 4 5 6 7 
//void main()
//{
//	u8 flag = 1;
//	delay_init();
//	TM1640_Init();
//	while(1)
//	{
//	//地址自加  起始地址为00H 长度为8
//	if(flag)
//	{
//		TM1640Start();
//		TM1640WriteByte(0x40);
//		TM1640Stop();
//		TM1640Start();
//		TM1640_SendData(0xc0,Test_data,8);
//		TM1640Stop();
//		TM1640Start();
//		TM1640WriteByte(0x8b);
//		TM1640Stop();

//		flag = 0;
//	}
//	else
//	{
//		TM1640Start();
//		TM1640WriteByte(0x44);
//		TM1640Stop();
//		TM1640Start();
//		TM1640WriteByte(0xC0);
//		TM1640WriteByte(Test_data[6]);
//		TM1640Stop();
//		TM1640Start();
//		TM1640WriteByte(0xC1);
//		TM1640WriteByte(Test_data[0]);
//		TM1640Stop();
//		TM1640Start();
//		TM1640WriteByte(0xC2);
//		TM1640WriteByte(GRID1_8(Test_data[4]);
//		TM1640Stop();
//		flag = 0;
//	}
//	}
//	
//}

