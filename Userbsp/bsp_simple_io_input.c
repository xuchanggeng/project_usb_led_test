/**
  ******************************************************************************
  * @文件   bsp_simple_io_input.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "bsp_simple_io_input.h"
#include "inline_function.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
#define IO_INPUT_NUM					 4       				//IO 输入数量
#define IO_INPUT_X_DIY					 5       				//IO 输入滤波次数

/* 本地变量 ---------------------------------------------------------------*/
/*! 输入IO状态 */
static uint32_t s_ulIOInputX = 0;
/*! IO 输入滤波 */
static uint8_t  s_ucIOFilterDiy[IO_INPUT_NUM] = {9};
/*! IO输入按钮状态 */
static uint8_t  s_ucIOBtnState[IO_INPUT_NUM] = {0};
/*! IO输入按钮 旧状态 */
static uint8_t  s_ucIOBtnStateOld[IO_INPUT_NUM] = {0};
/*! IO输入按钮 新状态 */
static uint8_t  s_ucIOBtnStateNew[IO_INPUT_NUM] = {0};
/*! IO 管脚配置 */
static simple_io_input_t s_stSimpleIOInputConfig[] = 
{
  /*! IO1 管脚参数 */
  [0] =
  {
    GPIOD,
    RCC_AHB1Periph_GPIOD,
    GPIO_Pin_0,
  },
  /*! IO2 管脚参数 */
  [1] = 
  {
    GPIOD,
    RCC_AHB1Periph_GPIOD,
    GPIO_Pin_1,
  },
  /*! IO1 管脚参数 */
  [2] =
  {
    GPIOD,
    RCC_AHB1Periph_GPIOD,
    GPIO_Pin_2,
  },
  /*! IO2 管脚参数 */
  [3] = 
  {
    GPIOD,
    RCC_AHB1Periph_GPIOD,
    GPIO_Pin_3,
  },

};


/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 --------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 函数名称: bsp_simple_io_input_struct_init
* 功能描述: 电机定时器模块初始化
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void bsp_simple_io_input_struct_init(uint8_t ucIndex)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	/*! 打开GPIO的时钟 */
	RCC_AHB1PeriphClockCmd(s_stSimpleIOInputConfig[ucIndex].io_Clk, ENABLE);	   
	
	/*! 打开GPIO的管脚配置 */
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;   
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   
	GPIO_InitStructure.GPIO_Pin   = s_stSimpleIOInputConfig[ucIndex].io_Pin;	
	GPIO_Init(s_stSimpleIOInputConfig[ucIndex].io_Port, &GPIO_InitStructure);
	
}



/*******************************************************************************
* 函数名称: bsp_motor_pwm_init
* 功能描述: 电机pwm对应的通道初始化初始化
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void bsp_simple_io_input_gpio_init(void)
{
  uint16_t unIndex = 0;

  for(unIndex = 0; unIndex < IO_INPUT_NUM; unIndex++)
  {
    bsp_simple_io_input_struct_init(unIndex);
  }
	
}

/*******************************************************************************
* 函数名称: bsp_motor_pwm_init
* 功能描述: 电机pwm对应的通道初始化初始化
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void bsp_simple_io_input_x_filter(uint16_t unIOIndex)
{
	const uint32_t ulPosBit = 0x00000001;

	/*! 按键数量超过32 */
	if(unIOIndex >= IO_INPUT_NUM)
	{
		PRO_DEBUG(INIT_DEBUG, ("function: bsp_simple_io_input_x_filter， error:Parameter out of range \r\n"));
	}

	if(GPIO_ReadRegisterBit(s_stSimpleIOInputConfig[unIOIndex].io_Port, s_stSimpleIOInputConfig[unIOIndex].io_Pin)==Bit_RESET)
    {
		if(s_ucIOFilterDiy[unIOIndex]<IO_INPUT_X_DIY)
		{
			s_ucIOFilterDiy[unIOIndex]++;
		}
		else
		{
			s_ulIOInputX |= (ulPosBit << unIOIndex);
		}
	}//X00
	else
    {
		s_ucIOFilterDiy[unIOIndex] = 0;
		s_ulIOInputX &= ~(ulPosBit << unIOIndex);
	}
}
 
/*******************************************************************************
* 函数名称: bsp_simple_io_input_x_edge
* 功能描述: 电机定时器初始化
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void bsp_simple_io_input_x_edge(void)
{
	uint8_t i = 0;
	uint32_t  m = 0;
    
	/*! 按键数量超过32 */
//	if(unIOIndex >= IO_INPUT_NUM)
//	{
//		PRO_DEBUG(INIT_DEBUG, ("function: bsp_simple_io_input_x_edge， error:Parameter out of range \r\n"));
//	}
	
	/*! 读取新状态 */
	for(i = 0; i < IO_INPUT_NUM; i++)
	{
		m = 1<<i;
		if((s_ulIOInputX & m) != 0)
			s_ucIOBtnStateNew[i] = BTN_ON;
		else
			s_ucIOBtnStateNew[i] = BTN_OFF;
	}
	
	/*! 打开计数 */	
	for(i = 0; i < IO_INPUT_NUM; i++)
	{
		/*! 状态判断，IO边沿检测 */
		if(s_ucIOBtnStateOld[i] != s_ucIOBtnStateNew[i])
		{
			if(s_ucIOBtnStateOld[i] == BTN_OFF && s_ucIOBtnStateNew[i] == BTN_ON)
			{
				s_ucIOBtnState[i] = BTN_OFF_TO_ON;

			}
			else if(s_ucIOBtnStateOld[i] == BTN_ON && s_ucIOBtnStateNew[i]==BTN_OFF)
			{
				s_ucIOBtnState[i] = BTN_ON_TO_OFF;

			}
		}
		
		/*! 更新上一次状态 */
		s_ucIOBtnStateOld[i] = s_ucIOBtnStateNew[i];
	}	

}

/*******************************************************************************
* 函数名称: bsp_motor_pwm_init
* 功能描述: 电机定时器初始化
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_simple_io_input_init(void)
{
	bsp_simple_io_input_gpio_init();
}



/*******************************************************************************
* 函数名称: bsp_set_motor_pwm_output
* 功能描述: 设置BLDC 或 PMSM电机的U V W 的输出
* 输入参数: 
			unUa ：基地址    
			unUb ：偏移地址
			unUc : 数据
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_simple_io_input_tick(void)
{
	uint16_t unIndex = 0;
	
	/*! IO 输入处理 通用滤波*/
	for(unIndex = 0; unIndex < IO_INPUT_NUM; unIndex++)
	{
		bsp_simple_io_input_x_filter(unIndex);
	}
	
	/*! IO 输入处理 边沿检测*/
    bsp_simple_io_input_x_edge();
}

/*******************************************************************************
* 函数名称: bsp_set_motor_pwm_output
* 功能描述: 设置BLDC 或 PMSM电机的U V W 的输出
* 输入参数: 
			unKeyIndex : 按键序号 
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
simple_io_input_state_e bsp_simple_io_input_get_value(uint16_t unKeyIndex)
{
	simple_io_input_state_e stState = IO_KEY_OFF;
	uint32_t ulPosBit  = 0;
     
     if(unKeyIndex > IO_INPUT_NUM)
     {
		PRO_DEBUG(INIT_DEBUG, ("function: bsp_simple_io_input_x_edge， error:Parameter out of range \r\n"));
     }

     ulPosBit = 0x00000001 << unKeyIndex;
     /*! 判断对应的位 */
     if((s_ulIOInputX & ulPosBit) != 0)
     {
        stState = IO_KEY_ON;
     }

     return stState;

}


/*******************************************************************************
* 函数名称: bsp_set_motor_pwm_output
* 功能描述: 设置BLDC 或 PMSM电机的U V W 的输出
* 输入参数: 
			unKeyIndex : 按键序号 
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
simple_io_btnstate_e bsp_simple_io_input_get_btn_state(uint16_t unKeyIndex)
{
    simple_io_btnstate_e stState = (simple_io_btnstate_e)0;

    stState = (simple_io_btnstate_e)s_ucIOBtnState[unKeyIndex];

     return stState;

}

/*******************************************************************************
* 函数名称: bsp_set_motor_pwm_output
* 功能描述: 设置BLDC 或 PMSM电机的U V W 的输出
* 输入参数: 
			unKeyIndex : 按键序号 
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_simple_io_input_clear_btn_state(uint16_t unKeyIndex)
{
    s_ucIOBtnState[unKeyIndex] = 0;
}



/*******************************************************************************
* 函数名称: bsp_simple_io_input_test
* 功能描述: 测试用例
* 输入参数: 
			unUa ：基地址    
			unUb ：偏移地址
			unUc : 数据
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_simple_io_input_test(uint16_t unUa, uint16_t unUb, uint16_t unUc)
{


}
























