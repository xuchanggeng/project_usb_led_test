/**
  ******************************************************************************
  * @文件   key_event.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __Key_Event_H
#define __Key_Event_H

/* 文件包含 ------------------------------------------------------------------*/
#include "defines.h"
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
void key_init(void);
void key_event_handle(void);
/* 导出变量 ---------------------------------------------------------- */

#endif /* __Key_Event_H */

