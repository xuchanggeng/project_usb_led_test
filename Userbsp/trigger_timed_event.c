/**
  ******************************************************************************
  * @文件   trigger_timed_event.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "trigger_timed_event.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 --------------------------------------------------------------*/
static void timed_event_calori_temp_update(void);
/* 本地变量 ---------------------------------------------------------------*/
static trigger_timed_event_t s_astTimeEventControl[]=
{
    {EVENT_RUN, RUN_INTERVAL_100MS, 0, timed_event_calori_temp_update},


};


/* 本地函数 ----------------------------------------------------------------*/
/*******************************************************************************
* 函数名称: timed_event_calori_temp_update
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void timed_event_calori_temp_update(void)
{
//    PRO_DEBUG(INFO_DEBUG,("trigger_timed_event_start index err"));
}


/*******************************************************************************
* 函数名称: trigger_timed_event_start
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void trigger_timed_event_start(uint8_t ucIndex, uint8_t ucFlag, uint32_t ucIntarval)
{
    if(ucIndex >= ARRAY_SIZE(s_astTimeEventControl) )
    {
        PRO_DEBUG(ERR_DEBUG,("trigger_timed_event_start index err"));
        return;
    }

    s_astTimeEventControl[ucIndex].ucRunFlag  = ucFlag;
    s_astTimeEventControl[ucIndex].ulInterval = ucIntarval;
}
 
/*******************************************************************************
* 函数名称: trigger_timed_event_task
* 功能描述: 定时任务执行
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void trigger_timed_event_task()
{
    for(int8_t ucIndex = 0; ucIndex < ARRAY_SIZE(s_astTimeEventControl); ucIndex++)
    {
        s_astTimeEventControl[ucIndex].ulTick++;
        if(s_astTimeEventControl[ucIndex].ulTick > s_astTimeEventControl[ucIndex].ulInterval)
        {
            /*! 有效函数指针 */
            if(VALID_POINTER(s_astTimeEventControl[ucIndex].timed_event_func) && 
                             (s_astTimeEventControl[ucIndex].ucRunFlag == EVENT_RUN))
            {
                s_astTimeEventControl[ucIndex].timed_event_func();
            }

            s_astTimeEventControl[ucIndex].ulTick = 0;
        }

    }

}









