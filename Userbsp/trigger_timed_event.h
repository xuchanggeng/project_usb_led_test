/**
  ******************************************************************************
  * @文件   trigger_timed_event.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __trigger_timed_event_H
#define __trigger_timed_event_H

/* 文件包含 ------------------------------------------------------------------*/
#include "defines.h"
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
typedef struct __tag_trigger_timed_event_t
{
    uint8_t  ucRunFlag;
    uint32_t ulInterval;
    uint32_t ulTick;
    /* data */
    void (*timed_event_func)();
} trigger_timed_event_t;

/*! 定时任务编号 */
typedef enum 
{
    TIMED_EVENT_ID_1,
    TIMED_EVENT_ID_2,
}timed_event_id_e;

/* 导出常量 -----------------------------------------------------------*/
/*! 间隔运行状态 */
#define EVENT_RUN                   1
#define EVENT_STOP                  0

/*! 调用间隔1ms */
#define RUN_INTERVAL_1MS            (1)

/*! 调用间隔10ms */
#define RUN_INTERVAL_10MS           (10*(RUN_INTERVAL_1MS))

/*! 调用间隔100ms */
#define RUN_INTERVAL_100MS          (10*(RUN_INTERVAL_10MS))

/*! 调用间隔1000ms */
#define RUN_INTERVAL_1000MS         (100*(RUN_INTERVAL_10MS))


/* 导出函数 ---------------------------------------------------------- */
/*! 触发定时时间执行 */
void trigger_timed_event_start(uint8_t ucIndex, uint8_t ucFlag, uint32_t ucIntarval);

/*! 定时执行任务 */
void trigger_timed_event_task(void);

/* 导出变量 ---------------------------------------------------------- */

#endif /* __trigger_timed_event_H */



