/**
  ******************************************************************************
  * @文件   bsp_simple_io_input.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __bsp_simple_io_input_H
#define __bsp_simple_io_input_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
/*! IO 输入管脚配置 */
typedef struct _tag_simple_io_input_t
{
    GPIO_TypeDef*  io_Port;
    uint32_t       io_Clk;
    uint16_t       io_Pin;
	
}simple_io_input_t;

/* 导出常量 -----------------------------------------------------------*/
/*! 按键输入状态 */
typedef enum _tag_simple_io_input_state
{
	IO_KEY_OFF      = 0,	           //按键弹起
	IO_KEY_ON       = 1,	           //按键按下
	IO_KEY_REVERSE  = 2,	           //反转按键状态
}simple_io_input_state_e;
/*! IO 按钮边沿状态 */
typedef enum _tag_simple_io_btnstate
{
    BTN_OFF        = 0,     //按钮松开中
    BTN_OFF_TO_ON  = 1,     //按钮按下边缘
    BTN_ON         = 2,     //按钮按下中
    BTN_ON_TO_OFF  = 3,     //按钮松开边缘
}simple_io_btnstate_e;

/*! IO 输入信号编号 */
typedef enum _tag_simple_io_input_xpos
{
    INPUT_IO_1                      = 0,                     //输入编号为1
    INPUT_IO_2                      = 1,                     //输入编号为2
    INPUT_IO_3                      = 2,                     //输入编号为3

}simple_io_input_xpos_e;


/* 导出函数 ---------------------------------------------------------- */
void bsp_simple_io_input_init(void);
void bsp_simple_io_input_tick(void);
simple_io_input_state_e bsp_simple_io_input_get_value(uint16_t unKeyIndex);
simple_io_btnstate_e bsp_simple_io_input_get_btn_state(uint16_t unKeyIndex);
void bsp_simple_io_input_clear_btn_state(uint16_t unKeyIndex);



/* 导出变量 ---------------------------------------------------------- */

#endif /*! __bsp_simple_io_input_H */

