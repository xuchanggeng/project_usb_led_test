/**********************************************************************
*
*	文件名称：bsp_ds18b20.c
*	功能说明：DS18B20温度传感器驱动程序
*	特殊说明：
*			：1.DS18B20芯片出厂配置为12位模式
*           ：2.DS18B20芯片在12位模式下最大转换时间为750ms  
*			：3.DS18B20芯片上电温度值为85度
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2028, www.geniusmedica.com
*
**********************************************************************/

#include "includes.h"
#include "bsp_ds18b20.h"

/*! 定义DS18B20传感器初始化结构体 */
typedef struct
{
	uint8_t ucId;				// 传感器ID
	uint32_t ulRcc;				// 传感器引脚对应时钟
	GPIO_TypeDef* pstGpio;		// 传感器对应GPIO口
	uint16_t unPin;				// 传感器对应PIN脚		
}ds18b20_init_t;

/*! 初始化DS18B20传感器初始化结构体 */
ds18b20_init_t s_astDs18b20Param[ MAX_DS18B20_NUM ] = 
{
	{ BSP_DS18B20_TEMP1, RCC_APB2Periph_GPIOB, GPIOB, GPIO_Pin_9 },
//	{ BSP_DS18B20_TEMP2, RCC_APB2Periph_GPIOA, GPIOA, GPIO_Pin_5 },
//	{ BSP_DS18B20_TEMP3, RCC_APB2Periph_GPIOA, GPIOA, GPIO_Pin_6 },
//	{ BSP_DS18B20_TEMP4, RCC_APB2Periph_GPIOA, GPIOA, GPIO_Pin_7 },
//	{ BSP_DS18B20_TEMP5, RCC_APB2Periph_GPIOC, GPIOC, GPIO_Pin_4 },
//	{ BSP_DS18B20_TEMP6, RCC_APB2Periph_GPIOC, GPIOC, GPIO_Pin_5 },
//	{ BSP_DS18B20_TEMP7, RCC_APB2Periph_GPIOB, GPIOB, GPIO_Pin_0 },
};

#define	DS18B20_DQ_OUT(pstGpio,unPin,data)        GPIO_WriteBit(pstGpio,unPin,(BitAction)data)
#define	DS18B20_DQ_STATUS(pstGpio,unPin)          GPIO_ReadInputDataBit(pstGpio,unPin)

static void bsp_ds18b20_reset( uint8_t ucId );
static uint8_t bsp_ds18b20_check( uint8_t ucId );


/**********************************************************************
* 函数名称：get_average
* 功能描述：滤波算法,求平均值
* 输入参数：
*		  ：pnData：输入数据组指针		ucMaxNum：输入数据的个数
*		  ：ucFilterMin：最小值过滤的个数		ucFilterMax：最大值过滤的个数
* 输出参数：无
* 返 回 值：
*		  ：滤波后的平均值
* 其他说明：无
**********************************************************************/
static float get_average(float *pfData, uint8_t ucMaxNum, uint8_t ucFilterMin,uint8_t ucFilterMax)
{
	uint8_t i,j;
	float fTemp = 0, fAverage = 0;
	
	for(i = 0; i < ucMaxNum - 1;i++)
	{
		for(j = i + 1; j < ucMaxNum; j++)
		{
			if(pfData[i]>pfData[j])
			{
				fTemp = pfData[j];
				pfData[j] = pfData[i];
				pfData[i] = fTemp;
			}
		}
	}
	fTemp = 0.0;
	for(i = ucFilterMin; i < ucMaxNum - ucFilterMax; i++)
	{
		fTemp += pfData[i];
	}
	fAverage = fTemp / (ucMaxNum - ucFilterMin - ucFilterMax);
	
	return fAverage;
}


/**********************************************************************
* 函数名称：bsp_ds18b20_delay_us
* 功能描述：软件延时函数(单位：us)
* 输入参数：
*         ：unUs：延时微妙数 
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ds18b20_delay_us( uint16_t unUs )
{
	uint8_t j,k;
	uint16_t i;

	for (i = 0; i < unUs; i++)
	{
		for (j = 0; j < 8; j++)
		{
			k++;
		}
	}
}

/**********************************************************************
* 函数名称：bsp_ds18b20_init
* 功能描述：温度传感器组(7个)引脚初始化  
* 输入参数：无
* 输出参数：无
* 返 回 值：
*		  ：初始化结果
* 其它说明：无
**********************************************************************/
uint8_t bsp_ds18b20_init( void )
{
	uint8_t ucIntex = 0, ucInitStaus = 0;

	GPIO_InitTypeDef GPIO_InitStructure;
	
	for ( ucIntex = 0; ucIntex < MAX_DS18B20_NUM; ucIntex++ )
	{
		RCC_APB2PeriphClockCmd(	s_astDs18b20Param[ucIntex].ulRcc ,ENABLE );	
		
		GPIO_InitStructure.GPIO_Pin = s_astDs18b20Param[ucIntex].unPin;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
		GPIO_Init(s_astDs18b20Param[ucIntex].pstGpio,&GPIO_InitStructure);
		GPIO_SetBits(s_astDs18b20Param[ucIntex].pstGpio,s_astDs18b20Param[ucIntex].unPin); 

		__disable_irq();
		
		bsp_ds18b20_reset( ucIntex );
		
		if ( bsp_ds18b20_check( ucIntex ) )
		{
			ucInitStaus += (0x01 << ucIntex);
//			BSP_ERR_LIGHT_ON();
			PRO_DEBUG( INIT_DEBUG,( "bsp ds18b20[%d] init err...\r\n",ucIntex ) );
		}
		else
		{
			PRO_DEBUG( INIT_DEBUG,( "bsp ds18b20[%d] init ok...\r\n",ucIntex ) );
			
			/*! 防止第一次读取的数据为85度 */
			bsp_ds18b20_read_temp(ucIntex);
		}

		__enable_irq();
		
	}
	
	return ucInitStaus;
}

/**********************************************************************
* 函数名称：bsp_ds18b20_gpio_out
* 功能描述：设置DS18B20引脚为输出模式   
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7） 
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ds18b20_gpio_out( uint8_t ucId )
{
    GPIO_InitTypeDef GPIO_InitStructure;	
	
	GPIO_InitStructure.GPIO_Pin = s_astDs18b20Param[ucId].unPin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(s_astDs18b20Param[ucId].pstGpio,&GPIO_InitStructure);
}

/**********************************************************************
* 函数名称：bsp_ds18b20_gpio_in
* 功能描述：设置DS18B20引脚为输入模式    
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7） 
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ds18b20_gpio_in( uint8_t ucId )
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	
	GPIO_InitStructure.GPIO_Pin = s_astDs18b20Param[ucId].unPin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(s_astDs18b20Param[ucId].pstGpio,&GPIO_InitStructure);
}

/**********************************************************************
* 函数名称：bsp_ds18b20_reset
* 功能描述：复位温度传感器   
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7） 
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ds18b20_reset( uint8_t ucId )	   
{
	bsp_ds18b20_gpio_out(ucId); 
	DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,0);
    bsp_ds18b20_delay_us(750);  
	DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,1);			
	bsp_ds18b20_delay_us(15);   	    	
}

/**********************************************************************
* 函数名称：bsp_ds18b20_check
* 功能描述：等待DS18B20的回应
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7） 
* 输出参数：
*         : 1:未检测到传感器   0：检测到传感器
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static uint8_t bsp_ds18b20_check( uint8_t ucId ) 	   
{   
	uint8_t ucRetry = 0;
	
	bsp_ds18b20_gpio_in( ucId ); 
	while ( DS18B20_DQ_STATUS( s_astDs18b20Param[ucId].pstGpio, s_astDs18b20Param[ucId].unPin ) && (ucRetry < 200) )
	{
		ucRetry++;
		bsp_ds18b20_delay_us(1);
	};	 
	if ( ucRetry >= 200 )
	{
		return 1;
	}
	else
	{
		ucRetry = 0;
	}		
    while ( !DS18B20_DQ_STATUS( s_astDs18b20Param[ucId].pstGpio, s_astDs18b20Param[ucId].unPin ) && (ucRetry < 240) )
	{
		ucRetry++;
		bsp_ds18b20_delay_us(1);
	}
	if ( ucRetry >= 240 )
	{
		return 1;
	}	
    
	return 0;
}

/**********************************************************************
* 函数名称：bsp_ds18b20_read_bit
* 功能描述：读取DS18B20的一个bit
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7） 
* 输出参数：无
* 返 回 值：
*         : 读出结果
* 其它说明：无
**********************************************************************/
static uint8_t bsp_ds18b20_read_bit( uint8_t ucId )			 
{
    uint8_t ucResult = 0;
	
	bsp_ds18b20_gpio_out( ucId );
    DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,0);
	bsp_ds18b20_delay_us(2);
    DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,1); 
	bsp_ds18b20_gpio_in( ucId );
	bsp_ds18b20_delay_us(12);
	if ( DS18B20_DQ_STATUS( s_astDs18b20Param[ucId].pstGpio, s_astDs18b20Param[ucId].unPin ) )
	{
		ucResult = 1;
	}
    else
	{
		ucResult = 0;	
	}		 
    bsp_ds18b20_delay_us(50);  
	
    return ucResult;
}

/**********************************************************************
* 函数名称：bsp_ds18b20_read_byte
* 功能描述：读取DS18B20的一个字节
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7）
* 输出参数：无
* 返 回 值：
*         : 读出结果
* 其它说明：无
**********************************************************************/
static uint8_t bsp_ds18b20_read_byte( uint8_t ucId )	    
{        
    uint8_t ucIntex = 0,ucBit,ucResult = 0;

	for ( ucIntex = 1; ucIntex <= 8; ucIntex++ ) 
	{
        ucBit = bsp_ds18b20_read_bit( ucId );
        ucResult = (ucBit << 7) | (ucResult >> 1);
    }	
	
    return ucResult;
}

/**********************************************************************
* 函数名称：bsp_ds18b20_write_byte
* 功能描述：写DS18B20的一个字节
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7）
*		  ：ucData：写入的字节
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ds18b20_write_byte( uint8_t ucId, uint8_t ucData)     
 {             
    uint8_t ucIntex = 0;
	 
	bsp_ds18b20_gpio_out( ucId );
    for ( ucIntex = 1; ucIntex <= 8; ucIntex++ ) 
	{  
        if ( ucData & 0x01 ) 
        {
            DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,0);
            bsp_ds18b20_delay_us(2);                            
            DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,1);
            bsp_ds18b20_delay_us(60);             
        }
        else 
        {
            DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,0);
            bsp_ds18b20_delay_us(60);             
            DS18B20_DQ_OUT(s_astDs18b20Param[ucId].pstGpio,s_astDs18b20Param[ucId].unPin,1);
            bsp_ds18b20_delay_us(2);                          
        }
		ucData = ucData >> 1;
    }
}
 
/**********************************************************************
* 函数名称：bsp_ds18b20_start
* 功能描述：开始温度转换
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7）
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ds18b20_start( uint8_t ucId )
{   						               
    bsp_ds18b20_reset( ucId );	   
	bsp_ds18b20_check( ucId );	 
    bsp_ds18b20_write_byte(ucId, 0xcc);				// skip rom
    bsp_ds18b20_write_byte(ucId, 0x44);				// convert
} 

/**********************************************************************
* 函数名称：bsp_ds18b20_check_state
* 功能描述：检测ds18b20工作状态   
* 输入参数：无
* 输出参数：无
* 返 回 值：
*  		  ：0：传感器工作正常	1：传感器工作异常
* 其它说明：无
**********************************************************************/
uint8_t bsp_ds18b20_check_state( void )
{
	uint8_t ucIntex = 0, ucInitStaus = 0;

	for ( ucIntex = 0; ucIntex < MAX_DS18B20_NUM; ucIntex++ )
	{
		bsp_ds18b20_reset( ucIntex );
		
		if ( bsp_ds18b20_check( ucIntex ) )
		{
			ucInitStaus += (0x01 << ucIntex);
//			BSP_ERR_LIGHT_ON();
		}		
	}
	
	return ucInitStaus;
}

/**********************************************************************
* 函数名称：bsp_ds18b20_read_original_temp
* 功能描述：读取DS18B20的原始温度值(AD值)
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7）
* 输出参数：无
* 返 回 值：
*         : 读出结果
* 其它说明：
*         ：在读取温度值时,可能会被中断打断造成读取错误,但是又不能关闭中断
*         ：如果关闭中断,则有可能会使串口接收数据错误；解决办法是多读取
*		  ：几次后再滤波处理
**********************************************************************/
uint16_t bsp_ds18b20_read_original_temp( uint8_t ucId )
{
    uint8_t ucTLvalue = 0,ucTHvalue = 0;
	uint16_t lResult = 0;
	
	/*! 在读取数据时一定要关闭中断，否则读取数据偶尔错误 */
    bsp_ds18b20_start( ucId );                  	// 开始转换
    bsp_ds18b20_reset( ucId );
    bsp_ds18b20_check( ucId );	 
    bsp_ds18b20_write_byte(ucId, 0xcc);				// skip rom
    bsp_ds18b20_write_byte(ucId, 0xbe);				// convert	    
    ucTLvalue = bsp_ds18b20_read_byte( ucId ); 		// LSB   
    ucTHvalue = bsp_ds18b20_read_byte( ucId ); 		// MSB  

    lResult = ucTHvalue; 							// 获得高八位
    lResult <<= 8;     
    lResult += ucTLvalue;							// 获得底八位
	
	/*! 如果温度为零度以下,则直接改成0; 因为零度以下对PID控制来说没有意义,
		同时将输出值改成0方便PID计算*/
	if ( ucTHvalue > 7 )
	{
		lResult = 0;
	}
	
	return (lResult); 								// 返回温度值 
}

/**********************************************************************
* 函数名称：bsp_ds18b20_read_temp
* 功能描述：读取DS18B20的温度值(浮点数)
* 输入参数：
*         ：ucId：DS18B20的ID号（DS18B20_TEMP1/.../DS18B20_TEMP7）
* 输出参数：无
* 返 回 值：
*         : 读出结果
* 其它说明：
*         ：温度值 （-55~125） 精度：0.1C
*         ：在读取温度值时,可能会被中断打断造成读取错误,但是又不能关闭中断
*         ：如果关闭中断,则有可能会使串口接收数据错误
**********************************************************************/
float bsp_ds18b20_read_temp( uint8_t ucId )
{
    uint8_t ucValue = 0,ucTLvalue = 0,ucTHvalue = 0;
	int16_t lResult = 0;
	float fTemp = 0.0;
	
	/*! 在读取数据时一定要关闭中断，否则读取数据偶尔错误 */
	__disable_irq();
	
    bsp_ds18b20_start( ucId );                  	
    bsp_ds18b20_reset( ucId );
    bsp_ds18b20_check( ucId );	 
    bsp_ds18b20_write_byte(ucId, 0xcc);				
    bsp_ds18b20_write_byte(ucId, 0xbe);					    
    ucTLvalue = bsp_ds18b20_read_byte( ucId ); 		   
    ucTHvalue = bsp_ds18b20_read_byte( ucId ); 		  
	
	__enable_irq();
	
    if ( ucTHvalue > 7 )							// 温度为负
    {
        ucTHvalue =~ ucTHvalue;
        ucTLvalue =~ ucTLvalue + 1; 
        ucValue = 0;								  
    }else											//温度为正
	{
		ucValue = 1;									 
	}	 	  
    lResult = ucTHvalue; 							//获得高八位
    lResult <<= 8;     
    lResult += ucTLvalue;							//获得底八位

    fTemp = (float)lResult*0.0625;					//转换成浮点数  
	
	if ( ucValue )
	{
		return (fTemp); 							//返回温度值
	}
	else
	{
		return (-fTemp);  
	}		  
} 

/**********************************************************************
* 函数名称：bsp_temp_read
* 功能描述：读取温度值
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其他说明：
*         ；该温度值是通过读取20次传感器值后，滤除最小8个数据和最大8个
*		  ：数据，取其中4个数据求平均值所得(通过验证该滤波效果比较稳定)
**********************************************************************/
uint16_t bsp_temp_read(uint8_t ucId)
{
	uint8_t ucIntex = 0;
	float fResult = 0, afReadTemp[20] = {0};
	
	for (ucIntex = 0; ucIntex < 20; ucIntex++)
	{
		afReadTemp[ucIntex] = bsp_ds18b20_read_temp(ucId);
	}
	
//	for (ucIntex = 0; ucIntex < 20; ucIntex++)
//	{
//		PRO_DEBUG(1, ("%0.2f ",afReadTemp[ucIntex]));
//	}
	fResult = get_average(afReadTemp, 20, 4, 14);
//	PRO_DEBUG(1, ("[%0.2f] \r\n",fResult));
	
	return fResult*10;
}

/**********************************************************************
* 函数名称：bsp_temp_to_ds18b20
* 功能描述：将温度值转换成DS18B20所对应的寄存器值
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其他说明：无
**********************************************************************/
int16_t bsp_temp_to_ds18b20(float fTemp)
{
	int16_t ulDs18b20Reg = 0;
	
	ulDs18b20Reg = (int16_t)(fTemp / 0.0625);
	
	return ulDs18b20Reg;
}
