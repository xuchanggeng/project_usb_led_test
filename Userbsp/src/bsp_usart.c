/**********************************************************************
*
*	文件名称：bsp_usart.c
*	功能说明：STM32内部USART驱动程序
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

//#include "bsp_delay.h"
#include "bsp_usart.h"

/**********************************************************************
* 函数名称：bsp_usart_init
* 功能描述：USART初始化函数
* 输入参数：
*         ：USARTx    ：初始化串口号(USART1/USART2/USART3/UART4)
*         ：ulBaudRate：USART波特率配置 
* 输出参数：无
* 返 回 值：无
* 其他说明：
*         ：该驱动默认配置串口为(8-N-1)，同时使能接收中断
**********************************************************************/
void bsp_usart_init(USART_TypeDef* USARTx,uint32_t ulBaudRate)
{
	GPIO_InitTypeDef 		GPIO_InitStructure;
	USART_InitTypeDef 		USART_InitStructure;
	NVIC_InitTypeDef        NVIC_InitStructure;

    if(USART1 == USARTx)
    {
        /* 使能USART1和GPIOA的时钟 */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
        
        /* 将 PA9 映射为 USART1_TX */
//        GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
        /* 将 PA10 映射为 USART1_RX */
//        GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
        
        /* 配置USART1 Rx (PA.10) 端口为输入浮空模式 */
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init(GPIOA, &GPIO_InitStructure);

        /* 配置USART1 Tx (PA.09) 端口为复用推挽模式 */
        GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
        GPIO_Init(GPIOA, &GPIO_InitStructure);

        NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0 ;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			
        NVIC_Init(&NVIC_InitStructure);

        /* USART1的相关配置如下:
            - 波特率 = 115200 baud  
            - 数据长度 = 8 Bits
            - 一个停止位
            - 无奇偶校验
            - 失能硬件控制流(RTS 和 CTS 信号)
            - 使能接收和发送功能
        */
        USART_InitStructure.USART_BaudRate = 115200;
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;
        USART_InitStructure.USART_StopBits = USART_StopBits_1;
        USART_InitStructure.USART_Parity = USART_Parity_No;
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
        USART_Init(USART1, &USART_InitStructure);

        USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

        USART_Cmd(USART1, ENABLE);
        USART_ClearFlag(USART1,USART_FLAG_TC);	
    }
	else if (USART2 == USARTx) 
	{
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
		RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART2 , ENABLE );
		
        /* 将 PA2 映射为 USART2_TX */
//        GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
        /* 将 PA3 映射为 USART2_RX */
//        GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);
        
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init(GPIOA, &GPIO_InitStructure);
        
        GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
        GPIO_Init(GPIOA, &GPIO_InitStructure);
        
		NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0 ;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;		
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			
		NVIC_Init(&NVIC_InitStructure);

		USART_InitStructure.USART_BaudRate = ulBaudRate;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
		USART_Init(USART2, &USART_InitStructure);

		USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
		USART_Cmd(USART2, ENABLE);
		
		/*! 必须加延时,否则发送的第一个数据将会丢失;因为该函数在初始化
		    过程中被调用,所以延时时间可以大一些 */
		//bsp_delay_ms(10);		
		USART_ClearFlag(USART2,USART_FLAG_TC);
	}
	else if (USART3 == USARTx)
	{
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB  , ENABLE );
		RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3 , ENABLE );
		
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOB, &GPIO_InitStructure);

		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOB, &GPIO_InitStructure);

		NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0 ;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;		
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			
		NVIC_Init(&NVIC_InitStructure);

		USART_InitStructure.USART_BaudRate = ulBaudRate;
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
		USART_Init(USART3, &USART_InitStructure);

		USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
		USART_Cmd(USART3, ENABLE);
		
		/*! 必须加延时,否则发送的第一个数据将会丢失;因为该函数在初始化
		    过程中被调用,所以延时时间可以大一些 */
		//bsp_delay_ms(10);
		USART_ClearFlag(USART3,USART_FLAG_TC);
	}	
	else if (UART4 == USARTx)
	{
        RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC  , ENABLE );
        RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART4 , ENABLE );
            
		/* 将 PC10 映射为 UART4_TX */
//        GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_UART4);
        /* 将 PC11 映射为 UART4_RX */
//        GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_UART4);

        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init(GPIOC, &GPIO_InitStructure);

        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
        GPIO_Init(GPIOC, &GPIO_InitStructure);

    #ifdef PRO_SHELL_ENABLE	
        NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0 ;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;		
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			
        NVIC_Init(&NVIC_InitStructure);
    #endif  // PRO_SHELL_ENABLE

        /* USART1的相关配置如下:
            - 波特率 = 115200 baud  
            - 数据长度 = 8 Bits
            - 一个停止位
            - 无奇偶校验
            - 失能硬件控制流(RTS 和 CTS 信号)
            - 使能接收和发送功能
        */
        USART_InitStructure.USART_BaudRate = 115200;
        USART_InitStructure.USART_WordLength = USART_WordLength_8b;
        USART_InitStructure.USART_StopBits = USART_StopBits_1;
        USART_InitStructure.USART_Parity = USART_Parity_No;
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
        USART_Init(UART4, &USART_InitStructure);

    #ifdef PRO_SHELL_ENABLE	
        USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);
    #endif  // PRO_SHELL_ENABLE

        USART_Cmd(UART4, ENABLE);
            
        /*! 必须加延时,否则发送的第一个数据将会丢失;因为该函数在初始化
            过程中被调用,所以延时时间可以大一些 */
        //bsp_delay_ms(10);		
        USART_ClearFlag(UART4,USART_FLAG_TC);
	}	
}

/**********************************************************************
* 函数名称：bsp_usart1_send_byte
* 功能描述：串口1发送一个字节函数
* 输入参数：
*         : ucData：待发送字节数
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_usart1_send_byte(unsigned char ucData)
{
	USART1->SR;
    USART1->DR = (uint8_t) ucData;
    while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
}
/**********************************************************************
* 函数名称：bsp_usart2_send_byte
* 功能描述：串口2发送一个字节函数
* 输入参数：
*         : ucData：待发送字节数
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_usart2_send_byte(unsigned char ucData)
{
	USART2->SR;
    USART2->DR = (uint8_t) ucData;
    while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
}

/**********************************************************************
* 函数名称：bsp_usart3_send_byte
* 功能描述：串口3发送一个字节函数
* 输入参数：
*         : ucData：待发送字节数
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_usart3_send_byte(unsigned char ucData)
{
	USART3->SR;
    USART3->DR = (uint8_t) ucData;
    while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);
}

/**********************************************************************
* 函数名称：bsp_uart4_send_byte
* 功能描述：串口4发送一个字节函数
* 输入参数：
*         : ucData：待发送字节数
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_uart4_send_byte(unsigned char ucData)
{
	UART4->SR;
    UART4->DR = (uint8_t) ucData;
    while(USART_GetFlagStatus(UART4, USART_FLAG_TC) == RESET);
}

/**********************************************************************
* 函数名称：bsp_usart_send_char
* 功能描述：串口发送一个0-0xff的整形值，一般用来发送ASCII码
* 输入参数：
*         :iCom:串口选择  iData:数据值
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_usart_send_char(USART_TypeDef* pstCom,uint8_t ucData) 
{	if ( USART1 == pstCom )
	{
		bsp_usart1_send_byte(ucData);
	}
    
	if ( USART2 == pstCom )
	{
		bsp_usart2_send_byte(ucData);
	}
	else if ( USART3 == pstCom )
	{
		bsp_usart3_send_byte(ucData);
	}
	else if ( UART4 == pstCom )
	{
		bsp_uart4_send_byte(ucData);
	}
}

/**********************************************************************
* 函数名称：bsp_usart_send_string
* 功能描述：串口发送一串字符
* 输入参数：
*         ：pstCom:串口选择  *pucBufferPtr:数据指针  ulLength：数据长度
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_usart_send_string(USART_TypeDef* pstCom,uint8_t *pucBufferPtr, uint32_t ulLength) 
{
	while ( ulLength != 0 )
	{
		bsp_usart_send_char( pstCom,((uint8_t)*pucBufferPtr++) );
		ulLength--;
	}
}











