
#ifndef __BSP_PUMP_VALVE_C__
#define __BSP_PUMP_VALVE_C__

#include "bsp_pump_valve.h"

tPV_Config PV_Config[E_PV_MAX]; 
tPV_Check  PV_Check[E_PV_MAX];

static void bsp_config_pv_struct(ePV_Type ePV)
{
    PV_PIN(ePV,PV_Config[ePV].PV_Pin);
    PV_PORT(ePV,PV_Config[ePV].PV_Port);
    PV_CLK(ePV,PV_Config[ePV].PV_Clk);
}

static void bsp_pv_init(ePV_Type ePV)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    
    bsp_config_pv_struct(ePV);
    RCC_APB2PeriphClockCmd(PV_Config[ePV].PV_Clk, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin   = PV_Config[ePV].PV_Pin;				
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP; 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		
    GPIO_Init(PV_Config[ePV].PV_Port, &GPIO_InitStructure); 
    
	GPIO_ResetBits(PV_Config[ePV].PV_Port, PV_Config[ePV].PV_Pin);  
}

void bsp_pv_all_init(void)
{
    UINT8 i = 0;
	
    for (i = 0; i < E_PV_MAX; i++)
    {
        bsp_pv_init( (ePV_Type)i);
    }
}

void bsp_pv_open(ePV_Type PV_Type)
{
    GPIO_SetBits(PV_Config[PV_Type].PV_Port, PV_Config[PV_Type].PV_Pin); 
}

void bsp_pv_close(ePV_Type PV_Type)
{
    GPIO_ResetBits(PV_Config[PV_Type].PV_Port, PV_Config[PV_Type].PV_Pin);   
}

void bsp_set_pv_state(ePV_Type PV_Type, FunctionalState NewState)
{
	if(NewState == ENABLE)
	{
		GPIO_SetBits(PV_Config[PV_Type].PV_Port, PV_Config[PV_Type].PV_Pin); 
	}
	else
	{
		GPIO_ResetBits(PV_Config[PV_Type].PV_Port, PV_Config[PV_Type].PV_Pin);   
	}
}

UINT8 bsp_pv_get_state(ePV_Type PV_Type)
{
    if (PV_Config[PV_Type].PV_Port->IDR & PV_Config[PV_Type].PV_Pin)
	{
        return Bit_RESET;
	}
    else
	{
        return Bit_SET;
	}
}

void bsp_pv_timeout_check(ePV_Type PV_Type, UINT16 unTimeOut)
{
	if (PV_Type >= E_PV_MAX)
	{
		return;
	}

	if (unTimeOut > 0)
	{
		bsp_pv_open((ePV_Type)PV_Type);
		PV_Check[PV_Type].State = 1;
		PV_Check[PV_Type].TimeOut = unTimeOut;
	}
	else
	{
		PV_Check[PV_Type].State = 0;
	}
}

void bsp_pv_timeout_handle(void)
{
	UINT8 i = 0;
	
	for (i = 0; i < E_PV_MAX; i++)
	{
		if (PV_Check[i].State == 1)
		{
			if (PV_Check[i].TimeOut > 0)
			{
				PV_Check[i].TimeOut--;
			}
			else
			{
				PV_Check[i].State = 0;
				bsp_pv_close((ePV_Type)i);
			}
		}
	}
}

#endif
