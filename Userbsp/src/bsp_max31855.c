/**
  ******************************************************************************
  * @文件   Temlate.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "bsp_max31855.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
#define SPI_CS_GPIO_RCC			  RCC_APB2Periph_GPIOA			// I2C硬件接口    
#define SPI_CS_GPIO 			    GPIOA
#define SPI_CS_PIN 			  	  GPIO_Pin_15

#define SPI_SCK_GPIO_RCC			RCC_APB2Periph_GPIOA			// I2C硬件接口    
#define SPI_SCK_GPIO 			    GPIOA
#define SPI_SCK_PIN 				  GPIO_Pin_15

#define SPI_SO_GPIO_RCC			  RCC_APB2Periph_GPIOA			// I2C硬件接口    
#define SPI_SO_GPIO 			    GPIOA
#define SPI_SO_PIN 				    GPIO_Pin_15

/* 本地变量 ---------------------------------------------------------------*/
/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 --------------------------------------------------------------*/

/* 本地函数 ----------------------------------------------------------------*/



/*******************************************************************************
* 函数名称: bsp_max31855_init
* 功能描述: 示例
* 输入参数: 
			:ucBaseAddr：基地址    ucOffsAddr：偏移地址
			：pucData 数据
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_max31855_init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
 	
    RCC_APB2PeriphClockCmd(SPI_CS_GPIO_RCC , ENABLE);	 
	
	 //CS SCK
    GPIO_InitStructure.GPIO_Pin   = SPI_CS_PIN | SPI_SCK_PIN;				 
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP; 		 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 
    GPIO_Init(SPI_CS_GPIO, &GPIO_InitStructure);	

	 //DSO
    GPIO_InitStructure.GPIO_Pin   = SPI_SO_PIN ;				 
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING; 		 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 
    GPIO_Init(SPI_SO_GPIO, &GPIO_InitStructure);	
    
    //CS 状态初始化
    GPIO_SetBits(SPI_CS_GPIO, SPI_CS_PIN);	
	
}

/*******************************************************************************
* 函数名称: spi_delay_ms
* 功能描述: 示例
* 输入参数: 
			:ucBaseAddr：基地址    ucOffsAddr：偏移地址
			:pucData 数据
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void spi_delay_ms(uint16_t unCount) 
{ 
	uint8_t j,k;
	uint16_t i;

	for ( i = 0; i < unCount; i++ )
	{
		for ( j = 0; j < 8; j++ )
		{
			k++;
		}
	}

}

/*******************************************************************************
* 函数名称: bsp_max31855_read
* 功能描述: 示例
* 输入参数: 
			:ucBaseAddr：基地址    ucOffsAddr：偏移地址
			：pucData 数据
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
float bsp_max31855_read(uint8_t ucCh)
{
    uint8_t count = 0;	
    int32_t temp=0;
    float   value = 0;

    GPIO_SetBits(GPIOB,SPI_CS_PIN);
    spi_delay_ms(10);
    GPIO_ResetBits(GPIOB, SPI_CS_PIN);//CS=0;
    for(count=0;count<=31;count++)
    {
        GPIO_SetBits(GPIOB, SPI_SCK_PIN);//read data SO
        spi_delay_ms(1);
        temp<<=1;
        if(GPIO_ReadInputDataBit(GPIOB,SPI_SO_PIN))
        temp|=0x00000001;
        GPIO_ResetBits(GPIOB, SPI_SCK_PIN);
        spi_delay_ms(1);
    }
    GPIO_SetBits(GPIOB, SPI_CS_PIN);//CS=1;

    temp>>=17;	
    value = (temp*100)/4;

  return  value;


}

   










   






















