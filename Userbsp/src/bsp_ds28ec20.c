/**********************************************************************
*
*	�ļ����ƣ�bsp_ds28ec20.c
*	����˵����ds28ec20�¶ȴ�������������
*	����˵����
*			��1.ds28ec20оƬ��������Ϊ12λģʽ
*           ��2.ds28ec20оƬ��12λģʽ�����ת��ʱ���?750ms  
*			��3.ds28ec20оƬ�ϵ��¶�ֵΪ85��
*   �޸ļ�¼��
*	   �汾��    ����       ����      ˵��  
*	   V1.0.0    2018-3-9   yxh       ʵ�ֻ�������
*
*   Copyright (C), 2018-2028, www.geniusmedica.com
*
**********************************************************************/

#include "includes.h"
#include "bsp_ds28ec20.h"

/*! ����ds28ec20��������ʼ���ṹ�� */
typedef struct
{
	uint8_t ucId;				// ������ID
	uint32_t ulRcc;				// ���������Ŷ�Ӧʱ��
	GPIO_TypeDef* pstGpio;		// ��������ӦGPIO��
	uint16_t unPin;				// ��������ӦPIN��		
}ds28ec20_init_t;

/*! ��ʼ��ds28ec20��������ʼ���ṹ�� */
ds28ec20_init_t s_astds28ec20Param[ MAX_DS28EC20_NUM ] = 
{
	{ BSP_DS28EC20_TEMP1, RCC_APB2Periph_GPIOA, GPIOA, GPIO_Pin_0 },
};

#define	ds28ec20_DQ_OUT(pstGpio,unPin,data)        GPIO_WriteBit(pstGpio,unPin,(BitAction)data)
#define	ds28ec20_DQ_STATUS(pstGpio,unPin)          GPIO_ReadInputDataBit(pstGpio,unPin)

static void bsp_ds28ec20_reset( uint8_t ucId );
static uint8_t bsp_ds28ec20_check( uint8_t ucId );


/**********************************************************************
* �������ƣ�get_average
* �����������˲��㷨,��ƽ��ֵ
* ���������?
*		  ��pnData������������ָ��		ucMaxNum���������ݵĸ���
*		  ��ucFilterMin����Сֵ���˵ĸ���		ucFilterMax�����ֵ���˵ĸ���?
* �����������?
* �� �� ֵ��
*		  ���˲����ƽ���?
* ����˵������
**********************************************************************/
float get_average(float *pfData, uint8_t ucMaxNum, uint8_t ucFilterMin,uint8_t ucFilterMax)
{
	uint8_t i,j;
	float fTemp = 0, fAverage = 0;
	
	for(i = 0; i < ucMaxNum - 1;i++)
	{
		for(j = i + 1; j < ucMaxNum; j++)
		{
			if(pfData[i]>pfData[j])
			{
				fTemp = pfData[j];
				pfData[j] = pfData[i];
				pfData[i] = fTemp;
			}
		}
	}
	fTemp = 0.0;
	for(i = ucFilterMin; i < ucMaxNum - ucFilterMax; i++)
	{
		fTemp += pfData[i];
	}
	fAverage = fTemp / (ucMaxNum - ucFilterMin - ucFilterMax);
	
	return fAverage;
}


/**********************************************************************
* �������ƣ�bsp_ds28ec20_delay_us
* ����������������ʱ����(��λ��us)
* ���������?
*         ��unUs����ʱ΢���� 
* �����������?
* �� �� ֵ����
* ����˵������
**********************************************************************/
static void bsp_ds28ec20_delay_us( uint16_t unUs )
{
	uint8_t j,k;
	uint16_t i;

	for (i = 0; i < unUs; i++)
	{
		for (j = 0; j < 8; j++)
		{
			k++;
		}
	}
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_init
* �����������¶ȴ�������(7��)���ų�ʼ��  
* �����������?
* �����������?
* �� �� ֵ��
*		  ����ʼ�����?
* ����˵������
**********************************************************************/
uint8_t bsp_ds28ec20_init( void )
{
	uint8_t ucIntex = 0, ucInitStaus = 0;

	GPIO_InitTypeDef GPIO_InitStructure;
	
	for ( ucIntex = 0; ucIntex < MAX_DS28EC20_NUM; ucIntex++ )
	{
		RCC_APB2PeriphClockCmd(	s_astds28ec20Param[ucIntex].ulRcc ,ENABLE );	
		
		GPIO_InitStructure.GPIO_Pin = s_astds28ec20Param[ucIntex].unPin;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
		GPIO_Init(s_astds28ec20Param[ucIntex].pstGpio, &GPIO_InitStructure);
		GPIO_SetBits(s_astds28ec20Param[ucIntex].pstGpio, s_astds28ec20Param[ucIntex].unPin); 

		__disable_irq();
		
		bsp_ds28ec20_reset( ucIntex );
		
		if ( bsp_ds28ec20_check( ucIntex ) )
		{
			ucInitStaus += (0x01 << ucIntex);
//			BSP_ERR_LIGHT_ON();
			PRO_DEBUG( INIT_DEBUG,( "bsp ds28ec20[%d] init err...\r\n",ucIntex ) );
		}
		else
		{
			PRO_DEBUG( INIT_DEBUG,( "bsp ds28ec20[%d] init ok...\r\n",ucIntex ) );
			
			/*! ��ֹ��һ�ζ�ȡ������Ϊ85�� */
			// bsp_ds28ec20_read_temp(ucIntex);
		}

		__enable_irq();
		
	}
	
	return ucInitStaus;
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_gpio_out
* ��������������ds28ec20����Ϊ���ģ�?   
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7�� 
* �����������?
* �� �� ֵ����
* ����˵������
**********************************************************************/
static void bsp_ds28ec20_gpio_out( uint8_t ucId )
{
    GPIO_InitTypeDef GPIO_InitStructure;	
	
	GPIO_InitStructure.GPIO_Pin = s_astds28ec20Param[ucId].unPin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(s_astds28ec20Param[ucId].pstGpio,&GPIO_InitStructure);
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_gpio_in
* ��������������ds28ec20����Ϊ����ģʽ    
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7�� 
* �����������?
* �� �� ֵ����
* ����˵������
**********************************************************************/
static void bsp_ds28ec20_gpio_in( uint8_t ucId )
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	
	GPIO_InitStructure.GPIO_Pin = s_astds28ec20Param[ucId].unPin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(s_astds28ec20Param[ucId].pstGpio,&GPIO_InitStructure);
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_reset
* ������������λ�¶ȴ�����   
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7�� 
* �����������?
* �� �� ֵ����
* ����˵������
**********************************************************************/
static void bsp_ds28ec20_reset( uint8_t ucId )	   
{
	bsp_ds28ec20_gpio_out(ucId); 
	ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,0);
    bsp_ds28ec20_delay_us(480);  
	ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,1);			
	bsp_ds28ec20_delay_us(15);   	    	
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_check
* �����������ȴ�ds28ec20�Ļ�Ӧ
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7�� 
* ���������?
*         : 1:δ��⵽������?   0����⵽������?
* �� �� ֵ����
* ����˵������
**********************************************************************/
static uint8_t bsp_ds28ec20_check( uint8_t ucId ) 	   
{   
	uint8_t ucRetry = 0;
	
	bsp_ds28ec20_gpio_in( ucId ); 
	while ( ds28ec20_DQ_STATUS( s_astds28ec20Param[ucId].pstGpio, s_astds28ec20Param[ucId].unPin ) && (ucRetry < 200) )
	{
		ucRetry++;
		bsp_ds28ec20_delay_us(1);
	};	 
	if ( ucRetry >= 200 )
	{
		return 1;
	}
	else
	{
		ucRetry = 0;
	}

    while ( !ds28ec20_DQ_STATUS( s_astds28ec20Param[ucId].pstGpio, s_astds28ec20Param[ucId].unPin ) && (ucRetry < 240) )
	{
		ucRetry++;
		bsp_ds28ec20_delay_us(1);
	}

	if ( ucRetry >= 240 )
	{
		return 1;
	}	
    
	return 0;
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_read_bit
* ������������ȡds28ec20��һ��bit
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7�� 
* �����������?
* �� �� ֵ��
*         : �������?
* ����˵������
**********************************************************************/
static uint8_t bsp_ds28ec20_read_bit( uint8_t ucId )			 
{
    uint8_t ucResult = 0;
	
	bsp_ds28ec20_gpio_out( ucId );
    ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,0);
	bsp_ds28ec20_delay_us(2);
    ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,1); 
	bsp_ds28ec20_gpio_in( ucId );
	bsp_ds28ec20_delay_us(12);
	if ( ds28ec20_DQ_STATUS( s_astds28ec20Param[ucId].pstGpio, s_astds28ec20Param[ucId].unPin ) )
	{
		ucResult = 1;
	}
    else
	{
		ucResult = 0;	
	}		 
    bsp_ds28ec20_delay_us(50);  
	
    return ucResult;
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_read_byte
* ������������ȡds28ec20��һ���ֽ�
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7��
* �����������?
* �� �� ֵ��
*         : �������?
* ����˵������
**********************************************************************/
static uint8_t bsp_ds28ec20_read_byte( uint8_t ucId )	    
{        
    uint8_t ucIntex = 0,ucBit,ucResult = 0;

	for ( ucIntex = 1; ucIntex <= 8; ucIntex++ ) 
	{
        ucBit = bsp_ds28ec20_read_bit( ucId );
        ucResult = (ucBit << 7) | (ucResult >> 1);
    }	
	
    return ucResult;
}

/**********************************************************************
* �������ƣ�bsp_ds28ec20_write_byte
* ����������дds28ec20��һ���ֽ�
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7��
*		  ��ucData��д����ֽ�?
* �����������?
* �� �� ֵ����
* ����˵������
**********************************************************************/
static void bsp_ds28ec20_write_byte( uint8_t ucId, uint8_t ucData)     
 {             
    uint8_t ucIntex = 0;
	 
	bsp_ds28ec20_gpio_out( ucId );
    for ( ucIntex = 1; ucIntex <= 8; ucIntex++ ) 
	{  
        if ( ucData & 0x01 ) 
        {
            ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,0);
            bsp_ds28ec20_delay_us(2);                            
            ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,1);
            bsp_ds28ec20_delay_us(60);             
        }
        else 
        {
            ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,0);
            bsp_ds28ec20_delay_us(60);             
            ds28ec20_DQ_OUT(s_astds28ec20Param[ucId].pstGpio,s_astds28ec20Param[ucId].unPin,1);
            bsp_ds28ec20_delay_us(2);                          
        }
		ucData = ucData >> 1;
    }
}
 
/**********************************************************************
* �������ƣ�bsp_ds28ec20_start
* ������������ʼ�¶�ת��
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7��
* �����������?
* �� �� ֵ����
* ����˵������
**********************************************************************/
static void bsp_ds28ec20_start( uint8_t ucId )
{   						               
    bsp_ds28ec20_reset( ucId );	   
	bsp_ds28ec20_check( ucId );	 
    bsp_ds28ec20_write_byte(ucId, 0xcc);				// skip rom
    bsp_ds28ec20_write_byte(ucId, 0x44);				// convert
} 


/**********************************************************************
* �������ƣ�bsp_ds28ec20_read_temp
* ������������ȡds28ec20���¶�ֵ(������)
* ���������?
*         ��ucId��ds28ec20��ID�ţ�ds28ec20_TEMP1/.../ds28ec20_TEMP7��
* �����������?
* �� �� ֵ��
*         : �������?
* ����˵����
*         ���¶�ֵ ��-55~125�� ���ȣ�0.1C
*         ���ڶ�ȡ�¶�ֵʱ,���ܻᱻ�жϴ����ɶ�ȡ����,�����ֲ��ܹر��ж�
*         ������ر��ж�?,���п��ܻ�ʹ���ڽ������ݴ���
**********************************************************************/
void bsp_ds28ec20_read_memory( uint8_t ucId, uint16_t unDataAddr, uint8_t *ucpData, uint16_t* ucDataLen)
{
    uint8_t ucAddrLow  = 0;
	uint8_t ucAddrHigh = 0;
	uint16_t unIndex = 0;
	uint8_t  ucTempData = 0;
	
	ucAddrLow  = unDataAddr & 0xFF;
	ucAddrHigh = (unDataAddr >> 8) & 0xFF;
    
	__disable_irq();
	
    bsp_ds28ec20_reset( ucId );
    bsp_ds28ec20_check( ucId );	 
    bsp_ds28ec20_write_byte(ucId, 0xcc);	     //����skip Rom����			
    bsp_ds28ec20_write_byte(ucId, 0xf0);         //���Ͷ��ڴ�����
	bsp_ds28ec20_write_byte(ucId, ucAddrLow);    //���͵�ַ���ֽ�		
    bsp_ds28ec20_write_byte(ucId, ucAddrHigh);   //���͵�ַ���ֽ�					    

    for ( unIndex = 0; unIndex < 0xA25; unIndex++ )
	{
//    	ucpData[unIndex]
		ucTempData = bsp_ds28ec20_read_byte( ucId ); 		   
		(*ucDataLen)++;
		PRO_DEBUG(COMM_DEBUG,("ucpData[%d] = 0x%02x\r\n", unIndex, ucTempData));
	}
	
	__enable_irq();
	
  
} 

/**********************************************************************
* �������ƣ�bsp_temp_read
* ������������ȡ�¶�ֵ
* �����������?
* �����������?
* �� �� ֵ����
* ����˵����
*         �����¶�ֵ��ͨ����ȡ20�δ�����ֵ���˳���С8�����ݺ����?8��
*		  �����ݣ�ȡ����4��������ƽ��ֵ����(ͨ����֤���˲�Ч���Ƚ��ȶ�)
**********************************************************************/
uint16_t bsp_data_read(uint8_t ucId)
{
	uint8_t ucIntex = 0;
	float fResult = 0, afReadTemp[20] = {0};
	
	for (ucIntex = 0; ucIntex < 20; ucIntex++)
	{
//		afReadTemp[ucIntex] = bsp_ds28ec20_read_temp(ucId);
	}
	
	return fResult*10;
}


void bsp_memory_data_write(uint16_t unMenAddr, uint8_t *ucpData)
{
	uint8_t ucId = 0;
    uint8_t ucAddrLow  = 0;
	uint8_t ucAddrHigh = 0;
	uint16_t unIndex = 0;
	
	ucAddrLow  = unMenAddr & 0xFF;
	ucAddrHigh = (unMenAddr >> 8) & 0xFF;
    
	__disable_irq();
	
    bsp_ds28ec20_reset( ucId );
    bsp_ds28ec20_check( ucId );	 
    bsp_ds28ec20_write_byte(ucId, 0xcc);	     //����skip Rom����			
    bsp_ds28ec20_write_byte(ucId, 0xf0);         //���Ͷ��ڴ�����
	bsp_ds28ec20_write_byte(ucId, ucAddrLow);    //���͵�ַ���ֽ�		
    bsp_ds28ec20_write_byte(ucId, ucAddrHigh);   //���͵�ַ���ֽ�					    

    for ( unIndex = 0; unIndex < 0xA3F; unIndex++ )
	{
    	ucpData[unIndex] = bsp_ds28ec20_read_byte( ucId ); 		   
	}
	
	__enable_irq();
	
}











