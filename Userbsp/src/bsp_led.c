
#ifndef __BSP_LED_C__
#define __BSP_LED_C__

#include "bsp_led.h"

#define LED_GPIO_RCC			RCC_APB2Periph_GPIOA			// I2C硬件接口    
#define LED_GPIO 			    GPIOA
#define LED_PIN 				GPIO_Pin_4

#define ALERT_GPIO_RCC			RCC_APB2Periph_GPIOA			// I2C硬件接口    
#define ALERT_GPIO 			    GPIOA
#define ALERT_PIN 				GPIO_Pin_5


void bsp_led_init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
 	
    RCC_APB2PeriphClockCmd(LED_GPIO_RCC | RCC_APB2Periph_AFIO, ENABLE);	 
	
	
    GPIO_InitStructure.GPIO_Pin   = LED_PIN | ALERT_PIN;				 
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP; 		 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		 
    GPIO_Init(LED_GPIO, &GPIO_InitStructure);					 
    GPIO_SetBits(LED_GPIO, LED_PIN);	
    GPIO_SetBits(LED_GPIO, ALERT_PIN);	

	// GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
}
 
void bsp_led_toggle(void)
{
	LED_GPIO->ODR = LED_GPIO->ODR ^ LED_PIN;
}


#endif
