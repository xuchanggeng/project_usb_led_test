/**********************************************************************
*
*	文件名称：bsp_eeprom.c
*	功能说明：AT24CXX芯片驱动程序
*	特殊说明：该软件采用软件I2C协议,适用于AT24C02/.../AT24C512等芯片	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

#include "bsp_eeprom.h"

#define EE_I2C_SCL_H 	GPIO_SetBits( EE_I2C_GPIO, EE_I2C_SCL )
#define EE_I2C_SCL_L 	GPIO_ResetBits( EE_I2C_GPIO, EE_I2C_SCL )
#define EE_I2C_SDA_H 	GPIO_SetBits( EE_I2C_GPIO, EE_I2C_SDA )
#define EE_I2C_SDA_L 	GPIO_ResetBits( EE_I2C_GPIO, EE_I2C_SDA )
#define EE_WP_ENABLE    GPIO_SetBits( EE_WP_GPIO, EE_WP_PIN ) 
#define EE_WP_DISABLE   GPIO_ResetBits( EE_WP_GPIO, EE_WP_PIN )

static void bsp_ee_i2c_delay_ms( uint16_t unMs );

/**********************************************************************
* 函数名称：bsp_eeprom_init
* 功能描述：eeprom的I2C引脚初始化  
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_eeprom_init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
		
	RCC_APB2PeriphClockCmd(	EE_I2C_GPIO_RCC | EE_WP_GPIO_RCC, ENABLE );	

	GPIO_InitStructure.GPIO_Pin  = EE_I2C_SCL | EE_I2C_SDA;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_OD;	 
	GPIO_Init( EE_I2C_GPIO, &GPIO_InitStructure );
	
	/*! 写保护硬件直接拉地，没有使用 */
//	GPIO_InitStructure.GPIO_Pin   = EE_WP_PIN;  
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
//	GPIO_Init( EE_WP_GPIO, &GPIO_InitStructure );
	
	EE_I2C_SCL_H;
	EE_I2C_SDA_H;
//	EE_WP_DISABLE;					//不使用写保护功能，写功能一直有效
	bsp_ee_i2c_delay_ms( 10 );		//必须延时大于1ms，否则总线不稳定	
	
//	PRO_DEBUG( INIT_DEBUG,( "bsp eeprom init ok...\r\n" ) );
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_delay_us
* 功能描述：软件延时函数(单位：us)
* 输入参数：
*         ：unUs：延时微妙数 
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_delay_us( uint16_t unUs )
{
	uint8_t j,k;
	uint16_t i;

	for ( i = 0; i < unUs; i++ )
	{
		for ( j = 0; j < 8; j++ )
		{
			k++;
		}
	}
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_delay_ms
* 功能描述：软件延时函数(单位：ms)
* 输入参数：
*         ：unMs：延时毫秒数 
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_delay_ms( uint16_t unMs )
{
	uint16_t i;

	for ( i = 0; i < unMs; i++ )
	{
		bsp_ee_i2c_delay_us( 1000 );
	}
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_sda_out
* 功能描述：设置EEPROM的I2C的SDA脚为输出模式   
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_sda_out( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;	

	GPIO_InitStructure.GPIO_Pin   = EE_I2C_SDA;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_Init( EE_I2C_GPIO, &GPIO_InitStructure );
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_sda_in
* 功能描述：设置EEPROM的I2C的SDA脚为输入模式    
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_sda_in( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;	

	GPIO_InitStructure.GPIO_Pin = EE_I2C_SDA;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_OD;	 
	GPIO_Init( EE_I2C_GPIO, &GPIO_InitStructure );
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_start
* 功能描述：I2C的起始信号    
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_start( void )
{
    bsp_ee_i2c_sda_out();	
	EE_I2C_SDA_H;
	EE_I2C_SCL_H;
	bsp_ee_i2c_delay_us( 5 );
	EE_I2C_SDA_L;
	bsp_ee_i2c_delay_us( 6 );
	EE_I2C_SCL_L;
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_stop
* 功能描述：I2C的结束信号     
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_stop( void )
{	
	bsp_ee_i2c_sda_out();
	EE_I2C_SCL_L;
	EE_I2C_SDA_L;
	EE_I2C_SCL_H;
	bsp_ee_i2c_delay_us( 6 );
	EE_I2C_SDA_H;
	bsp_ee_i2c_delay_us( 6 );
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_ack
* 功能描述：I2C的应答信号      
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_ack( void )
{	
	EE_I2C_SCL_L;
	bsp_ee_i2c_sda_out();
	EE_I2C_SDA_L;
	bsp_ee_i2c_delay_us( 2 );
	EE_I2C_SCL_H;
	bsp_ee_i2c_delay_us( 5 );
	EE_I2C_SCL_L;
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_nack
* 功能描述：I2C无应答信号      
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_nack( void )
{	
	EE_I2C_SCL_L;
	bsp_ee_i2c_sda_out();
	EE_I2C_SDA_H;
	bsp_ee_i2c_delay_us( 2 );
	EE_I2C_SCL_H;
	bsp_ee_i2c_delay_us( 5 );
	EE_I2C_SCL_L;
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_wait_ack
* 功能描述：等待I2C应答信号       
* 输入参数：无
* 输出参数：无
* 返 回 值：
*         ： 1：等待失败
*         ： 2：等待成功
* 其它说明：无
**********************************************************************/
static uint8_t bsp_ee_i2c_wait_ack( void )
{
	uint8_t ucTempTime=0;
	
	bsp_ee_i2c_sda_in();
	EE_I2C_SDA_H;
	bsp_ee_i2c_delay_us( 1 );
	EE_I2C_SCL_H;
	bsp_ee_i2c_delay_us( 1 );
	while ( GPIO_ReadInputDataBit( EE_I2C_GPIO,EE_I2C_SDA ) )
	{
		ucTempTime++;
		if ( ucTempTime > 250 )
		{
			bsp_ee_i2c_stop();
			return 1;
		}	 
	}
	EE_I2C_SCL_L;
	
	return 0;
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_write_byte
* 功能描述：写一个字节数据     
* 输入参数：
*         ：ucTxd: 发送的数据
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
static void bsp_ee_i2c_write_byte( uint8_t ucTxd )
{
	uint8_t i=0;
	
	bsp_ee_i2c_delay_us( 10 );

	bsp_ee_i2c_sda_out();
	EE_I2C_SCL_L;
	for ( i = 0; i < 8; i++ )
	{
		if ( ( ucTxd & 0x80 ) > 0 ) 
		{
			EE_I2C_SDA_H;
		}
		else
		{
			EE_I2C_SDA_L;
		}

		ucTxd <<= 1;
		EE_I2C_SCL_H;
		bsp_ee_i2c_delay_us( 5 ); 
		EE_I2C_SCL_L;
		bsp_ee_i2c_delay_us( 5 );
	}

	bsp_ee_i2c_delay_us( 10 );
}

/**********************************************************************
* 函数名称：bsp_ee_i2c_read_byte
* 功能描述：读取一个字节数据       
* 输入参数：
*         ：ucAck: 是否需要应答(0：不需要应答 1：需要应答)
* 输出参数：无
* 返 回 值：
*         ：读取的数据 
* 其它说明：无
**********************************************************************/
static uint8_t bsp_ee_i2c_read_byte( uint8_t ucAck )
{
   	uint8_t i = 0,ucResult = 0;

	bsp_ee_i2c_delay_us( 10 );

	bsp_ee_i2c_sda_in();
	for ( i = 0; i < 8; i++ )
	{
		EE_I2C_SCL_L;
		bsp_ee_i2c_delay_us( 2 );
		EE_I2C_SCL_H;
		ucResult <<= 1;
		if ( GPIO_ReadInputDataBit( EE_I2C_GPIO,EE_I2C_SDA ) )
		{
			ucResult++;
		}
		bsp_ee_i2c_delay_us( 1 );	
	}
	
	if ( ucAck == 0 )
	{
		bsp_ee_i2c_nack();
	}
	else
	{
		bsp_ee_i2c_ack();
	}

	bsp_ee_i2c_delay_us( 10 );
	
	return ucResult;
}

/**********************************************************************
* 函数名称：bsp_eeprom_rd8
* 功能描述：读取一个字节数据 
* 输入参数：
*         ：unReadAddr: 数据地址	
* 输出参数：无
* 返 回 值：
*         ：读取的数据 
* 其它说明：无
**********************************************************************/
uint8_t bsp_eeprom_rd8( uint16_t unReadAddr )
{
	uint8_t ucResult = 0;

	bsp_ee_i2c_start();	
	if ( EE_TYPE > AT24C16 )
	{
		bsp_ee_i2c_write_byte( 0xA0 );
		bsp_ee_i2c_wait_ack();
		bsp_ee_i2c_write_byte( unReadAddr >> 8 );					//双字节数据地址高位
	}else
	{
	   bsp_ee_i2c_write_byte( 0xA0 + ( ( unReadAddr / 256) << 1 ) );//单字节器件地址+数据地址
	}
	bsp_ee_i2c_wait_ack();
	bsp_ee_i2c_write_byte( unReadAddr % 256 );						//双字节数据地址低位或单字节是数据地址低位		
	bsp_ee_i2c_wait_ack();

	bsp_ee_i2c_start();
	bsp_ee_i2c_write_byte( 0xA1 );	 								//读模式
	bsp_ee_i2c_wait_ack();

	ucResult = bsp_ee_i2c_read_byte( 0 ); 							//0：代表NACK
	bsp_ee_i2c_stop();
	
	/*! 连续调用该函数时，需延时大于1ms，否则读取数据偶尔会出错*/
	bsp_ee_i2c_delay_ms( 1 );	
	
	return ucResult;	
}

/**********************************************************************
* 函数名称：bsp_eeprom_rd16_2th
* 功能描述：读取两个字节数据  
* 输入参数：
*         ：unReadAddr: 数据地址	
* 输出参数：无
* 返 回 值：
*         ：读取的数据 
* 其它说明：无
**********************************************************************/
//uint16_t bsp_eeprom_rd16_2th( uint16_t unReadAddr )
//{
//	uint16_t unResult = 0;
//	
//	unResult = bsp_eeprom_rd8(unReadAddr+1);
//	unResult = (unResult<< 8) + bsp_eeprom_rd8(unReadAddr);
//	
//	return unResult;
//}

/**********************************************************************
* 函数名称：bsp_eeprom_rd16
* 功能描述：读取两个字节数据  
* 输入参数：
*         ：unReadAddr: 数据地址	
* 输出参数：无
* 返 回 值：
*         ：读取的数据 
* 其它说明：无
**********************************************************************/
uint16_t bsp_eeprom_rd16( uint16_t unReadAddr )
{
	uint16_t unResult = 0;

	bsp_ee_i2c_start();
	
	if ( EE_TYPE > AT24C16 )
	{
		bsp_ee_i2c_write_byte( 0xA0 );
		bsp_ee_i2c_wait_ack();
		bsp_ee_i2c_write_byte( unReadAddr >> 8 );
	}else
	{
	   	bsp_ee_i2c_write_byte( 0xA0 + ( ( unReadAddr / 256 ) << 1) );
	}
	bsp_ee_i2c_wait_ack();
	bsp_ee_i2c_write_byte( unReadAddr % 256 );								
	bsp_ee_i2c_wait_ack();

	bsp_ee_i2c_start();
	bsp_ee_i2c_write_byte( 0xA1 );
	bsp_ee_i2c_wait_ack();
	unResult = bsp_ee_i2c_read_byte( 1 );	
	unResult += (uint16_t)( bsp_ee_i2c_read_byte(0) << 8 ); 

	bsp_ee_i2c_stop();
	
	/*! 连续调用该函数时，需延时大于1ms，否则读取数据偶尔会出错*/
	bsp_ee_i2c_delay_ms( 3 );	
	
	return unResult;	
}

/**********************************************************************
* 函数名称：bsp_eeprom_rd32
* 功能描述：读取四个字节数据 
* 输入参数：
*         ：unReadAddr: 数据地址	
* 输出参数：无
* 返 回 值：
*         ：读取的数据 
* 其它说明：无
**********************************************************************/
uint32_t bsp_eeprom_rd32( uint16_t unReadAddr )
{
	uint32_t lResult = 0;

	bsp_ee_i2c_start();
	
	if ( EE_TYPE > AT24C16 )
	{
		bsp_ee_i2c_write_byte( 0xA0 );
		bsp_ee_i2c_wait_ack();
		bsp_ee_i2c_write_byte( unReadAddr >> 8 );
	}else
	{
	   	bsp_ee_i2c_write_byte( 0xA0 + ( (unReadAddr / 256 ) << 1) );
	}
	bsp_ee_i2c_wait_ack();
	bsp_ee_i2c_write_byte( unReadAddr % 256 );								
	bsp_ee_i2c_wait_ack();

	bsp_ee_i2c_start();
	bsp_ee_i2c_write_byte( 0xA1 );
	bsp_ee_i2c_wait_ack();
	
	lResult = bsp_ee_i2c_read_byte( 1 );
	lResult += (uint32_t)( bsp_ee_i2c_read_byte(1) << 8 );
	lResult += (uint32_t)( bsp_ee_i2c_read_byte(1) << 16 );	
	lResult += (uint32_t)( bsp_ee_i2c_read_byte(0) << 24 ); 
 
	bsp_ee_i2c_stop();
	
	/*! 连续调用该函数时，需延时大于1ms，否则读取数据偶尔会出错*/
	bsp_ee_i2c_delay_ms( 1 );	
	
	return lResult;	
}

/**********************************************************************
* 函数名称：bsp_eeprom_wt8
* 功能描述：写一个字节数据 
* 输入参数：
*         ：unWriteAddr: 数据地址	ucData：数据 	
* 输出参数：无
* 返 回 值：无
* 其它说明：
*         ：连续调用该函数时，需延时大于1ms，否则读取数据偶尔会出错
**********************************************************************/
void bsp_eeprom_wt8( uint16_t unWriteAddr, uint8_t ucData )
{
	bsp_ee_i2c_start();
	if ( EE_TYPE > AT24C16 )
	{
		bsp_ee_i2c_write_byte( 0xA0 );
		bsp_ee_i2c_wait_ack();
		bsp_ee_i2c_write_byte( unWriteAddr >> 8 );	
	}else
	{
	   	bsp_ee_i2c_write_byte( 0xA0 + ( (unWriteAddr / 256) << 1) );
	}
	bsp_ee_i2c_wait_ack();
	bsp_ee_i2c_write_byte( unWriteAddr % 256 );									
	bsp_ee_i2c_wait_ack();
	
	bsp_ee_i2c_write_byte( ucData );
	bsp_ee_i2c_wait_ack();
	bsp_ee_i2c_stop();

	bsp_ee_i2c_delay_ms( 10 );
}

/**********************************************************************
* 函数名称：bsp_eeprom_wt16
* 功能描述：写两个字节数据 
* 输入参数：
*         ：unWriteAddr: 数据地址	unData：数据 	
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_eeprom_wt16( uint16_t unWriteAddr, uint16_t unData )
{
	uint8_t i,ucTemp = 0;
	
	for ( i = 0; i < 2; i++ )
	{
		ucTemp = (uint8_t)( ( unData >> ( i * 8) ) & 0xFF );
		bsp_eeprom_wt8(unWriteAddr + i,ucTemp);
	}
}

/**********************************************************************
* 函数名称：bsp_eeprom_wt32
* 功能描述：写四个字节数据
* 输入参数：
*         ：unWriteAddr: 数据地址	lData：数据 	
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_eeprom_wt32( uint16_t unWriteAddr, uint32_t lData )
{
	uint8_t i,ucTemp = 0;
	
	for ( i = 0; i < 4; i++ )
	{
		ucTemp = (uint8_t)( ( lData >> ( i * 8) ) & 0xFF );
		bsp_eeprom_wt8(unWriteAddr + i,ucTemp);
	}
}

/**********************************************************************
* 函数名称：bsp_eeprom_read
* 功能描述：连续读取数据
* 输入参数：
*         ：unReadAddr : 数据地址		*pucBuffer：数据指针   
*         : unNumToRead：数据长度	
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_eeprom_read( uint16_t unReadAddr, uint8_t *pucBuffer, uint16_t unNumToRead )
{
	while ( unNumToRead )
	{
		*pucBuffer++ = bsp_eeprom_rd8(unReadAddr++);	
		unNumToRead--;
	}
} 

/**********************************************************************
* 函数名称：bsp_eeprom_rd16
* 功能描述：读取两个字节数据  
* 输入参数：
*         ：unReadAddr: 数据地址	
* 输出参数：无
* 返 回 值：
*         ：读取的数据 
* 其它说明：无
**********************************************************************/
uint16_t bsp_eeprom_rd16_2th( uint16_t unReadAddr )
{
	uint16_t unResult = 0;

	unResult = bsp_eeprom_rd8( unReadAddr );	
	unResult += (uint16_t)( bsp_eeprom_rd8(unReadAddr+1) << 8 ); 

	return unResult;	
}


/**********************************************************************
* 函数名称：bsp_eeprom_write
* 功能描述：连续写入数据
* 输入参数：
*         ：unWriteAddr : 数据地址		*pucBuffer：数据指针   
*		  : unNumToWrite：数据长度	
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void bsp_eeprom_write( uint16_t unWriteAddr, uint8_t *pucBuffer, uint16_t unNumToWrite) 
{
	while ( unNumToWrite-- )
	{
		bsp_eeprom_wt8(unWriteAddr,*pucBuffer);
		unWriteAddr++;
		pucBuffer++;
	}
}



#if 0

/*******************************调试代码*******************************/
void test_eeprom8(uint8_t ucStart)
{
	uint16_t i;
	uint8_t aucReadData[256] = { 0 };
	uint8_t aucWriteData[256] = { 0 };
	
	do
	{
		for (i = ucStart; i <= 0xff + ucStart; i++)
		{
			aucWriteData[i - ucStart] = i - ucStart;
			bsp_eeprom_wt8(i,i - ucStart);
		}
		
		for (i = ucStart; i <= 0xff + ucStart; i++)
		{
			aucReadData[i - ucStart] = bsp_eeprom_rd8(i);
			if (aucReadData [i - ucStart] != aucWriteData[i - ucStart] )
			{
				PRO_DEBUG(1,("eeprom check err... [%d]\r\n",i - ucStart) );
				return;
			}
		}
		
		PRO_DEBUG(1,("eeprom check ok ,StartAdder = %d \r\n",ucStart));
		
	}while(0);
}

void test_eeprom16(uint8_t ucStart)
{
	uint16_t i;
	uint8_t aucReadData[256] = { 0 };
	uint8_t aucWriteData[256] = { 0 };
	
	do
	{
		for (i = ucStart; i <= 0xff + ucStart; i = i + 2)
		{
			aucWriteData[i - ucStart] = i - ucStart;
			bsp_eeprom_wt16(i,i - ucStart);
		}
		
		for (i = ucStart; i <= 0xff + ucStart; i = i + 2)
		{
			aucReadData[i - ucStart] = bsp_eeprom_rd16(i);
			if (aucReadData [i - ucStart] != aucWriteData[i - ucStart] )
			{
				PRO_DEBUG(1,("eeprom check err... [%d]\r\n",i - ucStart) );
				return;
			}
		}
		
		PRO_DEBUG(1,("eeprom check ok ,StartAdder = %d \r\n",ucStart));
		
	}while(0);
}

void test_eeprom32(uint8_t ucStart)
{
	uint16_t i;
	uint8_t aucReadData[256] = { 0 };
	uint8_t aucWriteData[256] = { 0 };
	
	do
	{
		for (i = ucStart; i <= 0xff + ucStart; i = i + 4)
		{
			aucWriteData[i - ucStart] = i - ucStart;
			bsp_eeprom_wt32(i,i - ucStart);
		}
		
		for (i = ucStart; i <= 0xff + ucStart; i = i + 4)
		{
			aucReadData[i - ucStart] = bsp_eeprom_rd32(i);
			if (aucReadData [i - ucStart] != aucWriteData[i - ucStart] )
			{
				PRO_DEBUG(1,("eeprom check err... [%d]\r\n",i - ucStart) );
				return;
			}
		}
		
		PRO_DEBUG(1,("eeprom check ok ,StartAdder = %d \r\n",ucStart));
		
	}while(0);
}

#endif
