#include "ValvePumpCtl.h" 
#include "bsp_fpga.h"
#include "fpga_adder.h"
#include "bsp_delay.h"



typedef struct _tag_pump_valve_timeout_t
{
//	__IO uint32_t ucReserved;
	__IO uint8_t ucState;
	__IO uint16_t ucTimeout;
}pump_valve_timeout_t;



/*! 阀状态存储buffer */
uint8_t  s_aucValveStates[ MAX_VALVE_BUFFER_LEN ] = { 0,0,0 };
__IO pump_valve_timeout_t s_astPumpValueTimeControl[32] = {0};

/**********************************************************************
* 函数名称：cmd_reset_all_valve
* 功能描述：复位所有阀
* 输入参数：
*         ：无 
* 输出参数：无
* 返 回 值：
*         ：无
* 其它说明：
*         ：无
**********************************************************************/
uint8_t cmd_reset_all_valve(void) 
{
    uint8_t  ret = 0;
	uint8_t index = 0;
    /*! 泵数据 */
    uint8_t  ucData = 0;
    fpga_data_t astFpgaData[MAX_VALVE_BUFFER_LEN];

    do
    {
        /*! 全部关闭 */
        ucData = 0x00;

        /*! 泵数量 */
        for(index = 0; index < MAX_VALVE_BUFFER_LEN; index++ )
        {
            /*! 保存当前泵的状态 */	
            s_aucValveStates[index] = ucData;
            astFpgaData[index].ucData = ucData;
            astFpgaData[index].ucBaseAddr = (uint8_t)FPGA_VALVE_BASE_ADDER;
            astFpgaData[index].ucOffsAddr = (uint8_t)index;
        }
        /*! 写数据到FPGA输出 */
        if (EXIT_FAILURE == bsp_fpga_write(astFpgaData, MAX_VALVE_BUFFER_LEN))
        {	
			ret = ERR_FPGA_EXE;
			return  ret;
        }
				
    }while(0);
	
    return  ret;

}

/**********************************************************************
* 函数名称：cmd_set_all_valve
* 功能描述：设置所有阀
* 输入参数：
*         ：无 
* 输出参数：无
* 返 回 值：
*         ：无
* 其它说明：
*         ：无
**********************************************************************/
uint8_t cmd_set_all_valve(void)
{
    uint8_t  ret = 0;
	uint8_t index = 0;
    /*! 泵数据 */
    uint8_t  ucData = 0;
    fpga_data_t astFpgaData[MAX_VALVE_BUFFER_LEN];

    do
    {
        /*! 全部打开 */
        ucData = 0xFF;

        /*! 泵数量 */
        for(index = 0; index < MAX_VALVE_BUFFER_LEN; index++ )
        {
            /*! 保存当前泵的状态 */	
            s_aucValveStates[index] = ucData;
            astFpgaData[index].ucData = ucData;
            astFpgaData[index].ucBaseAddr = (uint8_t)FPGA_VALVE_BASE_ADDER;
            astFpgaData[index].ucOffsAddr = (uint8_t)index;
        }
        /*! 写数据到FPGA输出 */
        if (EXIT_FAILURE == bsp_fpga_write(astFpgaData, MAX_VALVE_BUFFER_LEN))
        {	
			ret = ERR_FPGA_EXE;
			return  ret;
        }
				
    }while(0);
    return  ret;



}

/**********************************************************************
* 函数名称：cmd_set_single_valve
* 功能描述：设置单个阀
* 输入参数：ucValveData：阀编号(1~32)
*         ：operateflg： 阀值(0~1)
* 输出参数：无
* 返 回 值：类型：int32_t
*         ：EXIT_FAILURE: 失败  EXIT_SUCCESS: 成功
* 其它说明：
*         ：无
**********************************************************************/
uint8_t cmd_set_single_valve(uint8_t ucValveData,uint8_t operateflg)
{
	uint8_t ret = 0;
    uint8_t  ucData = 0;       /*!< 阀值 */
    uint8_t  ucNumber = 0;     /*!< 阀编号 */
    uint8_t  ucRegNumber = 0;  /*!< 阀对应的那个寄存器地址偏移量 */
    uint8_t  ucUpdateBit = 0;  /*!< 要更新的位 */
    fpga_data_t stFpgaData = {0};
	
	do
	{
		//阀编号(1~32)
		ucValveData = ucValveData-1;
		if(ucValveData<8)
        {
            ucNumber = ucValveData;
            ucNumber &= 0xFF;
            ucRegNumber = 0;
        }
        else if(ucValveData<16)
        {
            ucNumber = ucValveData -8;
            ucNumber &= 0xFF;
            ucRegNumber = 1;
        }
        else if(ucValveData<21)
        {
            ucNumber = ucValveData -16;
            ucNumber &= 0xFF;
            ucRegNumber = 2;
        }
//		else if(ucValveData<32)
//        {
//            ucNumber = ucValveData -16;
//            ucNumber &= 0xFF;
//            ucRegNumber = 3;
//        }
        else
        {
            //报错，无此阀号
            ret = ERR_VALVE_NUM_OVER;
			return  ret;
        }
        /*! 0~7分别对应BIT0~BIT7，寄存器偏移下操作的阀值 */
        ucUpdateBit = BIT0;
        ucUpdateBit <<= ucNumber;

        /*! 得到记忆中的阀值 */
        ucData = (s_aucValveStates[ucRegNumber] & ucUpdateBit)>>ucNumber;				

        /*! 得到记忆中的阀值 */
        ucData = s_aucValveStates[ucRegNumber];

        if( TURN_ON_VALVE == operateflg )//operateflg为1开，为0关
        {
            /*! 打开阀命令 */
            ucData |= ucUpdateBit;
        }
        else
        {
            /*! 关闭阀命令 */
            ucData &= ~ucUpdateBit;
        }

        /*! 更新记忆中的阀值 */
        s_aucValveStates[ucRegNumber] = ucData;

        /*! 得到阀寄存器的基地址 */
        stFpgaData.ucBaseAddr = FPGA_VALVE_BASE_ADDER;
        /*! 得到实际的地址 */
        stFpgaData.ucOffsAddr = ucRegNumber;
        /*! 阀值输出 */
        stFpgaData.ucData = ucData;

        /*! 写数据到FPGA输出 */
        if ( EXIT_FAILURE == bsp_fpga_write(&stFpgaData, 1) )
		{
			ret = ERR_FPGA_EXE;
			return  ret;	
		}

        stFpgaData.ucBaseAddr = (uint8_t)FPGA_VALVE_BASE_ADDER;
        stFpgaData.ucOffsAddr = (uint8_t)ucRegNumber;
        stFpgaData.ucData = 0;

        /*! 读数据*/
        if ( EXIT_FAILURE == bsp_fpga_read(&stFpgaData, 1) )
        {		
			ret = ERR_FPGA_EXE;
			return  ret;	
        }

        if(s_aucValveStates[ucRegNumber] != stFpgaData.ucData)   
        {
            ret = ERR_VALVE_VALUE;
			return  ret;	
        }								

	}while(0);

	return ret;
}

/**********************************************************************
* 函数名称：cmd_set_single_valve
* 功能描述：设置单个阀
* 输入参数：ulNumber：阀编号(按位解析 bit0=1：1号阀 ... bit31=1: 32号阀)
*         ：ucValue：阀值(0~1)
* 输出参数：无
* 返 回 值：类型：int32_t
*         ：EXIT_FAILURE: 失败  EXIT_SUCCESS: 成功
* 其它说明：
*         ：无
**********************************************************************/
uint8_t cmd_set_multiple_valve(uint32_t ulNumber,uint8_t ucValue)
{
		uint8_t ret = 0;


		return ret;
}

/**********************************************************************
* 函数名称：cmd_get_all_valve_state
* 功能描述：获取所有阀的状态
* 输入参数：无
*         ：
* 输出参数：无
* 返 回 值：类型：uint8_t*
*         ：
* 其它说明：
*         ：无
**********************************************************************/
uint8_t* cmd_get_all_valve_state(void)
{	
	return  s_aucValveStates;
}


/**********************************************************************
* 函数名称：set_pump_valve_state
* 功能描述： 设置泵阀打开指定的时间
* 输入参数：无
*         ：
* 输出参数：无
* 返 回 值：类型：uint8_t*
*         ：uint8_t ucValveId     泵阀编号
		   uint16_t unTimeout    泵阀打开时间(10ms为单位)
* 其它说明：
*         ：无
**********************************************************************/
void set_pump_valve_state(uint8_t ucValveId, uint16_t unTimeout )
{
	OS_ERR	err;
	
	if(ucValveId > 32)
	{
		return;
	}
	
	OSSchedLock(&err);
	
	if(unTimeout > 0)
	{
		//memset(&s_astPumpValueTimeControl[ucValveId], 0, sizeof(s_astPumpValueTimeControl[ucValveId]));
		cmd_set_single_valve(ucValveId, TURN_ON_VALVE);
		s_astPumpValueTimeControl[ucValveId].ucState   = 1;
		s_astPumpValueTimeControl[ucValveId].ucTimeout = unTimeout;
	}
	
	OSSchedUnlock(&err);
	
} 


/**********************************************************************
* 函数名称：pump_valve_state_control_main
* 功能描述： 定时检测泵阀的状态（10ms）
* 输入参数：无
*         ：
* 输出参数：无
* 返 回 值：类型：void
*         ：
* 其它说明：
*         ：无
**********************************************************************/
void pump_valve_state_control_main(void)
{
	__IO uint8_t i = 0;
	OS_ERR	err;
	
	OSSchedLock(&err);
	
	for(i = 0; i < 32; i++)
	{
		if(s_astPumpValueTimeControl[i].ucState == 1)
		{
			if(s_astPumpValueTimeControl[i].ucTimeout > 0)
			{
				s_astPumpValueTimeControl[i].ucTimeout--;
			}
			else
			{
				s_astPumpValueTimeControl[i].ucState = 0;
				cmd_set_single_valve(i, TURN_OFF_VALVE);
				PRO_DEBUG(INIT_DEBUG, ("No %d, state %d ,time %d\r\n", i, s_astPumpValueTimeControl[i].ucState, OSTimeGet(&err)));
			}
		
		}
	
	}
	
	OSSchedUnlock(&err);

}


/**********************************************************************
* 函数名称： get_valve_pump_state
* 功能描述： 获取泵阀的状态（10ms）
* 输入参数： uint8_t ucValveId
*         ：
* 输出参数：无
* 返 回 值：类型：void
*         ：
* 其它说明：
*         ：无
**********************************************************************/
uint8_t get_valve_pump_state(uint8_t ucValveId)
{
	return s_astPumpValueTimeControl[ucValveId].ucState;
}


/**********************************************************************
* 函数名称： wait_valve_pump_state_idle
* 功能描述： 等待泵阀的状态变为空闲（10ms）
* 输入参数： uint8_t ucValveId
*         ：
* 输出参数：无
* 返 回 值：类型：void
*         ：
* 其它说明：
*         ：无
**********************************************************************/
uint8_t wait_valve_pump_state_idle(uint8_t ucValveId)
{
	OS_ERR      err  ;
	uint8_t ucRet = 0;
	
	while(s_astPumpValueTimeControl[ucValveId].ucState == 1)
	{
		OSTimeDlyHMSM ( 0, 0, 0, 3, OS_OPT_TIME_DLY,  &err );
	}

	return ucRet;
}


