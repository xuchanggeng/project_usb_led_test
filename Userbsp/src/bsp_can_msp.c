/**
  ******************************************************************************
  * @file    bsp_xxx_msp.c
  * @author  Tk Application Team
  * @version V1.0.0
  * @date   
  * @brief   CAN	驱动模块 
  ******************************************************************************
  * @attention
  *

  ******************************************************************************
  */




/* Includes ------------------------------------------------------------------*/ 
#include "includes.h"
#include "bsp_can_msp.h"
#include "Util_Queue.h"

/**************************************************/
/*                   宏定义                       */
/**************************************************/



/**************************************************/
/*                   内部结构                     */
/**************************************************/



/**************************************************/
/*                   内部数据存储定义             */
/**************************************************/
/*! 缓冲读取下标 */
unsigned short int s_unCanReadIndex = 0;
/*! 缓冲写下标 */
unsigned short int s_unCanWriteIndex = 0;
/*! 数据接收缓冲区 */ 
// unsigned char s_ucCanBuffer[CAN_BUF_SIZE]={0};  
// /*! 数据接发送冲区 */ 
// unsigned char s_ucCanSentBuffer[CAN_BUF_TXD_SIZE]={0};  

static frame_info_t s_stCanFrameRecv = {0};


extern frame_queue_t s_stRecevFrameQueue;


/**************************************************/
/*                   内部接口实现                 */
/**************************************************/

/**
  * @brief  CAN 管脚配置
  * @param 		   
	* @retval  					
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
	*/
void CAN_pin_conf(void)
{
	GPIO_InitTypeDef GPIO_InitStructure; 

	/* 复用功能和GPIOB端口时钟使能*/	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);                        											 

	/* CAN1 模块时钟使能 */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE); 

	/* Configure CAN pin: RX PD0 */	  
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;	 // 上拉输入
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure CAN pin: TX PD1 */   
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP; // 复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// GPIO_PinRemapConfig(GPIO_Remap2_CAN1, ENABLE);
	
}



/**
  * @brief  CAN_RX1  中断配置
  * @param 		   
	* @retval  					
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
	*/
void CAN_Interrupt(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* enabling interrupt */
  	NVIC_InitStructure.NVIC_IRQChannel=USB_LP_CAN1_RX0_IRQn;
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  	NVIC_Init(&NVIC_InitStructure);
	
}



/**
  * @brief  CAN初始化
  * @param 		   
	* @retval  					
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
	*/
void CAN_init(void)
{
	uint32_t CAN_FilterIDList_1=0;
	uint32_t CAN_FilterIDMask_1=0;

	CAN_InitTypeDef        CAN_InitStructure;
	CAN_FilterInitTypeDef  CAN_FilterInitStructure;
	//  CanTxMsg TxMessage;  

	/* CAN register init */
	CAN_DeInit(CAN1);	//将外设CAN的全部寄存器重设为缺省值
	CAN_StructInit(&CAN_InitStructure);//把CAN_InitStruct中的每一个参数按缺省值填入

	/* CAN cell init */
	CAN_InitStructure.CAN_TTCM=DISABLE;					//没有使能时间触发模式
	CAN_InitStructure.CAN_ABOM=ENABLE;					//没有使能自动离线管理
	CAN_InitStructure.CAN_AWUM=DISABLE;					//没有使能自动唤醒模式
	CAN_InitStructure.CAN_NART=DISABLE;					//没有使能非自动重传模式
	CAN_InitStructure.CAN_RFLM=DISABLE;					//没有使能接收FIFO锁定模式
	CAN_InitStructure.CAN_TXFP=ENABLE;					//没有使能发送FIFO优先级
	CAN_InitStructure.CAN_Mode=CAN_Mode_Normal;	//CAN设置为正常模式
	CAN_InitStructure.CAN_SJW=CAN_SJW_3tq; 			//重新同步跳跃宽度1个时间单位
	CAN_InitStructure.CAN_BS1=CAN_BS1_9tq; 			//时间段1为3个时间单位
	CAN_InitStructure.CAN_BS2=CAN_BS2_8tq;			//时间段2为2个时间单位
    //CAN_InitStructure.CAN_Prescaler=60;  				//时间单位长度为60 波特率为：72M/2/60(1+3+2)=0.1 即100K
	CAN_InitStructure.CAN_Prescaler=4;  				 //时间单位长度为30 波特率为：72M/2/4(1+9+8)=0.5 即500K
	CAN_Init(CAN1,&CAN_InitStructure);
                                      
	/*>. 滤波器设置
				a>.滤波器设置为4个16位列表模式
				b>.RTR 设置为数据帧,不接受远程帧
				c>.IED 设置为标准帧
				c>.EXIDE 为零
				*/									
	//  CAN_FilterIDList_1=CAN_TARGET_ID;					/*>.驱动板CAN本地ID			*/
	//  CAN_FilterIDList_1=CAN_FilterIDList_1<<5;
		
	//  CAN_FilterIDList_2=EL180_RADIO_CAN_ID;					/*>.驱动板广播ID			*/
	//  CAN_FilterIDList_2=CAN_FilterIDList_2<<5;
	 
	//  CAN_FilterIDList_3=0;
	//  CAN_FilterIDList_4=0;
		 

	/* 设置CAN滤波器0 */
	CAN_FilterInitStructure.CAN_FilterNumber = 0;						    /* 滤波器序号，0-13，共14个滤波器 */
	CAN_FilterInitStructure.CAN_FilterMode 	 = CAN_FilterMode_IdMask;		/* 滤波器模式，设置ID掩码模式 */
	CAN_FilterInitStructure.CAN_FilterScale  = CAN_FilterScale_32bit;	    /* 32位滤波 */
	CAN_FilterInitStructure.CAN_FilterIdHigh = (((uint32_t)CAN_TARGET_ID<<3)&0xFFFF0000)>>16;		     /* 掩码后ID的高16bit */
	CAN_FilterInitStructure.CAN_FilterIdLow =  (((uint32_t)CAN_TARGET_ID<<3)|CAN_ID_EXT|CAN_RTR_DATA)&0xFFFF;			/* 掩码后ID的低16bit */
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = (((uint32_t)TARGET_ID_MASK<<3)&0xFFFF0000)>>16;              /* ID掩码值高16bit */
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = (((uint32_t)TARGET_ID_MASK<<3))&0xFFFF;	/* ID掩码值低16bit */
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO0;			/* 滤波器绑定FIFO 0 */
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;						/* 使能滤波器 */
	CAN_FilterInit(&CAN_FilterInitStructure);


  /* CAN FIFO0 message pending interrupt enable */ 
	CAN_ITConfig(CAN1,CAN_IT_FMP0, ENABLE); //使能FIFO0消息挂号中断
 
 }  

 
 /**
  * @brief  CAN中断服务
  * @param 		   
	* @retval  
	* @Not		USB中断和CAN接收中断服务程序，USB跟CAN共用I/O，这里只用到CAN的中断
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
	*/
void USB_LP_CAN1_RX0_IRQHandler(void)
{	
  
	unsigned char i=0;
	CanRxMsg RxMessage;
	uint8_t  ucIndex = 0;
	uint32_t ulCanId = 0;

	memset(&RxMessage, 0, sizeof(RxMessage));
	memset(&s_stCanFrameRecv, 0, sizeof(s_stCanFrameRecv));

	//接收FIFO0中的数据 
	CAN_Receive(CAN1, CAN_FIFO0, &RxMessage); 
 
    ulCanId = RxMessage.ExtId;

	/*! 接收数据帧 */
	s_stCanFrameRecv.ucBit28    =  (uint8_t)((ulCanId & BIT28_MASK)>>BIT28_POS);
	s_stCanFrameRecv.ucTargetID =  (uint16_t)((ulCanId & TARGET_ID_MASK)>>TARGET_ID_POS);
	s_stCanFrameRecv.ucBit18    =  (uint8_t)((ulCanId & BIT18_MASK)>>BIT18_POS);
	s_stCanFrameRecv.ucSourceID =  (uint16_t) ((ulCanId & SOURCE_ID_MASK)>>SOURCE_ID__POS);
	s_stCanFrameRecv.ucC1C0     =  (uint8_t)((ulCanId & C1_C0_MASK)>>C1_C0_POS);
	s_stCanFrameRecv.ucCMD_RegAddr =  (uint8_t)((ulCanId & CMD_REG_ADDR_MASK)>>CMD_REG_ADDR_POS);
	s_stCanFrameRecv.ucDataLen  = RxMessage.DLC;
	for(i = 0; i < RxMessage.DLC; i++)
	{
		s_stCanFrameRecv.ucaData[i] = RxMessage.Data[i];	
	}

	/*! 数据帧放入队列 */
	FRAME_InQueue(&s_stRecevFrameQueue, &s_stCanFrameRecv);
	
	
}




/**************************************************/
/*                   外部接口                     */
/**************************************************/


/**
  * @brief  配置can资源配置
	* @param  
	* @retval EXIT_SUCCESS：成功，EXIT_FAILURE：失败						
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
				Tkqin					2020/03/18					首版
	*/
void  can_sourse_init(void)
{
	CAN_pin_conf();
	CAN_Interrupt();
	CAN_init();
}




/**
* @brief  CAN发送数据
	* @param  
	* @retval EXIT_SUCCESS：成功，EXIT_FAILURE：失败						
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
				Tkqin					2020/03/18					首版
	*/
void bsp_can_send_bytes(uint32_t ulStdId, uint8_t *ucpSendData, uint8_t ucSendDataLen)
{
//	unsigned short int Index=0;
	unsigned char  i=0,j=0,ucPackNum=0;
	unsigned char ucRemainNum     = 0;
	unsigned int  TimeOut         = 0;
	unsigned char TransmitMailbox = 0;
	CanTxMsg      TxMessage;
	
	TxMessage.StdId = ulStdId;							//标准标识符为0x00
	TxMessage.ExtId = 0x0000; 									//扩展标识符0x0000
	TxMessage.IDE   = CAN_ID_STD;									//使用标准标识符
	TxMessage.RTR   = CAN_RTR_DATA;								//为数据帧
	
	/*! 通过模块通讯接口发送给模块 */
	ucPackNum  = ucSendDataLen/8;
	
	PRO_DEBUG(COMM_DEBUG, ("can send data %d start \r\n", TimeOut));
	/*! 发送整帧msg */
	for(i = 0; i < ucPackNum; i++)
	{
		TxMessage.DLC=8;													
		for(j = 0; j < 8; j++)
		{
			TxMessage.Data[j] = ucpSendData[i*8 + j]; 								
		}

		TransmitMailbox=CAN_Transmit(CAN1, &TxMessage);
		TimeOut = 0;
		while((CAN_TransmitStatus(CAN1, TransmitMailbox) != CANTXOK) && (TimeOut != 0xFFFF))
		{
			TimeOut++;
		}
	}
	
	if(TimeOut == 0xFFFF)
	{
		PRO_DEBUG(COMM_DEBUG, ("can error data:")); 
		for(j = 0; j < ucSendDataLen; j++)
		{
			PRO_DEBUG(COMM_DEBUG, ("%02x ",ucpSendData[j])); 								
		}
	}
	PRO_DEBUG(COMM_DEBUG, ("can send time %d \r\n", TimeOut));
	
	/*! 发送剩余msg */
	ucRemainNum = ucSendDataLen%8;
	TxMessage.DLC = ucRemainNum;												
	for(j = 0; j < ucRemainNum; j++)
	{
		TxMessage.Data[j] = ucpSendData[(ucPackNum * 8) +j]; 							
	}
	
	TransmitMailbox=CAN_Transmit(CAN1, &TxMessage);
	TimeOut = 0;
	while((CAN_TransmitStatus(CAN1, TransmitMailbox) != CANTXOK) && (TimeOut != 0xFFFF))
	{
		TimeOut++;
	}
	PRO_DEBUG(COMM_DEBUG, ("can send time %d \r\n", TimeOut));
	
}



/**
* @brief  CAN发送数据
	* @param  
	* @retval EXIT_SUCCESS：成功，EXIT_FAILURE：失败						
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
				Tkqin					2020/03/18					首版
	*/
void bsp_can_send_frame(frame_info_t *stFrame)
{
	unsigned char j               = 0;
	unsigned int  TimeOut         = 0;
	unsigned char TransmitMailbox = 0;
	CanTxMsg      TxMessage;
	uint32_t      ulStdId         = 0;
	uint32_t      ulTemp          = 0;

	ulTemp = stFrame->ucBit28;
    ulStdId += (ulTemp << BIT28_POS)& BIT28_MASK;
	ulTemp = stFrame->ucTargetID;
    ulStdId += (ulTemp << TARGET_ID_POS)& TARGET_ID_MASK;
	ulTemp = stFrame->ucBit18;
    ulStdId += (ulTemp << BIT18_POS)& BIT18_MASK;
	ulTemp = stFrame->ucSourceID;
    ulStdId += (ulTemp << SOURCE_ID__POS)& SOURCE_ID_MASK;
	ulTemp = stFrame->ucC1C0;
    ulStdId += (ulTemp << C1_C0_POS)& C1_C0_MASK;
	ulTemp = stFrame->ucCMD_RegAddr;
    ulStdId += (ulTemp << CMD_REG_ADDR_POS)& CMD_REG_ADDR_MASK;

	TxMessage.StdId = 0x0000;							//标准标识符为0x00
	TxMessage.ExtId = ulStdId; 							//扩展标识符0x0000
	TxMessage.IDE   = CAN_ID_EXT;						//使用标准标识符
	TxMessage.RTR   = CAN_RTR_DATA;						//为数据帧

	TxMessage.DLC = stFrame->ucDataLen;													
	for(j = 0; j < stFrame->ucDataLen; j++)
	{
		TxMessage.Data[j] = stFrame->ucaData[j]; 								
	}

	TransmitMailbox = CAN_Transmit(CAN1, &TxMessage);
	TimeOut = 0;
	while((CAN_TransmitStatus(CAN1, TransmitMailbox) != CANTXOK) && (TimeOut != 0xFFFF))
	{
		TimeOut++;
	}

	PRO_DEBUG(COMM_DEBUG, ("can send time %d \r\n", TimeOut));

}


/**
* @brief  CAN发送数据
	* @param  
	* @retval EXIT_SUCCESS：成功，EXIT_FAILURE：失败						
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
				Tkqin					2020/03/18					首版
	*/
void bsp_can_send_frame_separate(uint16_t _ucTargetId, uint16_t _ucSourceId, uint8_t _ucC1C0, uint8_t _ucCmdRegAddr, uint8_t *_ucData, uint8_t _ucDatalen)
{
	unsigned char j               = 0;
	unsigned int  TimeOut         = 0;
	unsigned char TransmitMailbox = 0;
	CanTxMsg      TxMessage;
	uint32_t      ulStdId         = 0;
	uint32_t      ulTemp          = 0;

	ulTemp = 0;
    ulStdId += (ulTemp << BIT28_POS)& BIT28_MASK;
	ulTemp = _ucTargetId;
    ulStdId += (ulTemp << TARGET_ID_POS)& TARGET_ID_MASK;
	ulTemp = 0;
    ulStdId += (ulTemp << BIT18_POS)& BIT18_MASK;
	ulTemp = _ucSourceId;
    ulStdId += (ulTemp << SOURCE_ID__POS)& SOURCE_ID_MASK;
	ulTemp = _ucC1C0;
    ulStdId += (ulTemp << C1_C0_POS)& C1_C0_MASK;
	ulTemp = _ucCmdRegAddr;
    ulStdId += (ulTemp << CMD_REG_ADDR_POS)& CMD_REG_ADDR_MASK;

	TxMessage.StdId = 0x0000;							//标准标识符为0x00
	TxMessage.ExtId = ulStdId; 							//扩展标识符0x0000
	TxMessage.IDE   = CAN_ID_EXT;						//使用标准标识符
	TxMessage.RTR   = CAN_RTR_DATA;						//为数据帧

	TxMessage.DLC = _ucDatalen;													
	for(j = 0; j < _ucDatalen; j++)
	{
		TxMessage.Data[j] = _ucData[j]; 								
	}

	TransmitMailbox = CAN_Transmit(CAN1, &TxMessage);
	TimeOut = 0;
	while((CAN_TransmitStatus(CAN1, TransmitMailbox) != CANTXOK) && (TimeOut != 0xFFFF))
	{
		TimeOut++;
	}

	PRO_DEBUG(COMM_DEBUG, ("can send time %d \r\n", TimeOut));

}






/**
  * @brief  	获得CAN接收缓冲区数据写下标
  * @param 
	* @retval  	BUF 写下标地址					
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
				Tkqin					2020/03/18					首版
	*/
unsigned short int* retrieve_can_recvbuf_write_index( void )
{
   return &s_unCanWriteIndex;
}

/**
  * @brief  	获得CAN接收缓冲区数据读取下标
  * @param 

	* @retval  	BUF读下标地址					
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
				Tkqin					2020/03/18					首版
	*/
unsigned short int* retrieve_can_recvbuf_read_index( void )
{
   return &s_unCanReadIndex;
}


/**
  * @brief  	获得CAN接收缓冲区首地
  * @param 	
	* @retval   &s_ucUartBuffer[0]：串口接收缓冲指针。
	*           NULL：接口不存在	
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
	*/
unsigned char* retrieve_can_receive_buffer( void)
{
//    return &s_ucCanBuffer[0];  
}


/**
  * @brief  	复制包头到包尾
  * @param 	
	* @retval   
	*           NULL：接口不存在	
	*  \b 历史记录:     
	*     <作者>        <时间>        <修改记录> <br>
	*/
void can_data_buf_cpy( void)
{
	// memcpy(&s_ucCanBuffer[CAN_BUF_SIZE],&s_ucCanBuffer[0],CAN_BUF_COMPENS_SIZE);  
}





























/*****************************  (END OF FILE) *********************************/
