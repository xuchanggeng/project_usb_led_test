
#ifndef __BSP_CAN_C__
#define __BSP_CAN_C__

#include "includes.h"
#include "pro_include.h"

_EXT_ OS_EVENT *Mid_Sem;
m_tCANFunList   CANFunList;

IO_ UINT8 CAN_Recieve_Buf[CAN1_RX_BUF_LEN];

UINT8 FUN_CAN_Init(void)
{
    GPIO_InitTypeDef 		GPIO_InitStructure; 
	CAN_InitTypeDef        	CAN_InitStructure;
	CAN_FilterInitTypeDef  	CAN_FilterInitStructure;
	NVIC_InitTypeDef  		NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(CAN1_TX_GPIO_CLK | CAN1_RX_GPIO_CLK, ENABLE);                  											 
	RCC_APB1PeriphClockCmd(CAN1_CLK, ENABLE);

    GPIO_InitStructure.GPIO_Pin = CAN1_TX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(CAN1_TX_GPIO_PORT, &GPIO_InitStructure);			

	GPIO_InitStructure.GPIO_Pin = CAN1_RX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	
	GPIO_Init(CAN1_RX_GPIO_PORT, &GPIO_InitStructure);			

    //CAN单元设置
	CAN_InitStructure.CAN_TTCM = DISABLE;			// 非时间触发通信模式  
	CAN_InitStructure.CAN_ABOM = ENABLE;			// 软件自动离线管理	 
	CAN_InitStructure.CAN_AWUM = DISABLE;			// 睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)
	CAN_InitStructure.CAN_NART = DISABLE;			// 禁止报文自动传送 
	CAN_InitStructure.CAN_RFLM = ENABLE;		 	// 报文不锁定,新的覆盖旧的  
	CAN_InitStructure.CAN_TXFP = ENABLE;			// 优先级由报文标识符决定 
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;	// 模式设置： mode:0,普通模式;1,回环模式; 

    //设置波特率   36000/[(CAN_BS1+CAN_BS2+1)*CAN_Prescaler]
	CAN_InitStructure.CAN_SJW = CAN1_SJW;			// 重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位  
	CAN_InitStructure.CAN_BS1 = CAN1_BS1; 			// Tbs1=tbs1+1个时间单位CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStructure.CAN_BS2 = CAN1_BS2;			// Tbs2=tbs2+1个时间单位CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStructure.CAN_Prescaler = CAN1_BPR;     // 分频系数(Fdiv)为brp+1	
	CAN_Init(CTRLCAN1, &CAN_InitStructure);        	// 初始化CAN1 
    
    // filter channels can1:1~19 , can2:20~27
    CAN_SlaveStartBank(1);
    
    CAN_FilterInitStructure.CAN_FilterNumber = 0;	//过滤器0
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask; 	//屏蔽位模式
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit; 	//32位宽 
	CAN_FilterInitStructure.CAN_FilterIdHigh = (UINT16)((CAN_ID << 5) | 0x0000);	//32位ID
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0xFFE0;//32位MASK
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0;//过滤器0关联到FIFO0
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);			
	
    // 发送中断
	CAN_ITConfig(CTRLCAN1, CAN_IT_TME, ENABLE);				//FIFO0消息挂号中断允许.		    

	NVIC_InitStructure.NVIC_IRQChannel = USB_HP_CAN1_TX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;     // 主优先级为1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;            // 次优先级为0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
    
    // ---FMP0
    CAN_ITConfig(CTRLCAN1, CAN_IT_FMP0, ENABLE);						//FIFO0消息挂号中断允许.		    
    CAN_ITConfig(CTRLCAN1, CAN_IT_FF0, ENABLE);
    CAN_ITConfig(CTRLCAN1, CAN_IT_FOV0, ENABLE);
    
	/*! 修改CAN接收中断优先级,由原先的(0,USB_LP_CAN1_RX0_IRQn)改成(0,0) 20190712 */
	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;     
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;            
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
    
    // ---SCE
    CAN_ITConfig(CTRLCAN1, CAN_IT_EWG, ENABLE);						//FIFO0消息挂号中断允许.		    
    CAN_ITConfig(CTRLCAN1, CAN_IT_EPV, ENABLE);
    CAN_ITConfig(CTRLCAN1, CAN_IT_BOF, ENABLE);
    CAN_ITConfig(CTRLCAN1, CAN_IT_LEC, ENABLE);
    CAN_ITConfig(CTRLCAN1, CAN_IT_ERR, ENABLE);
    
	/*! CAN错误处理中断优先级设置 */
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_SCE_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;    
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;         
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
    return e_true;
}

UINT8 FUN_CAN_ClearFlagTI(void)
{
    CAN_ClearITPendingBit(CTRLCAN1,CAN_IT_TME);
    return e_true;
}

UINT8 FUN_CAN_ClearFlagRI(void)
{
//    CAN_ClearITPendingBit(CTRLCAN1,CAN_IT_FMP0);
    return e_true;
}

UINT8 FUN_CAN_CheckFlagTI(void)
{
    return (RESET == CAN_GetITStatus(CTRLCAN1, CAN_IT_TME))? 0: 1;
}

UINT8 FUN_CAN_CheckFlagRI(void)
{
    return (RESET == CAN_GetITStatus(CTRLCAN1, CAN_IT_FMP0))? 0: 1;
}

UINT8 FUN_CAN_CheckFlagTXE(void)
{
    return (((CTRLCAN1->TSR&CAN_TSR_TME0) == CAN_TSR_TME0) ||\
            ((CTRLCAN1->TSR&CAN_TSR_TME1) == CAN_TSR_TME1) ||\
            ((CTRLCAN1->TSR&CAN_TSR_TME2) == CAN_TSR_TME2))? 0: 1;
}

UINT8 FUN_CAN_IntTxEnable(void)
{
    CAN_ITConfig(CTRLCAN1,CAN_IT_TME,ENABLE);
    return e_true;
}

UINT8 FUN_CAN_IntRxEnable(void)
{
    CAN_ITConfig(CTRLCAN1, CAN_IT_FMP0, ENABLE);						    
    CAN_ITConfig(CTRLCAN1, CAN_IT_FF0, ENABLE);
    CAN_ITConfig(CTRLCAN1, CAN_IT_FOV0, ENABLE);
    return e_true;
}

UINT8 FUN_CAN_IntTxDisable(void)
{
    CAN_ITConfig(CTRLCAN1, CAN_IT_TME, DISABLE);
    return e_true;
}

UINT8 FUN_CAN_IntRxDisable(void)
{
    CAN_ITConfig(CTRLCAN1, CAN_IT_FMP0, DISABLE);					    
    CAN_ITConfig(CTRLCAN1, CAN_IT_FF0, DISABLE);
    CAN_ITConfig(CTRLCAN1, CAN_IT_FOV0, DISABLE);
    return e_true;
}

UINT8 FUN_CAN_MsgTx(tCANMsgTx *ptMsgTx)
{
    IO_ UINT8 ch;
    CanTxMsg TxMessage;
    
	TxMessage.StdId = CAN_ID;			// 标准标识符 
	TxMessage.ExtId = CAN_ID;			// 设置扩展标示符 
	TxMessage.IDE = CAN_Id_Standard; 	// 标准帧
	TxMessage.RTR = CAN_RTR_Data;		// 数据帧
	TxMessage.DLC = ptMsgTx->DLC;		// 要发送的数据长度
	for (ch = 0; ch < 8; ch++)
	{
        TxMessage.Data[ch] = ptMsgTx->Data[ch];	
	}		
	ch = CAN_Transmit(CAN1, &TxMessage);   

	return e_true;	 
}

UINT8 FUN_CAN_MsgRx(tCANMsgRx *ptMsgRx)
{
    IO_ UINT8 ch;
	CanRxMsg RxMessage;
    
    CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
    
    ptMsgRx->Id = RxMessage.ExtId;
    ptMsgRx->DLC = RxMessage.DLC;
    ptMsgRx->FMI = RxMessage.FMI;
    
    for (ch = 0; ch < 8; ch++)
	{
        ptMsgRx->Data[ch] = RxMessage.Data[ch];
	}		
	return e_true;	
}

UINT8 FUN_CAN_MonitorRXP(void)
{
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_FOV0))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_FOV0);
    }
    
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_FF0))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_FF0);
    }
    return e_true;
}

UINT8 FUN_CAN_MonitorSCE(void)
{
    if (1)
    {
        printf("\r\n");
        printf("---LEC:%d---\r\n",CAN_GetLastErrorCode(CTRLCAN1)>>4);
        printf("---REC:%d---\r\n",CAN_GetReceiveErrorCounter(CTRLCAN1));
        printf("---TEC:%d---\r\n",CAN_GetLSBTransmitErrorCounter(CTRLCAN1));
        printf("\r\n");
    }
    
    // WKU
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_WKU))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_WKU);
        if (set_debug)
        {
            printf("INT-WKU\r\n");
        }
    }
    
    // WKU
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_SLK))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_SLK);
        if (set_debug)
        {
            printf("INT-SLK\r\n");
        }
    }
    
    // EWG
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_EWG))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_EWG);
        if (set_debug)
        {
            printf("INT-EWG\r\n");
        }
    }
    
    // EPV
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_EPV))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_EPV);
        if (set_debug)
        {
            printf("INT-EPV\r\n");
        }
    }
    
    // BOF
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_BOF))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_BOF);
        if (set_debug)
        {
            printf("INT-BOF\r\n");
        }
    }
    
    // LEC
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_LEC))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_LEC);
        if (set_debug)
        {
            printf("INT-LEC\r\n");
        }
    }
    
    // ERR
    if (SET == CAN_GetITStatus(CTRLCAN1, CAN_IT_ERR))
    {
        CAN_ClearITPendingBit(CTRLCAN1, CAN_IT_ERR);
        if (set_debug)
        {
            printf("INT-ERR\r\n");
        }
    }
    return e_true;
}

void CAN_CONFIG_INIT(void)
{
    CANFunList.canInit = FUN_CAN_Init;
    CANFunList.canCheckFlagRI = FUN_CAN_CheckFlagRI;
    CANFunList.canCheckFlagTI =FUN_CAN_CheckFlagTI;
    CANFunList.canCheckFlagTXE = FUN_CAN_CheckFlagTXE;
    CANFunList.canClearFlagRI = FUN_CAN_ClearFlagRI;
    CANFunList.canClearFlagTI = FUN_CAN_ClearFlagTI;
    CANFunList.canIntRxDisable = FUN_CAN_IntRxDisable;
    CANFunList.canIntRxEnable = FUN_CAN_IntRxEnable;
    CANFunList.canIntTxDisable = FUN_CAN_IntTxDisable;
    CANFunList.canIntTxEnable = FUN_CAN_IntTxEnable;
    CANFunList.canMonitorRXP = FUN_CAN_MonitorRXP;
    CANFunList.canMonitorSCE = FUN_CAN_MonitorSCE;
    CANFunList.canMsgRx = FUN_CAN_MsgRx;
    CANFunList.canMsgTx = FUN_CAN_MsgTx;
    CANFunList.canRecieveBuf = Mid_msg_buf;
}

UINT8 CAN_InitHW(void)
{
    CAN_CONFIG_INIT();
    CANFunList.canInit();
    queue_inital(CANFunList.canRecieveBuf, CAN1_RX_BUF_LEN);
    return e_true;
}

void CAN_IsrRXPHandling(void)
{
    IO_ UINT8 chReturn = e_true;
    IO_ UINT8 chNum = 0;
    tCANMsgRx RxMessage;
	
////#if OS_CRITICAL_METHOD == 3u                 
////    OS_CPU_SR  cpu_sr = 0u;
////#endif
////    
////    OS_ENTER_CRITICAL(); 
 
    CANFunList.canMonitorRXP();
    while (1 == CANFunList.canCheckFlagRI())
    {
        CANFunList.canClearFlagRI();
        CANFunList.canIntRxDisable();
        
        CANFunList.canMsgRx(&RxMessage);
        
        chReturn = queue_in_fast(CANFunList.canRecieveBuf, (UINT8 *)RxMessage.Data,RxMessage.DLC);
        if (e_true != chReturn)
        {
            
        }
        CANFunList.canIntRxEnable();
    }
	
	/*! 由于协议本身的缺陷,一帧数据有时需要2次或者
	    更多次数进入中断 20190625 */
    if (!Mid_Run)
    {
        Mid_Run = 1;
        OSSemPost(Mid_Sem);
    }

////    OS_EXIT_CRITICAL();  	
}

void CAN_TriggerSend(UINT8* CanSendBuf, UINT16 len)
{
    IO_ UINT8 ch = 0;
    IO_ UINT8 chBreak = 0;
    IO_ UINT8 chReturn = e_true;
    IO_ UINT8 chNum = 0;
    IO_ UINT8 *pBuf = CanSendBuf;
    tCANMsgTx tTxMessage;
    
    CANFunList.canIntTxDisable();
    chBreak = 0;
    
    while (len)
    {
        chNum = 0;
        while (chNum < 8)
        {
            ch = *pBuf++;
            tTxMessage.Data[chNum++] = ch;
            len--;
            if (len == 0)
			{
                break;
			}
        }
        if (chNum)
        {
            tTxMessage.Id  = CAN_ID;
            tTxMessage.DLC = chNum;
			
			// wait empty
            while (CANFunList.canCheckFlagTXE());
			
            chReturn = CANFunList.canMsgTx(&tTxMessage);
        }
    } 
}

void CAN1_SCE_IRQHandler(void)
{
	pro_sys_int_enter();
    CANFunList.canMonitorSCE();
	pro_sys_int_exit();
}

void USB_LP_CAN1_RX0_IRQHandler(void)
{
	pro_sys_int_enter();
    CAN_IsrRXPHandling();
	pro_sys_int_exit();
}

#endif
