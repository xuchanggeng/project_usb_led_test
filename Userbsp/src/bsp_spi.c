
#ifndef __BSP_SPI_C__
#define __BSP_SPI_C__

#include "bsp_spi.h"

tSpiConfig SpiConfig[SPI_NUM];

static void bsp_config_spi_struct(UINT8 eSpiId)
{
    SPII(eSpiId, SpiConfig[eSpiId].Ctrl_Spi);
    SPI_CLK(eSpiId, SpiConfig[eSpiId].Ctrl_Spi_Clk);
    SPI_SCK_GPIO_CLK(eSpiId, SpiConfig[eSpiId].Spi_sck_clk);
    SPI_SCK_GPIO_PORT(eSpiId, SpiConfig[eSpiId].Spi_sck_port);
    SPI_SCK_GPIO_PIN(eSpiId, SpiConfig[eSpiId].Spi_sck_pin);
    SPI_MISO_GPIO_CLK(eSpiId, SpiConfig[eSpiId].Spi_miso_clk);
    SPI_MISO_GPIO_PORT(eSpiId, SpiConfig[eSpiId].Spi_miso_port);
    SPI_MISO_GPIO_PIN(eSpiId, SpiConfig[eSpiId].Spi_miso_pin);
    SPI_MOSI_GPIO_CLK(eSpiId, SpiConfig[eSpiId].Spi_mosi_clk);
    SPI_MOSI_GPIO_PORT(eSpiId, SpiConfig[eSpiId].Spi_mosi_port);
    SPI_MOSI_GPIO_PIN(eSpiId, SpiConfig[eSpiId].Spi_mosi_pin);
    SPI_BRP(eSpiId, SpiConfig[eSpiId].Spi_bpr);
}

void bsp_spi_init(UINT8 eSpiId)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef  SPI_InitStructure;
    
    bsp_config_spi_struct(eSpiId);
    
    if (SpiConfig[eSpiId].Ctrl_Spi == SPI1)
    {
		/*! SPI1时钟使能 */
        RCC_APB2PeriphClockCmd(	SpiConfig[eSpiId].Ctrl_Spi_Clk,  ENABLE );	
    }
    else
	{
		/*! SPI2时钟使能 */
        RCC_APB1PeriphClockCmd(	SpiConfig[eSpiId].Ctrl_Spi_Clk,  ENABLE );
	}
	
	/*! PORT时钟使能 */
	RCC_APB2PeriphClockCmd(	SpiConfig[eSpiId].Spi_sck_clk | SpiConfig[eSpiId].Spi_miso_clk | \
                            SpiConfig[eSpiId].Spi_mosi_clk, ENABLE );
 
	GPIO_InitStructure.GPIO_Pin = SpiConfig[eSpiId].Spi_sck_pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SpiConfig[eSpiId].Spi_sck_port, &GPIO_InitStructure);
    GPIO_SetBits(SpiConfig[eSpiId].Spi_sck_port, SpiConfig[eSpiId].Spi_sck_pin); 
    
    GPIO_InitStructure.GPIO_Pin = SpiConfig[eSpiId].Spi_miso_pin;
	GPIO_Init(SpiConfig[eSpiId].Spi_miso_port, &GPIO_InitStructure);
    GPIO_SetBits(SpiConfig[eSpiId].Spi_miso_port, SpiConfig[eSpiId].Spi_miso_pin);
    
    GPIO_InitStructure.GPIO_Pin = SpiConfig[eSpiId].Spi_mosi_pin;
	GPIO_Init(SpiConfig[eSpiId].Spi_mosi_port, &GPIO_InitStructure);
    GPIO_SetBits(SpiConfig[eSpiId].Spi_mosi_port, SpiConfig[eSpiId].Spi_mosi_pin);

	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;		
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SpiConfig[eSpiId].Spi_bpr;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SpiConfig[eSpiId].Ctrl_Spi, &SPI_InitStructure);
 
	SPI_Cmd(SpiConfig[eSpiId].Ctrl_Spi, ENABLE);
	
	bsp_spi_read_write_byte(eSpiId, 0xff);		 
}   

UINT8 bsp_spi_read_write_byte(UINT8 eSpiId,UINT8 TxData)
{		
	u8 retry = 0;	

    SpiConfig[eSpiId].Ctrl_Spi->DR = TxData;
	while ( (SpiConfig[eSpiId].Ctrl_Spi->SR &0x01) == RESET) 
    {
		retry++;
		if (retry > 200)
		{
			return 0;
		}
    }			  
	 						    
	return SpiConfig[eSpiId].Ctrl_Spi->DR;			    
}

#endif
