/**
  ******************************************************************************
  * @文件   bsp_motor_cmd.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "bsp_motor_cmd.h"
#include "bsp_can_msp.h"
#include "string.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
#define 	MOTOR_OP_CMD_ENA 			0x01 	//电机使能
#define 	MOTOR_OP_CMD_OFF 			0x02 	//电机失能
#define 	MOTOR_OP_CMD_ORG 			0x03 	//设置当前位置
#define 	MOTOR_OP_CMD_STP 			0x04 	//停止运行
#define 	MOTOR_OP_CMD_MOV 			0x05 	//速度模式运行
#define 	MOTOR_OP_CMD_POS 			0x06 	//绝对位置运行
#define 	MOTOR_OP_CMD_RMV 			0x0B 	//相对位置运行

/* 本地宏定义 -------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 ---------------------------------------------------------------*/
/* 本地函数 ---------------------------------------------------------------*/


/*******************************************************************************
* 函数名称: bsp_motor_cmd_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
//static void bsp_motor_cmd_init(void)
//{


//}
// void bsp_can_send_frame_separate(uint16_t _ucTargetId, uint16_t _ucSourceId, uint8_t _ucC1C0, uint8_t _ucCmdRegAddr, uint8_t *_ucData, uint8_t _ucDatalen)

/*******************************************************************************
* 函数名称: bsp_motor_cmd_move_pos
* 功能描述: 相对位置运行
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_motor_cmd_move_pos(uint8_t ucMotorId, int32_t lStep)
{
  uint16_t _ucTargetId  = 0;
  uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;
  uint8_t _ucData[8]    = {0};
  uint8_t _ucDatalen    = 0;

  /*! 参数填充 */
  _ucTargetId = ucMotorId;
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;
  _ucC1C0     = CONTROL_C1C0_CMD_OPERATION;
  _ucCmdRegAddr = MOTOR_OP_CMD_RMV;
  _ucData[_ucDatalen++] = (uint8_t)((lStep>>24)&0xFF);
  _ucData[_ucDatalen++] = (uint8_t)((lStep>>16)&0xFF);
  _ucData[_ucDatalen++] = (uint8_t)((lStep>>8)&0xFF);
  _ucData[_ucDatalen++] = (uint8_t)((lStep)&0xFF);

  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, _ucData, _ucDatalen);


}


/*******************************************************************************
* 函数名称: bsp_motor_cmd_move_speed
* 功能描述: 以指定的速度运行
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_motor_cmd_move_speed(uint8_t ucMotorId, float fSpeed)
{
  uint16_t _ucTargetId  = 0;
  uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;
  uint8_t _ucData[8]    = {0};
  uint8_t _ucDatalen    = 0;

  /*! 参数填充 */
  _ucTargetId = ucMotorId;
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;
  _ucC1C0     = CONTROL_C1C0_CMD_OPERATION;
  _ucCmdRegAddr = MOTOR_OP_CMD_MOV;

  _ucDatalen = 4;
  memcpy(_ucData, &fSpeed, 4);

  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, _ucData, _ucDatalen);

}


/*******************************************************************************
* 函数名称: bsp_motor_cmd_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_motor_cmd_stop(uint8_t ucMotorId)
{
  uint16_t _ucTargetId  = 0;
  uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;
  uint8_t _ucData[8]    = {0};
  uint8_t _ucDatalen    = 0;

  /*! 参数填充 */
  _ucTargetId = ucMotorId;
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;
  _ucC1C0     = CONTROL_C1C0_CMD_OPERATION;
  _ucCmdRegAddr = MOTOR_OP_CMD_STP;

  _ucDatalen = 1;
  _ucData[0] = 0;

  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, _ucData, _ucDatalen);

}



/*******************************************************************************
* 函数名称: bsp_motor_cmd_enable
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_motor_cmd_enable(uint8_t ucMotorId)
{
 uint16_t _ucTargetId  = 0; 
 uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;
  uint8_t _ucData[8]    = {0};
  uint8_t _ucDatalen    = 0;

  /*! 参数填充 */
  _ucTargetId = ucMotorId;
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;
  _ucC1C0     = CONTROL_C1C0_CMD_OPERATION;
  _ucCmdRegAddr = MOTOR_OP_CMD_ENA;

  _ucDatalen = 0;
  _ucData[0] = 0;

  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, _ucData, _ucDatalen);


}


/*******************************************************************************
* 函数名称: bsp_motor_cmd_off
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void bsp_motor_cmd_off(uint8_t ucMotorId)
{
 uint16_t _ucTargetId  = 0; 
 uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;
  uint8_t _ucData[8]    = {0};
  uint8_t _ucDatalen    = 0;

  /*! 参数填充 */
  _ucTargetId = ucMotorId;
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;
  _ucC1C0     = CONTROL_C1C0_CMD_OPERATION;
  _ucCmdRegAddr = MOTOR_OP_CMD_OFF;

  _ucDatalen = 0;
  _ucData[0] = 0;

  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, _ucData, _ucDatalen);

}
