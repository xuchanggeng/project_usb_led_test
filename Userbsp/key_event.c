/**
  ******************************************************************************
  * @文件   key_event.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "key_event.h"
#include "bsp_simple_io_input.h"
#include "test.h"
#include "bsp_max31855.h"
#include "measure_main.h"


/* 本地类型 --------------------------------------------------------------*/
typedef struct _tag_key_function_t
{
    simple_io_btnstate_e state;
    void (*func)();

}key_func_t;

/* 本地常量 --------------------------------------------------------------*/
#define KEY_EVENT_NUM                   4
/* 本地宏定义 ------------------------------------------------------------*/

    
/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 --------------------------------------------------------------*/
static void key_1_event_func(void);
static void calori_start_measure(void);

/* 本地变量 ---------------------------------------------------------------*/
static key_func_t s_stKeyHandleFunc[KEY_EVENT_NUM] = 
{
    {BTN_OFF_TO_ON, calori_start_measure},
    {BTN_OFF_TO_ON, key_1_event_func},
    {BTN_OFF_TO_ON, key_1_event_func},
    {BTN_OFF_TO_ON, key_1_event_func},
};


/* 本地函数 ----------------------------------------------------------------*/
/*******************************************************************************
* 函数名称: template_module_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void key_1_event_func(void)
{

}

/*******************************************************************************
* 函数名称: template_module_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void calori_start_measure(void)
{
//    int32_t ulCurTemp = 0;
//    int32_t ulCurRefTemp = 0;
    /*! 显示标题栏 */
    
    /*! 显示温度 和 重量 左右分列显示*/
    // bsp_max31855_read(3, &ulCurTemp, &ulCurRefTemp);
    // sow_calori_temp(ulCurTemp);
    
    start_measure_aciton(1);

    /*! 显示状态栏 */
    
    bsp_simple_io_input_clear_btn_state(0);
}


/*******************************************************************************
* 函数名称: template_module_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void key_init(void)
{

}

/*******************************************************************************
* 函数名称: key_event_handle
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void key_event_handle(void)
{
    uint16_t unIndex = 0;
    simple_io_btnstate_e eState = (simple_io_btnstate_e)0;
    
    /*! 按照按键的序号，执行对应的函数 */
    for(unIndex = 0; unIndex < KEY_EVENT_NUM; unIndex++)
    {
        eState = bsp_simple_io_input_get_btn_state(unIndex);
        if(eState == s_stKeyHandleFunc[unIndex].state && VALID_POINTER(s_stKeyHandleFunc[unIndex].func))
        {
            s_stKeyHandleFunc[unIndex].func();
        }
    }

}    




