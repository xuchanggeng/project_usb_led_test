/**
  ******************************************************************************
  * @文件   Temlate.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __bsp_tm1640_H
#define __bsp_tm1640_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"
//#include "sys.h"

/* 导出类型 ------------------------------------------------------------*/
/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */

/* 导出变量 ---------------------------------------------------------- */

/* 
控制显示：
0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f 0x0X(这里X代表十六进制的任意值)
1/16,  2/16,  4/16,  10/16, 11/16, 12/16, 13/16, 14/16, 关灯  
 
0x40,0x44  分别对应  地址自动加1 和 固定地址      
*/  


#define SCLK_GPIO_PORT      GPIOB		              
#define SCLK_GPIO_CLK 	    RCC_APB2Periph_GPIOB		
#define SCLK_GPIO_PIN	    GPIO_Pin_6		        
 
#define DIN_GPIO_PORT       GPIOB		              
#define DIN_GPIO_CLK 	    RCC_APB2Periph_GPIOB		
#define DIN_GPIO_PIN	    GPIO_Pin_7

#define TM1640SLK_LOW      GPIO_WriteBit( SCLK_GPIO_PORT, SCLK_GPIO_PIN,0)
#define TM1640SLK_HING     GPIO_WriteBit( SCLK_GPIO_PORT, SCLK_GPIO_PIN,1)
#define TM1640DAT_LOW      GPIO_WriteBit( DIN_GPIO_PORT, DIN_GPIO_PIN,0)
#define TM1640DAT_HING     GPIO_WriteBit( DIN_GPIO_PORT, DIN_GPIO_PIN,1)

void TM1640_Init(void);
void TM1640Start(void);
void TM1640Stop(void);
void TM1640WriteByte(u8 date);
void TM1640_Config(u8 InValue);
void TM1640_SendData(u8 Addr1640,u8 *a,u8 DataLong);







#endif /* __bsp_tm1640_H */

