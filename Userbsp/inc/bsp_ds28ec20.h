/**********************************************************************
*
*	文件名称：bsp_ds28ec20.h
*	功能说明：ds28ec20温度传感器驱动程序
*	特殊说明：
*			：1.ds28ec20芯片出厂配置为12位模式
*           ：2.ds28ec20芯片在12位模式下最大转换时间为750ms  
*			：3.ds28ec20芯片上电温度值为85度
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2028, www.geniusmedica.com
*
**********************************************************************/

#ifndef __BSP_DS28EC20_H__
#define __BSP_DS28EC20_H__

#include "includes.h"

/*! 定义ds28ec20传感器类型，按照原理图命名 */
enum
{
	BSP_DS28EC20_TEMP1 = 0x00,			// 1号反应杯温度传感器
	MAX_DS28EC20_NUM,
};

uint8_t bsp_ds28ec20_init( void );
uint8_t bsp_ds28ec20_check_state( void );
uint16_t bsp_ds28ec20_read_original_temp( uint8_t ucId );
float bsp_ds28ec20_read_temp( uint8_t ucId );
uint16_t bsp_data_read(uint8_t ucId);
int16_t bsp_temp_to_ds28ec20(float fTemp);
void bsp_ds28ec20_read_memory( uint8_t ucId, uint16_t unDataAddr, uint8_t *ucpData, uint16_t* ucDataLen);


#endif
