/**
  ******************************************************************************
  * @文件   bsp_motor_cmd.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __BSP_MOTOR_CMD_H
#define __BSP_MOTOR_CMD_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
/*! 电机以指定的速度，运行指定的距离 */
void bsp_motor_cmd_move_pos(uint8_t ucMotorId, int32_t lStep);
/*! 电机以指定的速度运行，一直运行 */
void bsp_motor_cmd_move_speed(uint8_t ucMotorId, float fSpeed);
/*! 电机停止 */
void bsp_motor_cmd_stop(uint8_t ucMotorId);

void bsp_motot_cmd_reset(uint8_t ucMotorId);
/*! 电机使能 */
void bsp_motor_cmd_enable(uint8_t ucMotorId);
/*! 电机脱机（断电） */
void bsp_motor_cmd_off(uint8_t ucMotorId);
/*!  */ 



/* 导出变量 ---------------------------------------------------------- */

#endif /* __BSP_MOTOR_CMD_H */

