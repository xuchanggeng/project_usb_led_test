
#ifndef __BSP_CAN_H__
#define __BSP_CAN_H__

#include "pro_type.h"

// 最多可以开启4个串口，串口的参数可以配置，配置后就可使用

// Uart num   串口个数
#define CAN_NUM						1


#define CAN_ID                      1

#define CTRLCAN1				    CAN1    
#define CAN1_CLK					RCC_APB1Periph_CAN1

#define CAN1_TX_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CAN1_TX_GPIO_PORT			GPIOA
#define CAN1_TX_GPIO_PIN			GPIO_Pin_12

#define CAN1_RX_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CAN1_RX_GPIO_PORT			GPIOA
#define CAN1_RX_GPIO_PIN			GPIO_Pin_11

//设置波特率   36000/[(CAN_BS1+CAN_BS2+1)*CAN_Prescaler]
#define CAN1_SJW                    CAN_SJW_2tq
#define CAN1_BS1                    CAN_BS1_9tq
#define CAN1_BS2                    CAN_BS2_8tq
#define CAN1_BPR                    4

#define CAN1_RX_BUF_LEN             256




typedef struct
{
  UINT32 Id;  /*!< Specifies the standard identifier.
                        This parameter can be a value between 0 to 0x7FF. */

  UINT8 IDE;     /*!< Specifies the type of identifier for the message that 
                        will be transmitted. This parameter can be a value 
                        of @ref CAN_identifier_type */

  UINT8 RTR;     /*!< Specifies the type of frame for the message that will 
                        be transmitted. This parameter can be a value of 
                        @ref CAN_remote_transmission_request */

  UINT8 DLC;     /*!< Specifies the length of the frame that will be 
                        transmitted. This parameter can be a value between 
                        0 to 8 */

  UINT8 Data[8]; /*!< Contains the data to be transmitted. It ranges from 0 
                        to 0xFF. */
} tCANMsgTx;

typedef struct
{
  UINT32 Id;  /*!< Specifies the standard identifier.
                        This parameter can be a value between 0 to 0x7FF. */

  UINT8 IDE;     /*!< Specifies the type of identifier for the message that 
                        will be received. This parameter can be a value of 
                        @ref CAN_identifier_type */

  UINT8 RTR;     /*!< Specifies the type of frame for the received message.
                        This parameter can be a value of 
                        @ref CAN_remote_transmission_request */

  UINT8 DLC;     /*!< Specifies the length of the frame that will be received.
                        This parameter can be a value between 0 to 8 */

  UINT8 Data[8]; /*!< Contains the data to be received. It ranges from 0 to 
                        0xFF. */

  UINT8 FMI;     /*!< Specifies the index of the filter the message stored in 
                        the mailbox passes through. This parameter can be a 
                        value between 0 to 0xFF */
} tCANMsgRx;


typedef UINT8 (*funCANOne)  (void);
typedef UINT8 (*funCANTwo)  (tCANMsgTx*);
typedef UINT8 (*funCANTree)  (tCANMsgRx*);
typedef UINT8 (*funCANFour)  (UINT8,UINT32);

typedef struct
{
    funCANOne   canInit;
    funCANOne   canCheckFlagRI;
    funCANOne   canCheckFlagTI;
    funCANOne   canCheckFlagTXE;
    funCANOne   canClearFlagRI;
    funCANOne   canClearFlagTI;
    funCANOne   canIntRxDisable;
    funCANOne   canIntRxEnable;
    funCANOne   canIntTxDisable;
    funCANOne   canIntTxEnable;
    funCANOne   canMonitorRXP;
    funCANOne   canMonitorSCE;
    funCANTree   canMsgRx;
    funCANTwo   canMsgTx;
    UINT8*      canRecieveBuf;
}m_tCANFunList;

UINT8 CAN_InitHW(void);
void CAN_TriggerSend(UINT8* CanSendBuf, UINT16 len);
#endif
