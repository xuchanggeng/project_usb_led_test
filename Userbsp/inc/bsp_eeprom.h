/**********************************************************************
*
*	文件名称：bsp_eeprom.h
*	功能说明：AT24CXX芯片驱动程序
*	特殊说明：该软件采用软件I2C协议,适用于AT24C02/.../AT24C512等芯片	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

#ifndef __BSP_EEPROM_H__
#define __BSP_EEPROM_H__

#include "includes.h"
//#include "eeprom_adder.h"

/*! eeprom种类定义 !*/
typedef enum		      
{
	AT24C01 = 0,          //128
	AT24C02,              //256
	AT24C04,              //512
	AT24C08,              //1024
	AT24C16,              //2k
	AT24C32,              //4k
	AT24C64,              //8k
	AT24C128,             //16k
	AT24C256,             //32k
	AT24C512,			  //64k	
}eeprom_e;

/*********************************************************************/
/*                             用户定义接口                          */
/*********************************************************************/
#define EE_TYPE  AT24C512							  			// eeprom芯片类型 

#define EE_I2C_GPIO_RCC			RCC_APB2Periph_GPIOB			// I2C硬件接口    
#define EE_I2C_GPIO 			GPIOB
#define EE_I2C_SCL 				GPIO_Pin_6
#define EE_I2C_SDA 				GPIO_Pin_7

#define EE_WP_GPIO_RCC			RCC_APB2Periph_GPIOB			// 芯片写保护硬件接口 
#define EE_WP_GPIO 				GPIOB
#define EE_WP_PIN 				GPIO_Pin_5
/*********************************************************************/
/*                           用户定义接口结束                        */
/*********************************************************************/

void bsp_eeprom_init (void );
uint8_t bsp_eeprom_rd8( uint16_t unReadAddr );
uint16_t bsp_eeprom_rd16( uint16_t unReadAddr );
uint32_t bsp_eeprom_rd32( uint16_t unReadAddr );
void bsp_eeprom_wt8( uint16_t unWriteAddr, uint8_t ucData );
void bsp_eeprom_wt16( uint16_t unWriteAddr, uint16_t unData );
void bsp_eeprom_wt32( uint16_t unWriteAddr, uint32_t lData );
void bsp_eeprom_read( uint16_t unReadAddr, uint8_t *pucBuffer, uint16_t unNumToRead );
void bsp_eeprom_write( uint16_t unWriteAddr, uint8_t *pucBuffer, uint16_t unNumToWrite );
uint16_t bsp_eeprom_rd16_2th( uint16_t unReadAddr );
#endif
