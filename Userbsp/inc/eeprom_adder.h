/**********************************************************************
*
*	文件名称：eeprom_adder.h
*	功能说明：EEPROM地址使用分配情况
*	特殊说明：无       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2028, www.geniusmedica.com
*
**********************************************************************/

#ifndef __EEPROM_ADDER_H__
#define __EEPROM_ADDER_H__

#define	EEPROM_IS_INIT_PARA_VERSION_1_ADDER				                (1)		    // 参数版1地址
#define	EEPROM_IS_INIT_PARA_VERSION_2_ADDER				                (2)		    // 参数版2地址
#define	EEPROM_MOTOR_SPEED_LEVEL_ADDER				                    (3)		    // 电机速度等级地址

#define	EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER				    (70)		// 载体水平电机    pos步数         2个位置
#define	EEPROM_INIT_LOAD_P_PUSH_MOTOR_POS_STEP_ADDER				    (30)		// 载体卡条推杆电机    pos步数     2个位置
#define	EEPROM_INIT_LOAD_P_FLIP_MOTOR_POS_STEP_ADDER				    (40)		// 载体卡条门翻转电机    pos步数         3个位置
#define	EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER				    (50)		// TIP头仓盒电机    pos步数     2个位置
#define	EEPROM_INIT_CARD_SLICE_BOX_MOTOR_POS_STEP_ADDER				    (60)		// 卡条盒进卡电机    pos步数   2个位置
	
#define	EEPROM_INIT_TEMP_SET_ADDER				                        (100)		// 孵育盒温度设置值
#define	EEPROM_INIT_TEMP_OFFSET_ADDER				                    (102)		// 孵育盒温度补偿值


#endif
