/**********************************************************************
*
*	文件名称：bsp_ds18b20.h
*	功能说明：DS18B20温度传感器驱动程序
*	特殊说明：
*			：1.DS18B20芯片出厂配置为12位模式
*           ：2.DS18B20芯片在12位模式下最大转换时间为750ms  
*			：3.DS18B20芯片上电温度值为85度
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2028, www.geniusmedica.com
*
**********************************************************************/

#ifndef __BSP_DS18B20_H__
#define __BSP_DS18B20_H__

#include "includes.h"

/*! 定义DS18B20传感器类型，按照原理图命名 */
enum
{
	BSP_DS18B20_TEMP1 = 0x00,			// 1号反应杯温度传感器
//	BSP_DS18B20_TEMP2 = 0x01,			// 2号反应杯温度传感器
//	BSP_DS18B20_TEMP3 = 0x02,			// 3号反应杯温度传感器
//	BSP_DS18B20_TEMP4 = 0x03,			// 4号反应杯温度传感器
//	BSP_DS18B20_TEMP5 = 0x04,			// 5号反应杯温度传感器
//	BSP_DS18B20_TEMP6 = 0x05,			// 6号反应杯温度传感器
//	BSP_DS18B20_TEMP7 = 0x06,			// 试剂制冷温度传感器
	MAX_DS18B20_NUM,
};

uint8_t bsp_ds18b20_init( void );
uint8_t bsp_ds18b20_check_state( void );
uint16_t bsp_ds18b20_read_original_temp( uint8_t ucId );
float bsp_ds18b20_read_temp( uint8_t ucId );
uint16_t bsp_temp_read(uint8_t ucId);
int16_t bsp_temp_to_ds18b20(float fTemp);

#endif
