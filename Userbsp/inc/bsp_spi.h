
#ifndef __BSP_SPI_H__
#define __BSP_SPI_H__

#include "stm32f10x_conf.h"
#include "pro_type.h"

typedef struct
{
    SPI_TypeDef*    Ctrl_Spi;
    UINT32          Ctrl_Spi_Clk;
    GPIO_TypeDef*   Spi_sck_port;
    UINT16          Spi_sck_pin;
    UINT32          Spi_sck_clk;
    GPIO_TypeDef*   Spi_miso_port;
    UINT16          Spi_miso_pin;
    UINT32          Spi_miso_clk;
    GPIO_TypeDef*   Spi_mosi_port;
    UINT16          Spi_mosi_pin;
    UINT32          Spi_mosi_clk;
    UINT16          Spi_bpr;
}tSpiConfig;

enum eSpi_type
{
	eSpi_1 = 0,
	eSpi_2 = 1
};

typedef UINT8 (*funSPIOne)   (void);
typedef UINT8 (*funSPITwo)   (UINT8);
typedef UINT8 (*funSPITree)  (UINT8,UINT8);
typedef UINT8 (*funSPIFour)  (UINT8,UINT32);

//================================================================================
#define CTRL_SPI(x)						CTRL_SPI##x
#define CTRL_SPI_CLK(x)					CTRL_SPI##x##_CLK

#define CTRL_SPI_CS_GPIO_CLK(x)			CTRL_SPI##x##_CS_GPIO_CLK
#define CTRL_SPI_CS_GPIO_PORT(x)        CTRL_SPI##x##_CS_GPIO_PORT
#define CTRL_SPI_CS_GPIO_PIN(x)			CTRL_SPI##x##_CS_GPIO_PIN

#define CTRL_SPI_SCK_GPIO_CLK(x)        CTRL_SPI##x##_SCK_GPIO_CLK
#define CTRL_SPI_SCK_GPIO_PORT(x)       CTRL_SPI##x##_SCK_GPIO_PORT
#define CTRL_SPI_SCK_GPIO_PIN(x)        CTRL_SPI##x##_SCK_GPIO_PIN

#define CTRL_SPI_MISO_GPIO_CLK(x)       CTRL_SPI##x##_MISO_GPIO_CLK
#define CTRL_SPI_MISO_GPIO_PORT(x)      CTRL_SPI##x##_MISO_GPIO_PORT
#define CTRL_SPI_MISO_GPIO_PIN(x)       CTRL_SPI##x##_MISO_GPIO_PIN

#define CTRL_SPI_MOSI_GPIO_CLK(x)       CTRL_SPI##x##_MOSI_GPIO_CLK
#define CTRL_SPI_MOSI_GPIO_PORT(x)      CTRL_SPI##x##_MOSI_GPIO_PORT
#define CTRL_SPI_MOSI_GPIO_PIN(x)       CTRL_SPI##x##_MOSI_GPIO_PIN

#define CTRL_SPI_BRP(x)                 CTRL_SPI##x##_BRP

//================================================================================
#define SPII(x,y)                       do{if(x==0) y = CTRL_SPI(1);\
										else if(x==1) y = CTRL_SPI(2);\
                                        }while(0)

#define SPI_CLK(x,y)                    do{if(x==0) y = CTRL_SPI_CLK(1);\
										else if(x==1) y = CTRL_SPI_CLK(2);\
                                        }while(0)

#define SPI_SCK_GPIO_CLK(x,y)		    do{if(x==0) y = CTRL_SPI_SCK_GPIO_CLK(1);\
										else if(x==1) y = CTRL_SPI_SCK_GPIO_CLK(2);\
                                        }while(0)

#define SPI_SCK_GPIO_PORT(x,y)          do{if(x==0) y = CTRL_SPI_SCK_GPIO_PORT(1);\
										else if(x==1) y = CTRL_SPI_SCK_GPIO_PORT(2);\
                                        }while(0)

#define SPI_SCK_GPIO_PIN(x,y)		    do{if(x==0) y = CTRL_SPI_SCK_GPIO_PIN(1);\
										else if(x==1) y = CTRL_SPI_SCK_GPIO_PIN(2);\
                                        }while(0)

#define SPI_MISO_GPIO_CLK(x,y)		    do{if(x==0) y = CTRL_SPI_MISO_GPIO_CLK(1);\
										else if(x==1) y = CTRL_SPI_MISO_GPIO_CLK(2);\
                                        }while(0)

#define SPI_MISO_GPIO_PORT(x,y)         do{if(x==0) y = CTRL_SPI_MISO_GPIO_PORT(1);\
										else if(x==1) y = CTRL_SPI_MISO_GPIO_PORT(2);\
                                        }while(0)

#define SPI_MISO_GPIO_PIN(x,y)		    do{if(x==0) y = CTRL_SPI_MISO_GPIO_PIN(1);\
										else if(x==1) y = CTRL_SPI_MISO_GPIO_PIN(2);\
                                        }while(0)

#define SPI_MOSI_GPIO_CLK(x,y)		    do{if(x==0) y = CTRL_SPI_MOSI_GPIO_CLK(1);\
										else if(x==1) y = CTRL_SPI_MOSI_GPIO_CLK(2);\
                                        }while(0)

#define SPI_MOSI_GPIO_PORT(x,y)         do{if(x==0) y = CTRL_SPI_MOSI_GPIO_PORT(1);\
										else if(x==1) y = CTRL_SPI_MOSI_GPIO_PORT(2);\
                                        }while(0)

#define SPI_MOSI_GPIO_PIN(x,y)		    do{if(x==0) y = CTRL_SPI_MOSI_GPIO_PIN(1);\
										else if(x==1) y = CTRL_SPI_MOSI_GPIO_PIN(2);\
                                        }while(0)

#define SPI_BRP(x,y)		            do{if(x==0) y = CTRL_SPI_BRP(1);\
										else if(x==1) y = CTRL_SPI_BRP(2);\
                                        }while(0)

//================================================================================
#define SPI_NUM                         1

//----------------------------------SPI1接口定义----------------------------------
#define CTRL_SPI1						SPI1
#define CTRL_SPI1_CLK					RCC_APB2Periph_SPI1

#define CTRL_SPI1_SCK_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CTRL_SPI1_SCK_GPIO_PORT			GPIOA
#define CTRL_SPI1_SCK_GPIO_PIN			GPIO_Pin_5

#define CTRL_SPI1_MISO_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CTRL_SPI1_MISO_GPIO_PORT        GPIOA
#define CTRL_SPI1_MISO_GPIO_PIN			GPIO_Pin_6

#define CTRL_SPI1_MOSI_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CTRL_SPI1_MOSI_GPIO_PORT        GPIOA
#define CTRL_SPI1_MOSI_GPIO_PIN			GPIO_Pin_7

#define CTRL_SPI1_BRP                   SPI_BaudRatePrescaler_64

//----------------------------------SPI1接口定义----------------------------------
#define CTRL_SPI2						SPI2
#define CTRL_SPI2_CLK					RCC_APB1Periph_SPI2

#define CTRL_SPI2_CS_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CTRL_SPI2_CS_GPIO_PORT			GPIOA
#define CTRL_SPI2_CS_GPIO_PIN			GPIO_Pin_4

#define CTRL_SPI2_SCK_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CTRL_SPI2_SCK_GPIO_PORT			GPIOA
#define CTRL_SPI2_SCK_GPIO_PIN			GPIO_Pin_5

#define CTRL_SPI2_MISO_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CTRL_SPI2_MISO_GPIO_PORT        GPIOA
#define CTRL_SPI2_MISO_GPIO_PIN			GPIO_Pin_6

#define CTRL_SPI2_MOSI_GPIO_CLK			RCC_APB2Periph_GPIOA
#define CTRL_SPI2_MOSI_GPIO_PORT        GPIOA
#define CTRL_SPI2_MOSI_GPIO_PIN			GPIO_Pin_7

#define CTRL_SPI2_BRP                   SPI_BaudRatePrescaler_256

//================================================================================
void bsp_spi_init(UINT8 eSpiId);
UINT8 bsp_spi_read_write_byte(UINT8 eSpiId, UINT8 TxData);

#endif
