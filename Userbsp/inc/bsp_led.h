
#ifndef __BSP_LED_H__
#define __BSP_LED_H__	 

#include "pro_sys.h"

#define LED0 PCout(2)

void bsp_led_init(void);
		 	
void bsp_led_toggle(void);

#endif
