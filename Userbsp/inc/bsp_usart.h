/**********************************************************************
*
*	文件名称：bsp_usart.h
*	功能说明：STM32内部USART驱动程序
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

#ifndef __BSP_USART_H__
#define __BSP_USART_H__

#include "includes.h"

void bsp_usart_init(USART_TypeDef* USARTx,uint32_t ulBaudRate);
void bsp_usart2_send_byte(unsigned char ucData);
void bsp_usart3_send_byte(unsigned char ucData);
void bsp_uart4_send_byte(unsigned char ucData);
void bsp_usart_send_char(USART_TypeDef* pstCom,uint8_t ucData);
void bsp_usart_send_string(USART_TypeDef* pstCom,uint8_t *pucBufferPtr, uint32_t ulLength);

#endif
