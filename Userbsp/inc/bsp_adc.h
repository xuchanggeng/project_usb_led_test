/**
  ******************************************************************************
  * @文件   bsp_max31855.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  *     All rights reserved	
  ******************************************************************************  
  */ 

#ifndef __BSP_ADC_H
#define __BSP_ADC_H	
#include "includes.h"


void bsp_adc_init(void);
uint16_t bsp_get_adc(uint8_t ucChn); 
uint16_t bsp_get_adc_average(uint8_t ucChn, uint8_t ucTimes); 
 
#endif /*! __BSP_ADC_H */
