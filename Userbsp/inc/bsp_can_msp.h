/*
*********************************************************************************************************
*
*	模块名称 : xxx驱动模块
*	文件名称 : bsp_xxx_msp.h
*	版    本 : V1.0
*	说    明 : 头文件
*
*	Copyright (C)
*
*********************************************************************************************************
*/

#ifndef __BSP_CAN_MSP_H
#define __BSP_CAN_MSP_H
#include "includes.h"



/*!CAN	缓冲区大小*/
#define    CAN_BUF_SIZE 		  512
/*! 设定了一个临时性的扩展存储区域。即串口buffer的前500字节为环形存储区域，后25个字节为临时扩展存储区域 */
#define    CAN_BUF_COMPENS_SIZE   512
/*!CAN	发送缓冲区大小*/
#define    CAN_BUF_TXD_SIZE       256


/*! 电机状态表 */
typedef enum
{
	CAN_ACT_EXEC_STEP1 = 0x10,
	CAN_ACT_EXEC_STEP2,
	CAN_ACT_EXEC_STEP3,
	CAN_ACT_EXEC_STEP4,
	CAN_ACT_EXEC_STEP5,
	CAN_ACT_EXEC_STEP6,
	CAN_ACT_EXEC_STEP_INVALID,
} _can_act_step;




/*! 电机状态表 */
typedef enum
{
	CAN_BUS_IDEL= 0x00,
  CAN_BUS_BUZY ,   
} _can_act_statu;






/*! 协议解析过程属性结构体定义	 */
typedef struct
{
	unsigned char ucMode;          			/*!< 协议解析模式 								*/
	unsigned char ucStatus;        			/*!< 协议解析状态值 							*/
	unsigned char ucExecFlag;      			/*!< 协议动作执行标志位 					*/
	unsigned char StdId;								/*!< 协议动作驱动板ID							*/
	unsigned char sActStatu;        		/*!< 协议解析状态值 CAN总线当前状态 							*/
	volatile signed int iActionSysTime;	/*!< 协议动作执行时当前的系统时间，用于监控CAN动作时间*/
	unsigned char  ucIndex;       			/*!< 协议解析过程应答帧数据buffer写索引存储 */
	unsigned char  ucBufLen;      			/*!< 协议解析过程应答帧数据buffer最大长度 */
	unsigned char *pucBufAddr;    			/*!< 协议解析过程应答帧数据buffer地址 */
	
}CAN_VetTypeDef;






/**
  * @brief  配置can资源配置
	*/
void  can_sourse_init(void);

/**
  * @brief  复位CAN
	*/
void  can_statu_init(void);


/**
  * @brief  	复制包头到包尾
	*/
void can_data_buf_cpy( void);


void bsp_can_send_bytes(uint32_t ulStdId, uint8_t *ucpSendData, uint8_t ucSendDataLen);

//void bsp_can_send_bytes(uint32_t ulStdId);

void bsp_can_send_frame(frame_info_t *stFrame);

void bsp_can_send_frame_separate(uint16_t _ucTargetId, uint16_t _ucSourceId, uint8_t _ucC1C0, uint8_t _ucCmdRegAddr, uint8_t *_ucData, uint8_t _ucDatalen);

#endif

/*****************************  (END OF FILE) *********************************/
