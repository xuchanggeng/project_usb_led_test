
#ifndef __BSP_PUMP_VALVE_H__
#define __BSP_PUMP_VALVE_H__	

#include "includes.h"

typedef struct
{
    GPIO_TypeDef*  PV_Port;
    UINT32         PV_Clk;
    UINT16         PV_Pin;
}tPV_Config;
//================================================================================

#define PVx_PIN(x)			PV_##x##_PIN
#define PVx_PORT(x)         PV_##x##_PORT
#define PVx_CLK(x)			PV_##x##_CLK
//================================================================================

#define PV_PIN(x,y)		    do{  if(x==0) y = PVx_PIN(1);\
                            else if(x==1) y = PVx_PIN(2);\
                            else if(x==2) y = PVx_PIN(3);\
                            else if(x==3) y = PVx_PIN(4);\
                            else if(x==4) y = PVx_PIN(5);\
                            else if(x==5) y = PVx_PIN(6);\
                            }while(0)
                                        
#define PV_PORT(x,y)        do{  if(x==0) y = PVx_PORT(1);\
                            else if(x==1) y = PVx_PORT(2);\
                            else if(x==2) y = PVx_PORT(3);\
                            else if(x==3) y = PVx_PORT(4);\
                            else if(x==4) y = PVx_PORT(5);\
                            else if(x==5) y = PVx_PORT(6);\
                            }while(0)
                                        
#define PV_CLK(x,y)		    do{  if(x==0) y = PVx_CLK(1);\
                            else if(x==1) y = PVx_CLK(2);\
                            else if(x==2) y = PVx_CLK(3);\
                            else if(x==3) y = PVx_CLK(4);\
                            else if(x==4) y = PVx_CLK(5);\
                            else if(x==5) y = PVx_CLK(6);\
                            }while(0)
//================================================================================
typedef struct
{
    UINT8   State;
    UINT16  TimeOut;
}tPV_Check;	

//================================================================================
//------------------------------用户定义接口信息----------------------------------
typedef enum
{
    E_PV_1  = 0,				//出针控制
    E_PV_2  = 1,				//收针控制
	E_PV_MAX
}ePV_Type;

// 电磁铁IO口定义
#define PV_1_PIN        		GPIO_Pin_2
#define PV_1_PORT       		GPIOA
#define PV_1_CLK        		RCC_APB2Periph_GPIOA

#define PV_2_PIN        		GPIO_Pin_3
#define PV_2_PORT       		GPIOA
#define PV_2_CLK        		RCC_APB2Periph_GPIOA

#define PV_3_PIN        		GPIO_Pin_6
#define PV_3_PORT       		GPIOA
#define PV_3_CLK        		RCC_APB2Periph_GPIOA

#define PV_4_PIN        		GPIO_Pin_7
#define PV_4_PORT       		GPIOA
#define PV_4_CLK        		RCC_APB2Periph_GPIOA

#define PV_5_PIN        		GPIO_Pin_1
#define PV_5_PORT       		GPIOA
#define PV_5_CLK        		RCC_APB2Periph_GPIOA

#define PV_6_PIN        		GPIO_Pin_0
#define PV_6_PORT       		GPIOA
#define PV_6_CLK        		RCC_APB2Periph_GPIOA

void bsp_pv_all_init(void);
void bsp_pv_open(ePV_Type PV_Type);
void bsp_pv_close(ePV_Type PV_Type);
UINT8 bsp_pv_get_state(ePV_Type PV_Type);
void bsp_pv_timeout_check(ePV_Type PV_Type, UINT16 unTimeOut);
void bsp_pv_timeout_handle(void);
void bsp_set_pv_state(ePV_Type PV_Type, FunctionalState NewState);

#endif
