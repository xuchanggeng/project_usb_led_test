/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "Com_Dispatcher.h"
#include "confdef.h"
#include "Util_Queue.h"
#include "bsp_usart.h"
#include "Com_Prase.h"
#include "load_platform_task.h"
#include "tip_head_box_task.h"
#include "card_slice_box_task.h"
#include "bsp_can_msp.h"
#include "temp_unit_task.h"

/* 本地类型 --------------------------------------------------------------*/

/* 本地常量 --------------------------------------------------------------*/

/* 本地宏定义 ------------------------------------------------------------*/

/* 本地变量 ---------------------------------------------------------------*/
extern frame_queue_t stReceFrameQueue ;
extern frame_queue_t stSendFrameQueue;

frame_info_t s_stCmdFrameInfo = {0};
frame_info_t s_stSendFrameInfo = {0};
static uint8_t s_ucaSendFrameBuffer[MAX_PARA_NUM] = {0};
//static cmd_result_t s_stCmdResult={0};

/* 函数原型 ----------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/



/*******************************************************************************
* 名称: com_host_debug_test
* 功能: 上位机发送命令调试
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void com_host_debug_test(void)
{


}

/*******************************************************************************
* 名称: ProcessCommandFrame
* 功能: 重队列中取出数据，分发到各个模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t ProcessCommandFrame(frame_info_t*  stFrame)
{
	uint8_t ret = 0;
	/*! 在函数还回后还要用结果指针，不能用局部变量 */
//	cmd_result_t result={0};
    
	switch(stFrame->ucTarget)
	{
		/*! 发送到中位机单元 */
		case MID_PART_UNIT_ID:
		break;

        /*! 发送到采样轴单元 */
		case SAMP_PROBE_UNIT_ID:
			//ret = start_samp_probe_action(stFrame, 1);
		break;
        
        /*! 发送到tip头仓盒单元   */
		case TIP_HEAD_BOX_UNIT_ID:
			start_tip_head_box_action(stFrame, 1);
		break;
        
        /*! 发送到卡条盒单元 */
		case CARD_SLICE_BOX_UNIT_ID:
			start_card_slice_box_action(stFrame, 1);
		break;
        
        /*! 发送到载体单元 */
		case LOAD_PLATFORM_UNIT_ID:
			start_load_platform_action(stFrame, 1);
		break;
        
        /*! 发送到轨道单元 */
		case SAMP_RAIL_UNIT_ID:
		break;
		
        /*! 发送到孵育盒拍照组件或卡条卸载单元   */
		case INCUBATE_TEST_UNIT_ID:
			//start_incubate_box_action(stFrame, 1);
		break;
        
        /*! 发送到显微镜单元或计数卡推杆单元  */
        case MICROSCOPE_BAR_UNIT_ID:
			//start_microscope_bar_action(stFrame, 1);
            break;
        
        /*! 发送到染色注射器单元 */
        case DYEING_SYRINGE_UNIT_ID:
			//start_dyeing_syringe_action(stFrame, 1);
            break;
		
        /*! 发送到温控单元 */
        case TEMP_UNIT_ID:
			start_temp_unit_action(stFrame, 1);
            break;
		
		default: 
		break;
	}


	return ret;
}



/*******************************************************************************
* 名称: ProcessReplyFrame
* 功能: 重队列中取出数据，分发到各个模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t ProcessReplyFrame(frame_info_t*  stFrame)
{
	uint8_t ret = 0;
	
	ret = AddFrameToSendQueue(stFrame);
	
	return ret;
}

/*******************************************************************************
* 名称: ComFrameDispatcher
* 功能: 重队列中取出数据，分发到各个模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t ProcessResultFrame(frame_info_t*  stFrame)
{
	uint8_t ret = 0;
	
	ret = AddFrameToSendQueue(stFrame);
	
	return ret;
}

/*******************************************************************************
* 名称: ProcessDataFrame
* 功能: 重队列中取出数据，分发到各个模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t ProcessDataFrame(frame_info_t*  stFrame)
{
	uint8_t ret = 0;
	
	ret = AddFrameToSendQueue(stFrame);
	
	return ret;
}


/*******************************************************************************
* 名称: ProcessWarningFrame
* 功能: 重队列中取出数据，分发到各个模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t ProcessWarningFrame(frame_info_t*  stFrame)
{
	uint8_t ret = 0;
	
	ret = AddFrameToSendQueue(stFrame);
	
	return ret;
}


/*******************************************************************************
* 名称: ComFrameDispatcher
* 功能: 读取上位机接受队列中取出数据，分发到各个模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t ComFrameDispatcher(void)
{
	uint8_t ret = 0;
	
	/*! 读取上位机队列中的数据帧，分发各个单元模块 */
	if(!FRAME_IsQueueEmpty(&stReceFrameQueue))
	{
		FRAME_OutQueue(&stReceFrameQueue, &s_stCmdFrameInfo);
		
		switch(s_stCmdFrameInfo.ucFrametype)
		{
			case FRAME_COMMAND:
				ret = ProcessCommandFrame(&s_stCmdFrameInfo);
			break;
		
			case FRAME_REPLY:
				ret = ProcessReplyFrame(&s_stCmdFrameInfo);
			break;

			case FRAME_RESULT:
				ret = ProcessResultFrame(&s_stCmdFrameInfo);
			break;

			case FRAME_DATA:
				ret = ProcessDataFrame(&s_stCmdFrameInfo);
			break;

			case FRAME_WARNING:
				ret = ProcessWarningFrame(&s_stCmdFrameInfo);
			break;

			default: 
				/*! 错误的单元号 */
			break;
			
		}
	
	}
	return ret;
}


/*******************************************************************************
* 名称: AddFrameToSendQueue
* 功能: 把消息帧添加到发送队列， 发送通信帧功能
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t AddFrameToSendQueue(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	
	if(!FRAME_IsQueueFull(&stSendFrameQueue))
	{
		ret = FRAME_InQueue(&stSendFrameQueue, stFrame);
	}
	else
	{
		/*! 队列满 */
		PRO_DEBUG(COMM_DEBUG,("send queue full error...\r\n"));
	}
	
	return ret;
	
}

uint16_t get_frame_can_id(frame_info_t *stFrame)
{
    uint16_t unCanId = 0;

    switch (stFrame->ucSender) {

    /*! 发送到上位机单元 */
    case PC_PART_UNIT_ID:
		/*! 错误 */
        unCanId = 0x01;
    break;	
	
    /*! 发送自中位机单元 */
    case MID_PART_UNIT_ID:
		/*! 错误 */
        unCanId = 0x01;
    break;

    /*! 发送自采样轴单元 */
    /*! 发送自染色注射器单元 */
    case SAMP_PROBE_UNIT_ID:
    case DYEING_SYRINGE_UNIT_ID:
        unCanId = EL180_DRIVER_BOARD_2 + EL180_DRIVER_BOARD_BACK_OFFSET;
    break;

    /*! 发送自tip头仓盒单元 */
    /*! 发送自卡条盒单元 */
    /*! 发送自载体单元 */
    case TIP_HEAD_BOX_UNIT_ID:
    case CARD_SLICE_BOX_UNIT_ID:
    case LOAD_PLATFORM_UNIT_ID:
        unCanId = EL180_DRIVER_BOARD_3 + EL180_DRIVER_BOARD_BACK_OFFSET;
    break;

    /*! 发送自轨道单元 */
    case SAMP_RAIL_UNIT_ID:
        unCanId = EL180_DRIVER_BOARD_1 + EL180_DRIVER_BOARD_BACK_OFFSET;
    break;
	
    /*! 发送自孵育盒拍照组件或卡条卸载单元 */
    /*! 发送自显微镜单元或计数卡推杆单元 */
    case INCUBATE_TEST_UNIT_ID:
    case MICROSCOPE_BAR_UNIT_ID:
        unCanId = EL180_DRIVER_BOARD_4 + EL180_DRIVER_BOARD_BACK_OFFSET;
    break;
	
    /*! 发送自温控表按 */
    case TEMP_UNIT_ID:
        unCanId = EL180_DRIVER_BOARD_5 + EL180_DRIVER_BOARD_BACK_OFFSET;
    break;
	
    default:
        break;
    }

    return unCanId;
}


/*******************************************************************************
* 名称: SendFrameToPorts
* 功能:  通过串口，发送数据帧到各个板卡
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t SendFrameToPorts(frame_info_t * stFrame)
{
	uint8_t    ret = 0;
	uint16_t   ucLength = 0;
	uint16_t   i = 0;
    uint8_t    ucCanId = 0;
	
	/*! 从数据帧中取出发送的内容*/
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucHead;
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucLength;
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucMachine;
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucFrametype;
    s_ucaSendFrameBuffer[ucLength++] = stFrame->ucFrame;
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucSender;
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucTarget;
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucCmdId;
	
	for(i = 0; i < stFrame->ucLength - 6; i++)
	{
		s_ucaSendFrameBuffer[ucLength++] = stFrame->ucaPara[i];
	}
    s_ucaSendFrameBuffer[ucLength++] = GetFrameChecksum(s_ucaSendFrameBuffer,stFrame->ucLength+2);
	s_ucaSendFrameBuffer[ucLength++] = stFrame->ucTail;
	
	
	/*! 根据数据帧的目的地， 通过相应的串口发送出去  */
	ucCanId = get_frame_can_id(stFrame);

	bsp_can_send_bytes(ucCanId, s_ucaSendFrameBuffer, ucLength);

	return ret;
}


/*******************************************************************************
* 名称: ComSendFrameMain
* 功能: 发送数据帧任务
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
frame_info_t s_stSendFrameTemp = {0};
void ComSendFrameMain(void)
{
	/*! 发送队列中有数据数据帧，发送到PC 或 Slave */
	if(!FRAME_IsQueueEmpty(&stSendFrameQueue))
	{
		/*! 取出发送数据帧 */
		 FRAME_OutQueue(&stSendFrameQueue, &s_stSendFrameTemp);
		
		OSSchedLock();
		/*! 发送数据帧 */
		SendFrameToPorts(&s_stSendFrameTemp);
		OSSchedUnlock();
	}

}



