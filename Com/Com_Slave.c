/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "Com_Slave.h"
#include "Util_ringbuffer.h"
#include "Util_Queue.h"
#include "Com_Dispatcher.h"


/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
#define        		SLAVE_COM_RING_BUGGER_NUM                   1000
static uint8_t  s_ucaComRingBufer[SLAVE_COM_RING_BUGGER_NUM]    = {0};
static com_ring_buffer_t  s_stComSlaveBuffer   = {0, 0, s_ucaComRingBufer};

static uint8_t s_ucSlaveReceState       = 0;
static uint16_t s_ucSlaveReceConuter    = 0;
static uint16_t s_ucSlaveReceNum        = 0;
static uint8_t  s_ucaSlaveReceBuffer[200] = {0};
	
static frame_info_t s_stSlaveReceFrame     = {0};


extern uint8_t ProcessSlaveResultFrame(frame_info_t *stFrame);

/* 函数原型 -----------------------------------------------*/

/* 本地函数 ----------------------------------------------------------------*/

/*******************************************************************************
* 名称: ProcessReceSlaveFrame
* 功能:  接受处理下位机命令帧，分发到自动进样模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void ProcessReceSlaveResult(frame_info_t *stFrame)
{
    switch(stFrame->ucSender)
	{
        /*! 发送到轨道单元 */
		case SAMP_RAIL_UNIT_ID:
			ProcessSlaveResultFrame(stFrame);
            break;

		default: 
			
            break;
	}
}

/*******************************************************************************
* 名称: ProcessReceSlaveFrame
* 功能:  接受处理下位机命令帧，分发到自动进样模块
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t ProcessReceSlaveFrame(frame_info_t *stFrame)
{
	uint8_t ret = 0;
	
    if(MID_PART_UNIT_ID == stFrame->ucTarget)
    {
        switch(stFrame->ucFrametype)
        {
            case FRAME_REPLY:
                
                break;
            case FRAME_RESULT:
                ProcessReceSlaveResult(stFrame);
                break;
            default:
                break;
        }
    }
    else
    {
        /*! 下位机还回的数据帧
            1、  可能是周期指令的还回，ACK或结果
            2、  可能是上位机指令的还回，ACK或结果
        */       
        switch(stFrame->ucFrametype)
        {
            case FRAME_COMMAND:
                ret = ProcessCommandFrame(stFrame);
            break;
        
            case FRAME_REPLY:
                ret = ProcessReplyFrame(stFrame);
            break;

            case FRAME_RESULT:
                ret = ProcessResultFrame(stFrame);
            break;

            case FRAME_DATA:
                ret = ProcessDataFrame(stFrame);
            break;

            case FRAME_WARNING:
                ret = ProcessWarningFrame(stFrame);
            break;

            default: 
                /*! 错误的单元号 */
            break;
                
        }
    }
	return ret;
}


extern uint8_t GetFrameChecksum(uint8_t* ucaBuffer, uint16_t unLength);
/*******************************************************************************
* 名称: ReceOneSlaveFrameData
* 功能: 下位机帧的保存和处理
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void ReceOneSlaveFrameData(void)
{
	uint8_t ucFrameChecksum  = 0;
	uint8_t i = 0;

	ucFrameChecksum = s_ucaSlaveReceBuffer[s_ucSlaveReceConuter -2];

	if(ucFrameChecksum == GetFrameChecksum(s_ucaSlaveReceBuffer, s_ucSlaveReceConuter-2))
	{
		s_stSlaveReceFrame.ucHead = s_ucaSlaveReceBuffer[0];
		s_stSlaveReceFrame.ucLength = s_ucaSlaveReceBuffer[1];
		s_stSlaveReceFrame.ucMachine = s_ucaSlaveReceBuffer[2];
		s_stSlaveReceFrame.ucFrametype = s_ucaSlaveReceBuffer[3];
		s_stSlaveReceFrame.ucFrame = s_ucaSlaveReceBuffer[4];
		s_stSlaveReceFrame.ucSender = s_ucaSlaveReceBuffer[5];
		s_stSlaveReceFrame.ucTarget = s_ucaSlaveReceBuffer[6];
		s_stSlaveReceFrame.ucCmdId = s_ucaSlaveReceBuffer[7];
		
		for(i = 0; i < s_ucSlaveReceConuter - 6; i++)
		{
			s_stSlaveReceFrame.ucaPara[i] = s_ucaSlaveReceBuffer[8+i];
		}
		
		s_stSlaveReceFrame.ucCheckSum = s_ucaSlaveReceBuffer[s_ucSlaveReceConuter-2];
		s_stSlaveReceFrame.ucTail = s_ucaSlaveReceBuffer[s_ucSlaveReceConuter-1];
		
		/*! 处理和分发通信帧 */
		ProcessReceSlaveFrame(&s_stSlaveReceFrame);
	}
	else
	{
		/*! 校验码错误 */
	
	}
}


/*******************************************************************************
* 名称: ComFramePrase
* 功能: 通信帧的解析, 发送下位机数据到处理
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void ComFrameSlave(void)
{
	uint8_t ucTempData = 0;
	
	/*! 接受数据 */
	while(s_stComSlaveBuffer.unComringBuferReadPtr != s_stComSlaveBuffer.unComringBuferWritePtr)
	{
		/*! 接受数据解析 */
		ucTempData = s_stComSlaveBuffer.pucComringBufer[s_stComSlaveBuffer.unComringBuferReadPtr];
		s_stComSlaveBuffer.unComringBuferReadPtr++;
		
		if(s_stComSlaveBuffer.unComringBuferReadPtr == SLAVE_COM_RING_BUGGER_NUM)
		{
			s_stComSlaveBuffer.unComringBuferReadPtr = 0;
		}
		
		/*! 判断和接受数据头 */
		if(s_ucSlaveReceState == 0 && ucTempData == 0xFA)
		{
			s_ucSlaveReceState = 1;
			s_ucSlaveReceConuter = 0;
			s_ucaSlaveReceBuffer[s_ucSlaveReceConuter++] = ucTempData;
		}
		else if(s_ucSlaveReceState == 1)
		{
			/*! 判断和接受数据头 */
			if(s_ucSlaveReceConuter == 1)
			{
				s_ucSlaveReceNum = ucTempData; 
			}
			s_ucaSlaveReceBuffer[s_ucSlaveReceConuter++] = ucTempData; 
			
			/*! 接受完成一帧数据 */
			if(s_ucSlaveReceConuter == s_ucSlaveReceNum +4)
			{
				ReceOneSlaveFrameData();
                s_ucSlaveReceConuter = 0;
				s_ucSlaveReceState   = 0;
			}
		}	
	}
}

/**********************************************************************
* 函数名称：COM_IRQHandler
* 功能描述：串口接收中断
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/	
void SLAVE_COM_IRQHandler(void)
{
	if(SET == USART_GetITStatus(SLAVE_USART, USART_IT_RXNE))
	{
		s_stComSlaveBuffer.pucComringBufer[s_stComSlaveBuffer.unComringBuferWritePtr++] = USART_ReceiveData(SLAVE_USART);
		if ( s_stComSlaveBuffer.unComringBuferWritePtr == SLAVE_COM_RING_BUGGER_NUM )
		{
			s_stComSlaveBuffer.unComringBuferWritePtr = 0;		
		}
		USART_ClearITPendingBit(SLAVE_USART, USART_IT_RXNE); 
	}    
}


