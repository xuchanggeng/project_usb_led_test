/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "Com_Prase.h"
#include "Util_ringbuffer.h"
#include "Util_Queue.h"
#include "Com_FrameMake.h"
#include "bsp_can_msp.h"
#include "bsp_usart.h"
#include "cmd_process_task.h"





/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
#define 						COM_RING_BUFFER_SIZE					512
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
static uint8_t      s_ucReceUsartHeadFlag   = 0;
static uint16_t     s_ucReceUsartCounter    = 0; 
static uint16_t     s_ucReceUsartFrameNum   = 0; 
static uint8_t      s_ucaReceUsartBuffer[100] = {0};
static uint8_t      s_ucaSendUsartBuffer[100] = {0};
static frame_info_t stReceFrameInfo  = {0};
frame_queue_t       stReceFrameQueue = {0};								//接受PC数据帧队列
frame_queue_t       stSendFrameQueue = {0};								//发送到PC数据帧队列

static uint8_t      s_ucaRingBuffer[COM_RING_BUFFER_SIZE]   = {0};
ring_buffer_t		stHostComRingBuffer = {0, 0, COM_RING_BUFFER_SIZE, s_ucaRingBuffer};



/* 函数原型 -----------------------------------------------*/

/* 本地函数 ----------------------------------------------------------------*/

//void Send

/*******************************************************************************
* 名称: ComFramePrase
* 功能: 通信帧的解析
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t GetFrameChecksum(uint8_t*  ucaBuffer, uint16_t unLength)
{
	uint16_t i = 0;
	uint8_t sum =0;
	
	for(i = 0; i < unLength; i++)
	{
		sum = sum + ucaBuffer[i];
	}

	return sum;
}



/*******************************************************************************
* 名称: SaveOneFrameData
* 功能: 数据放入上位机接受队列
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
	// uint8_t ucBit28;						 //固定 0，1位
	// uint16_t ucTargetID;					  //目标设备号,9位
	// uint8_t ucBit18;						 //非广播指令时固定 0，1位
	// uint16_t ucSourceID;                 	 //源设备号;9位
	// uint8_t ucC1C0;                 	     //控制字；2位
	// uint8_t ucCMD_RegAddr;                	 //指令/寄存器地址 7位;
	// uint8_t ucaData[8];						 // CAN数据
	// uint8_t ucDataLen;						 // CAN数据长度

void SaveOneFrameData(void)
{
	uint8_t ucFrameChecksum  = 0;
	uint8_t i = 0;
	uint8_t  ucTempData = 0;
//    uint8_t ret = 0;
    
	ucFrameChecksum = s_ucaReceUsartBuffer[s_ucReceUsartCounter-2];

	// if(ucFrameChecksum == GetFrameChecksum(s_ucaReceUsartBuffer, s_ucReceUsartCounter-2))
	{
		ucTempData = s_ucaReceUsartBuffer[0];                         //抛弃
		stReceFrameInfo.ucDataLen     = s_ucaReceUsartBuffer[1];
		stReceFrameInfo.ucTargetID    = s_ucaReceUsartBuffer[2];
		stReceFrameInfo.ucBit18       = s_ucaReceUsartBuffer[3];
		stReceFrameInfo.ucSourceID    = s_ucaReceUsartBuffer[4];
		stReceFrameInfo.ucC1C0        = s_ucaReceUsartBuffer[5];
		stReceFrameInfo.ucCMD_RegAddr = s_ucaReceUsartBuffer[6];

		for(i = 0; i < stReceFrameInfo.ucDataLen - 7; i++)
		{
			stReceFrameInfo.ucaData[i] = s_ucaReceUsartBuffer[7+i];
		}
		
		ucTempData  = s_ucaReceUsartBuffer[s_ucReceUsartCounter-2];     //抛弃
		ucTempData  = s_ucaReceUsartBuffer[s_ucReceUsartCounter-1];     //抛弃
		
		// if(!FRAME_IsQueueFull(&stReceFrameQueue))
		// {
		// 	/*! 放入队列 */
		// 	FRAME_InQueue(&stReceFrameQueue, &stReceFrameInfo);
		// }
		// else
		// {
		// 	/*! 队列满 */
		// }

		stReceFrameInfo.ucDataLen = stReceFrameInfo.ucDataLen - 7;
        
        /*! 发送到命令执行模块 */
        if(stReceFrameInfo.ucTargetID == CMD_MID_PART_UNIT_CAN_ID)
        {
            /*! 发给中位机的指令 */
            start_cmd_process_action(&stReceFrameInfo, 0);
        }
        else
        {
            /*! 直接通过CAN发送出去 */
//            bsp_can_send_frame(&stReceFrameInfo);
        }
		


        //发送应答
       // SendReply(&stReceFrameInfo, 0);
	}

	
	
}

/*******************************************************************************
* 名称: SaveOneFrameData
* 功能: 数据放入上位机接受队列
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void send_frame_to_host(frame_info_t *stFrame)
{
	uint8_t ucFrameChecksum  = 0;
	uint8_t i = 0;
	uint8_t  ucTempData = 0;
	uint8_t  ucDataLen  = 0;
//    uint8_t ret = 0;

	/*! 组装成主机帧的格式，发送出去 */
	s_ucaSendUsartBuffer[0] =   0xFA                                   ;            
	s_ucaSendUsartBuffer[1] =   stFrame->ucDataLen +  7                ;
	s_ucaSendUsartBuffer[2] =   stFrame->ucTargetID                    ;
	s_ucaSendUsartBuffer[3] =   stFrame->ucBit18                       ;
	s_ucaSendUsartBuffer[4] =   stFrame->ucSourceID                    ;
	s_ucaSendUsartBuffer[5] =   stFrame->ucC1C0                        ;
	s_ucaSendUsartBuffer[6] =   stFrame->ucCMD_RegAddr                 ;

	for(i = 0; i < stFrame->ucDataLen ; i++)
	{
		 s_ucaSendUsartBuffer[7+i] = stFrame->ucaData[i] ;
	}
	
	s_ucaSendUsartBuffer[stFrame->ucDataLen + 7] = GetFrameChecksum(s_ucaSendUsartBuffer, stFrame->ucDataLen + 7) ;    //校验码
	s_ucaSendUsartBuffer[stFrame->ucDataLen + 8] = 0xFB ;     //0xFB
		


	/*! 直接通过USART发送出去, */
	bsp_usart_send_string(HOST_USART, s_ucaSendUsartBuffer, stFrame->ucDataLen + 9);
	

}





/*******************************************************************************
* 名称: com_frame_prase
* 功能: 通信帧的解析
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void host_com_frame_prase(uint8_t ucRecvData)
{
	uint8_t ucTempData = 0;
	
	ucTempData = ucRecvData;

	/*! 数据帧的头接受 */
	if(ucTempData == 0xFA && s_ucReceUsartHeadFlag == 0)
	{
		s_ucReceUsartHeadFlag = 1;
		s_ucReceUsartCounter = 0;
		s_ucaReceUsartBuffer[s_ucReceUsartCounter++] = ucTempData;
	}
	else if(s_ucReceUsartHeadFlag == 1)
	{
		if(s_ucReceUsartCounter == 1)
		{
			/*! 接受长度 */
			s_ucReceUsartFrameNum = ucTempData;
		}
		
		s_ucaReceUsartBuffer[s_ucReceUsartCounter++] = ucTempData;

		/*! 接受完成一帧数据 */
		if(s_ucReceUsartCounter == s_ucReceUsartFrameNum + 2)
		{
			SaveOneFrameData();
			s_ucReceUsartCounter = 0;
			s_ucReceUsartHeadFlag = 0;
		}

	}

}


/*******************************************************************************
* Function Name  : com_can_recv_main
* Description    : CAN接受数据
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void com_can_recv_main(void)
{
	unsigned char ucTempData = 0;
	unsigned short int* unpCanWrite;
	unsigned short int* unpCanRead;
	unsigned char *ucpCanBuffer;

//	unpCanWrite = retrieve_can_recvbuf_write_index();
//	unpCanRead  = retrieve_can_recvbuf_read_index();
//	ucpCanBuffer = retrieve_can_receive_buffer();
	
	while(*unpCanRead != *unpCanWrite)
	{
		ucTempData = ucpCanBuffer[*unpCanRead];
		(*unpCanRead)++;
		if(*unpCanRead >= CAN_BUF_SIZE)
		{
		  *unpCanRead = 0;
		}
		
		/*! 发送到帧解析函数 */
		host_com_frame_prase(ucTempData);
	}

}


/*******************************************************************************
* 名称: SaveOneFrameData
* 功能: 数据放入上位机接受队列
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void host_com_recv_main(void)
{
	uint8_t ucTempData = 0;

	/*! 环形缓冲区数据不为空 */
	while(is_not_ringbuffer_empty(&stHostComRingBuffer))
	{
		/*! 环形缓冲区数据取出数据 */
		ringbuffer_read(&stHostComRingBuffer, &ucTempData);
		/*! 环形缓冲区数据取出数据 */
		host_com_frame_prase(ucTempData);
	}
}


/*******************************************************************************
* 名称: HOST_COM_IRQHandler
* 功能: 通信帧的解析
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void HOST_COM_IRQHandler(void)
{
	uint8_t ucTempData = 0;
	
	if(USART_GetITStatus(HOST_USART, USART_IT_RXNE) != RESET)
	{
		ucTempData = USART_ReceiveData(HOST_USART);
		
		/*! 主串口得到数据就放到环形缓冲区 */
		ringbuffer_write(&stHostComRingBuffer, ucTempData);
		
		USART_ClearITPendingBit(HOST_USART, USART_IT_RXNE);
	}

	HOST_USART->DR;
	HOST_USART->SR;

}










