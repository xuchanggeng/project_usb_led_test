/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "Com_Dispatcher.h"
#include "confdef.h"
#include "Util_Queue.h"
#include "bsp_usart.h"

/* 本地类型 --------------------------------------------------------------*/

/* 本地常量 --------------------------------------------------------------*/

/* 本地宏定义 ------------------------------------------------------------*/

/* 本地变量 ---------------------------------------------------------------*/
/* 函数原型 -----------------------------------------------*/


/* 本地函数 ----------------------------------------------------------------*/

//	uint8_t ucLength;
//	uint8_t ucTarget;                 //MODULE_ID targetId;
//	uint8_t ucCmdId;
//	uint8_t ucaPara[MAX_PARA_NUM];
//	uint8_t ucCheckSum;
//	uint8_t ucTail;




uint8_t FramePackingCheckSum(frame_info_t* frame)
{
	uint8_t ucCheckSum = 0;
	uint8_t ucIndex = 0;
	
	ucCheckSum = ucCheckSum + frame->ucHead;
	ucCheckSum = ucCheckSum + frame->ucLength;
	ucCheckSum = ucCheckSum + frame->ucMachine;
	ucCheckSum = ucCheckSum + frame->ucFrametype;
	ucCheckSum = ucCheckSum + frame->ucFrame;
	ucCheckSum = ucCheckSum + frame->ucSender;
	ucCheckSum = ucCheckSum + frame->ucTarget;
	ucCheckSum = ucCheckSum + frame->ucCmdId;
	
	for(ucIndex = 0; ucIndex < (frame->ucLength - FRAME_FIX_DATALEN_SIZE); ucIndex++)
	{
		ucCheckSum = ucCheckSum + frame->ucaPara[ucIndex];
	}

	return ucCheckSum;
}



frame_info_t FrameMakeReply(const frame_info_t* frame, uint8_t ucAck )
{
    frame_info_t replyFrame;

    replyFrame.ucHead      = FRAME_HEAD;
    replyFrame.ucMachine   = MACHINE_ID;
    replyFrame.ucFrametype = FRAME_REPLY;
    replyFrame.ucFrame = frame->ucFrame;
    replyFrame.ucSender = frame->ucTarget;
    replyFrame.ucTarget = frame->ucSender;
    replyFrame.ucCmdId = frame->ucCmdId;
    replyFrame.ucaPara[0] = frame->ucaPara[0];
    replyFrame.ucaPara[1] = ucAck;
    replyFrame.ucLength = 2+6;
	
	replyFrame.ucCheckSum = FramePackingCheckSum(&replyFrame);
	replyFrame.ucTail     = FRAME_TAIL;
	
    return replyFrame;
}


frame_info_t FrameMakeResult(const frame_info_t* frame, uint8_t ucResult, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr )
{
    frame_info_t resultFrame;

    resultFrame.ucHead      = FRAME_HEAD;
    resultFrame.ucMachine   = MACHINE_ID;
    resultFrame.ucFrametype = FRAME_RESULT;
    resultFrame.ucFrame  = frame->ucFrame;
    resultFrame.ucSender = frame->ucTarget;
    resultFrame.ucTarget = frame->ucSender;
    resultFrame.ucCmdId  = frame->ucCmdId;

    resultFrame.ucaPara[0] = frame->ucaPara[0];
    resultFrame.ucaPara[1] = ucResult;//执行结果
    resultFrame.ucaPara[2] = 1;//错误模块个数
    resultFrame.ucaPara[3] = ucUnit;
	resultFrame.ucaPara[4] = frame->ucCmdId;
    resultFrame.ucaPara[5] = frame->ucaPara[0];
    resultFrame.ucaPara[6] = ucMotor;
    resultFrame.ucaPara[7] = ucErr;
	resultFrame.ucLength = 6 + 8;
	
	resultFrame.ucCheckSum = FramePackingCheckSum(&resultFrame);
	resultFrame.ucTail     = FRAME_TAIL;

    return resultFrame;

}



frame_info_t FrameMakeData(const frame_info_t* frame, uint8_t* ucData, uint32_t unDataLen)
{
    frame_info_t dataFrame;
    uint32_t i = 0;

    dataFrame.ucHead      = FRAME_HEAD;
    dataFrame.ucMachine   = MACHINE_ID;
    dataFrame.ucFrametype = FRAME_DATA;
    dataFrame.ucFrame  = frame->ucFrame;
    dataFrame.ucSender = frame->ucTarget;
    dataFrame.ucTarget = frame->ucSender;
    dataFrame.ucCmdId  = frame->ucCmdId;
	
    for(i=0;i<unDataLen;i++)
	{
		dataFrame.ucaPara[i] = ucData[i];
	}
	
	dataFrame.ucLength = unDataLen + 6;
	dataFrame.ucCheckSum = FramePackingCheckSum(&dataFrame);
	dataFrame.ucTail     = FRAME_TAIL;

    return dataFrame;
}

/*******************************************************************************
* 名称: FrameMakeWarning
* 功能: 根据警告信息，产生警告信息帧
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
frame_info_t FrameMakeWarning(const frame_info_t* frame, uint8_t ucResult, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr)
{
    frame_info_t warningFrame;

    warningFrame.ucHead      = FRAME_HEAD;
    warningFrame.ucMachine   = MACHINE_ID;
    warningFrame.ucFrametype = FRAME_WARNING;
    warningFrame.ucFrame  = frame->ucFrame;
    warningFrame.ucSender = frame->ucTarget;
    warningFrame.ucTarget = frame->ucSender;
    warningFrame.ucCmdId  = frame->ucCmdId;
	
    warningFrame.ucaPara[0] = frame->ucaPara[0];
    warningFrame.ucaPara[1] = ucResult;
	warningFrame.ucaPara[2] = ucUnit;
	warningFrame.ucaPara[3] = ucMotor;
	warningFrame.ucaPara[4] = ucErr;
	warningFrame.ucLength = 5 + 6;
	
	warningFrame.ucCheckSum = FramePackingCheckSum(&warningFrame);
	warningFrame.ucTail     = FRAME_TAIL;
	
    return warningFrame;

}




/*******************************************************************************
* 名称: SendReply
* 功能: 发送命令Ack
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SendReply(const frame_info_t* stCmdframe, uint8_t ucAck)
{
    //根据接收结果，创建一个应答帧
    frame_info_t replyFrame = FrameMakeReply(stCmdframe, ucAck);

    //将应答帧放入发送缓冲区
    ProcessReplyFrame(&replyFrame);
    
}

/*******************************************************************************
* 名称: SendResult
* 功能: 发送命令结果帧
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SendResult(const frame_info_t* cmd, uint8_t ucResult, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr)
{
	frame_info_t resultFrame = FrameMakeResult(cmd, ucResult, ucUnit, ucMotor, ucErr);

	ProcessResultFrame(&resultFrame);
}

/*******************************************************************************
* 名称: SendData
* 功能: 发送数据信息帧
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SendData(const frame_info_t* cmd, uint8_t* ucData, uint32_t unDataLen)
{
	frame_info_t dataFrame;
	
	if (unDataLen < 1 && unDataLen > MAX_PARA_NUM )
		return ;
	
	dataFrame = FrameMakeData(cmd, ucData, unDataLen);
	
	ProcessDataFrame(&dataFrame);
}


/*******************************************************************************
* 名称: SendWarning
* 功能: 发送警告信息帧
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SendWarning(const frame_info_t* cmd, uint8_t ucWarning, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr)
{
	frame_info_t warningFrame = FrameMakeWarning(cmd, ucWarning, ucUnit, ucMotor, ucErr);

	ProcessWarningFrame(&warningFrame);
}








