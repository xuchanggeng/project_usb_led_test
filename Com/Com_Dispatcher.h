/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V1.00.1
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef _COM_DISPATCHER_H
#define _COM_DISPATCHER_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
uint8_t ComFrameDispatcher(void);
uint8_t AddFrameToSendQueue(frame_info_t * stFrame);
uint8_t SendFrameToPorts(frame_info_t * stFrame);

uint8_t ProcessCommandFrame(frame_info_t*  stFrame);
uint8_t ProcessReplyFrame(frame_info_t*  stFrame);
uint8_t ProcessResultFrame(frame_info_t*  stFrame);
uint8_t ProcessDataFrame(frame_info_t*  stFrame);
uint8_t ProcessWarningFrame(frame_info_t*  stFrame);
void ComSendFrameMain(void);

#endif /* _COM_DISPATCHER_H */

