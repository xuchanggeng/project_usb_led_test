/******************** (C) COPYRIGHT 2018 XuChangGeng ******************************
* File Name          : frameinfo.h
* Author             : XuChangGeng
* Version            : V1.0
* Date               : 28/06/2018
* Description        : use firmware lib version: V2.0.1 06/13/2008
********************************************************************************
* 
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FRAME_H
#define __FRAME_H
#include "stm32f10x.h"
//#include "confdef.h"


#define 			MAX_PARA_NUM   			100
#define 			FRAME_HEAD 				0xFA					//帧头
#define				FRAME_TAIL 				0xFB					//帧尾
#define 			MACHINE_ID 				0x01					//机器类型

#define 			FRAME_HEAD_SIZE 			0x02					//帧头长度，包括帧头，帧长度
#define 			FRAME_FIX_DATALEN_SIZE 		0x06					//帧头长度，包括机器类型，命令ID
#define 			FRAME_TAIL_SIZE 			0x02					//帧尾长度，包括校验和和帧尾

//0-3 4-7 8-11 12-15 16-19 20-23 24-27 28-31
#define             BIT28_POS				    (28)
#define             TARGET_ID_POS				(19)
#define             BIT18_POS			     	(18)
#define             SOURCE_ID__POS			   	(9)
#define             C1_C0_POS			     	(7)
#define             CMD_REG_ADDR_POS			(0)

#define             BIT28_MASK				    (0x10000000)
#define             TARGET_ID_MASK				(0x0FF80000)
#define             BIT18_MASK			     	(0x00040000)
#define             SOURCE_ID_MASK			    (0x0003FE00)
#define             C1_C0_MASK			     	(0x00000180)
#define             CMD_REG_ADDR_MASK			(0x0000007F)


typedef enum _tag_action_state_e
{
	UNIT_ACTION_IDLE   = 0,
	UNIT_ACTION_START    ,
	UNIT_ACTION_FINISH   ,
	UNIT_ACTION_ERROR    ,
} action_state_e;


/*!< 各种帧类型  */
typedef enum _tag_frame_type_t{
	FRAME_DEFAULT,
	FRAME_COMMAND = 0x01,				//命令帧
	FRAME_REPLY,						//命令应答帧
	FRAME_RESULT,						//命令结果帧
	FRAME_DATA,							//数据帧
	FRAME_WARNING,						//警告信息帧
	FRAME_REPORT,						//下位机单元状态报告帧
	FRAME_RETRY_CMD,					//重发命令
	FRAME_PARAM_CONF,					//参数配置帧
	FRAME_PART_STATUS,					//部件状态反馈帧
	FRAME_MESSAGE,						//内部定义消息帧
	FRAME_TOTAL
}frame_type_e;


/*!< 结果信息结构  */
__packed typedef struct _tag_frame_info_t{
	uint8_t ucBit28;						 //固定 0，1位
	uint16_t ucTargetID;					  //目标设备号,9位
	uint8_t ucBit18;						 //非广播指令时固定 0，1位
	uint16_t ucSourceID;                 	 //源设备号;9位
	uint8_t ucC1C0;                 	     //控制字；2位
	uint8_t ucCMD_RegAddr;                	 //指令/寄存器地址 7位;
	uint8_t ucaData[8];						 // CAN数据
	uint8_t ucDataLen;						 // CAN数据长度
} frame_info_t;

// __packed typedef struct _tag_host_frame_info_t{
// 	uint8_t ucHead;
// 	uint8_t ucLength;						  //长度字段，为机器型号到参数结束，不包括长度字段本身
// 	uint8_t ucMachine;
// 	uint8_t ucFrametype;                 	  //FRAME_TYPE typeId;
// 	uint8_t ucFrame;
// 	uint8_t ucSender;                		  //MODULE_ID senderId;
// 	uint8_t ucTarget;                 		  //MODULE_ID targetId;
// 	uint8_t ucCmdId;
// 	uint8_t ucaPara[8];
// 	uint8_t ucCheckSum;
// 	uint8_t ucTail;
// } host_frame_info_t;




/*!< 结果信息结构  */
typedef struct _tag_cmd_result_t
{
	uint8_t  ucState;							//命令执行的状态, 周期控制器判断该命令是否执行完	
	uint8_t  moduleID;
	uint8_t  cmdID;
	uint8_t  para;
	uint16_t ucResult;
    
} cmd_result_t;



#endif /*__FRAME_H*/

