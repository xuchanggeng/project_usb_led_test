/**
  ******************************************************************************
  * @文件   Com_FrameMake.h
  * @作者   Xcg
  * @版本   V1.00.1
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef _COM_FRAME_MAKE_H
#define _COM_FRAME_MAKE_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */

void SendReply(const frame_info_t* stCmdframe, uint8_t ucAck);


void SendResult(const frame_info_t* cmd, uint8_t ucResult, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr);


void SendData(const frame_info_t* cmd, uint8_t* ucData, uint32_t unDataLen);


void SendWarning(const frame_info_t* cmd, uint8_t ucWarning, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr);


#endif /* _COM_FRAME_MAKE_H */

