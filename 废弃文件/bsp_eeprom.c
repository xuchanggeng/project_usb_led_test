
#ifndef __BSP_EEPROM_C__
#define __BSP_EEPROM_C__	

#include "includes.h"
#include "bsp_spi.h"
#include "bsp_eeprom.h"

#define FM25CL64_WREN_INST   0x06   //дʹ��
#define FM25CL64_WRDI_INST   0x04   //дʧ��
#define FM25CL64_RDSR_INST   0x05   //��״̬�Ĵ���
#define FM25CL64_WRSR_INST   0x01   //д״̬�Ĵ���
#define FM25CL64_WRITE_INST  0x02   //д����
#define FM25CL64_READ_INST   0x03   //������
#define FM25CL64_STATUS_REG  0x00   //״̬�Ĵ������ݡ����
#define FM25CL64_INIT_INST   0x09   //

//-------------------------------------
void bsp_eeprom_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(	FRAME_CS_GPIO_CLK, ENABLE );
    
    // cs
    GPIO_InitStructure.GPIO_Pin = FRAME_CS_GPIO_PIN;  
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(FRAME_CS_GPIO_PORT, &GPIO_InitStructure);
 	GPIO_SetBits(FRAME_CS_GPIO_PORT, FRAME_CS_GPIO_PIN);
    
    bsp_spi_init(FRAME_SPI);
}

//1. ��״̬
static UINT8 ReadState(void)
{
    UINT8 r;
    FM25CL64_CS_LOW();
    r=bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_RDSR_INST);
    r=bsp_spi_read_write_byte(FRAME_SPI,0);
    FM25CL64_CS_HIGH();
    return(r);
}

//2. ����״̬
static UINT8 Check_Start(void)
{
    UINT8 i=0,r=255;
    do 
    {
        r=ReadState();
        i++;
    } while ((r&0x01)&& i<254);
    if (i>=254)
        return 1;
  
    return 0;
}

//3. дʹ��
static void WriteEnable(void)
{
    FM25CL64_CS_LOW();
    bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_WREN_INST);
    FM25CL64_CS_HIGH();
}

//4. д״̬<----������ûʲô��
void WriteState(void)
{
    WriteEnable();
    FM25CL64_CS_LOW();
    bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_WRSR_INST);
    bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_STATUS_REG);
    FM25CL64_CS_HIGH();
    Check_Start();
}

//5. ���洢�������ӳ���
UINT8 Read_FM25CL64_Byte(UINT16 addre)
{
    UINT8 dat;
    FM25CL64_CS_LOW();
    bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_READ_INST);
    bsp_spi_read_write_byte(FRAME_SPI,(addre&0xFF00)>>8);
    dat=bsp_spi_read_write_byte(FRAME_SPI,(addre&0x00FF));
    dat=bsp_spi_read_write_byte(FRAME_SPI,0);
    FM25CL64_CS_HIGH();
    return(dat);
}

//6. ���洢�������ӳ���(���ֽ�)
UINT8 Read_FM25CL64_nByte(UINT16 addre,UINT8* buff,UINT16 len)
{
    UINT8 dat=0;
    UINT16 i = 0;
    FM25CL64_CS_LOW();
    bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_READ_INST);
    bsp_spi_read_write_byte(FRAME_SPI,(addre&0xFF00)>>8);
    bsp_spi_read_write_byte(FRAME_SPI,(addre&0x00FF));
    for (i=0;i<len;i++)
    {
        buff[i]=bsp_spi_read_write_byte(FRAME_SPI,0);
    }
    FM25CL64_CS_HIGH();
    return(dat);
}
//7. д�洢�������ӳ���
UINT8 Write_FM25CL64_Byte(UINT16 addre,UINT8 dat)
{
    WriteEnable();
    FM25CL64_CS_LOW();
    bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_WRITE_INST);
    bsp_spi_read_write_byte(FRAME_SPI,(addre&0xFF00)>>8);
    bsp_spi_read_write_byte(FRAME_SPI,(addre&0x00FF));
    bsp_spi_read_write_byte(FRAME_SPI,dat);
    FM25CL64_CS_HIGH();
    return(dat);
}


//8. д�洢�������ӳ���(���ֽ�)
void Write_FM25CL64_nByte(UINT16 addre,UINT8* buff,UINT16 len)
{
    UINT16 i = 0;
    WriteEnable();
    FM25CL64_CS_LOW();
    bsp_spi_read_write_byte(FRAME_SPI,FM25CL64_WRITE_INST);
    bsp_spi_read_write_byte(FRAME_SPI,(addre&0xFF00)>>8);
    bsp_spi_read_write_byte(FRAME_SPI,(addre&0x00FF));
    for (i=0;i<len;i++)
    {
        bsp_spi_read_write_byte(FRAME_SPI,buff[i]);
    }
    FM25CL64_CS_HIGH();
}

#endif
