/**********************************************************************
*
*	文件名称：ringbuffer.c
*	功能说明：环形缓冲区驱动代码
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

#ifndef __RINGBUFFER_H__
#define __RINGBUFFER_H__

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
typedef struct _tag_ring_buffer_t
{
    uint16_t ucWptr;                          	//环形缓冲区队写指针
    uint16_t ucRptr;                           	//环形缓冲区队读指针
    uint16_t ucSize;                           	//环形缓冲区队尾指针
    uint8_t *ucpRingBuffer;  					//环形缓冲区
}ring_buffer_t;


/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
uint8_t ringbuffer_write(ring_buffer_t *stRingBuffer ,uint8_t ucData);
uint8_t ringbuffer_read(ring_buffer_t *stRingBuffer, uint8_t* pucData);
uint8_t is_not_ringbuffer_empty(ring_buffer_t *stRingBuffer);
void ringbuffer_clear(ring_buffer_t *stRingBuffer);
void ringbuffer_init(ring_buffer_t *pstRingBuffer, uint8_t *ucpRingBuffer, uint16_t ucRingBufferSize);


#endif
