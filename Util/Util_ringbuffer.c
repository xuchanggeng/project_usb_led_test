/**********************************************************************
*
*	文件名称：ringbuffer.c
*	功能说明：环形缓冲区驱动代码
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

#include "Util_ringbuffer.h"
#include "userdef.h"


/**********************************************************************
* 函数名称：ringbuffer_clear
* 功能描述：清空环形缓冲区数据
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/	
void ringbuffer_clear(ring_buffer_t *stRingBuffer)
{
	memset(stRingBuffer->ucpRingBuffer, 0, stRingBuffer->ucSize);
	stRingBuffer->ucWptr = 0;	
	stRingBuffer->ucRptr = 0;	
}

/**********************************************************************
* 函数名称：ringbuffer_init
* 功能描述：初始化环形缓冲区
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其他说明：无
**********************************************************************/	
void ringbuffer_init(ring_buffer_t *pstRingBuffer, uint8_t *ucpRingBuffer, uint16_t ucRingBufferSize)
{
	pstRingBuffer->ucSize = ucRingBufferSize;
	pstRingBuffer->ucpRingBuffer = ucpRingBuffer;
	ringbuffer_clear(pstRingBuffer);
	
//	PRO_DEBUG(INIT_DEBUG,("ringbuffer init ok...\r\n"));
}

/**********************************************************************
* 函数名称：ringbuffer_write
* 功能描述：写数据到环形缓冲区中
* 输入参数：
*         : ucData：存入到缓冲区中的数据
* 输出参数：无
* 返 回 值：无
* 其它说明：
*         ：该函数未做缓冲区满报警机制，所以一定要注意
**********************************************************************/	
uint8_t ringbuffer_write(ring_buffer_t *stRingBuffer ,uint8_t ucData)
{
	/* 环形缓冲区满，则直接退出 */
	if( (stRingBuffer->ucWptr +1) ==  stRingBuffer->ucRptr)
	{
        return 0;		
	}
	
	stRingBuffer->ucpRingBuffer[stRingBuffer->ucWptr] = ucData;
	stRingBuffer->ucWptr++;
	stRingBuffer->ucWptr = stRingBuffer->ucWptr % stRingBuffer->ucSize;
	
	return 1;		
}




/**********************************************************************
* 函数名称：ringbuffer_read
* 功能描述：读取环形缓冲区数据
* 输入参数：
*         ： *pucData：读取数据指针
* 输出参数：无
* 返 回 值：
*         ：0：读取数据成功
*    	  ：1：读取数据失败
* 其它说明：无
**********************************************************************/	
uint8_t ringbuffer_read(ring_buffer_t *stRingBuffer, uint8_t* pucData)
{
	/* 环形缓冲区空，则直接退出 */
	if( (stRingBuffer->ucWptr) ==  stRingBuffer->ucRptr)
	{
        return 0;		
	}
	
	*pucData = stRingBuffer->ucpRingBuffer[stRingBuffer->ucRptr] ;
	stRingBuffer->ucRptr++;
	stRingBuffer->ucRptr = stRingBuffer->ucRptr % stRingBuffer->ucSize;
	
	return 1;

}

/**********************************************************************
* 函数名称：ringbuffer_not_empty
* 功能描述：判断环形缓冲区是否为空
* 输入参数：无
* 输出参数：无
* 返 回 值：
*         ：0：环形缓存区为空
*    	  ：1：环形缓存区非空
* 其它说明：无
**********************************************************************/	
uint8_t is_not_ringbuffer_empty(ring_buffer_t *stRingBuffer)
{
	if( stRingBuffer->ucWptr ==  stRingBuffer->ucRptr)
	{
		return 0;
	}
	
	return 1;
}




