/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V1.00.1
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef _COM_QUEUE_H
#define _COM_QUEUE_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
#define QueueMaxSize		5


typedef struct _tag_frame_queue_t
{
    frame_info_t astFrameQueue[QueueMaxSize];   //数据结构体
    uint16_t front;                          //队列头指针
    uint16_t rear;                           //队列尾指针

}frame_queue_t;



/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
u8 FRAME_IsQueueFull(frame_queue_t *Q);
u8 FRAME_IsQueueEmpty(frame_queue_t *Q);
u8 FRAME_OutQueue(frame_queue_t *Q, frame_info_t *Data);
u8 FRAME_InQueue(frame_queue_t *Q, frame_info_t *Data);




#endif /* _COM_QUEUE_H */

