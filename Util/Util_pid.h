/**
  ******************************************************************************
  * @文件   Util_pid.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __UTIL_PID_H
#define __UTIL_PID_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/

typedef struct __tag_pid_ctrl_t
{
	uint16_t unKp;
	uint16_t unKi;
	uint16_t unKd;
	
	int32_t lP_out;
	int32_t lI_out;
	int32_t lD_out;
	int32_t lTotal_out;
	int32_t lMax_output;
	int32_t lIntegralLimit;
	
	int32_t lErr;
	int32_t lLast_Err;
	
	int32_t lTarget;
	int32_t lCurrent;

}pid_ctrl_t;



/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
void pid_init(pid_ctrl_t *pstPid, uint16_t unKp, uint16_t unKi, uint16_t unKd, int32_t lTarget);
void pid_out_setting(pid_ctrl_t *pstPid, int32_t lMaxOut, int32_t lKiLimit);
void pid_set_target(pid_ctrl_t *pstPid, int32_t ulTarget);
void pid_calc(pid_ctrl_t  *pstPid );




/* 导出变量 ---------------------------------------------------------- */

#endif /* __UTIL_PID_H */

