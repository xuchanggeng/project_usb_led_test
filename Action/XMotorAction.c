/**
  ******************************************************************************
  * @文件   XMotorAction.c
  * @作者   Xcg
  * @版本   V1.01.1
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "XMotorAction.h"



/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
static motor_action_control_t  		s_stXMotorActionControl  = {0};

/* 外部变量导入 -----------------------------------------------------------*/

/* 函数原型 --------------------------------------------------------------*/
static void x_motor_action_finish(motor_action_cmd_t *stActionCmd);
static void x_motor_action_cmd_reset(void);
static void x_motor_action_cmd_self_check(void);
static void x_motor_action_cmd_move_step(void);
static void x_motor_action_cmd_move_pos(void);

/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 函数名称: start_cmd_x_motor_action_cmd
* 功能描述: 示例
* 输入参数:  无
* 输出参数:  无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void start_cmd_x_motor_action_cmd(motor_action_cmd_t *stActionCmd)
{
	/*! 输入参数范围判断 */
	if(stActionCmd->ucActionCmd >= MOTOR_ACTION_CMD_MAX)
	{
		return;
	}
	s_stXMotorActionControl.ucActionCmd = stActionCmd->ucActionCmd;
	s_stXMotorActionControl.unActionStep = 0;
	s_stXMotorActionControl.unActionTick = 0;
}

/*******************************************************************************
* 函数名称: start_cmd_x_motor_action_cmd
* 功能描述: 示例
* 输入参数:  无
* 输出参数:  无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void stop_x_motor_action(void)
{
	/*! 停止动作 */
	s_stXMotorActionControl.ucActionCmd = MOTOR_ACTION_CMD_NULL;
	s_stXMotorActionControl.unActionStep = 0;
	s_stXMotorActionControl.unActionTick = 0;
	
	/*! 停止电机 */
	
}


/*******************************************************************************
* 函数名称: x_motor_action_finish
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
static void x_motor_action_finish(motor_action_cmd_t *stActionCmd)
{
	s_stXMotorActionControl.ucActionCmd = MOTOR_ACTION_CMD_NULL;
	s_stXMotorActionControl.unActionStep = 0;
	s_stXMotorActionControl.unActionTick = 0;
}



/*******************************************************************************
* 函数名称: x_motor_action_cmd_reset
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
static void x_motor_action_cmd_reset(void)
{
	switch(s_stXMotorActionControl.unActionStep)
	{
		case 1:
			break;
	
		case 2:
			break;
		
		case 3:
			break;
		
		case 4:
			break;
		
		default:
			break;
	}
}

/*******************************************************************************
* 函数名称: x_motor_action_cmd_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
static void x_motor_action_cmd_self_check(void)
{
	switch(s_stXMotorActionControl.unActionStep)
	{
		case 1:
			break;
	
		case 2:
			break;
		
		case 3:
			break;
		
		case 4:
			break;
		
		default:
			break;
	}
}

/*******************************************************************************
* 函数名称: x_motor_action_cmd_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
static void x_motor_action_cmd_move_step(void)
{
	switch(s_stXMotorActionControl.unActionStep)
	{
		case 1:
			break;
	
		case 2:
			break;
		
		case 3:
			break;
		
		case 4:
			break;
		
		default:
			break;
	}
}

/*******************************************************************************
* 函数名称: x_motor_action_cmd_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
static void x_motor_action_cmd_move_pos(void)
{
	switch(s_stXMotorActionControl.unActionStep)
	{
		case 1:
			break;
	
		case 2:
			break;
		
		case 3:
			break;
		
		case 4:
			break;
		
		default:
			break;
	}
}




/*******************************************************************************
* 函数名称: template_module_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void x_motor_action_init(void)
{
	/*! 初始化x电机动作控制结构体 */
	memset(&s_stXMotorActionControl, 0, sizeof(s_stXMotorActionControl));

}

/*******************************************************************************
* 函数名称: x_motor_action_handle
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void x_motor_action_handle(void)
{
	switch(s_stXMotorActionControl.ucActionCmd)
	{
		case MOTOR_ACTION_CMD_RESET:
			x_motor_action_cmd_reset();
			break;
	
		case MOTOR_ACTION_CMD_SELF_CHECK:
			x_motor_action_cmd_self_check();
			break;
		
		case MOTOR_ACTION_CMD_MOVE_STEP:
			x_motor_action_cmd_move_step();
			break;
		
		case MOTOR_ACTION_CMD_MOVE_POS:
			x_motor_action_cmd_move_pos();
			break;
		
		default:
			break;
	}

}



