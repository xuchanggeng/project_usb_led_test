/**
  ******************************************************************************
  * @文件   MotorActionDefine.h
  * @作者   Xcg
  * @版本   V1.01.1
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __MOTOR_ACTION_DEFINE_H
#define __MOTOR_ACTION_DEFINE_H

/* 文件包含 ------------------------------------------------------------------*/
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
typedef struct _tag_motor_action_control_t
{
	uint8_t  ucActionCmd;
	uint16_t unActionStep;
	uint16_t unActionTick;
}motor_action_control_t;


typedef struct _tag_motor_action_cmd_t
{
	uint8_t ucActionCmd;
	uint8_t ucActionFlag;
}motor_action_cmd_t;

/* 导出常量 -----------------------------------------------------------*/
enum _tag_motor_action_define_e
{
	MOTOR_ACTION_CMD_NULL = 0,				/*! 无动作 */
	
	MOTOR_ACTION_CMD_RESET = 1,				/*! 复位动作 */
	MOTOR_ACTION_CMD_SELF_CHECK ,			/*! 自检动作 */
	MOTOR_ACTION_CMD_MOVE_STEP ,			/*! 走指定步数动动作 */
	MOTOR_ACTION_CMD_MOVE_POS  ,			/*! 走位置动作 */
	
	MOTOR_ACTION_CMD_MAX,			            /*! 最大动作 */
};


/* 导出函数 ---------------------------------------------------------- */
/* 导出变量 ---------------------------------------------------------- */

#endif /* __MOTOR_ACTION_DEFINE_H */

