
#ifndef __cmd_process_C__
#define __cmd_process_C__

#include "includes.h"
#include "pro_include.h"
#include "cmd_result.h"
#include "motor_port.h"
#include "motor_para.h"

_EXT_ OS_EVENT * DS_Sem;
extern tMtxState   	  MtxState[MTx_NUM];
extern motor_para_t   s_ucMotorOffsetPara;
/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
action_info_t  			  s_stSliceCardBoxActionInfo 		= {0};
static cmd_result_t       s_stCmdActionResult 		    	= {0};					//本地命令执行结果信息，


/* 函数原型 ---------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 名称: get_card_slice_box_action_state
* 功能:  获取卡条盒动作状态
* 形参:  
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t get_card_slice_box_action_state(void)
{
	return s_stSliceCardBoxActionInfo.ucUnitActionState ;
}


/*******************************************************************************
* 名称: start_card_slice_box_action
* 功能:  发送信号量，使卡条盒开始执行数据帧中对应的动作
* 形参:  	
			pstFrameInfo   数据帧
			pstFrameResult 结果数据
			ucISack        是否需要结果

* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t start_card_slice_box_action(frame_info_t *pstFrame, uint8_t ucISack)
{
	uint8_t  ret = 0;
	
	/*! 1 判断单元是否正在执行动作, 一个单元同一个时间只能执行一个动作*/
	if(s_stSliceCardBoxActionInfo.ucUnitActionState != UNIT_ACTION_IDLE)
	{
		ret = ERR_UNIT_ACT_EXE;
		return EXIT_FAILURE;
	}
	
	/*! 2 是否是本单元的指令*/
	if(pstFrame->ucTarget !=  CARD_SLICE_BOX_UNIT_ID)
	{
		ret = ERR_UNIT_NO_THIS_UNIT;
		return EXIT_FAILURE;
	}
	
	/*! 3  接受命令数据 */
	memset((uint8_t*)&s_stSliceCardBoxActionInfo, 0, sizeof(s_stSliceCardBoxActionInfo));
	s_stSliceCardBoxActionInfo.stFrameInfo = *pstFrame;
	s_stSliceCardBoxActionInfo.ucUnitActionState = UNIT_ACTION_START;
	s_stSliceCardBoxActionInfo.ucIsAck = ucISack;
	
	/*! 4 设置命令执行结果的初始状态 */
	memset((uint8_t*)&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
	
	/*! 5 发送信号量 */
	OSSemPost(DS_Sem);   
	
	return ret;
}



/*******************************************************************************
* 名称:  check_sampro_cmd_result
* 功能:  指令执行结果处理， 发送结果帧
* 形参:  
			stAction    单元动作信息结构体
			ucRet		单元的动作结果

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
void check_card_slice_box_cmd_result(action_info_t *stAction, uint8_t ucRet)
{
	/*! 电机编号的重新映射 */
	s_stSliceCardBoxActionInfo.ucErrMotor = get_redefine_motor_no(s_stSliceCardBoxActionInfo.ucErrMotor);
	
	/*! 1 设置命令处理结果 */
	if(ucRet == 0)
	{
		/*! 命令执行正确 */
		SetCmdFrameResult(&s_stSliceCardBoxActionInfo.stFrameInfo, &s_stCmdActionResult, 0, CARD_SLICE_BOX_UNIT_ID, 0, 0);
	}
	else
	{
		/*! 命令执行出错,命令执行结果格式: 1命令执行结果 2单元编号 3电机编号 4错误代码  */
		SetCmdFrameResult(&s_stSliceCardBoxActionInfo.stFrameInfo, &s_stCmdActionResult, 1, CARD_SLICE_BOX_UNIT_ID, s_stSliceCardBoxActionInfo.ucErrMotor, ucRet);
	}
	
	/*! 2 发送命令结果帧 到上位机 */
	if(s_stSliceCardBoxActionInfo.ucIsAck == 1)
	{
		SendResult(&s_stSliceCardBoxActionInfo.stFrameInfo, ucRet, CARD_SLICE_BOX_UNIT_ID, s_stSliceCardBoxActionInfo.ucErrMotor, ucRet);
	}

	/*! 3 设置单元的状态 */
	s_stSliceCardBoxActionInfo.ucUnitActionState = UNIT_ACTION_IDLE;
	
	if(ucRet)
	{
		PRO_DEBUG(ERR_DEBUG,("motor id%d, err id %d\r\n", s_stSliceCardBoxActionInfo.ucErrMotor, ucRet));
	}

}


/*******************************************************************************
* 名称:  cmd_card_slice_box_system
* 功能:  卡条盒单元系统指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_system  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
//	uint8_t ucDir = 0;
	
	(void)pucFrameData;
//	ucDir = pucFrameData[1];
	
	PRO_DEBUG(COMM_DEBUG,("cmd_card_slice_box_system\r\n"));
	
	ret = motor_move(INCU_LIFT_MT, 1000, eMtx_Dir_NEGA, 15, 0, 0);
	
//	ret = Mtx_Rst(CARD_SLICE_BOX_MT, 0, 0);
	
	/*! 直接还回0， 直接成功 */
	return ret;
}



/*******************************************************************************
* 名称:  cmd_card_slice_box_query
* 功能:  卡条盒单元系统指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_query  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t  ucaParaData[30] ;
	uint8_t	 ucParaLen  = 0;
	
	ucCmdType = pucFrameData[0];
	ucaParaData[0] = ucCmdType;
	(void)ucCmdType;
	
	switch(ucCmdType)
	{
		/*! 进卡电机的速度 */
		case 0:
			ucaParaData[1]  = MtxState[TIP_HEAD_BOX_MT].mMotorSpeed;
			ucParaLen = 2;
		break;
		
		/*! 进卡电机补偿步数 */
		case 1:
            //初始位
            ucaParaData[1] = s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[0]>>0x08;
            ucaParaData[2] = s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[0];
            //推卡位
            ucaParaData[3] = s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[1]>>0x08;
            ucaParaData[4] = s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[1];
			ucParaLen = 5;
		break;
				
		default:
		break;
	
	}
	
	/*! 用数据帧还回查询信息 */
	SendData(&s_stSliceCardBoxActionInfo.stFrameInfo, ucaParaData, ucParaLen);
	
	return ret;
}


/*******************************************************************************
* 名称:  cmd_card_slice_box_config
* 功能:  卡条盒单元系统指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_config  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucIndex  = 0;
	uint16_t unStep  = 0;
	
	ucCmdType = pucFrameData[0];
	
	switch(ucCmdType)
	{
		/*! 进卡电机的速度 */
		case 0:
			//SetSampleProbeSpeed(pucFrameData[1], pucFrameData[2],  pucFrameData[3],  pucFrameData[4]);
		break;
		
		/*! 进卡电机补偿步数 */
		case 1:
			for(ucIndex = 0; ucIndex < 2; ucIndex++)
			{
				unStep = (pucFrameData[ucIndex*2 + 1]<<8) + pucFrameData[ucIndex*2 + 2];
				Settip_head_boxMotorPosStep(ucIndex, unStep);
			}
		break;
		


				
		default:
		break;
	
	}
	
	return ret;
}

/*******************************************************************************
* 名称:  cmd_card_slice_box_rst
* 功能:  卡条盒单元复位指令指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_rst  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	
	ucCmdType = pucFrameData[0];
	
	switch(ucCmdType)
	{
		/*! 卡条盒单元复位 */
		case 1:
			/*! 卡条盒电机复位 */
			ret = motor_rst(CARD_SLICE_BOX_MT, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			}
			break;
		
		default:
		break;
	
	}
	
	return ret;
}



void em_slice_box_op (uint8_t ucstate1, uint8_t ucstate2, uint8_t ucstate3, uint8_t ucstate4)
{
	bsp_set_pv_state(E_PV_1, (FunctionalState)ucstate1);
	bsp_set_pv_state(E_PV_2, (FunctionalState)ucstate2);
	bsp_set_pv_state(E_PV_3, (FunctionalState)ucstate3);
	bsp_set_pv_state(E_PV_4, (FunctionalState)ucstate4);
}


/*******************************************************************************
* 名称:  cmd_card_slice_box_simple_op
* 功能:  卡条盒单元简单操作指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_simple_op  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	
	ucCmdType = pucFrameData[0];
	
	switch(ucCmdType)
	{
		/*! 推卡到位动作,推卡到装载台 */
		case 0:
			em_slice_box_op(pucFrameData[1], pucFrameData[2], pucFrameData[3], pucFrameData[4]);
		
			ret = motor_move_to_pos(CARD_SLICE_BOX_MT, REAL_POS_PUSH_IN_CARD_SLICE_BOX_MOTOR, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			}
		
			em_slice_box_op(ENABLE, ENABLE, ENABLE, ENABLE);
			
			/*! 复位动作 */
			ret = motor_rst(CARD_SLICE_BOX_MT, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			}
			em_slice_box_op(DISABLE, DISABLE, DISABLE, DISABLE);
			
			break;
		
			/*! 时序中有两个动作，用两条指令来区分，指令对中位机来区分 */
		case 1:
			em_slice_box_op(pucFrameData[1], pucFrameData[2], pucFrameData[3], pucFrameData[4]);
		
			ret = motor_move_to_pos(CARD_SLICE_BOX_MT, REAL_POS_PUSH_IN_CARD_SLICE_BOX_MOTOR, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			}
		
			em_slice_box_op(ENABLE, ENABLE, ENABLE, ENABLE);
			
			/*! 复位动作 */
			ret = motor_rst(CARD_SLICE_BOX_MT, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			}
			em_slice_box_op(DISABLE, DISABLE, DISABLE, DISABLE);
			
		break;			
			
		default:
		break;
	}
	
	return ret;
}

/*******************************************************************************
* 名称:  cmd_card_slice_box_move_to_pos
* 功能:  卡条盒单元运动到指定位置
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_move_to_pos  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	
	ucCmdType = pucFrameData[0];
	
	switch(ucCmdType)
	{
		/*! 运动到指定位置 */
		case 0:
			/*! 运动到复位位置 */
			ret = motor_rst(CARD_SLICE_BOX_MT, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			} 
			break;
			
		/*! 运动到推卡位 */
		case 1:
			em_slice_box_op(pucFrameData[1], pucFrameData[2], pucFrameData[3], pucFrameData[4]);
		
			ret = motor_move_to_pos(CARD_SLICE_BOX_MT, REAL_POS_PUSH_IN_CARD_SLICE_BOX_MOTOR, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			}
			break;
		
		default:
		break;
	
	}
	
	return ret;
}


/*******************************************************************************
* 名称:  cmd_card_slice_box_move_step
* 功能:  卡条盒单元运动步数指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_move_step(uint8_t * pucFrameData)
{
	uint8_t ret       = 0;
	uint8_t ucCmdType = 0;
	uint8_t  ucDir    = 0;
	uint16_t unStep   = 0;
	
	ucCmdType = pucFrameData[0];
	ucDir 	  = pucFrameData[1];
	unStep    = (pucFrameData[2]<<8) + pucFrameData[3];
	
	switch(ucCmdType)
	{
		/*! 卡条单元推片控制 */
		case 0:
			if(pucFrameData[2])
			{
				bsp_pv_open(pucFrameData[1]);
			}
			else
			{
				bsp_pv_close(pucFrameData[1]);
			}
		break;
	
		
		/*! 进卡电机运动指定步数 */
		case 1:
			ret = motor_move(CARD_SLICE_BOX_MT, unStep, (eMtxDir)ucDir, MtxState[CARD_SLICE_BOX_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stSliceCardBoxActionInfo.ucErrMotor = CARD_SLICE_BOX_MT;
				return ret;
			}	
		break;
			
		default:
		break;
		
	}
	
	return ret;
}


/*******************************************************************************
* 名称:  cmd_card_slice_box_io_state_update
* 功能:  卡条盒单元IO状态读取
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_card_slice_box_io_state_update  (uint8_t * pucFrameData)
{
	uint8_t ucRet = 0;
	uint8_t  ucCmdType  = 0;
	uint8_t  ucaParaData[30] ;
	uint8_t	 ucParaLen  = 0;
	
	ucCmdType      =  pucFrameData[0];
	ucaParaData[0] = ucCmdType;
	(void)ucCmdType;
	
	ucaParaData[1] = MtxFunList[CARD_SLICE_BOX_MT].mtResetRead(CARD_SLICE_BOX_MT);
	ucaParaData[2] = 1;
	ucaParaData[3] = 1;
	ucaParaData[4] = 1;
	ucaParaData[5] = 1;
	ucaParaData[6] = 1;
	ucParaLen   = 7;

	
	/*! 用数据帧还回查询信息 */
	SendData(&s_stSliceCardBoxActionInfo.stFrameInfo, ucaParaData, ucParaLen);
	
	return ucRet;
}


/*******************************************************************************
* 名称: card_slice_box_cmd_handle
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t card_slice_box_cmd_handle(void)
{
    uint8_t  ret = 0;
	uint8_t  *pucDataBuffer ;
	
	pucDataBuffer  = s_stSliceCardBoxActionInfo.stFrameInfo.ucaPara;
    
    switch (s_stSliceCardBoxActionInfo.stFrameInfo.ucCmdId)
    {
        case CMD_CARD_SLICE_BOX_SYSTEM:// 卡条盒单元系统指令 0x00
        {
			ret = cmd_card_slice_box_system(pucDataBuffer);
        }
		break;
		
        case CMD_CARD_SLICE_BOX_QUERY:// 卡条盒单元查询指令 0x01
        {
			ret = cmd_card_slice_box_query(pucDataBuffer);
        }
		break;
				
        case CMD_CARD_SLICE_BOX_CONFIG:// 卡条盒单元配置指令 0x02
        {
			ret = cmd_card_slice_box_config(pucDataBuffer);
        }
		break;		
				
		case CMD_CARD_SLICE_BOX_RST://卡条盒单元复位指令  0x03
        {
			ret = cmd_card_slice_box_rst(pucDataBuffer);
        }
		break;

		case CMD_CARD_SLICE_BOX_SIMPLE_OP://卡条盒单元简单操作  0x04
        {
			ret = cmd_card_slice_box_simple_op(pucDataBuffer);
        }
		break;		

		case CMD_CARD_SLICE_BOX_MOVE_TO_POS://卡条盒单元运动到指定位置  0x05
        {
			ret = cmd_card_slice_box_move_to_pos(pucDataBuffer);
        }
		break;		

		case CMD_CARD_SLICE_BOX_MOVE_STEP://卡条盒单元单个操作 0x06
        {
			ret = cmd_card_slice_box_move_step(pucDataBuffer);
        }
		break;		
		
		case CMD_CARD_SLICE_BOX_IO_STATE_UPDATE://卡条盒单元IO状态读取  0x07
        {
			ret = cmd_card_slice_box_io_state_update(pucDataBuffer);
        }
		break;		

		default:
		{
		}
		break;
		
	}
	
	return ret;
}


/*******************************************************************************
* 名称: card_slice_box_action_init
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
static void card_slice_box_action_init(void)
{
	/*! 1 复位变量 */
	memset(&s_stSliceCardBoxActionInfo, 0, sizeof(s_stSliceCardBoxActionInfo));
	memset(&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
	
}



/*******************************************************************************
* 名称: card_slice_box_task
* 功能:  卡条盒线程
* 形参:  	
* 返回: 无
* 说明: 无
********************************************************************************/
void card_slice_box_task(void *p_arg)
{
    OS_ERR err;
	UINT8 ret = ERR_NONE;
    
    (void)p_arg;
    
	/*! 本单元状态初始化 */
	card_slice_box_action_init();
	
//	em_slice_box_op(ENABLE, ENABLE, ENABLE, ENABLE);

    while (1)
    {	
		/*! 等待信号量触发 */
        OSSemPend(DS_Sem, 0, &err);
		
		/*!< 动作没有开始，是发给本单元的指令 */
		if(s_stSliceCardBoxActionInfo.ucActionFinishFlag == 0 && s_stSliceCardBoxActionInfo.stFrameInfo.ucTarget == CARD_SLICE_BOX_UNIT_ID)
		{
			ret = card_slice_box_cmd_handle();
			
			/*! 当动作执行完成，上报命令执行结果 */
            check_card_slice_box_cmd_result(&s_stSliceCardBoxActionInfo, ret);
		}		

    }
	
}



#endif /*! __cmd_process_C__ */
