
#ifndef __REALTIME_TASK_C__
#define __REALTIME_TASK_C__

#include <includes.h>
#include "bsp_led.h"
#include "bsp_pump_valve.h"
#include "mid_task.h"
#include "load_platform_task.h"
#include "Com_Prase.h"
#include "Com_Dispatcher.h"
#include "EXFunctionIO.h"
#include "MaintainMain.h"
#include "bsp_can_msp.h"
#include "ctrl_temp.h"
#include "multi_timer.h"
#include "bsp_ds28ec20.h"
#include "Util_Queue.h"
#include "cmd_process_task.h"
#include "three_led.h"

frame_queue_t s_stRecevFrameQueue = {0};
frame_info_t  s_stTempFrame       = {0};

static void test_ds28ec20_read_memory(void);


void timer_callback(void *vp)
{
	PRO_DEBUG(COMM_DEBUG,("timer_callback %d\r\n", 1));
}

/*******************************************************************************
* 名称: recev_frame_dispatch_loop
* 功能: 数据帧检测和分发
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void  recev_frame_dispatch_loop(void)
{
	/*! 检测接收队列是否有数据 */
	if(FRAME_IsQueueEmpty(&s_stRecevFrameQueue))
	{
		FRAME_OutQueue(&s_stRecevFrameQueue, &s_stTempFrame);

		/*! 发送到命令PC */
        send_frame_to_host(&s_stTempFrame);
		
	}

}


/*******************************************************************************
* 名称: system_state_update
* 功能: 系统状态更新
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void  system_state_update(void)
{
	/*! 主串口数据接受 */
	host_com_recv_main();

	/*! 主串口数据发送 */
	// ComSendFrameMain();

	/*!  */
	recev_frame_dispatch_loop();

	

}




/*******************************************************************************
* 名称: debug_printf_motor_io
* 功能: 系统状态输出
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void debug_printf_motor_io(void)
{
	uint8_t ucaData[7] = {0};
	uint8_t index = 0;

	for(index = 0; index < 7; index++)
	{
		ucaData[index] = index;
	}
//	bsp_can_send_bytes(0x007, ucaData, 7);
}





/*******************************************************************************
* 名称: SaveOneFrameData
* 功能: 数据放入上位机接受队列
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void realtime_task(void *p_arg)
{
	UINT16 unLedTime = 0;
	
	(void)p_arg;
		  
//	timer_init_start(&testTimer, timer_callback, 2000, 10, 0, NULL);
	
	while (1)
	{	
		OSTimeDly(1);
		
		/*! 【1】系统状态更新 */
		system_state_update();
		
		/*! 【2】单片机运行指示 */
		unLedTime++;
		if (500 == unLedTime)
		{
			unLedTime = 0;
			bsp_led_toggle();
			
		}
		

		/*! 【6】软件定时器 */
//		timer_loop();
//		timer_ticks();
	}   
}

#endif
