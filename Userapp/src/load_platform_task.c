
#ifndef __load_platform_C__
#define __load_platform_C__

#include "includes.h"
#include "pro_include.h"
#include "load_platform_task.h"
#include "cmd_result.h"
#include "motor_port.h"
#include "motor_para.h"

_EXT_ OS_EVENT * SP_Sem;
extern tMtxState      MtxState[MTx_NUM];
extern motor_para_t   s_ucMotorOffsetPara;
/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
action_info_t  			  s_stLoadPlatformActionInfo 		= {0};
static cmd_result_t       s_stCmdActionResult 		    = {0};					//本地命令执行结果信息，


/* 函数原型 ---------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 名称: get_load_platform_action_state
* 功能:  获取样本针动作状态
* 形参:  
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t get_load_platform_action_state(void)
{
	return s_stLoadPlatformActionInfo.ucUnitActionState ;
}


/*******************************************************************************
* 名称: start_load_platform_action
* 功能:  发送信号量，使样本针开始执行数据帧中对应的动作
* 形参:  	
			pstFrameInfo   数据帧
			pstFrameResult 结果数据
			ucISack        是否需要结果

* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t start_load_platform_action(frame_info_t *pstFrame, uint8_t ucISack)
{
	uint8_t  ret = 0;
	
	/*! 1 判断单元是否正在执行动作, 一个单元同一个时间只能执行一个动作*/
	if(s_stLoadPlatformActionInfo.ucUnitActionState != UNIT_ACTION_IDLE)
	{
		ret = ERR_UNIT_ACT_EXE;
		return EXIT_FAILURE;
	}
	
	/*! 2 是否是本单元的指令*/
	if(pstFrame->ucTarget !=  LOAD_PLATFORM_UNIT_ID)
	{
		ret = ERR_UNIT_NO_THIS_UNIT;
		return EXIT_FAILURE;
	}
	
	/*! 3  接受命令数据 */
	memset((uint8_t*)&s_stLoadPlatformActionInfo, 0, sizeof(s_stLoadPlatformActionInfo));
	s_stLoadPlatformActionInfo.stFrameInfo = *pstFrame;
	s_stLoadPlatformActionInfo.ucUnitActionState = UNIT_ACTION_START;
	s_stLoadPlatformActionInfo.ucIsAck = ucISack;
	
	/*! 4 设置命令执行结果的初始状态 */
	memset((uint8_t*)&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
	
	/*! 5 发送信号量 */
	OSSemPost(SP_Sem);   
	
	return ret;
}


/*******************************************************************************
* 名称:  check_sampro_cmd_result
* 功能:  指令执行结果处理， 发送结果帧
* 形参:  
			stAction    单元动作信息结构体
			ucRet		单元的动作结果

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
void check_sampro_cmd_result(action_info_t *stAction, uint8_t ucRet)
{
	/*! 电机编号的重新映射 */
	s_stLoadPlatformActionInfo.ucErrMotor = get_redefine_motor_no(s_stLoadPlatformActionInfo.ucErrMotor);
	
	/*! 1 设置命令处理结果 */
	if(ucRet == 0)
	{
		/*! 命令执行正确 */
		SetCmdFrameResult(&s_stLoadPlatformActionInfo.stFrameInfo, &s_stCmdActionResult, 0, LOAD_PLATFORM_UNIT_ID, 0, 0);
	}
	else
	{
		/*! 命令执行出错,命令执行结果格式: 1命令执行结果 2单元编号 3电机编号 4错误代码  */
		SetCmdFrameResult(&s_stLoadPlatformActionInfo.stFrameInfo, &s_stCmdActionResult, 1, LOAD_PLATFORM_UNIT_ID, s_stLoadPlatformActionInfo.ucErrMotor, ucRet);
	}
	
	/*! 2 发送命令结果帧 到上位机 */
	if(s_stLoadPlatformActionInfo.ucIsAck == 1)
	{
		SendResult(&s_stLoadPlatformActionInfo.stFrameInfo, ucRet, LOAD_PLATFORM_UNIT_ID, s_stLoadPlatformActionInfo.ucErrMotor, ucRet);
	}

	/*! 3 设置单元的状态 */
	s_stLoadPlatformActionInfo.ucUnitActionState = UNIT_ACTION_IDLE;
}


/*******************************************************************************
* 名称:  GetSampProbeActionResult
* 功能:  装载台单元系统指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_system  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
//	uint8_t ucDir = 0;
	
	(void)pucFrameData;
//	ucDir = pucFrameData[1];
	
	PRO_DEBUG(COMM_DEBUG,("cmd_load_platform_system\r\n"));
	
//	ret = motor_move(SAMP_HORZ_MT, 1000, eMtx_Dir_NEGA, 15, 1, 0);
//	
//	ret = motor_move(SAMP_LOAD_MT, 1000, eMtx_Dir_NEGA, 15, 1, 0);
	
	Mtx_Waiting_To_Idle(SAMP_HORZ_MT);
	Mtx_Waiting_To_Idle(SAMP_LOAD_MT);
//	ret = Mtx_Rst(SAMP_HORZ_MT, 0, 0);
	
	/*! 直接还回0， 直接成功 */
	return ret;
}



/*******************************************************************************
* 名称:  GetSampProbeActionResult
* 功能:  装载台单元查询
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_query  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t  ucaParaData[30] ;
	uint8_t	 ucParaLen  = 0;
	
	ucCmdType = pucFrameData[0];
	ucaParaData[0] = ucCmdType;
	
	switch(ucCmdType)
	{
		/*! 载体台x，卡条推杆电机，卡条门翻转电机的速度 */
		case 0:
			ucaParaData[1]  = MtxState[SAMP_HORZ_MT].mMotorSpeed;
			ucaParaData[2]  = MtxState[SAMP_LOAD_MT].mMotorSpeed;
			ucaParaData[3]  = MtxState[SAMP_SYRI_MT].mMotorSpeed;
            ucaParaData[4]  = MtxState[SAMP_PICK_MT].mMotorSpeed;
			ucParaLen = 5;
		
		break;
		//1号进卡位（复位位）
		//2号进卡位
		//计数卡染色位
		//计数卡加样位
		//计数卡混液位
		//检测卡加样位
		//检测卡孵育位
		//镜检进卡位		
		
		/*! 载体水平电机各个位置步数 */
		case 1:
            //1号进卡位（复位位）
            ucaParaData[1] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[0]>>0x08;
            ucaParaData[2] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[0];
            //2号进卡位
            ucaParaData[3] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[1]>>0x08;
            ucaParaData[4] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[1];
            //计数卡染色位
            ucaParaData[5] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[2]>>0x08; 
            ucaParaData[6] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[2]; 
            //
            ucaParaData[7] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[0]>>0x08;
            ucaParaData[8] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[0];
            //
            ucaParaData[9] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[1]>>0x08;
            ucaParaData[10] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[1];
            //
            ucaParaData[11] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[2]>>0x08; 
            ucaParaData[12] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[2]; 
            //
            ucaParaData[13] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[0]>>0x08;
            ucaParaData[14] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[0];
            //
            ucaParaData[15] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[1]>>0x08;
            ucaParaData[16] = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[1];
		
            ucParaLen = 17;

		break;
		
		/*! 卡条推杆电机各位置步数 */
		case 2:
            //复位位
            ucaParaData[1] = s_ucMotorOffsetPara.unload_p_pushMotorPosStep[0]>>8;
            ucaParaData[2] = s_ucMotorOffsetPara.unload_p_pushMotorPosStep[0];
            //翻转位
            ucaParaData[3] = s_ucMotorOffsetPara.unload_p_pushMotorPosStep[1]>>8;
            ucaParaData[4] = s_ucMotorOffsetPara.unload_p_pushMotorPosStep[1];
            //
			
            ucParaLen = 5;
		break;
		
		/*! 卡条门翻转电机各位置步数 */
		case 3:
            //复位位
            ucaParaData[1] = s_ucMotorOffsetPara.unload_p_flipMotorPosStep[REAL_POS_INIT_LOAD_P_FLIP_MOTOR]>>8;
            ucaParaData[2] = s_ucMotorOffsetPara.unload_p_flipMotorPosStep[REAL_POS_INIT_LOAD_P_FLIP_MOTOR];
            //拍照位
            ucaParaData[3] = s_ucMotorOffsetPara.unload_p_flipMotorPosStep[REAL_POS_FLIP_LOAD_P_FLIP_MOTOR]>>8; 
            ucaParaData[4] = s_ucMotorOffsetPara.unload_p_flipMotorPosStep[REAL_POS_FLIP_LOAD_P_FLIP_MOTOR];
			
            ucParaLen = 5;
		break;
				
		default:
			break;
	}
	
	/*! 用数据帧还回查询信息 */
	SendData(&s_stLoadPlatformActionInfo.stFrameInfo, ucaParaData, ucParaLen);
	
	return ret;
}


/*******************************************************************************
* 名称:  cmd_load_platform_config
* 功能:  装载台单元系统配置
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_config  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucIndex  = 0;
	uint16_t unStep  = 0;
	
	ucCmdType = pucFrameData[0];
	
	switch(ucCmdType)
	{
		/*! 设置电机速度 */
		case 0:
			SetSampleProbeSpeed(pucFrameData[1], pucFrameData[2],  pucFrameData[3],  pucFrameData[4]);
		break;
		
		/*! 载体水平电机各个位置步数 */
		case 1:
			for(ucIndex = 0; ucIndex < 3; ucIndex++)
			{
				unStep = (pucFrameData[ucIndex*2 + 1]<<8) + pucFrameData[ucIndex*2 + 2];
				Setload_p_horzMotorPosStep(ucIndex, unStep);
			}
		break;
		
		/*! 卡条推杆电机各位置步数*/
		case 2:
			for(ucIndex = 0; ucIndex < 6; ucIndex++)
			{
				unStep = (pucFrameData[ucIndex*2 + 1]<<8) + pucFrameData[ucIndex*2 + 2];
				Setload_p_pushMotorPosStep(ucIndex, unStep);
			}
		break;
		
		/*! 卡条门翻转电机各位置步数 */
		case 3:
			for(ucIndex = 0; ucIndex < 2; ucIndex++)
			{
				unStep = (pucFrameData[ucIndex*2 + 1]<<8) + pucFrameData[ucIndex*2 + 2];
				Setload_p_flipMotorPosStep(ucIndex, unStep);
			}
		break;
				
		default:
		break;
	
	}
	
	return ret;
}

/*******************************************************************************
* 名称:  cmd_load_platform_rst
* 功能:  装载台单元复位指令指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_rst  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	
	ucCmdType = pucFrameData[0];
	
	switch(ucCmdType)
	{
			
		/*! 载体台单元整体复位*/
		case 1:
			ret = motor_rst(LOAD_P_PUSH_MT, MtxState[LOAD_P_PUSH_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_PUSH_MT;
				return ret;
			}
			
			ret = motor_rst(LOAD_P_FLIP_MT, MtxState[LOAD_P_FLIP_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_FLIP_MT;
				return ret;
			}	
			
			ret = motor_rst(LOAD_P_HORZ_MT, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
				return ret;
			}
			
			
			break;
		
		/*! 载体台水平电机复位 */
		case 2:
			ret = motor_rst(LOAD_P_HORZ_MT, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
				return ret;
			}
		break;
		
		/*! 载体台推杆电机复位 */
		case 3:
			ret = motor_rst(LOAD_P_PUSH_MT, MtxState[LOAD_P_PUSH_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_PUSH_MT;
				return ret;
			}
		break;
		
		/*! 卡条翻转电机复位 */
		case 4:
			ret = motor_rst(LOAD_P_FLIP_MT, MtxState[LOAD_P_FLIP_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_FLIP_MT;
				return ret;
			}			
		break;

		default:
		break;
	
	}
	
	return ret;
}

uint8_t load_platform_in_card_op(void)
{
	uint8_t ret = 0;

	/*! 进卡位 */
	ret = motor_move_to_pos(LOAD_P_PUSH_MT, REAL_POS_PHOTO_CARD_LOAD_P_HORZ_MOTOR, MtxState[LOAD_P_PUSH_MT].mMotorSpeed, 0, 0);
	if(ret)
	{
		s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_PUSH_MT;
		return ret;
	}

	OSTimeDly(10);

	/*! 复位 */
	ret = motor_rst(LOAD_P_PUSH_MT, MtxState[LOAD_P_PUSH_MT].mMotorSpeed, 0, 0);
	if(ret)
	{
		s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_PUSH_MT;
		return ret;
	}

	return ret;
}


/*******************************************************************************
* 名称:  cmd_load_platform_simple_op
* 功能:  装载台单元简单操作指令
* 形参:  
		pucFrameData  命令帧中的数据
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_simple_op  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucIndex = 0;
	
	ucCmdType = pucFrameData[0];

	switch(ucCmdType)
	{
			
		/*! 载体台进卡动作 */
		case 0:
			/*! 运动到孵育盒进卡位 */
			ret = motor_move_to_pos(LOAD_P_HORZ_MT, REAL_POS_INCUB_IN__CARD_LOAD_P_PUSH, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
				return ret;
			}
			
			/*! 翻转电机，运动到翻转位 */
			ret = motor_move_to_pos(LOAD_P_FLIP_MT, REAL_POS_FLIP_LOAD_P_FLIP_MOTOR, MtxState[LOAD_P_FLIP_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_FLIP_MT;
				return ret;
			}
			
			
			ret = load_platform_in_card_op();
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
				return ret;
			}
			
			/*! 载体体翻转电机复位 */
			ret = motor_rst(LOAD_P_FLIP_MT, MtxState[LOAD_P_FLIP_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_FLIP_MT;
				return ret;
			}
			
			OSTimeDly(10);
			/*! 载体体水平电机复位 */
			ret = motor_rst(LOAD_P_HORZ_MT, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
				return ret;
			}

			break;
			
		/*! 载体台混匀动作 */
		case 1:
			/*! 运动到 卡条混匀位 */
			ret = motor_move_to_pos(LOAD_P_HORZ_MT, REAL_POS_CARD_INCU_LOAD_P_PUSH, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
				return ret;
			}
			
			for(ucIndex = 0; ucIndex  < 5; ucIndex++)
			{
				/*! 向前 */
				ret = motor_move(LOAD_P_HORZ_MT, 300, (eMtxDir)eMtx_Dir_NEGA, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
				if(ret)
				{
					s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
					return ret;
				}	
				
				OSTimeDly(20);
				
				/*! 向后 */
				ret = motor_move(LOAD_P_HORZ_MT, 300, (eMtxDir)eMtx_Dir_POSI, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
				if(ret)
				{
					s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
					return ret;
				}
				OSTimeDly(20);
			}
			
			break;
			
		/*! 载体台混匀动作后，执行进卡动作 */
		case 2:
			
			break;
			
		default:
		break;
	}
	
	return ret;
	
}



/*******************************************************************************
* 名称:  cmd_load_platform_simple_op
* 功能:  装载台单元水平定位
* 形参:  
		pucFrameData  命令帧中的数据
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_move_x_pos (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
//	uint8_t ucPos = 0;
	
	ucCmdType = pucFrameData[0];
//	ucPos 	  = pucFrameData[1]-1;
//	(void)ucCmdType;
	
	//0x00	复位
	//0x01	1号进卡位（复位位）
	//0x02	2号进卡位
	//0x03	计数卡染色位
	//0x04	计数卡加样位
	//0x05	检测卡加样位
	//0x06	检测卡孵育位
	//0x07	镜检进卡位
	
	ret = motor_move_to_pos(LOAD_P_HORZ_MT, ucCmdType, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
	if(ret)
	{
		s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
		return ret;
	}
	
	return ret;
	
}

/*******************************************************************************
* 名称:  cmd_load_platform_move_to_pos
* 功能:  装载台单元运动到指定位置
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_move_to_pos  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucPos = 0;
	
	ucCmdType = pucFrameData[0];
	ucPos 	  = pucFrameData[1];
	
	switch(ucCmdType)
	{
		/*! 复位位 */
		case 0:
			ret = motor_rst(INCU_LIFT_MT, MtxState[INCU_LIFT_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = INCU_LIFT_MT;
				return ret;
			} 
			break;
			
		/*! 载体台推杆电机运动到指定位置 */
		case 1:
			ret = motor_move_to_pos(LOAD_P_PUSH_MT, ucPos, MtxState[LOAD_P_PUSH_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_PUSH_MT;
				return ret;
			}
			break;
			
		/*! 卡条翻转电机运动到指定位置 */
		case 2:
			ret = motor_move_to_pos(LOAD_P_FLIP_MT, ucPos, MtxState[LOAD_P_FLIP_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_FLIP_MT;
				return ret;
			}
			break;
			
		default:
		break;
	}
	
	return ret;
}


/*******************************************************************************
* 名称:  cmd_load_platform_move_step
* 功能:  装载台单元运动步数指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_move_step(uint8_t * pucFrameData)
{
	uint8_t ret       = 0;
	uint8_t ucCmdType = 0;
	uint8_t  ucDir    = 0;
	uint16_t unStep   = 0;
	
	ucCmdType = pucFrameData[0];
	ucDir 	  = pucFrameData[1];
	unStep    = (pucFrameData[2]<<8) + pucFrameData[3];
	
	switch(ucCmdType)
	{
		/*! 载体台水平电机运动指定步数 */
		case 0:
			ret = motor_move(LOAD_P_HORZ_MT, unStep, (eMtxDir)ucDir, MtxState[LOAD_P_HORZ_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_HORZ_MT;
				return ret;
			}	
		break;
			
		/*! 载体推杆电机运运动指定步数 */
		case 1:
			ret = motor_move(LOAD_P_PUSH_MT, unStep, (eMtxDir)ucDir, MtxState[LOAD_P_PUSH_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_PUSH_MT;
				return ret;
			}	
		break;
			
		/*! 载体台翻转电机运动指定步数 */
		case 2:
			ret = motor_move(LOAD_P_FLIP_MT, unStep, (eMtxDir)ucDir, MtxState[LOAD_P_FLIP_MT].mMotorSpeed, 0, 0);
			if(ret)
			{
				s_stLoadPlatformActionInfo.ucErrMotor = LOAD_P_FLIP_MT;
				return ret;
			}	
		break;
		
		default:
		break;
		
	}
	
	return ret;
}


/*******************************************************************************
* 名称:  cmd_load_platform_io_state_update
* 功能:  装载台单元IO状态读取
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_load_platform_io_state_update  (uint8_t * pucFrameData)
{
	uint8_t  ucRet = 0;
	uint8_t  ucCmdType  = 0;
	uint8_t  ucaParaData[30] ;
	uint8_t	 ucParaLen  = 0;
	
	ucCmdType      =  pucFrameData[0];
	ucaParaData[0] = ucCmdType;
	(void)ucCmdType;
	
	ucaParaData[1] =  MtxFunList[LOAD_P_HORZ_MT].mtResetRead(LOAD_P_HORZ_MT);;
	ucaParaData[2] =  MtxFunList[LOAD_P_PUSH_MT].mtResetRead(LOAD_P_PUSH_MT);;
	ucaParaData[3] =  MtxFunList[LOAD_P_FLIP_MT].mtResetRead(LOAD_P_FLIP_MT);;
	ucaParaData[4] = 1;
	ucaParaData[5] = 1;
	ucaParaData[6] = 1;
	ucParaLen      = 7;

	
	/*! 用数据帧还回查询信息 */
	SendData(&s_stLoadPlatformActionInfo.stFrameInfo, ucaParaData, ucParaLen);
	
	return ucRet;
}








/*******************************************************************************
* 名称: load_platform_cmd_handle
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t load_platform_cmd_handle(void)
{
    uint8_t  ret = 0;
	uint8_t  *pucDataBuffer ;
	
	pucDataBuffer  = s_stLoadPlatformActionInfo.stFrameInfo.ucaPara;
    
    switch (s_stLoadPlatformActionInfo.stFrameInfo.ucCmdId)
    {
        case CMD_LOAD_PLATFORM_SYSTEM:// 装载台单元系统指令 0x00
        {
			ret = cmd_load_platform_system(pucDataBuffer);
        }
		break;
		
        case CMD_LOAD_PLATFORM_QUERY:// 装载台单元查询指令 0x01
        {
			ret = cmd_load_platform_query(pucDataBuffer);
        }
		break;
				
        case CMD_LOAD_PLATFORM_CONFIG:// 装载台单元配置指令 0x02
        {
			ret = cmd_load_platform_config(pucDataBuffer);
        }
		break;		
				
		case CMD_LOAD_PLATFORM_RST://装载台单元复位指令  0x03
        {
			ret = cmd_load_platform_rst(pucDataBuffer);
        }
		break;

		case CMD_LOAD_PLATFORM_SIMPLE_OP://装载台单元简单操作  0x08
        {
			ret = cmd_load_platform_simple_op(pucDataBuffer);
        }
		break;		

		case CMD_LOAD_PLATFORM_MOVE_TO_POS://装载台单元运动到指定位置  0x05
        {
			ret = cmd_load_platform_move_to_pos(pucDataBuffer);
        }
		break;		

		case CMD_LOAD_PLATFORM_MOVE_STEP://装载台单元单个操作 0x06
        {
			ret = cmd_load_platform_move_step(pucDataBuffer);
        }
		break;		
		
		case CMD_LOAD_PLATFORM_IO_STATE_UPDATE://装载台单元IO状态读取  0x07
        {
			ret = cmd_load_platform_io_state_update(pucDataBuffer);
        }
		break;		

		case CMD_LOAD_PLATFORM_MOVE_X_POS://装载台单元IO状态读取  0x07
        {
			ret = cmd_load_platform_move_x_pos(pucDataBuffer);
        }
		break;	
		
		default:
		{
		}
		break;
		
	}
	
	return ret;
}


/*******************************************************************************
* 名称: load_platform_action_init
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
static void load_platform_action_init(void)
{

	/*! 1 复位变量 */
	memset(&s_stLoadPlatformActionInfo, 0, sizeof(s_stLoadPlatformActionInfo));
	memset(&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
	
}



/*******************************************************************************
* 名称: load_platform_task
* 功能:  装载台线程
* 形参:  	
* 返回: 无
* 说明: 无
********************************************************************************/
void load_platform_task(void *p_arg)
{
    OS_ERR err;
	UINT8 ret = ERR_NONE;
    
    (void)p_arg;
    
	/*! 本单元状态初始化 */
	load_platform_action_init();
	
    while (1)
    {	
		/*! 等待信号量触发 */
        OSSemPend(SP_Sem, 0, &err);
		
		/*!< 动作没有开始，是发给本单元的指令 */
		if(s_stLoadPlatformActionInfo.ucActionFinishFlag == 0 && s_stLoadPlatformActionInfo.stFrameInfo.ucTarget == LOAD_PLATFORM_UNIT_ID)
		{
			ret = load_platform_cmd_handle();
			
			/*! 当动作执行完成，上报命令执行结果 */
            check_sampro_cmd_result(&s_stLoadPlatformActionInfo, ret);
		}		

    } 
}



#endif
