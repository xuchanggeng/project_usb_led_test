/**
  ******************************************************************************
  * @文件   Temlate.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "app.h"
#include "includes.h"
#include "pro_include.h"
#include "motor_para.h"
#include "bsp_can_msp.h"
#include "IO_Def.h"
#include "ctrl_temp.h"
#include "cmd_process_task.h"
#include "three_led.h"
#include "bsp_usart.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
OS_EVENT *Mid_Sem;		
OS_EVENT *SP_Sem;	
OS_EVENT *DS_Sem;
OS_EVENT *TP_Sem;

OS_FLAG_GRP *MtxEventFlags;
RCC_ClocksTypeDef RCC_Clocks;
/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 --------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/

/*! 启动线程 */
#define  SYS_START_TASK_PRIO                    3u

/*! 通信线程 */
//#define  MID_TASK_PRIO                          4u
//#define  MID_TASK_STK_SIZE                      512
//static   OS_STK  MID_TAST_STK[MID_TASK_STK_SIZE];
//void     mid_task(void *p_arg);

/*! 载体台线程 */
//#define  LOAD_PLATFORM_TAST_PRIO                5u
//#define  LOAD_PLATFORM_TASK_STK_SIZE            512
//static   OS_STK  LOAD_PLATFORM_TAST_STK[LOAD_PLATFORM_TASK_STK_SIZE];
//void     load_platform_task(void *p_arg);

///*! 卡条盒线程 */
//#define  CARD_SLICE_BOX_TAST_PRIO                6u
//#define  CARD_SLICE_BOX_TASK_STK_SIZE            512
//static   OS_STK CARD_SLICE_BOX_TAST_STK[CARD_SLICE_BOX_TASK_STK_SIZE];
//void    card_slice_box_task(void *p_arg);

///*! Tip头线程 */
//#define  TIP_HEAD_BOX_TAST_PRIO                7u
//#define  TIP_HEAD_BOX_TASK_STK_SIZE            512
//static   OS_STK TIP_HEAD_BOX_TAST_STK[TIP_HEAD_BOX_TASK_STK_SIZE];
//void    tip_head_box_task(void *p_arg);

/*! 温控线程 */
#define  CMD_PROCESS_TAST_PRIO                	8u
#define  CMD_PROCESS_TASK_STK_SIZE            	512
static   OS_STK CMD_PROCESS_TAST_STK[CMD_PROCESS_TASK_STK_SIZE];
void    cmd_process_task(void *p_arg);



/*! 实时监控线程 */
#define  REALTIME_TASK_PRIO                     9u
#define  REALTIME_TASK_STK_SIZE                 256 
static   OS_STK  REALTIME_TAST_STK[REALTIME_TASK_STK_SIZE];
void     realtime_task(void *p_arg);


/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void pro_app_init(void)
{
#ifdef PRO_DEBUG_ENABLE
	/*! 初始化调试信息 20190423 */
	pro_debug_init();
#endif
	
	/*! 获取系统各个时钟 */
	RCC_GetClocksFreq(&RCC_Clocks);
	SysTick_Config(RCC_Clocks.HCLK_Frequency/1000);
	
#ifdef PRO_DEBUG_ENABLE
	/*! 显示工程信息 20190423 */
   pro_show_version();
#endif
	
	/*! bsp底层初始化 */
	bsp_led_init();

	bsp_usart_init(USART2, 115200);
	// bsp_eeprom_init();
	// bsp_pv_all_init();
    
	/*! CAN通信 */
//	can_sourse_init();

	/*! 加载参数 */
//	InitReadAllMotorPara();
	
	/*! 按键IO输入 */
	// IO_Configuration();


}


/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void sys_start_task(void *pdata)
{
	OS_ERR err;
#if OS_CRITICAL_METHOD == 3u                 
    OS_CPU_SR  cpu_sr = 0u;
#endif
    
	pdata = pdata;
	
	OS_ENTER_CRITICAL(); 
	
	/*! 运用代码初始化 */
	pro_app_init();
	
    DS_Sem  = OSSemCreate(0);
    MtxEventFlags = OSFlagCreate(0, &err); 	
    
	/*! 统计线程 */
	OSStatInit();  
						  
	/*! 命令执行线程 */				
	(void)OSTaskCreateExt(cmd_process_task,
						  (void *)0,                                   
						  &CMD_PROCESS_TAST_STK[CMD_PROCESS_TASK_STK_SIZE - 1u],  
						  CMD_PROCESS_TAST_PRIO,                           
						  5,
						  &CMD_PROCESS_TAST_STK[0],                           
						  CMD_PROCESS_TASK_STK_SIZE,
						  (void *)0,                                   
						  OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR); 
						  
						  
	/*! 实时监控线程 */				
	(void)OSTaskCreateExt(realtime_task,
						  (void *)0,                                   
						  &REALTIME_TAST_STK[REALTIME_TASK_STK_SIZE - 1u],  
						  REALTIME_TASK_PRIO,                           
						  6,
						  &REALTIME_TAST_STK[0],                           
						  REALTIME_TASK_STK_SIZE,
						  (void *)0,                                   
						  OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);				
		
	/*! 挂起初始化线程 */				
	OSTaskSuspend(SYS_START_TASK_PRIO);
				
	OS_EXIT_CRITICAL();  
}





