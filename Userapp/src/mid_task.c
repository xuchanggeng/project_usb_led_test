
#ifndef __MID_TASK_C__
#define __MID_TASK_C__

#include "includes.h"
#include "pro_include.h"
#include "Com_Dispatcher.h"
#include "Com_Prase.h"


UINT8 RecieveBuf[MSG_BUF_LEN] = {0};
UINT8 Mid_msg_buf[MSG_BUF_LEN] = {0};
UINT8 ReBack_Buf[REBACK_BUF_LEN] = {0};

#if SYSTEM_SUPPORT_OS
UINT8 Mid_Run = 0;
#endif

tMsg sys_msg;           			// 系统命令参数信息
tMsg pv_msg;            			// 泵阀命令参数信息
tMsg sp_msg;            			// 试剂盘命令参数信息
tMsg para_msg;          			// 存储数据命令参数信息

UINT8 Sys_Msg_Data[16] = {0};       // 系统命令数据存储
UINT8 Pv_Msg_Data[16] = {0};        // 泵阀命令数据存储
UINT8 Sp_Msg_Data[32] = {0};        // 试剂盘命令数据存储
UINT8 Para_Msg_Data[64] = {0};      // 存储数据命令数据存储

UINT8 set_debug = 0;



/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void mid_task(void *p_arg)
{
    (void)p_arg;
	
    while (1)
    {
		/*! 数据接受 */
		com_can_recv_main();
		
		/*! 进行命令的分发 */
		ComFrameDispatcher();
		
		OSTimeDly(2);
    }   
}

#endif
