
#ifndef __CTRLTEMP_C__
#define __CTRLTEMP_C__


#include "binary_ctrl_temp.h"
#include "bsp_pump_valve.h"
#include "bsp_ds18b20.h"


#define BINARY_CTRL_NUM         	5U


static binary_ctrl_temp_t s_stBinaryTempCtrl[BINARY_CTRL_NUM] = {0};
static uint8_t            s_ucNumberOfTempCtrl                = 0;
static tCool_config_t     s_stCoolPinConfig[BINARY_CTRL_NUM] = {0};








/*******************************************************************************
* 函数名称: binary_ctrl_temp_main
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void binary_ctrl_temp_main(void)
{
	uint16_t unIndex = 0;
	int32_t  ltemp   = 0;
	
	for(unIndex = 0; unIndex < s_ucNumberOfTempCtrl; unIndex++)
	{
		/*! 判断风扇状态 */
		if(s_stBinaryTempCtrl[unIndex].read_fan_state(unIndex) == 1)
		{
			/*! 风扇状态不对，关闭制冷 */
			s_stBinaryTempCtrl[unIndex].output_ctrl_cool_off(unIndex);
		}
		
		/*! 读取温度 */
		ltemp = s_stBinaryTempCtrl[unIndex].read_temp(unIndex);
		
		/*! 判断温度,输出控制 */
		if(ltemp < s_stBinaryTempCtrl[unIndex].unTagetLowTemp + 1)
		{
			 /*! 关闭制冷 */
			 s_stBinaryTempCtrl[unIndex].output_ctrl_cool_off(unIndex);
		}
		else if(ltemp > s_stBinaryTempCtrl[unIndex].unTagetHighTemp - 1)
		{
			/*! 打开制冷 */
			s_stBinaryTempCtrl[unIndex].output_ctrl_cool_on(unIndex);
		}
	}
	
	

}





/*******************************************************************************
* 函数名称: binary_ctrl_temp_main
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint8_t register_temp_ctrl_struct(binary_ctrl_temp_t *stTempCtrl)
{
	ErrorStatus status = SUCCESS;
	
	if(s_ucNumberOfTempCtrl < BINARY_CTRL_NUM)
	{
		s_stBinaryTempCtrl[s_ucNumberOfTempCtrl].unTagetTemp = stTempCtrl->unTagetTemp;
		s_stBinaryTempCtrl[s_ucNumberOfTempCtrl].unTagetLowTemp = stTempCtrl->unTagetLowTemp;
		s_stBinaryTempCtrl[s_ucNumberOfTempCtrl].unTagetHighTemp = stTempCtrl->unTagetHighTemp;
		s_stBinaryTempCtrl[s_ucNumberOfTempCtrl].read_temp = stTempCtrl->read_temp;
		s_stBinaryTempCtrl[s_ucNumberOfTempCtrl].read_fan_state = stTempCtrl->read_fan_state;
		s_stBinaryTempCtrl[s_ucNumberOfTempCtrl].output_ctrl_cool_on = stTempCtrl->output_ctrl_cool_on;
		s_stBinaryTempCtrl[s_ucNumberOfTempCtrl].output_ctrl_cool_off = stTempCtrl->output_ctrl_cool_off;
		
		s_ucNumberOfTempCtrl++;
	}
	else
	{
		status = ERROR;
	}

	return status;
}


/*******************************************************************************
* 函数名称: binary_ctrl_temp_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
int32_t read_cool_temp(uint16_t unIndex)
{
	int32_t ucRet = 0;
	
	return ucRet;
}


/*******************************************************************************
* 函数名称: binary_ctrl_temp_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint8_t read_cool_fan(uint16_t unIndex)
{
	uint8_t ucRet = 0;
	
	return ucRet;
}


/*******************************************************************************
* 函数名称: binary_ctrl_temp_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint8_t temp_ctrl_cool_on(uint16_t unIndex)
{
	uint8_t ucRet = 0;
	
	return ucRet;
}


/*******************************************************************************
* 函数名称: binary_ctrl_temp_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint8_t temp_ctrl_cool_off(uint16_t unIndex)
{
	uint8_t ucRet = 0;
	
	return ucRet;
}





/*******************************************************************************
* 函数名称: binary_ctrl_temp_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void binary_ctrl_temp_para_init(void)
{
	binary_ctrl_temp_t stTemp = {0};
	
	/*!【0】*/
	stTemp.unTagetTemp     = 5;
	stTemp.unTagetLowTemp  = 2;
	stTemp.unTagetHighTemp = 8;
	stTemp.read_temp       = read_cool_temp;
	stTemp.read_fan_state  = read_cool_fan;
	stTemp.output_ctrl_cool_on  = temp_ctrl_cool_on;
	stTemp.output_ctrl_cool_off	= temp_ctrl_cool_off;
	
	register_temp_ctrl_struct(&stTemp);

}


/*******************************************************************************
* 函数名称: bsp_config_cool_struct
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void bsp_config_cool_struct(eCOOL_Type eCool)
{
	COOL_PIN(eCool, s_stCoolPinConfig[eCool].COOL_Pin);
	COOL_PORT(eCool, s_stCoolPinConfig[eCool].COOL_Port);
	COOL_CLK(eCool, s_stCoolPinConfig[eCool].COOL_Clk);
}





/*******************************************************************************
* 函数名称: bsp_cool_gpio_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void bsp_cool_gpio_init(eCOOL_Type eCool)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
	
	bsp_config_cool_struct(eCool);
	
	/*! 时钟 */
    RCC_APB2PeriphClockCmd(s_stCoolPinConfig[eCool].COOL_Clk, ENABLE);

    GPIO_InitStructure.GPIO_Pin   = s_stCoolPinConfig[eCool].COOL_Pin;				
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP; 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		
    GPIO_Init(s_stCoolPinConfig[eCool].COOL_Port, &GPIO_InitStructure); 
    
	GPIO_ResetBits(s_stCoolPinConfig[eCool].COOL_Port, s_stCoolPinConfig[eCool].COOL_Pin);  
	
}



/*******************************************************************************
* 函数名称: binary_ctrl_temp_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void binary_ctrl_temp_hw_init(void)
{
	uint8_t ucIndex  = 0;
	
	/*! 输出gpio初始化 */	
	for(ucIndex = 0; ucIndex < E_COOL_MAX; ucIndex++)
	{
		bsp_cool_gpio_init((eCOOL_Type)ucIndex);
	}
	
	

}




/*******************************************************************************
* 函数名称: binary_ctrl_temp_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void binary_ctrl_temp_init(void)
{
	
	binary_ctrl_temp_hw_init();

	binary_ctrl_temp_para_init();
}















#endif
