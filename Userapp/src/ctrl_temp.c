
#ifndef __CTRLTEMP_C__
#define __CTRLTEMP_C__


#include "ctrl_temp.h"
#include "bsp_pump_valve.h"
#include "bsp_ds18b20.h"
#include "Util_pid.h"

/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
extern UINT8 g_ucReactionRunFlag;

#define ADC_NUM         			2
#define TEMP_PID_PERIOD             500

/*! 孵育盘加热控制 */
#define REACTION_HEAT_ON   	        do{ bsp_pv_open(E_PV_5); bsp_pv_open(E_PV_6);}while(0)
#define REACTION_HEAT_OFF  	        do{ bsp_pv_close(E_PV_5); bsp_pv_close(E_PV_6);}while(0)

//#define REACTION_HEAT_ON   	        do{  bsp_pv_open(E_PV_6);}while(0)
//#define REACTION_HEAT_OFF  	        do{  bsp_pv_close(E_PV_6);}while(0)


tTempCtrlPara   TempCtrl[ADC_NUM];
tTempPid        TempPid[ADC_NUM];
UINT16          Temp_Tick = TEMP_PID_PERIOD;
lTEMP_Para      temp_para;
pid_ctrl_t      s_stHeatTemppid = {0};

/*! 帕尔帖(试剂盘)温度目标值 */
UINT8 g_ucPeltierCoolTempTarget = 4;
UINT8 g_ucReactionRunFlag = 1;


/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
UINT8 Fun_Mtx_TimCountON(UINT8 eTim)
{
    TIM_Cmd(TIM8, ENABLE);
    return e_true;
}

/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
UINT8 Fun_Mtx_TimCountOFF(UINT8 eTim)
{
    TIM_Cmd(TIM8, DISABLE);
    return e_true;
}


/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint16_t temp_ctrl_get_temp_incube(void)
{
	uint16_t ucTemp = 0;
	
//	ucTemp = TempPid[1].current;
	ucTemp = s_stHeatTemppid.lCurrent;
	
	return ucTemp;
}



/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
INT32 CalculatePID2(UINT8 eADC1Id, UINT8 mode)//位置式PID
{
    INT32 error;
    INT32 value = 0,temp = 0,temp2 = 0;
    if (mode == 1)
    {
        error = (INT32)(TempPid[eADC1Id].target) - (INT32)(TempPid[eADC1Id].current);
    }
    else
	{
        error = (INT32)(TempPid[eADC1Id].current) - (INT32)(TempPid[eADC1Id].target);
	}
	
    PRO_DEBUG(TEMP_DEBUG, ("error %d last_error %d\r\n", error, TempPid[eADC1Id].last_error) );
	
    value = TempCtrl[eADC1Id].P*error;
    temp = TempCtrl[eADC1Id].D*(error - TempPid[eADC1Id].last_error);
    temp2 = TempCtrl[eADC1Id].I * (error+TempPid[eADC1Id].last_error);
    
    value = value + temp + temp2;
    TempPid[eADC1Id].last_error = error;
	
    return value;
}

/*******************************************************************************
* 函数名称: Temperature_Ctrl
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
UINT8 Temperature_Ctrl(void)
{
    INT32 tick = 0;
	
    Fun_Mtx_TimCountOFF(0);
	
//    LED0 = !LED0;
    s_stHeatTemppid.lCurrent = bsp_temp_read(0);
	
    PRO_DEBUG(TEMP_DEBUG,( "TempPid %d %d\r\n", s_stHeatTemppid.lCurrent, s_stHeatTemppid.lTarget) );
    
//    tick = CalculatePID2(1,1);
	
	pid_calc(&s_stHeatTemppid);
	PRO_DEBUG(TEMP_DEBUG,("lTotal_out %d", s_stHeatTemppid.lTotal_out));
	
	tick = s_stHeatTemppid.lTotal_out;
	
    if (tick > TEMP_PID_PERIOD)
	{
        tick = TEMP_PID_PERIOD;
	}
    else if (tick < 0)
	{
        tick = 0;
	}
	
	/*! 输出限定 */
//	if(TempPid[1].current < TempPid[1].target - 10)
//	{
//		tick = TEMP_PID_PERIOD;
//	}
//	else if(TempPid[1].current > TempPid[1].target + 10)
//	{
//		tick = 0;
//	}

	/*! 输出参数 */
    TempPid[1].tick = tick;
    Temp_Tick = 0;
    PRO_DEBUG(TEMP_DEBUG,("tick = %d \r\n", tick) );
	
    Fun_Mtx_TimCountON(0);
	return 0;
}

/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void TIM8_UP_IRQHandler(void)
{
    TIM_ITConfig(TIM8, TIM_IT_Update, DISABLE);

    TIM_ClearITPendingBit(TIM8, TIM_IT_Update);  
    Temp_Tick++;
    if (TempPid[1].tick)
    {
        TempPid[1].tick--;
        REACTION_HEAT_ON;
    }
    else
    {
        REACTION_HEAT_OFF;
    }
    TIM_ITConfig(TIM8, TIM_IT_Update, ENABLE);
}

/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
//UINT16 Get_Current_Temp_Fun(UINT8 eADC1Id)
//{
//    TempPid[eADC1Id - 1].current = Get_ADC(eADC1Id); //孵育盘温度 REACTION_TEMP
//    return AD_TO_TEMP(TempPid[eADC1Id - 1].current);
//}

/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
//UINT16 Get_Current_Temp_Fun2(UINT8 eADC1Id)
//{
//    UINT16 AD = 0, temp = 0;
//    float voltage = 0;
//    
//    AD = Get_ADC(eADC1Id);
//    voltage = Get_ADCMath(AD);
//    temp  = 10 * voltage * 1000 / 4 / 10;     //实际温度放大10倍   
//	
//    return temp;
//}

/*******************************************************************************
* 函数名称: close_ctrl_heat
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void close_ctrl_heat(void)
{
	/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
	g_ucReactionRunFlag = 0;

	REACTION_HEAT_OFF;
}


/*******************************************************************************
* 函数名称: open_ctrl_heat
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void open_ctrl_heat(void)
{
	/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
	g_ucReactionRunFlag = 1;

	REACTION_HEAT_ON;
}


/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_tim_init(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
//    NVIC_InitTypeDef        NVIC_InitStructure;

    // 配置定时器参数
//    Fun_ConfigTimStruct((eTimType)eTim);
    
    TIM_DeInit(TIM8);

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE); //时钟使能
    
    //----------------------------------------------------------------------
    //定时器初始化
    TIM_TimeBaseStructure.TIM_Period = 999; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值  
    TIM_TimeBaseStructure.TIM_Prescaler =71; //设置用来作为TIMx时钟频率除数的预分频值
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位

    //----------------------------------------------------------------------
    //中断优先级NVIC设置
//    NVIC_InitStructure.NVIC_IRQChannel = TIM8_UP_IRQn;  //TIM3中断
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  //先占优先级0级
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 1;  //从优先级3级
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
//    NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器

    //-----------------------------------------------------------------------
    // 初始化设置
    TIM_ARRPreloadConfig(TIM8, ENABLE);
    TIM_ClearFlag(TIM8, TIM_FLAG_Update);
    
    TIM_ITConfig(TIM8, TIM_IT_Update, ENABLE); //使能指定的TIM3中断,允许更新中断

    TIM_Cmd(TIM8, ENABLE);  //使能TIMx   
	
}

/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_pid_para_init(void)
{
    UINT16 P = 105, I = 20, D = 100;
    
	/*! PID参数设置 */
	TempCtrl[1].P = P;
	TempCtrl[1].I = I;
	TempCtrl[1].D = D;
        
	/*! 目标温度设置值, 设置温度在25度 到 50度之间 */
	if( (temp_para.Set_Temp[0] >= 250) && (temp_para.Set_Temp[0] <= 500  ) ) 
	{
		 TempCtrl[1].target = temp_para.Set_Temp[0];
	}
	/*! 设置温度在25度 到 50度之间,默认设置位37度 */
	else
	{
		TempCtrl[1].target = 370;
	}
	
	TempCtrl[1].target = TempCtrl[1].target;
	
    /*! 正向5°间微调 */
    if (temp_para.Set_Temp[1] < 50) 
    {
        TempCtrl[1].target = TempCtrl[1].target + temp_para.Set_Temp[1];           
    }
	/*! 反向5°间微调 */
	else if ( (temp_para.Set_Temp[1] > 128) && (temp_para.Set_Temp[1] < 178))
	{
		TempCtrl[1].target = TempCtrl[1].target - (temp_para.Set_Temp[1] - 128);  
	}
	/*! 不补偿 */
    else
    {
        TempCtrl[1].target = TempCtrl[1].target + 0;
    }
    TempCtrl[1].add = 5;
	
    TempPid[1].target = TempCtrl[1].target;
	
}


/*******************************************************************************
* 函数名称: temperature_ctrl_update_temp
* 功能描述: 更新设置温度
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_update_temp(void)
{
	/*! 目标温度设置值, 设置温度在25度 到 50度之间 */
	if( (temp_para.Set_Temp[0] >= 250) && (temp_para.Set_Temp[0] <= 500  ) ) 
	{
		 TempCtrl[1].target = temp_para.Set_Temp[0];
	}
	/*! 设置温度在25度 到 50度之间,默认设置位37度 */
	else
	{
		TempCtrl[1].target = 370;
	}
	
	TempCtrl[1].target = TempCtrl[1].target - 30;
	
    /*! 正向5°间微调 */
    if (temp_para.Set_Temp[1] < 50) 
    {
        TempCtrl[1].target = TempCtrl[1].target + temp_para.Set_Temp[1];           
    }
	/*! 反向5°间微调 */
	else if ( (temp_para.Set_Temp[1] > 128) && (temp_para.Set_Temp[1] < 178))
	{
		TempCtrl[1].target = TempCtrl[1].target - (temp_para.Set_Temp[1] - 128);  
	}
	/*! 不补偿 */
    else
    {
        TempCtrl[1].target = TempCtrl[1].target + 0;
    }
    TempCtrl[1].add = 5;
	
    TempPid[1].target = TempCtrl[1].target;
	
}




/*******************************************************************************
* 函数名称: temperature_ctrl_main
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_main(void)
{
	/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
	if (g_ucReactionRunFlag)
	{
		if (Temp_Tick >= TEMP_PID_PERIOD)
		{
			Temperature_Ctrl();
		}
	}
	
}

/*******************************************************************************
* 函数名称: temperature_ctrl_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_init(void)
{
	/*! PID初始化 */
	temperature_ctrl_pid_para_init();
	
	/*! 温控Gpio初始化 */
	bsp_ds18b20_init();
	
	/*! 定时器初始化 */
	temperature_ctrl_tim_init();
	
	pid_init(&s_stHeatTemppid, 20, 2, 10, 370);
	
	pid_out_setting(&s_stHeatTemppid, 500, 300);
	
}



#endif
