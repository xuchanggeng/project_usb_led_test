
#ifndef __temp_unitE_C__
#define __temp_unitE_C__

#include "includes.h"
#include "pro_include.h"
#include "temp_unit_task.h"
#include "cmd_result.h"
#include "motor_port.h"
#include "motor_para.h"
#include "ctrl_temp.h"

_EXT_ OS_EVENT * TP_Sem;
extern tMtxState   	  MtxState[MTx_NUM];
extern motor_para_t   s_ucMotorOffsetPara;
extern lTEMP_Para     temp_para;

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
action_info_t  			  s_stTempUnitActionInfo 		= {0};
static cmd_result_t       s_stCmdActionResult 		    	= {0};					//本地命令执行结果信息，


/* 函数原型 ---------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 名称: get_temp_unit_action_state
* 功能:  获取温控动作状态
* 形参:  
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t get_temp_unit_action_state(void)
{
	return s_stTempUnitActionInfo.ucUnitActionState ;
}


/*******************************************************************************
* 名称: start_temp_unit_action
* 功能:  发送信号量，使温控开始执行数据帧中对应的动作
* 形参:  	
			pstFrameInfo   数据帧
			pstFrameResult 结果数据
			ucISack        是否需要结果

* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t start_temp_unit_action(frame_info_t *pstFrame, uint8_t ucISack)
{
	uint8_t  ret = 0;
	
	/*! 1 判断单元是否正在执行动作, 一个单元同一个时间只能执行一个动作*/
	if(s_stTempUnitActionInfo.ucUnitActionState != UNIT_ACTION_IDLE)
	{
		ret = ERR_UNIT_ACT_EXE;
		return EXIT_FAILURE;
	}
	
	/*! 2 是否是本单元的指令*/
	if(pstFrame->ucTarget !=  TEMP_UNIT_ID)
	{
		ret = ERR_UNIT_NO_THIS_UNIT;
		return EXIT_FAILURE;
	}
	
	/*! 3  接受命令数据 */
	memset((uint8_t*)&s_stTempUnitActionInfo, 0, sizeof(s_stTempUnitActionInfo));
	s_stTempUnitActionInfo.stFrameInfo = *pstFrame;
	s_stTempUnitActionInfo.ucUnitActionState = UNIT_ACTION_START;
	s_stTempUnitActionInfo.ucIsAck = ucISack;
	
	/*! 4 设置命令执行结果的初始状态 */
	memset((uint8_t*)&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
	
	/*! 5 发送信号量 */
	OSSemPost(TP_Sem);   
	
	return ret;
}



/*******************************************************************************
* 名称:  check_sampro_cmd_result
* 功能:  指令执行结果处理， 发送结果帧
* 形参:  
			stAction    单元动作信息结构体
			ucRet		单元的动作结果

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
void check_temp_unit_cmd_result(action_info_t *stAction, uint8_t ucRet)
{
	/*! 电机编号的重新映射 */
	s_stTempUnitActionInfo.ucErrMotor = get_redefine_motor_no(s_stTempUnitActionInfo.ucErrMotor);
	
	/*! 1 设置命令处理结果 */
	if(ucRet == 0)
	{
		s_stTempUnitActionInfo.ucErrMotor = 0;
		
		/*! 命令执行正确 */
		SetCmdFrameResult(&s_stTempUnitActionInfo.stFrameInfo, &s_stCmdActionResult, 0, TEMP_UNIT_ID, 0, 0);
	}
	else
	{
		/*! 命令执行出错,命令执行结果格式: 1命令执行结果 2单元编号 3电机编号 4错误代码  */
		SetCmdFrameResult(&s_stTempUnitActionInfo.stFrameInfo, &s_stCmdActionResult, 1, TEMP_UNIT_ID, s_stTempUnitActionInfo.ucErrMotor, ucRet);
	}
	
	/*! 2 发送命令结果帧 到上位机 */
	if(s_stTempUnitActionInfo.ucIsAck == 1)
	{
		SendResult(&s_stTempUnitActionInfo.stFrameInfo, ucRet, TEMP_UNIT_ID, s_stTempUnitActionInfo.ucErrMotor, ucRet);
	}

	/*! 3 设置单元的状态 */
	s_stTempUnitActionInfo.ucUnitActionState = UNIT_ACTION_IDLE;
	
	if(ucRet)
	{
		PRO_DEBUG(ERR_DEBUG,("motor id%d, err id %d\r\n", s_stTempUnitActionInfo.ucErrMotor, ucRet));
	}

}


/*******************************************************************************
* 名称:  GetSampProbeActionResult
* 功能:  温控单元系统指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_temp_unit_system  (uint8_t * pucFrameData)
{
	uint8_t ret = 0;
//	uint8_t ucDir = 0;
	
	(void)pucFrameData;
//	ucDir = pucFrameData[1];
	
	PRO_DEBUG(COMM_DEBUG,("cmd_temp_unit_system\r\n"));
	
	ret = motor_move(INCU_LIFT_MT, 1000, eMtx_Dir_NEGA, 15, 0, 0);
	
//	ret = Mtx_Rst(temp_unit_MT, 0, 0);
	
	/*! 直接还回0， 直接成功 */
	return ret;
}



/*******************************************************************************
* 名称:  cmd_temp_unit_query
* 功能:  温控单元系统指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_temp_unit_query(uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucaParaData[30] ;
	uint8_t	ucParaLen  = 0;
	uint16_t unTemp = 0;
	
	ucCmdType = pucFrameData[0];
	ucaParaData[0] = ucCmdType;
	(void)ucCmdType;
	
	switch(ucCmdType)
	{
		/*! 软件版本 */
		case 0:
			ucaParaData[1]  = MtxState[TIP_HEAD_BOX_MT].mMotorSpeed;
			ucParaLen = 2;
		break;
		
		/*! 孵育盘温度设置值 */
		case 1:
            ucaParaData[1] = temp_para.Set_Temp[0]>>0x08;
            ucaParaData[2] = temp_para.Set_Temp[0];
			ucParaLen = 3;
		break;
		
		/*! 孵育盘温度偏移量 */
		case 2:
            ucaParaData[1] = temp_para.Set_Temp[1];
            ucaParaData[2] = 0;
			ucParaLen = 3;
		break;
				
		/*! 孵育盘温度实际值 */
		case 3:
			unTemp = temp_ctrl_get_temp_incube();
            ucaParaData[1] = unTemp>>0x08;
            ucaParaData[2] = unTemp&0xFF;
			ucParaLen = 3;
		break;		
		
		default:
		break;
	
	}
	
	/*! 用数据帧还回查询信息 */
	SendData(&s_stTempUnitActionInfo.stFrameInfo, ucaParaData, ucParaLen);
	
	return ret;
}


/*******************************************************************************
* 名称:  cmd_temp_unit_config
* 功能:  温控单元系统指令
* 形参:  
			pucFrameData  命令帧中的数据

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_temp_unit_config(uint8_t * pucFrameData)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint16_t unStep   = 0;
	uint8_t  ucaParaData[20] = {0};
	uint8_t  ucParaLen = 0;
	
	ucCmdType = pucFrameData[0];
	
	switch(ucCmdType)
	{
		/*! 软件版本 */
		case 0:
            //
            ucaParaData[1] = PRO_VERSION;
            //
            ucaParaData[2] = PRO_SUBVERSION;
            //
            ucaParaData[3] = PRO_REVISION1; 
            //
            ucaParaData[4] = PRO_REVISION2;
			
            ucParaLen = 5;
			
		break;
		
		/*! 孵育盘温度设置值 */
		case 1:
			unStep = (pucFrameData[0*2 + 1]<<8) + pucFrameData[0*2 + 2];
			Set_temp_para(0, unStep);
			temperature_ctrl_update_temp();
		break;
			
		/*! 孵育盘温度偏移量 */
		case 2:
			unStep = (pucFrameData[0*2 + 1]);
			Set_temp_para(1, unStep);
			temperature_ctrl_update_temp();
		break;	

			
		default:
		break;
	
	}
	
	return ret;
}




/*******************************************************************************
* 名称: temp_unit_cmd_handle
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t temp_unit_cmd_handle(void)
{
    uint8_t  ret = 0;
	uint8_t  *pucDataBuffer ;
	
	pucDataBuffer  = s_stTempUnitActionInfo.stFrameInfo.ucaPara;
    
    switch (s_stTempUnitActionInfo.stFrameInfo.ucCmdId)
    {
        case CMD_TEMP_UNIT_SYSTEM:// 温控单元系统指令 0x00
        {
			ret = cmd_temp_unit_system(pucDataBuffer);
        }
		break;
		
        case CMD_TEMP_UNIT_QUERY:// 温控单元查询指令 0x01
        {
			ret = cmd_temp_unit_query(pucDataBuffer);
        }
		break;
				
        case CMD_TEMP_UNIT_CONFIG:// 温控单元配置指令 0x02
        {
			ret = cmd_temp_unit_config(pucDataBuffer);
        }
		break;		
				
		default:
		{
		}
		break;
		
	}
	
	return ret;
}


/*******************************************************************************
* 名称: temp_unit_action_init
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
static void temp_unit_action_init(void)
{
	/*! 1 复位变量 */
	memset(&s_stTempUnitActionInfo, 0, sizeof(s_stTempUnitActionInfo));
	memset(&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
}



/*******************************************************************************
* 名称: temp_unit_task
* 功能:  温控线程
* 形参:  	
* 返回: 无
* 说明: 无
********************************************************************************/
void temp_unit_task(void *p_arg)
{
    OS_ERR err;
	UINT8 ret = ERR_NONE;
    
    (void)p_arg;
    
	/*! 本单元状态初始化 */
	temp_unit_action_init();
	
    while (1)
    {	
		/*! 等待信号量触发 */
        OSSemPend(TP_Sem, 0, &err);
		
		/*!< 动作没有开始，是发给本单元的指令 */
		if(s_stTempUnitActionInfo.ucActionFinishFlag == 0 && s_stTempUnitActionInfo.stFrameInfo.ucTarget == TEMP_UNIT_ID)
		{
			ret = temp_unit_cmd_handle();
			
			/*! 当动作执行完成，上报命令执行结果 */
            check_temp_unit_cmd_result(&s_stTempUnitActionInfo, ret);
		}		

    }
	
}



#endif
