
#ifndef __CTRLTEMP_C__
#define __CTRLTEMP_C__


#include "temp_pid_ctrl.h"
#include "bsp_pump_valve.h"
#include "bsp_ds18b20.h"

/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
extern uint8_t g_ucReactionRunFlag;

#define PID_CTRL_NUM         			2
#define TEMP_PID_PERIOD             	500

/*! 孵育盘加热控制 */
#define REACTION_HEAT_ON   	        do{ bsp_pv_open(E_PV_5); bsp_pv_open(E_PV_6);}while(0)
#define REACTION_HEAT_OFF  	        do{ bsp_pv_close(E_PV_5); bsp_pv_close(E_PV_6);}while(0)


static temp_ctrl_t  s_stReactionTempCtrl[PID_CTRL_NUM] = {0};
static uint16_t     s_Temp_Tick						   = TEMP_PID_PERIOD;

 
/*! 帕尔帖(试剂盘)温度目标值 */
uint8_t g_ucPeltierCoolTempTarget = 4;
uint8_t g_ucReactionRunFlag = 1;


/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint8_t Fun_Mtx_TimCountON(uint8_t eTim)
{
    TIM_Cmd(TIM8, ENABLE);
    return e_true;
}

/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint8_t Fun_Mtx_TimCountOFF(uint8_t eTim)
{
    TIM_Cmd(TIM8, DISABLE);
    return e_true;
}


/*******************************************************************************
* 函数名称: temp_ctrl_get_temp_incube
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint16_t temp_ctrl_get_temp_incube(uint8_t ucCh)
{
	uint16_t ucTemp = 0;
	
	ucTemp = s_stReactionTempCtrl[ucCh].stPidCtrl.current;
	
	return ucTemp;
}



/*******************************************************************************
* 函数名称: calculate_pid
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
int32_t calculate_pid(uint8_t ucCh, uint8_t mode)//位置式PID
{
    int32_t error;
    int32_t value = 0,temp = 0,temp2 = 0;
	
	/*! 计算误差：目标值 和 实际值 */
    if (mode == 1)
    {
        error = (int32_t)(s_stReactionTempCtrl[ucCh].stPidCtrl.target) - (int32_t)(s_stReactionTempCtrl[ucCh].stPidCtrl.current);
    }
    else
	{
        error = (int32_t)(s_stReactionTempCtrl[ucCh].stPidCtrl.current) - (int32_t)(s_stReactionTempCtrl[ucCh].stPidCtrl.target);
	}
	
    PRO_DEBUG(TEMP_DEBUG, ("error %d last_error %d\r\n", error, s_stReactionTempCtrl[ucCh].stPidCtrl.last_error) );
	
	/*! 比例 */
    value = s_stReactionTempCtrl[ucCh].stPidCtrl.P*error;
	/*! 微分 */
    temp = s_stReactionTempCtrl[ucCh].stPidCtrl.D*(error - s_stReactionTempCtrl[ucCh].stPidCtrl.last_error);
	/*! 积分（累加） */
    temp2 = s_stReactionTempCtrl[ucCh].stPidCtrl.I * (error + s_stReactionTempCtrl[ucCh].stPidCtrl.last_error);
    
	/*! 输出值 */
    value = value + temp + temp2;
	
	/*! 记录上一次误差值 */
    TempPid[ucCh].last_error = error;
	
    return value;
}

/*******************************************************************************
* 函数名称: temperature_ctrl
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
uint8_t temperature_ctrl(uint8_t ucCh)
{
    int32_t tick = 0;
	
    Fun_Mtx_TimCountOFF(0);
	
	/*! PID运算指示灯输出 */
//    LED0 = !LED0;
	
	/*! 温度读取 */
    s_stReactionTempCtrl[ucCh].stPidCtrl.current = bsp_temp_read(ucCh);
    PRO_DEBUG(TEMP_DEBUG,( "TempPid %d %d\r\n", s_stReactionTempCtrl[ucCh].stPidCtrl.current, s_stReactionTempCtrl[ucCh].stPidCtrl.target) );
    
    tick = calculate_pid(1,1);
	
	/*! 输出限定 */
    if (tick > TEMP_PID_PERIOD)
	{
        tick = TEMP_PID_PERIOD;
	}
    else if (tick < 0)
	{
        tick = 0;
	}
	
	/*! 偏离目标过大，直接用满功率输出，加快的升温速度 */
	if(s_stReactionTempCtrl[ucCh].stPidCtrl.current < s_stReactionTempCtrl[ucCh].stPidCtrl.target - 10)
	{
		tick = TEMP_PID_PERIOD;
	}
	else if(s_stReactionTempCtrl[ucCh].stPidCtrl.current > s_stReactionTempCtrl[ucCh].stPidCtrl.target + 10)
	{
		tick = 0;
	}

	/*! 输出参数 */
    s_stReactionTempCtrl[ucCh].stPidCtrl.tick = tick;
    s_Temp_Tick = 0;
	
    PRO_DEBUG(TEMP_DEBUG,("tick = %d \r\n", tick) );
	
    Fun_Mtx_TimCountON(0);
	return 0;
}

/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temp_pid_ctrl_tick(uint8_t ucCh)
{
    s_Temp_Tick++;
	
    if (s_stReactionTempCtrl[ucCh].stPidCtrl.tick)
    {
        s_stReactionTempCtrl[ucCh].stPidCtrl.tick--;
		/*! 调用输出函数，输出高 */
        REACTION_HEAT_ON;
    }
    else
    {
		/*! 调用输出函数，输出低*/
        REACTION_HEAT_OFF;
    }
	
}



/*******************************************************************************
* 函数名称: close_ctrl_heat
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void close_ctrl_heat(void)
{
	/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
	g_ucReactionRunFlag = 0;

	REACTION_HEAT_OFF;
}


/*******************************************************************************
* 函数名称: open_ctrl_heat
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void open_ctrl_heat(void)
{
	/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
	g_ucReactionRunFlag = 1;

	REACTION_HEAT_ON;
}


/*******************************************************************************
* 函数名称: app_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_tim_init(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    NVIC_InitTypeDef        NVIC_InitStructure;

    // 配置定时器参数
//    Fun_ConfigTimStruct((eTimType)eTim);
    
    TIM_DeInit(TIM8);

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE); //时钟使能
    
    //----------------------------------------------------------------------
    //定时器初始化
    TIM_TimeBaseStructure.TIM_Period = 999; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值  
    TIM_TimeBaseStructure.TIM_Prescaler =71; //设置用来作为TIMx时钟频率除数的预分频值
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位

    //----------------------------------------------------------------------
    //中断优先级NVIC设置
    NVIC_InitStructure.NVIC_IRQChannel = TIM8_UP_IRQn;  //TIM3中断
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  //先占优先级0级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 1;  //从优先级3级
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
    NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器

    //-----------------------------------------------------------------------
    // 初始化设置
    TIM_ARRPreloadConfig(TIM8, ENABLE);
    TIM_ClearFlag(TIM8, TIM_FLAG_Update);
    
    TIM_ITConfig(TIM8, TIM_IT_Update, ENABLE); //使能指定的TIM3中断,允许更新中断

    TIM_Cmd(TIM8, ENABLE);  //使能TIMx   
	
}


/*******************************************************************************
* 函数名称: update_reaction_ctrl_temp
* 功能描述: 更新设置温度
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void update_reaction_ctrl_temp(uint8_t ucCh, uint16_t setTarget, uint16_t unOffset)
{
	/*! 输入通道检测 */
	if(ucCh >= PID_CTRL_NUM)
	{
		return ;
	}
	
	/*! 输入温度参数检查 */
	if( (setTarget >= 250) && (setTarget <= 500 ) ) 
	{
		s_stReactionTempCtrl[ucCh].stPidPara.setTarget = setTarget;
	}
	else
	{
		s_stReactionTempCtrl[ucCh].stPidPara.setTarget = 370;
	}
	
    /*! 正向5°间微调 */
    if (unOffset < 50) 
    {
        s_stReactionTempCtrl[ucCh].stPidPara.setOffset  = unOffset;   
		s_stReactionTempCtrl[ucCh].stPidCtrl.target  = s_stReactionTempCtrl[ucCh].stPidPara.setTarget + unOffset;
    }
	/*! 反向5°间微调 */
	else if ( (unOffset > 128) && (unOffset < 178))
	{
        s_stReactionTempCtrl[ucCh].stPidPara.setOffset  = unOffset;   
		s_stReactionTempCtrl[ucCh].stPidCtrl.target  = s_stReactionTempCtrl[ucCh].stPidPara.setTarget - unOffset;
        
	}
	/*! 不补偿 */
    else
    {
		s_stReactionTempCtrl[ucCh].stPidCtrl.target = s_stReactionTempCtrl[ucCh].stPidPara.setTarget;
    }
	
}



/*******************************************************************************
* 函数名称: temperature_ctrl_pid_para_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_pid_para_init(void)
{
    uint16_t P = 105, I = 20, D = 100;
    
	/*! 各个通道PID参数设置 */
	s_stReactionTempCtrl[0].stPidCtrl.P = 105;
	s_stReactionTempCtrl[0].stPidCtrl.I = 105;
	s_stReactionTempCtrl[0].stPidCtrl.D = 105;
        
	s_stReactionTempCtrl[1].stPidCtrl.P = 105;
	s_stReactionTempCtrl[1].stPidCtrl.I = 105;
	s_stReactionTempCtrl[1].stPidCtrl.D = 105;
	
	/*! 设置Pid 目标温度 */
	update_reaction_ctrl_temp(0, 370, 0);
	update_reaction_ctrl_temp(1, 370, 0);
	
	/*! pid输出函数设置 */
//	s_stReactionTempCtrl[1].stPidOps.pid_write_high = ;
	
}





/*******************************************************************************
* 函数名称: temperature_ctrl_main
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_main(void)
{
	/*! 孵育盘加热控制标志(1:启动加热 0:停止加热) */
	if (g_ucReactionRunFlag)
	{
		if (s_Temp_Tick >= TEMP_PID_PERIOD)
		{
			Temperature_Ctrl();
		}
	}
	
}

/*******************************************************************************
* 函数名称: temperature_ctrl_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值:  无
* 其他说明:  无
********************************************************************************/
void temperature_ctrl_init(void)
{
	/*! PID初始化 */
	temperature_ctrl_pid_para_init();
	
	/*! 温控Gpio初始化 */
	bsp_ds18b20_init();
	
	/*! 定时器初始化 */
	temperature_ctrl_tim_init();
	
}



#endif
