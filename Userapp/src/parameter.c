
#ifndef __PARAMETER_C__
#define __PARAMETER_C__

#include "includes.h"
#include "pro_include.h"

lSP_Para sp_para;

UINT8 Handle_Para_Cmd(void)
{
    UINT8 ret;

    switch(para_msg.op)
    {
        case CMD_PARA_SP_SET:
        {
            Set_Paramenter();
        }break;
		
        case CMD_PARA_SP_LOAD:
        {
            Load_Paramenter();
        }break;
		
        default: break;
    }
	
//    send_cmd(&para_msg);
	
    return ret;
}



void Set_SP_Para(void)
{
    sp_para.SP_rot_pos[0]   = (para_msg.data[0]<<8) + para_msg.data[1];//旋转复位偏移量
    sp_para.SP_rot_pos[1]   = (para_msg.data[2]<<8) + para_msg.data[3];//试剂位R1旋转偏移量
    sp_para.SP_rot_pos[2]   = (para_msg.data[4]<<8) + para_msg.data[5];//试剂位R2旋转偏移量
    sp_para.SP_rot_pos[3]   = (para_msg.data[6]<<8) + para_msg.data[7];//试剂位R3旋转偏移量
    sp_para.SP_rot_pos[4]   = (para_msg.data[8]<<8) + para_msg.data[9];//试剂位R4旋转偏移量
    sp_para.SP_rot_pos[5]   = (para_msg.data[10]<<8) + para_msg.data[11];//样本位旋转偏移量
    sp_para.SP_rot_pos[6]   = (para_msg.data[12]<<8) + para_msg.data[13];//清洗位旋转偏移量
    sp_para.SP_rot_pos[7]   = (para_msg.data[14]<<8) + para_msg.data[15];//反应盘加样位旋转偏移量
    
    sp_para.SP_ud_rst_pos   = (para_msg.data[16]<<8) + para_msg.data[17];//垂直复位偏移量
    
    sp_para.SP_up_pos[0]    = (para_msg.data[18]<<8) + para_msg.data[19];//试剂位R1垂直杯口偏移量
    sp_para.SP_up_pos[1]    = (para_msg.data[20]<<8) + para_msg.data[21];//试剂位R2垂直杯口偏移量
    sp_para.SP_up_pos[2]    = (para_msg.data[22]<<8) + para_msg.data[23];//试剂位R3垂直杯口偏移量
    sp_para.SP_up_pos[3]    = (para_msg.data[24]<<8) + para_msg.data[25];//试剂位R4垂直杯口偏移量
    sp_para.SP_up_pos[4]    = (para_msg.data[26]<<8) + para_msg.data[27];//样本位垂直杯口偏移量
    sp_para.SP_up_pos[5]    = (para_msg.data[28]<<8) + para_msg.data[29];//清洗位垂直杯口偏移量
    sp_para.SP_up_pos[6]    = (para_msg.data[30]<<8) + para_msg.data[31];//反应盘加样位垂直杯口偏移量
    
    sp_para.SP_dowm_pos[0]  = (para_msg.data[32]<<8) + para_msg.data[33];//试剂位R1垂直杯底偏移量
    sp_para.SP_dowm_pos[1]  = (para_msg.data[34]<<8) + para_msg.data[35];//试剂位R2垂直杯底偏移量
    sp_para.SP_dowm_pos[2]  = (para_msg.data[36]<<8) + para_msg.data[37];//试剂位R3垂直杯底偏移量
    sp_para.SP_dowm_pos[3]  = (para_msg.data[38]<<8) + para_msg.data[39];//试剂位R4垂直杯底偏移量
    sp_para.SP_dowm_pos[4]  = (para_msg.data[40]<<8) + para_msg.data[41];//样本位垂直杯底偏移量
    sp_para.SP_dowm_pos[5]  = (para_msg.data[42]<<8) + para_msg.data[43];//清洗位垂直杯底偏移量
    sp_para.SP_dowm_pos[6]  = (para_msg.data[44]<<8) + para_msg.data[45];//反应盘加样位垂直杯底偏移量
    
    sp_para.SP_z_pos        = (para_msg.data[46]<<8) + para_msg.data[47];//注射器复位偏移量
    sp_para.SP_z_value      = (para_msg.data[48]<<8) + para_msg.data[49];//注射器电机步数与液量的比值
    sp_para.SP_z_back       = (para_msg.data[50]<<8) + para_msg.data[51];//注射器回吸空气量（ul）
}

void Set_Paramenter(void)
{
    UINT16 addr = 0;
    UINT16 len = 0;
	
    switch (para_msg.op)
    {
        case CMD_PARA_SP_SET:
        {
            if (para_msg.len != 52)
            {
                para_msg.err = ERR_CMD_DATA_LEN;
                para_msg.len = 2;
                break;
            }
            addr = SP_FRAME_PAGE * FRAME_ADDR_PAGE_LEN;
            len = sizeof(lSP_Para);
            Set_SP_Para();
//            memcpy((UINT8*)&sp_para,para_msg.data,para_msg.len);
            sp_para.crc = 0;
            
            Write_FM25CL64_nByte(addr,(UINT8*)&sp_para,len);
            
            para_msg.len = 0;
        }break;
        
		default: break;
    }
}

void Load_SP_Para(void)
{
    para_msg.data[0]  = sp_para.SP_rot_pos[0]>>8;
    para_msg.data[1]  = sp_para.SP_rot_pos[0]&0xff;
    para_msg.data[2]  = sp_para.SP_rot_pos[1]>>8;
    para_msg.data[3]  = sp_para.SP_rot_pos[1]&0xff;
    para_msg.data[4]  = sp_para.SP_rot_pos[2]>>8;
    para_msg.data[5]  = sp_para.SP_rot_pos[2]&0xff;
    para_msg.data[6]  = sp_para.SP_rot_pos[3]>>8;
    para_msg.data[7]  = sp_para.SP_rot_pos[3]&0xff;
    para_msg.data[8]  = sp_para.SP_rot_pos[4]>>8;
    para_msg.data[9]  = sp_para.SP_rot_pos[4]&0xff;
    para_msg.data[10] = sp_para.SP_rot_pos[5]>>8;
    para_msg.data[11] = sp_para.SP_rot_pos[5]&0xff;
    para_msg.data[12] = sp_para.SP_rot_pos[6]>>8;
    para_msg.data[13] = sp_para.SP_rot_pos[6]&0xff;
    para_msg.data[14] = sp_para.SP_rot_pos[7]>>8;
    para_msg.data[15] = sp_para.SP_rot_pos[7]&0xff;
    
    para_msg.data[16] = sp_para.SP_ud_rst_pos>>8;
    para_msg.data[17] = sp_para.SP_ud_rst_pos&0xff;
    
    para_msg.data[18] = sp_para.SP_up_pos[0]>>8;
    para_msg.data[19] = sp_para.SP_up_pos[0]&0xff;
    para_msg.data[20] = sp_para.SP_up_pos[1]>>8;
    para_msg.data[21] = sp_para.SP_up_pos[1]&0xff;
    para_msg.data[22] = sp_para.SP_up_pos[2]>>8;
    para_msg.data[23] = sp_para.SP_up_pos[2]&0xff;
    para_msg.data[24] = sp_para.SP_up_pos[3]>>8;
    para_msg.data[25] = sp_para.SP_up_pos[3]&0xff;
    para_msg.data[26] = sp_para.SP_up_pos[4]>>8;
    para_msg.data[27] = sp_para.SP_up_pos[4]&0xff;
    para_msg.data[28] = sp_para.SP_up_pos[5]>>8;
    para_msg.data[29] = sp_para.SP_up_pos[5]&0xff;
    para_msg.data[30] = sp_para.SP_up_pos[6]>>8;
    para_msg.data[31] = sp_para.SP_up_pos[6]&0xff;
    
    para_msg.data[32] = sp_para.SP_dowm_pos[0]>>8;
    para_msg.data[33] = sp_para.SP_dowm_pos[0]&0xff;
    para_msg.data[34] = sp_para.SP_dowm_pos[1]>>8;
    para_msg.data[35] = sp_para.SP_dowm_pos[1]&0xff;
    para_msg.data[36] = sp_para.SP_dowm_pos[2]>>8;
    para_msg.data[37] = sp_para.SP_dowm_pos[2]&0xff;
    para_msg.data[38] = sp_para.SP_dowm_pos[3]>>8;
    para_msg.data[39] = sp_para.SP_dowm_pos[3]&0xff;
    para_msg.data[40] = sp_para.SP_dowm_pos[4]>>8;
    para_msg.data[41] = sp_para.SP_dowm_pos[4]&0xff;
    para_msg.data[42] = sp_para.SP_dowm_pos[5]>>8;
    para_msg.data[43] = sp_para.SP_dowm_pos[5]&0xff;
    para_msg.data[44] = sp_para.SP_dowm_pos[6]>>8;
    para_msg.data[45] = sp_para.SP_dowm_pos[6]&0xff;
    
    para_msg.data[46] = sp_para.SP_z_pos>>8;
    para_msg.data[47] = sp_para.SP_z_pos&0xff;
    para_msg.data[48] = sp_para.SP_z_value>>8;
    para_msg.data[49] = sp_para.SP_z_value&0xff;
    para_msg.data[50] = sp_para.SP_z_back>>8;
    para_msg.data[51] = sp_para.SP_z_back&0xff;

}

void Load_Paramenter(void)
{
    UINT16 addr = 0;
    UINT16 len = 0;
	
    switch (para_msg.op)
    {
        case CMD_PARA_SP_LOAD:
        {
            if (para_msg.len != 0)
            {
                para_msg.err = ERR_CMD_DATA_LEN;
                para_msg.len = 2;
                break;
            }
            addr = SP_FRAME_PAGE * FRAME_ADDR_PAGE_LEN;
            len = sizeof(lSP_Para);
//            memcpy((UINT8*)&sp_para,para_msg.data,para_msg.len);
            Read_FM25CL64_nByte(addr,(UINT8*)&sp_para,len);
            Load_SP_Para();
            sp_para.crc = 0;
            para_msg.len = 52;
        }break;
        
		default: break;
    }
}

void Load_SysPara(void)
{
    para_msg.len = 0;
    para_msg.op = CMD_PARA_SP_LOAD;
    Load_Paramenter();
}

#endif
