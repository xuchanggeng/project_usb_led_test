/*
 * Copyright (c) 2016 Zibin Zheng <znbin@qq.com>
 * All rights reserved
 */

#include <stdio.h>
#include "multi_timer.h"

//timer handle list head.
static struct Timer* head_handle = NULL;

//Timer ticks
//static uint32_t _timer_ticks = (1 << 32)- 1000; // only for test tick clock overflow
static uint32_t _timer_ticks = 0;




/**
  * @brief  Initializes the timer struct handle.
  * @param  handle: the timer handle strcut.
  * @param  timeout_cb: timeout callback.
  * @param  timeout: delay to start the timer.
  * @param  repeat: repeat interval time.
  * @param  arg: the input argument for timeout_cb fucntion.
  * @retval None
  */
void timer_init(struct Timer* handle, void (*timeout_cb)(void *arg), \
      uint32_t timeout, uint32_t repeat, void *arg)
{

    handle->timeout_cb          = timeout_cb;      
    handle->timeout             = timeout;
    handle->repeat              = repeat;
    handle->cur_ticks           = _timer_ticks;  
    handle->cur_expired_time    = handle->timeout;
    handle->arg                 = arg;
	
}



/**
  * @brief  Start the timer work, add the handle into work list.
  * @param  btn: target handle strcut.
  * @retval 0: succeed. -1: already exist.
  */
int timer_start(struct Timer* handle)
{
    struct Timer* target = head_handle;

    while(target) {
        if(target == handle) {
            return -1;  //already exist.
        }            
        target = target->next;
    }
    handle->next = NULL;
    head_handle  = handle;

    return 0;
}



/**
  * @brief  Start the timer work, add the handle into work list.
  * @param  btn: target handle strcut.
  * @retval 0: succeed. -1: already exist.
  */
int timer_init_start(struct Timer* handle, void (*timeout_cb)(void *arg), \
                     uint32_t timeout, uint32_t repeat, uint32_t mode, void *arg)
{
	int lRet = 0;
	
	/*! 初始化Timer结构体 */
	timer_init(handle, timeout_cb, timeout, repeat, arg);
	
	/*! 定时器模式 */
	handle->time_mode = mode;
	
	/*! Timer插入队列 */
	lRet = timer_start(handle);

	return lRet;
}


/**
  * @brief  Start the timer work, add the handle into work list.
  * @param  btn: target handle strcut.
  * @retval 0: succeed. -1: already exist.
  */
int timer_single_start(struct Timer* handle, void (*timeout_cb)(void *arg), \
                     uint32_t timeout, uint32_t repeat, void *arg)
{
	int lRet = 0;
	
	(void)repeat;
	
	/*! 初始化Timer结构体 */
	timer_init(handle, timeout_cb, timeout, 1, arg);
	
	handle->time_mode = TIMER_COUNT_MODE;
	
	/*! Timer插入队列 */
	lRet = timer_start(handle);

	return lRet;
}





/**
  * @brief  Stop the timer work, remove the handle off work list.
  * @param  handle: target handle strcut.
  * @retval 0: succeed. -1: timer not exist.
  */
int timer_stop(struct Timer* handle)
{
    struct Timer** curr;

    for(curr = &head_handle; *curr;) {
        struct Timer* entry = *curr;
        if(entry == handle) {
            *curr = entry->next;
            //free(entry);
            return 0; // found specified timer
        } else {
            curr = &entry->next;
        }            
    }

    return 0;
}



/**
  * @brief  timer main loop.
  * @param  None.
  * @retval None
  */
void timer_loop(void)
{
    struct Timer* target;

    for(target = head_handle; target; target = target->next) {
        /* 
        More detail on tick-clock overflow, please see https://blog.csdn.net/szullc/article/details/115332326
        */
        if(_timer_ticks - target->cur_ticks >= target->cur_expired_time) {
            printf("cur_ticks: %u, cur_expired_time: %u, _timer_ticks: %u\r\n", target->cur_ticks, target->cur_expired_time, _timer_ticks);
            
			switch(target->time_mode)
			{
				/*! 计数模式 */
				case TIMER_COUNT_MODE:
					target->repeat--;		
					if(target->repeat == 0) 
					{
						timer_stop(target);
					}

				break;
				
				/*! 无限模式 */
				case TIMER_INFINITE_MODE:
				break;
				
				default:
				break;
			
			}
			
			/*! 更新时间 */
			target->cur_ticks = _timer_ticks;
			target->cur_expired_time = target->timeout;
			
            target->timeout_cb(target->arg);
        }
    }
}



/**
  * @brief  background ticks, timer repeat invoking interval nms.
  * @param  None.
  * @retval None.
  */
void timer_ticks(void)
{
    _timer_ticks += CFG_TIMER_1_TICK_N_MS;
}



/**
  * @brief  background ticks, timer repeat invoking interval nms.
  * @param  None.
  * @retval None.
  */
void timer_ticks_run(void)
{
	/*! 定时器时间 */
	timer_ticks();
	
	timer_loop();
}








