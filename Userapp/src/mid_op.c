
#ifndef __MID_OP_C__
#define __MID_OP_C__

#include "includes.h"
#include "pro_include.h"

_EXT_ OS_EVENT * SP_Sem;

UINT8 set_debug = 0;

/*! 处理系统命令 */
static void sys_cmd(void)
{
    UINT8 ucIntex = 0;
	
    switch (sys_msg.op)
    {
		/*! 握手命令(获取MCU软件版本号) */
        case CMD_SYS_HANDLE:
        {
			/*! 协议数据长度错误报警 */
            if (sys_msg.len != 0)
            {
                sys_msg.err = ERR_CMD_DATA_LEN;
                sys_msg.len = 2;
                break;
            }
			
            sys_msg.data[ucIntex++] = PRO_VERSION;
			sys_msg.data[ucIntex++] = PRO_SUBVERSION;
			sys_msg.data[ucIntex++] = PRO_REVISION1;
			sys_msg.data[ucIntex++] = PRO_REVISION2;
            sys_msg.len = ucIntex;
        }break;
		
		/*! 开启/关闭MCU调试功能 */
        case CMD_SYS_DEBUG:
        {
            if (sys_msg.len != 1)
            {
                sys_msg.err = ERR_CMD_DATA_LEN;
                sys_msg.len = 2;
                break;
            }
            set_debug = sys_msg.data[0];
            sys_msg.len = 0;
        }break;
            
        default:
		{
            sys_msg.err = ERR_CMD_TYPE;
            sys_msg.len = 2;
			
		}break;
    }
	
//    send_cmd(&sys_msg);
}

/*! 处理泵阀命令 */
static void pv_cmd(void)
{
	UINT8 i;
    UINT8 retbuf[8] = {0};
    UINT8 ucPvId = 0, ucPvState = 1;
	UINT16 unPvTimeOut = 0;
	
    switch (pv_msg.op)
    {
		 /*! 泵阀控制开关命令0x10 20190425 */
        case CMD_PV_RT_CTRL:
        {
			/*! 协议数据长度错误报警 */
            if (pv_msg.len != 4)
            {
                pv_msg.err = ERR_CMD_DATA_LEN;
                pv_msg.len = 2;
                break;
            }
            
			/*! 参数1：泵阀ID号(1：针内壁泵 2：针外壁泵 
			    3：针内壁阀 4：废液泵 5：储杯盒直流电机控制)	*/
			/*! 参数2：开关状态(0：打开 1：关闭)
				参数3：泵阀超时时间 */
			ucPvId = pv_msg.data[0] - 1;
			ucPvState = pv_msg.data[1];
			unPvTimeOut = (pv_msg.data[2] << 8) + (pv_msg.data[3] & 0xFF);
			
			if (ucPvId >= E_PV_MAX )
			{
				pv_msg.err = ERR_CMD_DATA_TYPE;
                pv_msg.len = 2;
                break;
			}
			
            if (ucPvState)
            {
                bsp_pv_close( (ePV_Type)(pv_msg.data[0] - 1) );
            }
            else
			{
                bsp_pv_open( (ePV_Type)(pv_msg.data[0] - 1) );
				bsp_pv_timeout_check((ePV_Type)ucPvId, unPvTimeOut);
			}
        }break;
		
		/*! 泵阀状态查询命令0x111 20190425 */
        case CMD_PV_RT_CHECK:
        {
            if (pv_msg.len != 1)
            {
                pv_msg.err = ERR_CMD_DATA_LEN;
                pv_msg.len = 1;
                break;
            } 
			/*! 参数1：泵阀ID号(1：针内壁泵 2：针外壁泵 
			    3：针内壁阀 4：废液泵 5：储杯盒直流电机
				6：储杯盒上杯子有无检测光耦
				7：储杯盒下杯子有无检测光耦)
				8：废杯盒杯子满有无检测光耦 */
			ucPvId = pv_msg.data[0] - 1; 
			
			/*! 泵阀ID超出范围 */
			if (ucPvId >= E_PV_MAX)
			{
				pv_msg.err = ERR_CMD_DATA_TYPE;
                pv_msg.len = 2;
                break;
			}
			
			retbuf[0] = pv_msg.data[0];
			
			/*! 泵阀状态(0：打开 1：关闭) */
            retbuf[1] = bsp_pv_get_state( (ePV_Type)(ucPvId) );
            
			if ( (ucPvId == E_PV_7) || (ucPvId == E_PV_8) )
			{
				retbuf[1] = !retbuf[1];
			}
			
            pv_msg.len = 2;
            for (i = 0; i < pv_msg.len; i++)
			{
                pv_msg.data[i] = retbuf[i];
			}
        }break; 

		default:
		{
            pv_msg.err = ERR_CMD_TYPE;
            pv_msg.len = 2;
			
		}break;
    }
	
//    send_cmd(&pv_msg);
}

// 处理接收到的信息
void Handle_Msg(UINT8 *Recieve_buf, UINT16 msg_len)
{
    UINT16 data_len = 0;
    UINT8  Cmd = 0, op_cmd = 0;
    UINT8 *buf = Recieve_buf;
    UINT8  ret = ERR_NONE;
	
    //==================================================
    // 接收数据的判断
    //==================================================
    if (msg_len < 7)
    {
        ret = ERR_FRAM_LEN;
    }
    else if (buf[0] != FRAM_HEAD_DATA)
    {
        ret = ERR_FRAM_HEAD;
    }
    else if (buf[1] != FRAM_CTRL_DATA_M_S)
    {
        ret = ERR_FRAM_CTRL;
    }
    else if (buf[msg_len-1] != FRAM_TAIL_DATA)
    {
        ret = ERR_FRAM_TAIL;
    }
    
    data_len = (Recieve_buf[2] << 8) | Recieve_buf[3];
    Cmd = Recieve_buf[4];
    op_cmd = Cmd >> 4;
	
    if (ret == ERR_NONE)
    {
        switch (op_cmd)
        {
            //=======================================
            // 系统控制命令
            //=======================================
            case SYS_CMD:
            {
                sys_msg.op = Cmd;
                sys_msg.len = data_len - 1;
                memcpy(sys_msg.data, &Recieve_buf[5], sys_msg.len);
                sys_cmd();
            }break;
            
            //=======================================
            // 泵阀控制命令
            //=======================================
            case PV_CMD:
            {
                pv_msg.op = Cmd;
                pv_msg.len = data_len - 1;
                memcpy(pv_msg.data, &Recieve_buf[5], pv_msg.len);
                pv_cmd();
            }break;
            
            //=======================================
            // 加样针电机控制命令
            //=======================================
            case SP_CMD:
            {
//                queue_in(SP_QueBuf, Recieve_buf, msg_len);
#if SYSTEM_SUPPORT_OS
                OSSemPost(SP_Sem);                                  
#else
                sp_msg.op = Cmd;
                sp_msg.len = data_len - 1;
                memcpy(sp_msg.data, &Recieve_buf[5], sp_msg.len);
                Handle_SP_Cmd();
#endif
            }break;
			
            case PARA_CMD:
            {
                para_msg.op = Cmd;
                para_msg.len = data_len - 1;
                memcpy(para_msg.data, &Recieve_buf[5], para_msg.len);
                //Handle_Para_Cmd();
            }break;
			
            default: break;
        }
    }
    else
    {
        sys_msg.op = Cmd;
        sys_msg.err = ret;
        sys_msg.len = 2;
//        send_cmd(&sys_msg);
		PRO_DEBUG(ERR_DEBUG,("packet check err[%d] \r\n", ret));
    }
}

#endif
