/**
  ******************************************************************************
  * @文件   cmd_send_frame.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "cmd_send_frame.h"
#include "bsp_can_msp.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/

/* 函数原型 -----------------------------------------------*/

/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 名称: send_cmd_status_frame
* 功能: 发送命令帧
			unDes ： 目的地址;
			pucData ： 命令数据;
			ucDataLen ： 命令长度;
* 形参: 无
* 返回: 0 执行成功，1执行失败
* 说明: 无
********************************************************************************/
uint8_t send_cmd_status_frame(uint16_t unDes, uint8_t ucCmd_Addr, uint8_t *pucData, uint8_t ucDataLen)
{
  uint8_t ucRet = 0;
 uint16_t _ucTargetId  = 0; 
 uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;

  /*! 参数填充 */
  _ucTargetId = unDes;									//目的地址
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;				//本机地址
  _ucC1C0     = CONTROL_C1C0_STATUS_REG_READ;			//控制字，状态信息还回
  _ucCmdRegAddr = ucCmd_Addr;							//命令编号


  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, pucData, ucDataLen);

  return ucRet;
}


/*******************************************************************************
* 名称: send_cmd_data_frame
* 功能: 发送命令帧
			unDes ： 目的地址;
			pucData ： 命令数据;
			ucDataLen ： 命令长度;
* 形参: 无
* 返回: 0 执行成功，1执行失败
* 说明: 无
********************************************************************************/
uint8_t send_cmd_data_frame(uint16_t unDes, uint8_t ucCmd_Addr, uint8_t *pucData, uint8_t ucDataLen)
{
  uint8_t ucRet = 0;
 uint16_t _ucTargetId  = 0; 
 uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;

  /*! 参数填充 */
  _ucTargetId = unDes;									//目的地址
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;				//本机地址
  _ucC1C0     = CONTROL_C1C0_DATA_REG_READ;			//控制字，状态信息还回
  _ucCmdRegAddr = ucCmd_Addr;							//命令编号


  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, pucData, ucDataLen);

  return ucRet;
}


/*******************************************************************************
* 名称: send_cmd_op_frame
* 功能: 发送命令帧
			unDes ： 目的地址;
			pucData ： 命令数据;
			ucDataLen ： 命令长度;
* 形参: 无
* 返回: 0 执行成功，1执行失败
* 说明: 无
********************************************************************************/
uint8_t send_cmd_op_frame(uint16_t unDes, uint8_t ucCmd_Addr, uint8_t *pucData, uint8_t ucDataLen)
{
  uint8_t ucRet = 0;
  uint16_t _ucTargetId  = 0; 
  uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;

  /*! 参数填充 */
  _ucTargetId = unDes;									//目的地址
  _ucSourceId = CMD_PROCESS_UNIT_CAN_ID;				//本机地址
  _ucC1C0     = CONTROL_C1C0_CMD_OPERATION;			//控制字，状态信息还回
  _ucCmdRegAddr = ucCmd_Addr;							//命令编号


  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, pucData, ucDataLen);

  return ucRet;

}


/*******************************************************************************
* 名称: send_echo_frame
* 功能: 发送命令帧
			unDes ： 目的地址;
			pucData ： 命令数据;
			ucDataLen ： 命令长度;
* 形参: 无
* 返回: 0 执行成功，1执行失败
* 说明: 无
********************************************************************************/
uint8_t send_echo_frame(frame_info_t * stFrame)
{
  uint8_t ucRet = 0;
  uint16_t _ucTargetId  = 0; 
  uint16_t _ucSourceId  = 0; 
  uint8_t _ucC1C0       = 0;
  uint8_t _ucCmdRegAddr = 0;

  /*! 参数填充 */
  _ucTargetId = stFrame->ucSourceID;									//目的地址
  _ucSourceId = stFrame->ucTargetID;				//本机地址
  _ucC1C0     = stFrame->ucC1C0;			//控制字，状态信息还回
  _ucCmdRegAddr = stFrame->ucCMD_RegAddr;							//命令编号


  /*! 发送数据帧 */
  bsp_can_send_frame_separate(_ucTargetId, _ucSourceId, _ucC1C0, _ucCmdRegAddr, stFrame->ucaData, stFrame->ucDataLen);

  return ucRet;

}

