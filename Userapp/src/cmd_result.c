/**
  ******************************************************************************
  * @文件   cmd_result.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "cmd_result.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 ---------------------------------------------------------------*/
/* 本地宏定义 -------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
/* 函数原型 ----------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/



/*******************************************************************************
* 名称:  check_sampro_cmd_result
* 功能:  指令执行结果处理， 发送结果帧
* 形参:  
			stAction    单元动作信息结构体
			ucRet		单元的动作结果

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t  get_redefine_motor_no(uint8_t ucMotorNo)
{
	uint8_t ucRet = 0;
	
	switch(ucMotorNo)
	{
//		case SAMP_HORZ_MT:
//			ucRet = SAMP_PROBE_HORZ_MOTOR;
//		break;
//		
//		case SAMP_LOAD_MT:
//			ucRet = SAMP_PROBE_LOAD_MOTOR;
//		break;
//	
//		case SAMP_SYRI_MT:
//			ucRet = SAMP_PROBE_SYRI_MOTOR;
//		break;
//		
//		case SAMP_PICK_MT:
//			ucRet = SAMP_PROBE_PICK_MOTOR;
//		break;
//	
//		case SAMP_DSYR_MT:
//			ucRet = DYEING_SYRINGE_MOTOR;
//		break;
		
		default:
			ucRet = ucMotorNo;
			break;
		
	}
	
	return ucRet;
}


/*******************************************************************************
* 名称: SetCmdFrameResult
* 功能: 设置命令帧的执行结果
			ucResult  = 0;
			1命令执行结果 2单元编号 3电机编号 4错误代码 
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SetCmdFrameResult(const frame_info_t* cmd,cmd_result_t *pstFrameResult, uint8_t ucResult, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr)
{
	pstFrameResult->ucState  = UNIT_ACTION_FINISH;
	pstFrameResult->moduleID = ucUnit;
	pstFrameResult->ucResult = (ucMotor<<8) + ucErr;
//	pstFrameResult->cmdID = cmd->ucCmdId;
//    pstFrameResult->para = cmd->ucaPara[0];
}


