/**
  ******************************************************************************
  * @文件   three_led.c
  * @作者   Xcg
  * @版本   V1.01.005
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
#include "three_led.h"

/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
#define NUM_COLOR_LEDS                 3
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
static three_led_control_t s_stTricolorLed[NUM_COLOR_LEDS] = {0};
/* 外部变量导入 -----------------------------------------------------------*/
/* 外部函数导入 -----------------------------------------------------------*/
/* 函数原型 --------------------------------------------------------------*/

/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 函数名称: three_led_gpio_init
* 功能描述: 示例
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
void three_led_hw_init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

}


/*******************************************************************************
* 函数名称: module_unit_test
* 功能描述: 模块单元测试，用于测试本单元的功能
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/

void three_led_init(void)
{
  three_led_hw_init();
  start_three_led(0x07, 2, 1000, 1000);
}

/*******************************************************************************
* 函数名称: module_unit_test
* 功能描述: 模块单元测试，用于测试本单元的功能
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void set_led_output_state(uint8_t ucLedName, uint8_t ucLedState)
{
  if(ucLedName == 0)
  {
    GPIO_WriteBit(GPIOB, GPIO_Pin_10, ucLedState);
  }
  else if(ucLedName == 1)
  {
    GPIO_WriteBit(GPIOB, GPIO_Pin_11, ucLedState);
  }
  else if(ucLedName == 2)
  {
    GPIO_WriteBit(GPIOB, GPIO_Pin_12, ucLedState);
  }

}


static void all_led_output_disable(void)
{
  set_led_output_state(0, 1);
  set_led_output_state(1, 1);
  set_led_output_state(2, 1);
}

static void all_led_output_enable(void)
{
  set_led_output_state(0, 0);
  set_led_output_state(1, 0);
  set_led_output_state(2, 0);
}

/*******************************************************************************
* 函数名称: module_unit_test
* 功能描述: 模块单元测试，用于测试本单元的功能
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/
static void led_output_toggle(uint8_t ucLedName)
{
  if(ucLedName == 0)
  {
    GPIOB->ODR =GPIOB->ODR ^ GPIO_Pin_10;
  }
  else if(ucLedName == 1)
  {
    GPIOB->ODR =GPIOB->ODR ^ GPIO_Pin_11;
  }
  else if(ucLedName == 2)
  {
    GPIOB->ODR =GPIOB->ODR ^ GPIO_Pin_12;
  }

}

/*******************************************************************************
* 函数名称: module_unit_test
* 功能描述: 模块单元测试，用于测试本单元的功能
* 输入参数: 
           ledMode：0 全部关闭
             1 全部开启
             2 当个led以指定的频率，闪烁指定的次数
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/

int8_t start_three_led(uint8_t ledType, uint8_t ledMode, uint16_t _ledFreq, uint16_t _ledCount)
{
  
  memset(&s_stTricolorLed, 0, sizeof(s_stTricolorLed));

  if(ledMode == 0)
  {
    all_led_output_disable();
    return  0;
  }
  else if(ledMode == 1)
  {
    all_led_output_enable();
    return  0;
  }

  /*!  */
  if( (ledType&0x01) != 0x00)
  {
      s_stTricolorLed[0].ucState  = 1;
      s_stTricolorLed[0].ledCount = _ledCount;
      s_stTricolorLed[0].ledFreq  = _ledFreq;
  } 

  if( (ledType & 0x02) != 0x00)
  {
      s_stTricolorLed[1].ucState  = 1;
      s_stTricolorLed[1].ledCount = _ledCount;
      s_stTricolorLed[1].ledFreq  = _ledFreq;
  }

  if( (ledType & 0x04) != 0x00)
  {
      s_stTricolorLed[2].ucState = 1;
      s_stTricolorLed[2].ledCount = _ledCount;
      s_stTricolorLed[2].ledFreq = _ledFreq;
  }


    return 0;
}


/*******************************************************************************
* 函数名称: three_led_ticks
* 功能描述: 定时器闪烁评率，以1ms为时间单位
* 输入参数: 无
* 输出参数: 无
* 返 回 值: 无
* 其他说明: 无
********************************************************************************/

void three_led_ticks(uint16_t error)
{
  uint8_t ucIndex = 0;

  for( ucIndex = 0; ucIndex < NUM_COLOR_LEDS; ucIndex++)
  {
    if(s_stTricolorLed[ucIndex].ucState == 1)
    {
      /*!  */
      s_stTricolorLed[ucIndex].ulFreqTicks++;
      if(s_stTricolorLed[ucIndex].ulFreqTicks >= s_stTricolorLed[ucIndex].ledFreq)
      {
        s_stTricolorLed[ucIndex].ulFreqTicks = 0;

        led_output_toggle(ucIndex);

        /*! LED闪烁次数计数 */
        s_stTricolorLed[ucIndex].ulCountTicks++;
        if(s_stTricolorLed[ucIndex].ulCountTicks >= s_stTricolorLed[ucIndex].ledCount*2)
        {
          s_stTricolorLed[ucIndex].ulCountTicks = 0;
          set_led_output_state(ucIndex, 1);

          /*! 关闭LED */
          s_stTricolorLed[ucIndex].ucState = 0;
        }
      }
    }
  
  }

}
