
#ifndef __cmd_process_C__
#define __cmd_process_C__

#include "includes.h"
#include "pro_include.h"
#include "cmd_result.h"
#include "motor_para.h"
#include "bsp_pump_valve.h"
#include "bsp_motor_cmd.h"
#include "cmd_send_frame.h"
#include "three_led.h"
#include "Com_Prase.h"

_EXT_ OS_EVENT * DS_Sem;
//extern tMtxState   	  MtxState[MTx_NUM];
//extern motor_para_t   s_ucMotorOffsetPara;
/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
#define SALINE_MOTOR_SPEED 				19200
#define WATER_MOTOR_SPEED  				19200

/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/
action_info_t  			  s_stCmdProcessActionInfo 		    = {0};
static cmd_result_t       s_stCmdActionResult 		    	= {0};					//本地命令执行结果信息，


/* 函数原型 ---------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 名称: get_cmd_process_action_state
* 功能:  获取命令执行动作状态
* 形参:  
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t get_cmd_process_action_state(void)
{
	return s_stCmdProcessActionInfo.ucUnitActionState ;
}


/*******************************************************************************
* 名称: start_cmd_process_action
* 功能:  发送信号量，使命令执行开始执行数据帧中对应的动作
* 形参:  	
			pstFrameInfo   数据帧
			pstFrameResult 结果数据
			ucISack        是否需要结果

* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t start_cmd_process_action(frame_info_t *pstFrame, uint8_t ucISack)
{
	uint8_t  ret = 0;
	
	/*! 1 判断单元是否正在执行动作, 一个单元同一个时间只能执行一个动作*/
	if(s_stCmdProcessActionInfo.ucUnitActionState != UNIT_ACTION_IDLE)
	{
		ret = ERR_UNIT_ACT_EXE;
		return EXIT_FAILURE;
	}
	
	/*! 2 是否是本单元的指令*/
	if(pstFrame->ucTargetID !=  CMD_MID_PART_UNIT_CAN_ID)
	{
		ret = ERR_UNIT_NO_THIS_UNIT;
		return EXIT_FAILURE;
	}
	
	/*! 3  接受命令数据 */
	memset((uint8_t*)&s_stCmdProcessActionInfo, 0, sizeof(s_stCmdProcessActionInfo));
	s_stCmdProcessActionInfo.stFrameInfo = *pstFrame;
	s_stCmdProcessActionInfo.ucUnitActionState = UNIT_ACTION_START;
	s_stCmdProcessActionInfo.ucIsAck = ucISack;
	
	/*! 4 设置命令执行结果的初始状态 */
	memset((uint8_t*)&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
	
	/*! 5 发送信号量 */
	OSSemPost(DS_Sem);   
	
	return ret;
}


/*******************************************************************************
* 名称:  check_cmd_process_cmd_result
* 功能:  读状态寄存器
* 形参:  
			stAction 本单元动作执行指令
			ucResult 
          
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
void check_cmd_process_cmd_result(action_info_t *stAction,  uint8_t ucResult)
{
	/*! 命令执行有错误，发送命令执行结果 */
	stAction->ucUnitActionState = UNIT_ACTION_IDLE;

}


/*******************************************************************************
* 名称:  control_c1c0_status_reg_read
* 功能:  读状态寄存器
* 形参:  
			stFrame  命令帧
          
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t control_c1c0_status_reg_read(frame_info_t * stFrame)
{
	uint8_t ret = 0;


	return ret;

}

/*******************************************************************************
* 名称:  send_data_frame_to_host
* 功能:  发送数据帧到PC
* 形参:  
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
void send_data_frame_to_host(uint16_t unDes, uint8_t ucCmd_Addr, uint8_t *pucData, uint8_t ucDataLen)
{
    frame_info_t stFrame = {0};
    uint8_t    ucIndex = 0;
    
    stFrame.ucTargetID    = unDes;									//目的地址
    stFrame.ucSourceID    = CMD_MID_PART_UNIT_CAN_ID;				//本机地址
    stFrame.ucC1C0        = CONTROL_C1C0_DATA_REG_READ;			//控制字，状态信息还回
    stFrame.ucCMD_RegAddr = ucCmd_Addr;							//命令编号
    stFrame.ucDataLen     = ucDataLen;
    
    for(ucIndex = 0; ucIndex < ucDataLen; ucIndex++)
    {
        stFrame.ucaData[ucIndex] = pucData[ucIndex];
    }
    
    send_frame_to_host(&stFrame);
}

/*******************************************************************************
* 名称:  control_c1c0_data_reg_read
* 功能:  读数据寄存器控制字
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t control_c1c0_data_reg_read(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t  ucCmdType = 0;
	uint8_t  ucData[5] = {0};

	/*! stFrame->ucCMD_RegAddr 必须为0x00 */

	ucCmdType = stFrame->ucaData[0];
	switch(ucCmdType)
	{
		/*! 读取路由单元软件版本号 */
		case 0:
		ucData[0] = ucCmdType;
		ucData[1] = PRO_VERSION&0xFF;
		ucData[2] = PRO_SUBVERSION&0xFF;
		ucData[3] = PRO_REVISION1&0xFF;
		ucData[4] = PRO_REVISION2&0xFF;

		send_data_frame_to_host(PC_PART_UNIT_CAN_ID, stFrame->ucCMD_RegAddr, ucData, 5);
		break;

		/*! 调节电压输出的数字电位器的值 */
		case 1:
		break;

        default:
            break;
    }

	return ret;
}



/*******************************************************************************
* 名称:  control_c1c0_data_reg_write
* 功能:  写数据寄存器控制字
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t control_c1c0_data_reg_write(frame_info_t * stFrame)
{
	uint8_t ret = 0;


	return ret;
}


/*******************************************************************************
* 名称:  cmd_process_saline_motor_op
* 功能:  生理盐水电机操作指令
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_process_saline_motor_op(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;

	switch(ucCmdType)
	{
		/*! 复位指令 */
		case 0:

		break;

		/*! 使能指令 */
		case 1:
//			bsp_motor_cmd_enable(SALINE_PERFUSION_MOTOR);
		break;

		/*! 停止指令 */
		case 2:
//			bsp_motor_cmd_stop(SALINE_PERFUSION_MOTOR);
		break;

		/*! 脱机指令 */
		case 3:
//			bsp_motor_cmd_off(SALINE_PERFUSION_MOTOR);
		break;

		default:
		break;	

	}

	return ret;

}



/*******************************************************************************
* 名称:  cmd_process_saline_motor_run_speed
* 功能:  生理盐水电机指定速度运行指令
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_process_saline_motor_run_speed(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucDir     = 0;
	float   lMotorSpeed = 0;

	ucDir = stFrame->ucaData[1];
	lMotorSpeed = ucDir*SALINE_MOTOR_SPEED;

	ucCmdType = stFrame->ucaData[0];

	switch(ucCmdType)
	{
		case 0:
//			bsp_motor_cmd_move_speed(SALINE_PERFUSION_MOTOR, lMotorSpeed);
		break;

		case 1:
		break;

		default:
		break;	

	}
	return ret;
}


/*******************************************************************************
* 名称:  cmd_process_water_motor_op
* 功能:  无菌水电机操作指令
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_process_water_motor_op(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;

	switch(ucCmdType)
	{
		/*! 复位指令 */
		case 0:

		break;

		/*! 使能指令 */
		case 1:
//			bsp_motor_cmd_enable(WATER_PERFUSION_MOTOR);
		break;

		/*! 停止指令 */
		case 2:
//			bsp_motor_cmd_stop(WATER_PERFUSION_MOTOR);
		break;

		/*! 脱机指令 */
		case 3:
//			bsp_motor_cmd_off(WATER_PERFUSION_MOTOR);
		break;

		default:
		break;	

	}

	return ret;
}


/*******************************************************************************
* 名称:  cmd_process_water_motor_reject_vol
* 功能:  采无菌水电机注射指定的液量指令
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_process_water_motor_reject_vol(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucDir     = 0;
	uint32_t unVol    = 0;
	float   lMotorSpeed = 0;

	ucDir = stFrame->ucaData[1];
	lMotorSpeed = ucDir*SALINE_MOTOR_SPEED;

    unVol =  (stFrame->ucaData[2]<<8) + stFrame->ucaData[3];
	unVol = unVol * WATER_UL_TO_STEP;
	ucCmdType = stFrame->ucaData[0];

	switch(ucCmdType)
	{
		case 0:
			/*! 电机运行指定的距离 */
//			bsp_motor_cmd_move_pos(WATER_PERFUSION_MOTOR, unVol);
			// bsp_motor_cmd_move_speed(WATER_PERFUSION_MOTOR, lMotorSpeed);
		break;

		case 1:
		break;

		default:
		break;	

	}

	return ret;
}

/*******************************************************************************
* 名称:  cmd_process_water_motor_run_speed
* 功能:  无菌水电机指定速度运行指令
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_process_water_motor_run_speed(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;
	uint8_t ucDir     = 0;
	float   lMotorSpeed = 0;

	ucDir = stFrame->ucaData[1];
	lMotorSpeed = ucDir*SALINE_MOTOR_SPEED;

	ucCmdType = stFrame->ucaData[0];

	switch(ucCmdType)
	{
		case 0:
//			bsp_motor_cmd_move_speed(WATER_PERFUSION_MOTOR, lMotorSpeed);
		break;

		case 1:
		break;

		default:
		break;	

	}
	return ret;
}


/*******************************************************************************
* 名称:  cmd_process_three_led_op
* 功能:  三色灯控制
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_process_three_led_op(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;

	ucCmdType = stFrame->ucaData[0];

	switch(ucCmdType)
	{
		case 0:
		break;

		case 1:
		break;

		default:
		break;	

	}




	return ret;
}


/*******************************************************************************
* 名称:  cmd_process_needle_one_pulse_op
* 功能:  单脉冲控制（出针，收针）针控制指令
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t cmd_process_needle_one_pulse_op(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmdType = 0;

	ucCmdType = stFrame->ucaData[0];

	switch(ucCmdType)
	{
		case 0:
			/*! 先全部关闭 */
			bsp_pv_close(E_PV_1);
			bsp_pv_close(E_PV_2);

			/*!   1 出针*/
			if(stFrame->ucaData[1])
			{
				bsp_pv_timeout_check(E_PV_1, 100);
			}
			else
			{
				bsp_pv_timeout_check(E_PV_2, 100);
			}
		break;

		case 1:
		break;

		default:
		break;	

	}



	return ret;
}





/*******************************************************************************
* 名称:  control_c1c0_cmd_operation
* 功能:  命令操作控制字
* 形参:  
			stFrame  命令帧

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t control_c1c0_cmd_operation(frame_info_t * stFrame)
{
	uint8_t ret = 0;
	uint8_t ucCmd = 0;

	ucCmd = stFrame->ucCMD_RegAddr;

	/*! 数据帧是命令帧，根据命令编号进行分发 */
	switch(ucCmd)
	{
		/*! 生理盐水电机操作指令 */
		case CMD_PROCESS_SALINE_MOTOR_OP:
			ret = cmd_process_saline_motor_op(stFrame);
		break;

		/*! 生理盐水电机指定速度运行指令 */
		case CMD_PROCESS_SALINE_MOTOR_RUN_SPEED:
			ret = cmd_process_saline_motor_run_speed(stFrame);
		break;		

		/*! 无菌水电机操作指令 */
		case CMD_PROCESS_WATER_MOTOR_OP:
			ret = cmd_process_water_motor_op(stFrame);
		break;

		/*! 采无菌水电机注射指定的液量指令 */
		case CMD_PROCESS_WATER_MOTOR_REJECT_VOL:
			ret = cmd_process_water_motor_reject_vol(stFrame);
		break;

		/*! 无菌水电机指定速度运行指令 */
		case CMD_PROCESS_WATER_MOTOR_RUN_SPEED:
			ret = cmd_process_water_motor_run_speed(stFrame);
		break;		


		/*! 三色灯控制 */
		case CMD_PROCESS_THREE_LED_OP:
			ret = cmd_process_three_led_op(stFrame);
		break;		

		/*! 单脉冲控制（出针，收针）针控制指令 */
		case CMD_PROCESS_NEEDLE_ONE_PULSE_OP:
			ret = cmd_process_needle_one_pulse_op(stFrame);
		break;

		default:
		break;

	}

	return ret;
}



/*******************************************************************************
* 名称: cmd_process_cmd_handle
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t cmd_process_cmd_handle(void)
{
    uint8_t  ret = 0;
   
   switch (s_stCmdProcessActionInfo.stFrameInfo.ucC1C0)
   {
       case CONTROL_C1C0_STATUS_REG_READ:// 读状态寄存器控制字 0x00
       {
			ret = control_c1c0_status_reg_read(&(s_stCmdProcessActionInfo.stFrameInfo));
       }
		break;
		
       case CONTROL_C1C0_DATA_REG_READ:// 读数据寄存器控制字 0x01
       {
			ret = control_c1c0_data_reg_read(&(s_stCmdProcessActionInfo.stFrameInfo));
       }
		break;
				
       case CONTROL_C1C0_DATA_REG_WRITE:// 写数据寄存器控制字 0x02
       {
			ret = control_c1c0_data_reg_write(&(s_stCmdProcessActionInfo.stFrameInfo));
       }
		break;		
				
	   case CONTROL_C1C0_CMD_OPERATION:// 命令操作控制字  0x03
       {
			ret = control_c1c0_cmd_operation(&(s_stCmdProcessActionInfo.stFrameInfo));
       }
	   break;

		default:
		{
			
		}
		break;
		
	}
	
	return ret;
}


/*******************************************************************************
* 名称: cmd_process_action_init
* 功能: 示例
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
static void cmd_process_action_init(void)
{
	/*! 1 复位变量 */
	memset(&s_stCmdProcessActionInfo, 0, sizeof(s_stCmdProcessActionInfo));
	memset(&s_stCmdActionResult, 0, sizeof(s_stCmdActionResult));
	
}



/*******************************************************************************
* 名称: cmd_process_task
* 功能:  命令执行线程
* 形参:  	
* 返回: 无
* 说明: 无
********************************************************************************/
void cmd_process_task(void *p_arg)
{
    OS_ERR err;
	UINT8 ret = ERR_NONE;
    
    (void)p_arg;
    
	/*! 本单元状态初始化 */
	cmd_process_action_init();
	
    while (1)
    {	
		/*! 等待信号量触发 */
        OSSemPend(DS_Sem, 0, &err);
		
		/*!< 动作没有开始，是发给本单元的指令 */
		if(s_stCmdProcessActionInfo.ucUnitActionState == UNIT_ACTION_START && s_stCmdProcessActionInfo.stFrameInfo.ucTargetID == CMD_MID_PART_UNIT_CAN_ID)
		{
			ret = cmd_process_cmd_handle();
			
			/*! 当动作执行完成，上报命令执行结果 */
            check_cmd_process_cmd_result(&s_stCmdProcessActionInfo, ret);
		}		

    }
	
}



















#endif /*! __cmd_process_C__ */






