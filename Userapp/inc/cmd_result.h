/**
  ******************************************************************************
  * @文件   Temlate.c
  * @作者   Xcg
  * @版本   V1.00.1
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __CMD_RESULT_H
#define __CMD_RESULT_H

/* 文件包含 ------------------------------------------------------------------*/
#include <includes.h>
#include "action_info.h"


/* 导出类型 ------------------------------------------------------------*/
/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
void SetCmdFrameResult(const frame_info_t* cmd,cmd_result_t *pstFrameResult, uint8_t ucResult, uint8_t ucUnit, uint8_t ucMotor, uint8_t ucErr);

uint8_t  get_redefine_motor_no(uint8_t ucMotorNo);


#endif /* __CMD_RESULT_H */

