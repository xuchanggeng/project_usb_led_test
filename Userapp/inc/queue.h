/****************************************Copyright (c)**********************************
            深圳市格致微芯科技有限公司
            www.gezhitech.com
========================================================================================
                    文件描述
    队列管理程序
========================================================================================
编写:  
日期：
***************************************************************************************/
#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "pro_type.h"

/***************************************************************************************
        宏定义
***************************************************************************************/
#define Q_USED_SIZE     0                                           // 查询已入队数目
#define Q_FREE_SIZE     1                                           // 查询剩余空间
#define Q_MAX_SIZE      2                                           // 查询最大允许接受数据空间


#define Q_UART_DISABLE_RCV                      1                   // UART 停止接受数据
#define Q_USB_DISABLE_RCV                       2                   // USB  停止接受数据
#define Q_PAR_DISABLE_RCV                       4                   // 并口 停止接受数据
#define Q_ETHERNET_DISABLE_RCV                  8                   // 以太网停止接受数据

/***************************************************************************************
        数据结构定义
***************************************************************************************/

typedef struct {
    unsigned char       *out;                           // 数据头指针
    unsigned char       *in;                            // 数据尾指针
    unsigned char       *end;                           // 指向Buf的结束位置 
    unsigned short      flow_ctrl;                      // 流控

    unsigned long       len;                            // 数据长度
    unsigned long       max;                            // 最大数据长度

    unsigned char       buf[1];                         // 数据域,接收数据时的缓存

}STRUCT_QUEUE;

// extern STRUCT_QUEUE my_queue;
/***************************************************************************************
        接口函数
***************************************************************************************/
void queue_inital(void *buf, unsigned long buf_size);
long queue_in(void *buf, unsigned char *str, unsigned long len);
long queue_in_byte(void *buf, unsigned char byte);
long queue_out(void *buf, unsigned char *pbuf, 
                unsigned long rcvlen, unsigned long timeover);
long queue_out_byte(void *buf, 
                    unsigned char *pbyte, unsigned long timeover);
int queue_query(void *buf, unsigned char q_type);

long queue_in_byte_fast(void *buf, unsigned char byte);

long queue_in_fast(void *buf, unsigned char *str, unsigned long len);

UINT16 t_ms(UINT32 _tms);                            // 将‘毫秒’转换成‘系统节拍’


#endif
