
#ifndef __LOAD_PLATFORM_H__
#define __LOAD_PLATFORM_H__

#include "pro_type.h"
#include "includes.h"


uint8_t start_load_platform_action(frame_info_t *pstFrame, uint8_t ucISack);

#endif
