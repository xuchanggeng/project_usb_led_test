/**
  ******************************************************************************
  * @文件   cmd_send_frame.c
  * @作者   Xcg
  * @版本   V1.00.1
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __CMD_SEND_FRAME_H
#define __CMD_SEND_FRAME_H

/* 文件包含 ------------------------------------------------------------------*/
#include <includes.h>
#include "action_info.h"


/* 导出类型 ------------------------------------------------------------*/
/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
uint8_t send_cmd_status_frame(uint16_t unDes, uint8_t ucCmd_Addr, uint8_t *pucData, uint8_t ucDataLen);
uint8_t send_cmd_data_frame(uint16_t unDes, uint8_t ucCmd_Addr, uint8_t *pucData, uint8_t ucDataLen);
uint8_t send_cmd_op_frame(uint16_t unDes, uint8_t ucCmd_Addr, uint8_t *pucData, uint8_t ucDataLen);

uint8_t send_echo_frame(frame_info_t * stFrame);


#endif /* __CMD_SEND_FRAME_H */

