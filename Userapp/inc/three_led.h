/**
  ******************************************************************************
  * @文件   three_led.h
  * @作者   Xcg
  * @版本   V1.01.003
  * @日期   7-16-2020
  * @简介    XXX module.    
  ******************************************************************************
  *  
  * @功能说明
  *
  * @注意
  *		
  *		Copyright(c) 2017-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 
#ifndef __THREE_LED_H
#define __THREE_LED_H

/* 文件包含 ------------------------------------------------------------------*/
#include "define.h"
#include "includes.h"

/* 导出类型 ------------------------------------------------------------*/
typedef struct _tag_three_led_control_t
{
  uint8_t  ucState;
  uint32_t ulFreqTicks;
  uint32_t ulCountTicks;
  uint16_t ledFreq;
  uint16_t ledCount;
}three_led_control_t;


/* 导出常量 -----------------------------------------------------------*/
/* 导出函数 ---------------------------------------------------------- */
void three_led_init(void);
int8_t start_three_led(uint8_t ledType, uint8_t ledMode, uint16_t ledFreq, uint16_t ledCount);
void three_led_ticks(uint16_t error);
/* 导出变量 ---------------------------------------------------------- */

#endif /* __THREE_LED_H */

