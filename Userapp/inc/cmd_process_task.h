
#ifndef __CMD_PROCESS_H__
#define __CMD_PROCESS_H__

#include "pro_type.h"
#include "includes.h"

uint8_t start_cmd_process_action(frame_info_t *pstFrame, uint8_t ucISack);

#endif /*! __CMD_PROCESS_H__ */
