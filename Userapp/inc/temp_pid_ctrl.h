
#ifndef __CTRLTEMP_H__
#define __CTRLTEMP_H__

#include "pro_type.h"
#include "includes.h"


/*! AD与温度转换说明：
    1：硬件ADC的参考电压为3.3V；同时ADC前端进行4倍的放大;
	2：ADC的精度为12位；
	3：温度传感器LM35的转换关系为：10mv <----> 1℃
	4：系数825 = 3.3V * 1000 / 4(放大倍数) = 3300mV / 4 = 825
*/
#define AD_TO_TEMP(ad)      ( (ad * 825) >> 12)
#define TEMP_TO_AD(temp)    ( (temp << 12) / 825)  


/*! pid控制结构体 */
typedef struct _tag_temp_ctrl_para_t
{
	uint16_t setTarget;
	uint16_t setOffset;

}pid_ctrl_para_t;

/*! pid控制结构体 */
typedef struct	_tag_pid_ctrl_t
{
    uint16_t  P;
    uint16_t  I;
    uint16_t  D;
	
    uint32_t  error;
    uint32_t  last_error;
    uint16_t  tick;
	
    int16_t   target;
    int16_t   current;
	
}pid_ctrl_t;


/*!  pid操作函数 */
typedef struct _tag_pid_ctrl_ops_t
{
	uint8_t (*pid_read_temp)(uint8_t ch);
	uint8_t (*pid_write_high)(uint8_t ch);
	uint8_t (*pid_write_low)(uint8_t ch);

}pid_ctrl_ops_t;


/*! 温度控制参数 */
typedef struct _tag_temp_ctrl_t
{
	pid_ctrl_t      stPidCtrl;
	pid_ctrl_ops_t  stPidOps;
	pid_ctrl_para_t stPidPara;
}temp_ctrl_t;



_EXT_ pid_ctrl_para_t   TempCtrl[];
_EXT_ pid_ctrl_t        TempPid[];
_EXT_ uint16_t          Temp_Tick;

/*! 帕尔帖(试剂盘)温度目标值 */
_EXT_ uint8_t g_ucPeltierCoolTempTarget;

void Temp_TickPWM(void);
uint8_t Temperature_Ctrl(void);
uint16_t Get_Current_Temp_Fun(uint8_t eADC1Id);
void ctrl_peltier_cool_temperature(void);
uint16_t Get_Current_Temp_Fun2(uint8_t eADC1Id);

void temperature_ctrl_main(void);
void temperature_ctrl_init(void);
uint16_t temp_ctrl_get_temp_incube(uint8_t ucCh);
void temperature_ctrl_update_temp(void);

#endif
