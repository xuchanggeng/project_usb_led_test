
#ifndef __PARAMETER_H__
#define __PARAMETER_H__

#include "pro_type.h"

#define FRAME_ADDR_PAGE_LEN      256

#define SP_FRAME_PAGE            1

// 针参数
__packed typedef struct
{
    UINT16 SP_rot_pos[8];       // 针旋转偏移步数
    UINT16 SP_ud_rst_pos;       // 针上下复位偏移量
    UINT16 SP_up_pos[7];        // 针杯口偏移步数
    UINT16 SP_dowm_pos[7];      // 针杯底偏移步数
    UINT16 SP_z_pos;            // 针吸液复位偏移步数
    UINT16 SP_z_value;          // 注射器电机步数与液量的比值
    UINT16 SP_z_back;           // 注射器回吸空气量
    UINT8 crc;
}lSP_Para;

_EXT_ lSP_Para sp_para;

void Set_Paramenter(void);
void Load_Paramenter(void);
UINT8 Handle_Para_Cmd(void);
void Load_SysPara(void);

#endif

