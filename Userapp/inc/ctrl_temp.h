
#ifndef __CTRLTEMP_H__
#define __CTRLTEMP_H__

#include "pro_type.h"
#include "includes.h"


/*! AD与温度转换说明：
    1：硬件ADC的参考电压为3.3V；同时ADC前端进行4倍的放大;
	2：ADC的精度为12位；
	3：温度传感器LM35的转换关系为：10mv <----> 1℃
	4：系数825 = 3.3V * 1000 / 4(放大倍数) = 3300mV / 4 = 825
*/
#define AD_TO_TEMP(ad)      ( (ad * 825) >> 12)
#define TEMP_TO_AD(temp)    ( (temp << 12) / 825)  

typedef struct
{
    uint8_t  P;
    uint8_t  I;
    uint8_t  D;
    uint16_t target;
    int16_t add;
}tTempCtrlPara;

/*!  */
typedef struct
{
    UINT32    error;
    UINT32    last_error;
    uint16_t  tick;
    int16_t   target;
    int16_t   current;
}tTempPid;

// 针参数
__packed typedef struct
{
    uint16_t Set_Temp[5];       // 温度设置值
    uint16_t Set_PID[3];
    uint8_t crc;
}lTEMP_Para;


_EXT_ tTempCtrlPara TempCtrl[];
_EXT_ tTempPid      TempPid[];
_EXT_ uint16_t Temp_Tick;

/*! 帕尔帖(试剂盘)温度目标值 */
_EXT_ uint8_t g_ucPeltierCoolTempTarget;

void Temp_TickPWM(void);
uint8_t Temperature_Ctrl(void);
uint16_t Get_Current_Temp_Fun(uint8_t eADC1Id);
void ctrl_peltier_cool_temperature(void);
uint16_t Get_Current_Temp_Fun2(uint8_t eADC1Id);
void Temp_Ctrl_Function_Close(uint8_t num );
void Temp_Ctrl_Function_Open(uint8_t num );
void temperature_ctrl_main(void);
void temperature_ctrl_init(void);
uint16_t temp_ctrl_get_temp_incube(void);
void temperature_ctrl_update_temp(void);

#endif
