
#ifndef __TEMP_UNIT_H__
#define __TEMP_UNIT_H__

#include "pro_type.h"
#include "includes.h"

uint8_t start_temp_unit_action(frame_info_t *pstFrame, uint8_t ucISack);

#endif
