
#ifndef __ERROR_H__
#define __ERROR_H__

//#define ERR_NONE                    0           // 无错
#define ERR_FRAM_LEN                1           // 帧长度错误
#define ERR_FRAM_HEAD               2           // 帧头错误
#define ERR_FRAM_CTRL               3           // 帧控制位错误
#define ERR_FRAM_TAIL               4           // 帧尾错误
#define ERR_CMD_DATA_LEN            5           // 命令数据长度错误
#define ERR_CMD_DATA_TYPE			6			// 命令数据类型错误
#define	ERR_CMD_TYPE				7			// 命令类型错误

#define ERR_MOTOT_TIME_OVER         11          // 电机超时
#define ERR_MOTOR_OVER_DISTANCE     12          // 电机运行超出最大限制距离
#define ERR_MOTOR_CRASH             13          // 电机发生碰撞
#define ERR_MOTOR_NOT_RESET         15          // 电机没有复位
#define ERR_MOTOR_STATE				17			// 电机运动状态错误
#define ERR_MOTOR_IDLE				18			// 电机空闲状态错误
#define ERR_MOTOR_LOAD				19    		// 电机加载参数状态错误
#define ERR_MOTOT_LIGHT             20          // 电机光耦检测出错
#define ERR_MOTOR_RESET_TIGGER      21          // 电机复位光耦触发错误
#define ERR_MOTOR_ARRIVE_TIGGER     22          // 电机到位光耦触发错误
#define ERR_MOTOR_CODE_TIGGER       23          // 电机码盘光耦触发错误
#define ERR_MOTOR_FLAG_STATE       	24          // 电机运动状态错误
#define ERR_MOTOR_ARRIVE_STATE      25			// 电机到位光耦状态错误
#define ERR_MOTOR_CRASH_STATE      	26			// 电机撞针光耦状态错误

#define ERR_LIQUIT_CHECK            40          // 液面检测校准失败
#define ERR_NOT_RESET_BEFORE        41          // 清洗前没复位
#define ERR_LIQUIT_NOT_RIGHT        42          // 没有检测到液面

#endif
