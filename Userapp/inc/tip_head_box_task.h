
#ifndef __TIP_HEAD_BOX_H__
#define __TIP_HEAD_BOX_H__

#include "pro_type.h"
#include "includes.h"

uint8_t start_tip_head_box_action(frame_info_t *pstFrame, uint8_t ucISack);

#endif
