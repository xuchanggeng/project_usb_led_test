/******************** (C) COPYRIGHT 2018 XuChangGeng ******************************
* File Name          : acitoninfo.h
* Author             : XuChangGeng
* Version            : V1.0
* Date               : 28/06/2018
* Description        : use firmware lib version: V2.0.1 06/13/2008
********************************************************************************
* 
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _ACTION_INFO_H
#define _ACTION_INFO_H


#include "frame_info.h"




typedef struct _tag_action_info_t{
	frame_info_t stFrameInfo;
	uint8_t ucActionFinishFlag; 
	uint8_t ucUnitActionState;
	uint8_t ucErrMotor;
	uint8_t ucIsAck;
} action_info_t;


typedef struct _tag_action_result_t
{
	uint8_t ucActionRsultState; 
	uint8_t ucaActionRsultInfo[5]; 
	
} action_result_t;




#endif /*_ACTION_INFO_H*/

