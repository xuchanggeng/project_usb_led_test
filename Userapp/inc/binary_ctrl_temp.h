
#ifndef __binary_CTRLTEMP_H__
#define __binary_CTRLTEMP_H__

#include "pro_type.h"
#include "includes.h"

typedef struct _tag_binary_ctrl_temp_t
{
	uint16_t unTagetTemp;
	uint16_t unTagetLowTemp;
	uint16_t unTagetHighTemp;
	int32_t (*read_temp)(uint16_t unIndex);
	uint8_t (*output_ctrl_cool_on)(uint16_t unIndex);
	uint8_t (*output_ctrl_cool_off)(uint16_t unIndex);
	uint8_t (*read_fan_state)(uint16_t unIndex);
}binary_ctrl_temp_t;

//================================================================================
typedef struct _tage_tCool_config_t
{
    GPIO_TypeDef*  COOL_Port;
    UINT32         COOL_Clk;
    UINT16         COOL_Pin;
}tCool_config_t;

//================================================================================
#define COOLx_PIN(x)			COOL_##x##_PIN
#define COOLx_PORT(x)           COOL_##x##_PORT
#define COOLx_CLK(x)			COOL_##x##_CLK


//================================================================================
#define COOL_PIN(x,y)		do{  if(x==0) y = COOLx_PIN(0);\
                            else if(x==1) y = COOLx_PIN(1);\
                            else if(x==2) y = COOLx_PIN(2);\
                            else if(x==3) y = COOLx_PIN(3);\
                            else if(x==4) y = COOLx_PIN(4);\
                            else if(x==5) y = COOLx_PIN(5);\
                            }while(0)

#define COOL_PORT(x,y)		do{  if(x==0) y = COOLx_PORT(0);\
                            else if(x==1) y = COOLx_PORT(1);\
                            else if(x==2) y = COOLx_PORT(2);\
                            else if(x==3) y = COOLx_PORT(3);\
                            else if(x==4) y = COOLx_PORT(4);\
                            else if(x==5) y = COOLx_PORT(5);\
                            }while(0)


#define COOL_CLK(x,y)		do{  if(x==0) y = COOLx_CLK(0);\
                            else if(x==1) y = COOLx_CLK(1);\
                            else if(x==2) y = COOLx_CLK(2);\
                            else if(x==3) y = COOLx_CLK(3);\
                            else if(x==4) y = COOLx_CLK(4);\
                            else if(x==5) y = COOLx_CLK(5);\
                            }while(0)


//================================================================================

typedef enum
{
	E_COOL_1,
	E_COOL_2,
	E_COOL_3,
	E_COOL_4,
	E_COOL_5,
	E_COOL_6,
	E_COOL_7,
	E_COOL_MAX,
	
}eCOOL_Type;
//================================================================================

// �����IO�ڶ���
#define COOL_0_PIN        		GPIO_Pin_4
#define COOL_0_PORT       		GPIOA
#define COOL_0_CLK        		RCC_APB2Periph_GPIOA

#define COOL_1_PIN        		GPIO_Pin_4
#define COOL_1_PORT       		GPIOA
#define COOL_1_CLK        		RCC_APB2Periph_GPIOA

#define COOL_2_PIN        		GPIO_Pin_5
#define COOL_2_PORT       		GPIOA
#define COOL_2_CLK        		RCC_APB2Periph_GPIOA

#define COOL_3_PIN        		GPIO_Pin_6
#define COOL_3_PORT       		GPIOA
#define COOL_3_CLK        		RCC_APB2Periph_GPIOA

#define COOL_4_PIN        		GPIO_Pin_7
#define COOL_4_PORT       		GPIOA
#define COOL_4_CLK        		RCC_APB2Periph_GPIOA

#define COOL_5_PIN        		GPIO_Pin_1
#define COOL_5_PORT       		GPIOA
#define COOL_5_CLK        		RCC_APB2Periph_GPIOA

#define COOL_6_PIN        		GPIO_Pin_0
#define COOL_6_PORT       		GPIOA
#define COOL_6_CLK        		RCC_APB2Periph_GPIOA



#endif /*! __binary_CTRLTEMP_H__ */
