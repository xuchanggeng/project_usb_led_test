
#ifndef __MY_INCLUDE_H__
#define __MY_INCLUDE_H__	

#include "pro_sys.h"
#include "pro_delay.h"
#include "pro_error.h"

#include "bsp_led.h"
#include "bsp_can.h"
#include "bsp_spi.h"
#include "bsp_eeprom.h"
#include "bsp_pump_valve.h"


#include "queue.h"
#include "parameter.h"
#include "mid_op.h"
#include "mid_task.h"
#include "load_platform_task.h"

#endif
