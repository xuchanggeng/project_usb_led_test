
#ifndef __MID_TASK_H__
#define __MID_TASK_H__

#include "includes.h"

#define PUMP_VALVE_EN       1

#define MSG_BUF_LEN             256     // 数据长度
#define REBACK_BUF_LEN          256

#define FRAM_HEAD_DATA          0xAB    // 帧头
#define FRAM_TAIL_DATA          0xCD    // 帧尾
#define FRAM_CTRL_DATA_M_S      0x80    // 接收数据控制  主机返回给从机
#define FRAM_CTRL_DATA_S_M      0x20    // 发送数据控制  从机返回给主机
#define FRAM_CTRL_DATA_ERR      0xC0    // 返回错误帧
#define FRAM_CTRL_DATA_WAR      0xD0    // 返回警告

typedef enum
{
    Idel      = 0,
    Fram_Head = 1,
    Fram_Ctrl = 2,
    Fram_Len  = 3,
    Fram_Data = 4,
    Fram_Crc  = 5,
    Fram_Tail = 6
}RecFramState;

_EXT_ UINT8 RecieveBuf[];
_EXT_ UINT8 Mid_msg_buf[];

#if SYSTEM_SUPPORT_OS
_EXT_ UINT8 Mid_Run;
#endif

typedef struct
{
    UINT8  op;
    UINT8  err;
    UINT8  err_motor;
    UINT8 *data;
    UINT16 len;
}tMsg;

_EXT_ tMsg sys_msg;
_EXT_ tMsg pv_msg;
_EXT_ tMsg sp_msg;
_EXT_ tMsg para_msg;

void Msg_Init(void);
//void send_cmd(tMsg *bk_msg);
UINT8 MT_WaitFram(UINT8* Que_Msgbuf,UINT8 *RecFram);
void Sys_Init(void);
#endif

