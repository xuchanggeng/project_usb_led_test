
#ifndef __MID_OP_H__
#define __MID_OP_H__

#include <includes.h>

#define SYS_VERSION                 10

#define SYS_CMD                     0
#define PV_CMD                      1
#define SP_CMD                      2
#define PARA_CMD                    3

#define CMD_SYS_HANDLE              0x01            // 握手命令

#define CMD_SYS_DEBUG               0x04            // 调试控制开关

#define CMD_PV_RT_CTRL              0x10            // 泵阀控制开关命令
#define CMD_PV_RT_CHECK             0x11            // 泵阀状态查询命令

#define CMD_MT_SP_RST               0x20            // 加样针电机复位
#define CMD_MT_SP_MOVE              0x21            // 加样针电机运动
#define CMD_MT_SP_MOVE_TO           0x22            // 加样针电机运动到指定位置
#define CMD_MT_SP_SUCTION_MOVE_TO   0x23            // 加样针运动到指定位置吸排液
#define CMD_MT_SP_CLEAN             0x24            // 加样针清洗
#define CMD_MT_SP_SUCTION_MOVE      0x25            // 加样针吸排液
#define CMD_MT_SP_LIQUIT_CHECK      0x26            // 液面检测校准
#define CMD_MT_SP_IN_CUP_CTRL       0x27            // 进杯盒抖动电机抖动开关

#define CMD_PARA_SP_SET             0x30            // 设置加样针参数
#define CMD_PARA_SP_LOAD            0x31            // 加载加样针的参数

_EXT_ UINT8 set_debug;

void Handle_Msg(UINT8 *Recieve_buf, UINT16 msg_len);

#endif
