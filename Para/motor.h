/**********************************************************************
*
*	文件名称：motor.h
*	功能说明：电机相关的所有公共应用代码
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

#ifndef __MOTOR_H__
#define __MOTOR_H__

#include "includes.h"

/*! 步进电机控制变量定义 */
typedef struct
{
    uint8_t  ucState;             		// 电机状态（0：空闲；非零：表示不能对电机进行操作）
    uint8_t  ucCurrentSensor;     		// 当前光耦位置（1：在起始位置；0：不在起始位置）
    uint8_t  ucMoveDirection;     		// 运动方向定义（0：正；1：反）
    uint8_t  ucMotorPosition;     		// 电机位置（0：非法位；1：其他位）
    uint8_t  ucMotorSpeedLevel;   		// 电机速度等级（0为1档（最低）；15为16档（最高））
    uint8_t  ucMotorAcceleration;   	// 电机加速度等级（0~255）
    uint16_t unMoveStep;          		// 电机要走的步数/具体的位置 
    uint16_t unPassedStep;        		// 电机所走的步数，离起始位置步数 
    uint16_t unRemainStep;        		// 电机剩余的步数 	
    uint16_t unActionDelay;       		// 初始化动作切换之间的延时 
    uint16_t unMotorExecTime;     		// 电机下传的执行时间（以0.01秒为单位） 
    uint16_t unMotorMovedTime;    		// 电机实际的执行时间（以0.01秒为单位） 
	uint8_t  ucRunMode;					// 电机运行模式:0：复位模式；1：指定位置运行；2：指定步数运行）
    uint8_t  ucRunState;  				// 电机运行状态（0：电机停止；1：电机运行中）
    uint8_t  ucRunResult; 				// 电机运行结果	（0:电机运行成功；1：电机运行失败）
	uint8_t  ucMotorOffsetStep;      	// 电机运动补偿步数
}motor_param_t;


enum
{
    MOTOR_TYPE_CODE = 0,             	// 码盘电机
    MOTOR_TYPE_STEP,           			// 步数电机
};

/*! 电机类型参数 */
typedef struct 
{
    uint8_t  ucMotorType; 					//是否带码盘
	uint8_t  ucMotorRunSpeed;				//电机运动速度
	uint8_t  ucMotorLastRealPos;			//电机的上一个位置
	uint8_t  ucErrMotor;					//错误电机
	uint8_t  ucErrCode;						//错误代码
}motor_type_t;


typedef struct _tag_motor_check_liquid_t
{
	uint8_t ucMotorCheckLiquidFlag;
	uint8_t ucMotorCheckLiquidSignal;
	uint8_t ucMotorCheckHitSignal;
	uint16_t ucMotorCheckLiquidStep;
}motor_check_liquid_t;


/*! 电机运动模式定义 */
enum
{
    RESET_MODE_MOTOR = 0,             	// 电机复位模式,该模式下起始位置和结束位置相等
    STEP_MODE_MOTOR,           			// 电机步数模式,该模式下电机按照指定的步数运动
	POSITION_MODE_MOTOR,           		// 电机位置模式,该模式下电机按照指定的位置运动	
	LIQUID_MODE_MOTOR,           		// 电机液面检测模式,该模式下电机向下运动检测到液面，就走指定的插水深度	
};

/*! 电机速度 */
#define SPEED_SAMPROBE_X_MOTOR          5     /*! 样本针X电机 */
#define SPEED_SAMPROBE_Y_MOTOR          5     /*! 样本针Y电机 */
#define SPEED_REAGPROBE_X_MOTOR         5     /*! 试剂针针X电机 */
#define SPEED_REAGPROBE_Y_MOTOR_L       5     /*! 试剂针Y电机L */
#define SPEED_REAGPROBE_Y_MOTOR_R       5     /*! 试剂针R电机R */
#define SPEED_WASH_PROBE_MOTOR          1     /*! 清洗针垂直电机 */
#define SPEED_SCAN_MOTOR                1     /*! 旋转扫码电机  */
#define SPEED_REACTION_MOTOR            5     /*! 反应盘电机 */
#define SPEED_REAGENT_MOTOR             5     /*! 试剂盘电机 */
#define SPEED_SAMPLE_SYRINGE_MOTOR      5     /*! 样本注射器电机 */
#define SPEED_REAGENT_SYRINGE_MOTOR_1   5     /*! 试剂注射器1电机 */
#define SPEED_REAGENT_SYRINGE_MOTOR_2   5     /*! 试剂注射器2电机 */
#define SPEED_WATER_SYRINGE_MOTOR_1     5     /*! 清水注射器1电机 */
#define SPEED_WATER_SYRINGE_MOTOR_2     5     /*! 清水注射器2电机 */

#define SPEED_NULL_MOTOR                0     /*! 空电机 */

#define SPEED_DILUENT_SYRINGE_MOTOR     5     /*! 稀释液注射器电机 */

/*! 电机类型偏移位置 */
#define SAMPLE_OFFSET_INDEX		(0)      // 电机类型偏移位置


/*! 电机种类定义 */
enum
{
	SAMPLE_X_MOTOR = 0x00,  		// 采样针水平X轴电机   00
	SAMPLE_Y_MOTOR,           		// 采样针垂直Y轴电机   01
    REAGENT_X_MOTOR,				// 试剂针水平电机       02 
    REAGENT_Y_MOTOR_L,				// 试剂针垂直左电机       03
	REAGENT_Y_MOTOR_R,				// 试剂针垂直右电机       04(去掉)
    WASH_Y_MOTOR,					// 清洗针垂直电机         05
    SCAN_SPIN_MOTOR,				// 扫码旋转电机            06
    REACTION_SPIN_MOTOR,			// 反应盘电机              07
    REAGENT_SPIN_MOTOR,				// 试剂盘电机              08
    SAMPLE_SYRINGE_MOTOR,			// 样本注射器电机         09
    REAGENT_SYRINGE_MOTOR_1,		// 试剂注射器电机1 RS1    0A(去掉)
	REAGENT_SYRINGE_MOTOR_2,		// 试剂注射器电机2 RS2    0B
    WATER_SYRINGE_MOTOR_1,			// 清水注射器电机1 PS1   0C(去掉)
	WATER_SYRINGE_MOTOR_2,			// 清水注射器电机2 PS2   0D
    NULL_MOTOR,                     //空电机                   0E
    DILUENT_SYRINGE_MOTOR,	        // 稀释液注射器电机      0F
    
    

    MAX_MOTOR_NUM
};


/*! 样本种类定义 */
enum
{
	SAMP_TYPE_BLOOD = 0x00,  		// 全血   00
	SAMP_TYPE_SERUM ,  				// 血清   01
	SAMP_TYPE_PERIP ,  				// 末梢血   02
	SAMP_TYPE_URINE ,  				// 尿液   03
	
	SAMP_TYPE_MAX   ,  				// 样本类型最大   00
};


/*! 液面检测的电机步数 */
#define MOTOR_CHECK_LIQUID_NUM	(MAX_MOTOR_NUM)      

/*! 注射器类型偏移位置 */
#define SYRINGE_OFFSET_INDEX	(3)      

/*! 注射器电机种类定义 */
enum
{
    SAMPLE_SYRINGE = 0x00,             	 // 样本针注射器, 250ul 
	CUSHION_SYRINGE,           			 // 缓冲液注射器, 2.5ml 
    DILUTE_SYRINGE,           			 // 稀释液注射器, 5ml 
	WATER_SYRINGE,           			 // 纯净水注射器, 5ml 
};

/*! 电机种类判断 */
#define  VALID_MOTOR_REG_TYPE(t) ( 0 <= (t) < MAX_MOTOR_NUM )
#define  INVALID_MOTOR_REG_TYPE(t) ( (0 > (t)) || ((t) >= MAX_MOTOR_NUM) )

/*! 电机运行状态定义 */
enum
{
	MOTOR_STATE_STOP  = 0,           	 // 电机停止	
    MOTOR_STATE_RUN,             		 // 电机正在运行
};

/*! 电机运行结果定义 */
enum
{
    MOTOR_RESULT_OK = 0,             	 // 电机运行正确
	MOTOR_RESULT_ERR,           		 // 电机运行失败
};

/*! 注射器电机方向 */
enum
{
    SYRINGE_MOTOR_ABSORB = 0,             	     // 注射器电机吸液，方向向前洗液
	SYRINGE_MOTOR_EJECT     ,           		 // 注射器电机排液，方向向后排液
};


/*! 电机补偿值细分数设定 */
#define MOTOR_SUBDIVIDE_NUM     (1)      // 该值为1,则所有电机补偿单位为微步;
										 //	该值为8,则所有电机补偿单位为整步

#define SAMPLE_UL_TO_STEP      	(4*8)    // 采样针注射器电机转换系数(ul -> step)(*8转换成微步)
#define CUSHION_UL_TO_STEP      (0.4*8)  // 缓冲液注射器电机转换系数(ul -> step)(*8转换成微步)
#define DILUTE_UL_TO_STEP       (0.4*8)  // 稀释液/纯净水注射器点击转换系数(ul -> step)(*8转换成微步)

int32_t write_motor_param_to_fpga( int32_t lMotorType, motor_param_t *pstMotorParam );
int32_t read_motor_param_from_fpga( int32_t lMotorType, motor_param_t *pstMotorParam );

#endif
