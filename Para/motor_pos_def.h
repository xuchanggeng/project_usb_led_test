
#ifndef __MOTOR_POS_DEF_h__
#define __MOTOR_POS_DEF_h__
#include <includes.h>




/***********************************样本针电机位置定义，根据位置来设置或获取各个位置的补偿参数*****************************************/
/***********************************样本针电机位置定义*****************************************/
	
/*!  定义样本针X电机的位置(物理位置) */
enum
{															        //物理位置
	REAL_POS_INIT_SAMPROBE_X_MOTOR = 0,						        //初始位（复位位置） 0
	REAL_POS_REACTION_SAMPROBE_X_MOTOR,								//反应位 1
	REAL_POS_WASH_SAMPROBE_X_MOTOR,									//清洗位 2
	REAL_POS_R3_SAMPROBE_X_MOTOR,									//试剂位  3
	REAL_POS_DILUTE_SAMPROBE_X_MOTOR,								//稀释位  4 
	REAL_POS_SAMPRAIL_SAMPROBE_X_MOTOR,					            //自动进样位  5
	REAL_POS_EMERGENCY_SAMPROBE_X_MOTOR,					        //急诊位  6
	REAL_MAX_POS_SAMPROBE_X_MOTOR,									//最大的样本针X轴位置	
	REAL_POS_NONE_SAMPROBE_X_MOTOR,									//无效位置	
};	


/*!  定义样本针Y轴电机的位置(物理位置)， 注意：垂直电机是根据X电机所在的位置来得到下降的步数 */
enum
{															        //物理位置
	REAL_POS_INIT_SAMPROBE_Y_MOTOR = 0,						        //初始位（复位位置）
	REAL_POS_REACTION_SAMPROBE_Y_MOTOR,								//反应位
	REAL_POS_WASH_SAMPROBE_Y_MOTOR,									//清洗位
	REAL_POS_R3_SAMPROBE_Y_MOTOR,									//试剂位
	REAL_POS_DILUTE_SAMPROBE_Y_MOTOR,								//稀释位
	REAL_POS_SAMPRAIL_SAMPROBE_Y_MOTOR,					            //自动进样位
	REAL_POS_EMERGENCY_SAMPROBE_Y_MOTOR,					        //急诊位
	REAL_MAX_POS_SAMPROBE_Y_MOTOR,									//最大的样本针Y轴位置	
	REAL_POS_NONE_SAMPROBE_Y_MOTOR,									//无效位置	
};	


/***********************************试剂针X电机位置定义*****************************************/
enum
{															            //物理位置
	REAL_POS_INIT_REAGPROBE_X_MOTOR = 0,						        //初始位（复位位置）
    
	REAL_POS_TWO_REACTION_REAGPROBE_X_MOTOR,							//反应位左针和右针
	REAL_POS_L_WASH_R_RESET_REAGPROBE_X_MOTOR,							//左针清洗位，右针复位    
	REAL_POS_L_REAG_R_WASH_REAGPROBE_X_MOTOR,							//右针清洗和左针吸试剂
	REAL_POS_L_RESET_R_REAG_REAGPROBE_X_MOTOR,							//（R1试剂位）右针吸试剂和左针复位
	
	REAL_MAX_POS_REAGPROBE_X_MOTOR,									    //最大的样本针X轴位置	
	REAL_POS_NONE_REAGPROBE_X_MOTOR,									//无效位置	
};	


/***********************************试剂针Y电机L位置定义*****************************************/
enum
{															             //物理位置
	REAL_POS_INIT_REAGPROBE_Y_MOTOR_L = 0,						        //初始位（复位位置）
	REAL_POS_R3_REAGPROBE_Y_MOTOR_L,									//试剂位
	REAL_POS_WASH_REAGPROBE_Y_MOTOR_L,									//清洗位
	REAL_POS_REACTION_REAGPROBE_Y_MOTOR_L,								//反应位
	REAL_MAX_POS_REAGPROBE_Y_MOTOR_L,									//最大的试剂针Y电机位置	
	REAL_POS_NONE_REAGPROBE_Y_MOTOR_L,									//无效位置	
};	


/***********************************试剂针Y电机R位置定义*****************************************/
enum
{															            //物理位置
	REAL_POS_INIT_REAGPROBE_Y_MOTOR_R = 0,						        //初始位（复位位置）
	REAL_POS_R3_REAGPROBE_Y_MOTOR_R,									//试剂位
	REAL_POS_WASH_REAGPROBE_Y_MOTOR_R,									//清洗位
	REAL_POS_REACTION_REAGPROBE_Y_MOTOR_R,								//反应位
	REAL_MAX_POS_REAGPROBE_Y_MOTOR_R,									//最大的试剂针Y电机位置	
	REAL_POS_NONE_REAGPROBE_Y_MOTOR_R,									//无效位置	
};	


/***********************************清洗针电机应用定义*****************************************/
enum
{															        //物理位置
	REAL_POS_INIT_WASH_PROBE_MOTOR = 0,						        //初始位（复位位置）
	REAL_POS_CUP_WASH_PROBE_MOTOR,					                //杯口
    REAL_POS_BOTTLE_WASH_PROBE_MOTOR,					            //杯底
	REAL_MAX_POS_WASH_PROBE_MOTOR,									//最大的清洗针位置	
	REAL_POS_NONE_WASH_PROBE_MOTOR,									//无效位置	
};	

/***********************************旋转扫码电机应用定义*****************************************/
enum
{															        //物理位置
	REAL_POS_INIT_SCAN_MOTOR = 0,						            //初始位（试管条码扫描位置）
    REAL_POS_SAMPLE_SCAN_MOTOR,                                     //样本位  
	REAL_POS_REAG_SCAN_MOTOR,					                    //试剂位    
	REAL_MAX_POS_SCAN_MOTOR,									    //最大的旋转扫码电机位置	
	REAL_POS_NONE_SCAN_MOTOR,									    //无效位置	
};	

/***********************************反应盘电机应用定义*****************************************/
enum
{															        //物理位置
	REAL_POS_INIT_REACTION_MOTOR = 0,						        //初始位（复位位置）
	REAL_POS_CODE_REACTION_MOTOR,					                //杯位
	REAL_MAX_POS_REACTION_MOTOR ,									//最大的反应盘位置	
	REAL_POS_NONE_REACTION_MOTOR,									//无效位置	
};	

/***********************************试剂盘电机应用定义*****************************************/
enum
{															        //物理位置
	REAL_POS_INIT_REAGENT_MOTOR = 0,						        //初始位（复位位置）
	REAL_POS_REAG_CODE_REAGENT_MOTOR,					           	//杯位-相对试剂
	REAL_POS_SAMP_CODE_REAGENT_MOTOR,					           	//杯位-相对样本
	REAL_MAX_POS_REAGENT_MOTOR ,									//最大的试剂盘电位置	
	REAL_POS_NONE_REAGENT_MOTOR=41,									//无效位置	
};	

/***********************************样本注射器电机应用定义*****************************************/
enum
{
    REAL_POS_INIT_SAMPLE_SYRINGE_MOTOR = 0,						      //初始位（复位位置）
    REAL_MAX_POS_SAMPLE_SYRINGE_MOTOR ,							      //最大的注射器位置	
    REAL_POS_NONE_SAMPLE_SYRINGE_MOTOR,								  //无效位置	
};

/***********************************试剂注射器1电机应用定义*****************************************/
enum
{
    REAL_POS_INIT_REAGENT_SYRINGE1_MOTOR = 0,						     //初始位（复位位置）
    REAL_MAX_POS_REAGENT_SYRINGE1_MOTOR ,							     //最大的注射器位置	
    REAL_POS_NONE_REAGENT_SYRINGE1_MOTOR,                                //无效位置	
};

/***********************************试剂注射器2电机应用定义*****************************************/
enum
{
    REAL_POS_INIT_REAGENT_SYRINGE2_MOTOR = 0,						      //初始位（复位位置）
    REAL_MAX_POS_REAGENT_SYRINGE2_MOTOR ,							      //最大的注射器位置	
    REAL_POS_NONE_REAGENT_SYRINGE2_MOTOR,                                 //无效位置
};

/***********************************清水注射器1电机应用定义*****************************************/
enum
{
    REAL_POS_INIT_WATER_SYRINGE1_MOTOR = 0,						        //初始位（复位位置） 
    REAL_MAX_POS_WATER_SYRINGE1_MOTOR ,							        //最大的注射器位置	
    REAL_POS_NONE_WATER_SYRINGE1_MOTOR,                                 //无效位置
};

/***********************************清水注射器2电机应用定义*****************************************/
enum
{
    REAL_POS_INIT_WATER_SYRINGE2_MOTOR = 0,						        //初始位（复位位置）
    REAL_MAX_POS_WATER_SYRINGE2_MOTOR ,							      //最大的注射器位置	
    REAL_POS_NONE_WATER_SYRINGE2_MOTOR,                                 //无效位置
};

/***********************************稀释液注射器电机应用定义*****************************************/
enum
{
    REAL_POS_INIT_DILUENT_SYRINGE_MOTOR = 0,						    //初始位（复位位置）
    REAL_MAX_POS_DILUENT_SYRINGE_MOTOR ,							    //最大的注射器位置	
    REAL_POS_NONE_DILUENT_SYRINGE_MOTOR,                                //无效位置
};












#endif     //__MOTOR_POS_DEF_h__


