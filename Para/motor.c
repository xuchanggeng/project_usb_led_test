/**********************************************************************
*
*	文件名称：motor.c
*	功能说明：电机相关的所有公共应用代码
*	特殊说明：无	       
*   修改记录：
*	   版本号    日期       作者      说明  
*	   V1.0.0    2018-3-9   yxh       实现基本功能
*
*   Copyright (C), 2018-2020, www.geniusmedica.com
*
**********************************************************************/

#include "bsp_fpga.h"
#include "fpga_adder.h"
#include "motor.h"

/*! 电机控制变量 */
typedef struct
{
    uint8_t  ucMoveDirect;     		// 运动方向 
    uint8_t  ucMotorSpeed;     		// 电机速度等级（0为1档（最低）；15为16档（最高））
    uint8_t  ucMotorAcceleration;   // 电机加速度等级（0~255）	
    uint8_t  ucMoveStepLow;    		// 电机要走的步数低字节 
    uint8_t  ucMoveStepHigh;   		// 电机要走的步数高字节 
    uint8_t  ucRemainStepLow;  		// 电机剩余的步数低字节 
    uint8_t  ucRemainStepHigh; 		// 电机剩余的步数高字节 	
    uint8_t  ucMoveState;  			// 电机运行状态
    uint8_t  ucMoveResult; 			// 电机运行结果
	uint8_t  ucRunMode;             // 电机运动模式	
	uint8_t  ucMoveOffsetStep;      // 电机运动补偿步数
	uint8_t  ucLightDispOffset;     //  散射补偿
	uint8_t  ucLightPassOffset;     // 透射补偿
}motor_register_t; 

static motor_register_t s_astMotorRegisterTable = {

	FPGA_MOTOR_DIR_BASE_ADDER,
	FPGA_MOTOR_SPEED_BASE_ADDER,
	FPGA_MOTOR_ACCELERATION_BASE_ADDER,
	FPGA_MOTOR_STEPL_BASE_ADDER,
	FPGA_MOTOR_STEPRH_BASE_ADDER,
	FPGA_MOTOR_RESTEPL_BASE_ADDER,
	FPGA_MOTOR_RESTEPH_BASE_ADDER,
	FPGA_MOTOR_STATE_BASE_ADDER,
	FPGA_MOTOR_RESULT_BASE_ADDER,
	FPGA_MOTOR_RUN_MODE_BASE_ADDER,
	FPGA_MOTOR_OFFSET_BASE_ADDER,
	FPGA_LIGHT_DISP_OFFSETH_BASE_ADDER,
	FPGA_LIGHT_PASS_OFFSETH_BASE_ADDER
};

/**********************************************************************
* 函数名称：write_motor_param_to_fpga
* 功能描述：写电机信息到FPGA
* 输入参数：
*         ：lMotorType：电机类型	*pstMotorParam：电机参数指针
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其他说明：
*         ：运动步数一定要放在最后面,否则FPGA会跑死
**********************************************************************/
extern uint16_t GetReactionTrayOffsetStep( uint8_t MtxId, uint8_t eDir,  uint8_t ucPos);
int32_t write_motor_param_to_fpga( int32_t lMotorType, motor_param_t *pstMotorParam )
{
	uint8_t ucIntex = 0;
	int32_t lRet = EXIT_FAILURE;
	fpga_data_t astFpgaData[9];

	do
	{
		if ( INVALID_POINTER(pstMotorParam) )
		{
			break;
		}
		
		/*! 电机运动方向 */
		ucIntex = 0;
		astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucMoveDirect;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
		astFpgaData[ucIntex].ucData = (pstMotorParam->ucMoveDirection);		

		/*! 电机运动速度 */
		ucIntex++;
		astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucMotorSpeed;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
		astFpgaData[ucIntex].ucData = pstMotorParam->ucMotorSpeedLevel;
		
		/*! 电机运动加速度 */
		ucIntex++;
		astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucMotorAcceleration;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
		
//		if ( ucIntex == X_SAMPLE )
//		{
//			astFpgaData[ucIntex].ucData = (uint8_t)0x55;
//		}
//		else
//		{
			astFpgaData[ucIntex].ucData = (uint8_t)0x00;////(uint8_t)((pstMotorParam->ucMotorAcceleration) & 0xFF);
//		}
		
		/*! 电机运动模式 */
		ucIntex++;
		astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucRunMode;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
		astFpgaData[ucIntex].ucData = pstMotorParam->ucRunMode;

		/*! 电机补偿步数 */
		ucIntex++;
		astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucMoveOffsetStep;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
		astFpgaData[ucIntex].ucData = pstMotorParam->ucMotorOffsetStep;

		/*! 电机运动步数低8位 */
		ucIntex++;
		astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucMoveStepLow;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
		astFpgaData[ucIntex].ucData = (uint8_t)((pstMotorParam->unMoveStep) & 0xFF);

		/*! 电机运动步数高8位 */
		ucIntex++;
		astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucMoveStepHigh;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
		astFpgaData[ucIntex].ucData = (uint8_t)(((pstMotorParam->unMoveStep)>>8) & 0xFF);

		/*! 如果是反应盘电机，就设置散射和透射的采集补偿 */
		if ( lMotorType == REACTION_SPIN_MOTOR )
		{
			ucIntex++;
			astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucLightDispOffset;
			astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
			astFpgaData[ucIntex].ucData = (uint8_t)((GetReactionTrayOffsetStep(REACTION_SPIN_MOTOR, 0, 2)) & 0xFF);
			
			ucIntex++;
			astFpgaData[ucIntex].ucBaseAddr = s_astMotorRegisterTable.ucLightPassOffset;
			astFpgaData[ucIntex].ucOffsAddr = (uint8_t)lMotorType;
			astFpgaData[ucIntex].ucData = (uint8_t)((GetReactionTrayOffsetStep(REACTION_SPIN_MOTOR, 0, 3)) & 0xFF);
		}

		if ( EXIT_FAILURE == bsp_fpga_write(astFpgaData, ucIntex + 1 ) )
		{
			break;
		}
		
		lRet = EXIT_SUCCESS;

	}while(0);

	return lRet;
}

/**********************************************************************
* 函数名称：read_motor_param_from_fpga
* 功能描述：从FPGA读电机信息
* 输入参数：
*         ：lMotorType：电机类型	*pstMotorParam：电机参数指针
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其他说明：无
**********************************************************************/
int32_t read_motor_param_from_fpga( int32_t lMotorType, motor_param_t *pstMotorParam )
{
	uint16_t unTmpStep = 0;
    int32_t lRet = EXIT_FAILURE;
    fpga_data_t astFpgaData[4];
	
    do
    {
        if ( INVALID_POINTER(pstMotorParam) )
		{
			break;
		}
		
		/*! 读取电机剩余步数 */
        astFpgaData[0].ucBaseAddr = s_astMotorRegisterTable.ucRemainStepLow;	 
        astFpgaData[0].ucOffsAddr = (uint8_t)lMotorType;
        astFpgaData[0].ucData = 0;
        astFpgaData[1].ucBaseAddr = s_astMotorRegisterTable.ucRemainStepHigh; 
        astFpgaData[1].ucOffsAddr = (uint8_t)lMotorType;
        astFpgaData[1].ucData = 0;
		
		/*! 读取电机状态 */
        astFpgaData[2].ucBaseAddr = s_astMotorRegisterTable.ucMoveState;	 
        astFpgaData[2].ucOffsAddr = (uint8_t)lMotorType;
        astFpgaData[2].ucData = 0;
		
		/*! 读取电机结果 */
        astFpgaData[3].ucBaseAddr = s_astMotorRegisterTable.ucMoveResult; 
        astFpgaData[3].ucOffsAddr = (uint8_t)lMotorType;
        astFpgaData[3].ucData = 0;
		
        if ( EXIT_FAILURE == bsp_fpga_read(astFpgaData, 4) )
		{
			break;
		}
		
		/*! 保存电机剩余步数 */
        unTmpStep = (uint16_t)(astFpgaData[1].ucData);
        pstMotorParam->unRemainStep = (uint16_t)( ((unTmpStep << 8) & 0xFF00) + astFpgaData[0].ucData );
		
		/*! 保存电机状态 */
		pstMotorParam->ucRunState = astFpgaData[2].ucData;
		
		/*! 保存电机结果 */
		pstMotorParam->ucRunResult = astFpgaData[3].ucData;
		
		lRet = EXIT_SUCCESS;

    }while(0);

    return lRet;
}


	
	
	
	




