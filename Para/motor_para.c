/**
  ******************************************************************************
  * @文件   motor_para.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2020 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
//#include "send_frame_task.h"
#include <includes.h>
#include "Com_Dispatcher.h"
#include "motor_para.h"
#include "motor_app.h"
#include "bsp_eeprom.h"
#include "eeprom_adder.h"
#include "ctrl_temp.h"


/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
/* 本地宏定义 ------------------------------------------------------------*/
#define INIT_PARA_VERSION_1				      0x01		//驱动板参数版本，v1.1开始，用来判断驱动板中是否有参数
#define INIT_PARA_VERSION_2				      0x01

/* 本地变量 ---------------------------------------------------------------*/
motor_para_t           s_ucMotorOffsetPara;
extern tMtxState       MtxState[MTx_NUM];
extern lTEMP_Para      temp_para;

//extern motor_type_t   s_ucMotorType[MAX_MOTOR_NUM];

/* 函数原型 ---------------------------------------------------------------*/
/* 本地函数 ----------------------------------------------------------------*/


/*******************************************************************************
* 名称: SetAllMotorResetPosOffset
* 功能: 设置电机复位位置的默认值
* 形参: 
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void SetAllMotorResetPosOffset(void)
{
//    bsp_eeprom_wt8(EEPROM_INIT_SAMPROBE_X_MOTOR_FORWARD_OFFSET_ADDER + 1*0, 50);	   //样本针前方向复位位 0
//    bsp_eeprom_wt8(EEPROM_INIT_SAMPROBE_X_MOTOR_BACK_OFFSET_ADDER + 1*0, 50);		   //样本针反方向复位位 0
//    bsp_eeprom_wt16(EEPROM_INIT_SAMPROBE_Y_MOTOR_STEPS_ADDER + 2*0, 250);              //样本针Y复位位 0,复位补偿不能超过255，否则数据被截断
//    bsp_eeprom_wt8(EEPROM_INIT_REAGPROBE_X_MOTOR_FORWARD_OFFSET_ADDER + 1*0, 80);	   //试剂针前方向复位位 0
//    bsp_eeprom_wt8(EEPROM_INIT_REAGPROBE_X_MOTOR_BACK_OFFSET_ADDER + 1*0, 80);	       //试剂针后方向复位位 0
//    bsp_eeprom_wt16(EEPROM_INIT_REAGPROBE_Y_MOTOR_L_STEPS_ADDER + 2*0, 200);		  //复位位 0
}

/*!*******************************************************电机初始化设置*****************************************************************/
/*******************************************************************************
* 名称: SetAllMotorDefaultPara
* 功能: 设置电机参数为默认值,电路板烧录程序之后的，位置参数默认值
* 形参: 
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void  SetAllMotorDefaultPara(void)
{
///////////////////////////////////////1 载体台位置参数设置//////////////////////////////////////////////////////////////
	//载体台电机速度设置
    bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*0, 10);	   // 载体台水平电机 (0)
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*1, 10);	   //载体台载体卡条推杆电机（1）
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*2, 10);      //载体卡条门翻转电机（2）
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*3, 10);      //TIP头仓盒电机（3）
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*4, 10);      //卡条盒进卡电机（2）
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*5, 10);      //卡条盒进卡电机（2）
	
	// 载体台水平电机，POS位置步数
    bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*0, 0);	       //1号进卡位（复位位）
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*1, 2750);	   //2号进卡位
    bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*2, 10200);	   //计数卡染色位
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*3, 13200);	   //计数卡加样位
    bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*4, 13300);	   //计数卡测试位
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*5, 7700);	   //检测卡加样位
    bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*6, 13000);	   //检测卡孵育位
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*7, 13900);	   //镜检进卡位
	
	//载体卡条推杆电，POS位置步数
    bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_PUSH_MOTOR_POS_STEP_ADDER + 2*0, 0);	  //初始位（复位位置）0
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_PUSH_MOTOR_POS_STEP_ADDER + 2*1, 19000);	  //推杆位 1
	
	
	//载体卡条门翻转电机，POS位置步数
    bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_FLIP_MOTOR_POS_STEP_ADDER + 2*0, 0);	   //初始位（复位位置）0
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_FLIP_MOTOR_POS_STEP_ADDER + 2*1, 2000);	   //翻转位置 1
	
	
	//TIP头仓盒电机，POS位置步数
    bsp_eeprom_wt16(EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER + 2*0, 0);	   //初始位（复位位置）0
	bsp_eeprom_wt16(EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER + 2*1, 840);	   //装TIP位置 1
	bsp_eeprom_wt16(EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER + 2*2, 14000);   //脱TIP位置 1
	bsp_eeprom_wt16(EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER + 2*3, 26000);   //装TIP盒子
	bsp_eeprom_wt16(EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER + 2*4, 675);	   //间距 1
	
	//卡条盒进卡电机，POS位置步数
    bsp_eeprom_wt16(EEPROM_INIT_CARD_SLICE_BOX_MOTOR_POS_STEP_ADDER + 2*0, 0);	  	 //初始位（复位位置）0
	bsp_eeprom_wt16(EEPROM_INIT_CARD_SLICE_BOX_MOTOR_POS_STEP_ADDER + 2*1, 12800);	  //进卡位 1

	//孵育盒温度设置值
    bsp_eeprom_wt16(EEPROM_INIT_TEMP_SET_ADDER + 2*0, 370);	  	 //孵育盒温度设置值
    bsp_eeprom_wt16(EEPROM_INIT_TEMP_SET_ADDER + 2*1, 0);	  	 //孵育盒温度补偿值

}



/*******************************************************************************
* 名称: ReadAllMotorPara
* 功能: 从EEPROM读取所有的参数，写入motor_para_t结构体
* 形参: 
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void  ReadAllMotorPara(void)
{
	uint8_t ucIndex = 0;
	uint16_t ucParaData = 0;

///////////////////////////////////1 载体台位置参数读取//////////////////////////////////////////////////////////////
	/*! 载体台电机速度 */
	for(ucIndex = 0; ucIndex <6; ucIndex++ )
	{
		ucParaData = bsp_eeprom_rd8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*ucIndex);
		MtxState[ucIndex].mMotorSpeed = ucParaData;
	}
	
	/*! 载体台水平电机，POS位置步数 */
	for(ucIndex = 0; ucIndex <= 7; ucIndex++ )
	{
		ucParaData = bsp_eeprom_rd16_2th(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*ucIndex);
		s_ucMotorOffsetPara.unload_p_horzMotorPosStep[ucIndex] = ucParaData;
	}
	
	/*! 载体卡条推杆电，POS位置步数 */
	for(ucIndex = 0; ucIndex <= 1; ucIndex++ )
	{
		ucParaData = bsp_eeprom_rd16_2th(EEPROM_INIT_LOAD_P_PUSH_MOTOR_POS_STEP_ADDER + 2*ucIndex);
		s_ucMotorOffsetPara.unload_p_pushMotorPosStep[ucIndex] = ucParaData;
	}

	/*! 载体卡条门翻转电机，POS位置步数 */
	for(ucIndex = 0; ucIndex < 2; ucIndex++ )
	{
		ucParaData = bsp_eeprom_rd16_2th(EEPROM_INIT_LOAD_P_FLIP_MOTOR_POS_STEP_ADDER + 2*ucIndex);
		s_ucMotorOffsetPara.unload_p_flipMotorPosStep[ucIndex] = ucParaData;
	}
	
	/*! TIP头仓盒电机，POS位置步数 */
	for(ucIndex = 0; ucIndex < 5; ucIndex++ )
	{
		ucParaData = bsp_eeprom_rd16_2th(EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER + 2*ucIndex);
		s_ucMotorOffsetPara.untip_head_boxMotorPosStep[ucIndex] = ucParaData;
	}
	
	/*! 卡条盒进卡电机，POS位置步数 */
	for(ucIndex = 0; ucIndex < 2; ucIndex++)
	{
		ucParaData = bsp_eeprom_rd16_2th(EEPROM_INIT_CARD_SLICE_BOX_MOTOR_POS_STEP_ADDER + 2*ucIndex);
		s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[ucIndex] = ucParaData;
	}
	
	/*! 孵育盒温度设置 */
	for(ucIndex = 0; ucIndex < 2; ucIndex++)
	{
		ucParaData = bsp_eeprom_rd16_2th(EEPROM_INIT_TEMP_SET_ADDER + 2*ucIndex);
		temp_para.Set_Temp[ucIndex] = ucParaData;
	}
	
	
}

/*******************************************************************************
* 名称: ReadAllMotorPara
* 功能: 驱动板上电读取参数
* 形参: 
			 pstFrameInfo   命令帧中包含所有的电机参数，
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void InitReadAllMotorPara(void)
{
	uint8_t ucVersion1 = 0;
	uint8_t ucVersion2 = 0;
	
	ucVersion1 = bsp_eeprom_rd8(EEPROM_IS_INIT_PARA_VERSION_1_ADDER);
	ucVersion2 = bsp_eeprom_rd8(EEPROM_IS_INIT_PARA_VERSION_2_ADDER);
	
	/*! EEPROM里面已经报存参数了，就读取参数 */
	if(ucVersion1 == INIT_PARA_VERSION_1 && ucVersion2 == INIT_PARA_VERSION_2)
	{
		SetAllMotorResetPosOffset();
		ReadAllMotorPara();
	}
	/*! EEPROM里面没有参数，写入参数版本号，写入默认参数 读取参数 */
	else 
	{
		bsp_eeprom_wt8(EEPROM_IS_INIT_PARA_VERSION_1_ADDER, INIT_PARA_VERSION_1);
		bsp_eeprom_wt8(EEPROM_IS_INIT_PARA_VERSION_2_ADDER, INIT_PARA_VERSION_2);
		
		SetAllMotorDefaultPara();
		
		ReadAllMotorPara();
	}	
}

/*******************************************************************************
* 名称: SetAllMotorPara
* 功能: 上位机通过一个数据帧，把所有电机的参数一次性，通过数据帧传下来
* 形参: 
			 pstFrameInfo   命令帧中包含所有的电机参数，
* 返回: 错误代码
* 说明: 无
********************************************************************************/
uint16_t GetMotorPosStep(uint8_t ucMotorId, uint8_t ucPos)
{
	uint16_t ucPosStep = 0;
	
	switch(ucMotorId)
	{
		 // 载体台水平电机
		case LOAD_P_HORZ_MT   : 	
			ucPosStep = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[ucPos];
			break; 
		
		// 载体卡条推杆电
		case LOAD_P_PUSH_MT   : 
			ucPosStep = s_ucMotorOffsetPara.unload_p_pushMotorPosStep[ucPos];
			break; 
		
		// 载体卡条门翻转电机
		case LOAD_P_FLIP_MT   :
			ucPosStep = s_ucMotorOffsetPara.unload_p_flipMotorPosStep[ucPos];
			break; 
		
		// TIP头仓盒电机
		case TIP_HEAD_BOX_MT	: 	
			ucPosStep = s_ucMotorOffsetPara.untip_head_boxMotorPosStep[ucPos];
			break;
		
		// 卡条盒进卡电机5
		case CARD_SLICE_BOX_MT	: 
			ucPosStep = s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[ucPos];
			break;
		
		default:
			break;
	}
	
	return ucPosStep;
}


/*!*******************************************************电机参数整体设置*****************************************************************/
/*******************************************************************************
* 名称: SetAllMotorPara
* 功能: 上位机通过一个数据帧，把所有电机的参数一次性，通过数据帧传下来
* 形参: 
			 pstFrameInfo   命令帧中包含所有的电机参数，
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void SetAllMotorPara(frame_info_t *pstFrameInfo)
{


}


/*******************************************************************************
* 名称: GetAllMotorPara
* 功能: 把所有的电机的参数，组装成一个命令帧，传到上位机
* 形参: 
			 pstFrameInfo   命令帧中包含所有的电机参数，
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void GetAllMotorPara(frame_info_t *pstFrameInfo)
{



}



/*!*******************************************************载体台参数设置*****************************************************************/
/*******************************************************************************
* 名称: SetSampleProbeHORZMotorPosStep
* 功能: 载体台水平电机向指定方向运动指定步数
* 形参: 
			ucPos	      电机位置
            ucPosStep,    位置步数
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void SetSampleProbeSpeed(uint16_t ucHorzSpeed, uint16_t ucLoadSpeed, uint16_t ucSyriSpeed, uint16_t ucPickSpeed)
{
	MtxState[0].mMotorSpeed = ucHorzSpeed;
	MtxState[1].mMotorSpeed = ucLoadSpeed;
	MtxState[2].mMotorSpeed = ucSyriSpeed;
	MtxState[3].mMotorSpeed = ucPickSpeed;
	
    bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*0, ucHorzSpeed);	    //载体台水平电机 (0)
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*1, ucLoadSpeed);	    //载体台装载电机（1）
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*2, ucSyriSpeed);      //载体台注射器电机（2）
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + 1*3, ucPickSpeed);      //载体台推片电机（3）
	
}


/*******************************************************************************
* 名称: SetSampleProbeHORZMotorPosStep
* 功能: 载体台水平电机向指定方向运动指定步数
* 形参: 
			ucPos	      电机位置
            ucPosStep,    位置步数
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void Setload_p_horzMotorPosStep(uint8_t ucPos, uint16_t ucPosStep)
{
	//反应位 1
	s_ucMotorOffsetPara.unload_p_horzMotorPosStep[ucPos] = ucPosStep;
	
	/*! 写入存储器 */
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_HORZ_MOTOR_POS_STEP_ADDER + 2*ucPos,  ucPosStep);
	
}

/*******************************************************************************
* 名称: SetSampleProbeLOADMotorPosStep
* 功能: 载体台水平电机向指定方向运动指定步数
* 形参: 
			ucPos	      电机位置
            ucPosStep,    位置步数
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void Setload_p_pushMotorPosStep(uint8_t ucPos, uint16_t ucPosStep)
{
	//反应位 1
	s_ucMotorOffsetPara.unload_p_pushMotorPosStep[ucPos] = ucPosStep;
	
	/*! 写入存储器 */
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_PUSH_MOTOR_POS_STEP_ADDER + 2*ucPos,  ucPosStep);
	
}


/*******************************************************************************
* 名称: SetSampleProbeHORZMotorPosStep
* 功能: 载体台水平电机向指定方向运动指定步数
* 形参: 
			ucPos	      电机位置
            ucPosStep,    位置步数
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void Setload_p_flipMotorPosStep(uint8_t ucPos, uint16_t ucPosStep)
{
	//反应位 1
	s_ucMotorOffsetPara.unload_p_flipMotorPosStep[ucPos] = ucPosStep;
	
	/*! 写入存储器 */
	bsp_eeprom_wt16(EEPROM_INIT_LOAD_P_FLIP_MOTOR_POS_STEP_ADDER + 2*ucPos,  ucPosStep);
	
}

/*******************************************************************************
* 名称: SetSampleProbeLOADMotorPosStep
* 功能: 载体台水平电机向指定方向运动指定步数
* 形参: 
			ucPos	      电机位置
            ucPosStep,    位置步数
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void Settip_head_boxMotorPosStep(uint8_t ucPos, uint16_t ucPosStep)
{
	//反应位 1
	s_ucMotorOffsetPara.untip_head_boxMotorPosStep[ucPos] = ucPosStep;
	
	/*! 写入存储器 */
	bsp_eeprom_wt16(EEPROM_INIT_TIP_HEAD_BOX_MOTOR_POS_STEP_ADDER + 2*ucPos,  ucPosStep);
	
}

/*******************************************************************************
* 名称: SetSampleProbeLOADMotorPosStep
* 功能: 载体台水平电机向指定方向运动指定步数
* 形参: 
			ucPos	      电机位置
            ucPosStep,    位置步数
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void Setcard_slice_boxMMotorPosStep(uint8_t ucPos, uint16_t ucPosStep)
{
	//反应位 1
	s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[ucPos] = ucPosStep;
	
	/*! 写入存储器 */
	bsp_eeprom_wt16(EEPROM_INIT_CARD_SLICE_BOX_MOTOR_POS_STEP_ADDER + 2*ucPos,  ucPosStep);
	
}

/*******************************************************************************
* 名称: Set_temp_para
* 功能: 
* 形参: 
			ucPos	      电机位置
            ucPosStep,    位置步数
* 返回: 错误代码
* 说明: 无
********************************************************************************/
void Set_temp_para(uint8_t ucPos, uint16_t unTemp)
{
	//温度
	temp_para.Set_Temp[ucPos] = unTemp;
	
	/*! 写入存储器 */
	bsp_eeprom_wt16(EEPROM_INIT_TEMP_SET_ADDER + 2*ucPos,  unTemp);
	
}




/*******************************************************************************
* 名称: Getload_p_horzMotorPosStep
* 功能: 电机向指定方向运动指定步数
* 形参: 
			 ucPos   电机编号
* 返回: 错误代码
* 说明: 无
********************************************************************************/
uint16_t Getload_p_horzMotorPosStep( uint8_t ucPos)
{
	uint16_t nSteps = 0;

	nSteps = s_ucMotorOffsetPara.unload_p_horzMotorPosStep[ucPos];
	
	return nSteps;
}


/*******************************************************************************
* 名称: Getload_p_pushMotorPosStep
* 功能: 电机向指定方向运动指定步数
* 形参: 
			 ucPos   电机编号
* 返回: 错误代码
* 说明: 无
********************************************************************************/
uint16_t Getload_p_pushMotorPosStep( uint8_t ucPos)
{
	uint16_t nSteps = 0;

	nSteps = s_ucMotorOffsetPara.unload_p_pushMotorPosStep[ucPos];
	
	return nSteps;
}

/*******************************************************************************
* 名称: Getload_p_flipMotorPosStep
* 功能: 电机向指定方向运动指定步数
* 形参: 
			 ucPos   电机编号
* 返回: 错误代码
* 说明: 无
********************************************************************************/
uint16_t Getload_p_flipMotorPosStep( uint8_t ucPos)
{
	uint16_t nSteps = 0;

	nSteps = s_ucMotorOffsetPara.unload_p_flipMotorPosStep[ucPos];
	
	return nSteps;
}



/*******************************************************************************
* 名称: Gettip_head_boxMotorPosStep
* 功能: 电机向指定方向运动指定步数
* 形参: 
			 ucPos   电机编号
* 返回: 错误代码
* 说明: 无
********************************************************************************/
uint16_t Gettip_head_boxMotorPosStep( uint8_t ucPos)
{
	uint16_t nSteps = 0;

	nSteps = s_ucMotorOffsetPara.untip_head_boxMotorPosStep[ucPos];
	
	return nSteps;
}


/*******************************************************************************
* 名称: Gettip_head_boxMotorPosStep
* 功能: 电机向指定方向运动指定步数
* 形参: 
			 ucPos   电机编号
* 返回: 错误代码
* 说明: 无
********************************************************************************/
uint16_t Getcard_slice_boxMotorMotorPosStep( uint8_t ucPos)
{
	uint16_t nSteps = 0;

	nSteps = s_ucMotorOffsetPara.uncard_slice_boxMotorPosStep[ucPos];
	
	return nSteps;
}


/*******************************************************************************
* 名称: get_temp_para
* 功能: 获取温度
* 形参: 
			 ucPos   电机编号
* 返回: 错误代码
* 说明: 无
********************************************************************************/
uint16_t get_temp_para(uint8_t ucPos)
{
	uint16_t nSteps = 0;

	nSteps = temp_para.Set_Temp[ucPos];
	
	return nSteps;
}













