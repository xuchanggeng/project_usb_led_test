/**
  ******************************************************************************
  * @文件   send_frame_task.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2020 www.geniusmedica.com
  ******************************************************************************  
  */ 

/* 文件包含 ---------------------------------------------------------------*/
//#include "send_frame_task.h"
#include <includes.h>
#include "Com_Dispatcher.h"
#include "motor_app.h"
#include "motor.h"
#include "bsp_fpga.h"
#include "fpga_adder.h"
#include "motor_pos_def.h"
#include "motor_para.h"


/* 本地类型 --------------------------------------------------------------*/
/* 本地常量 --------------------------------------------------------------*/
#define 			MOTOR_CHECK_LIQUID_DEFAULT_VALUE					  0	
/* 本地宏定义 ------------------------------------------------------------*/
/* 本地变量 ---------------------------------------------------------------*/

/* 函数原型 ---------------------------------------------------------------*/
void motor_check_liquid_init(void);

/* 本地函数 ----------------------------------------------------------------*/


/*! 电机数据存储buffer */
motor_param_t  s_astSampleMotor[ MAX_MOTOR_NUM ];

/*! 电机正常运动时最大步数限制表: */
static uint16_t  s_aunSampleMaxStepTable[MAX_MOTOR_NUM] = 
{ 
    4710,//样本针X电机
    6100,//样本针Y电机     
    8500,//试剂针X电机   
    6900,//试剂针Y电机左  
    6900,//试剂针Y电机右     
    5000,//自动清洗Y电机     
    1000,//扫描枪旋转电机     
    22,//反应盘旋转电机 实际1446步 为找到初始位再走码盘    
    10,//试剂盘旋转电机   为找到初始为再走码盘     
    16500,//样本注射泵        
    16500,//试剂注射泵电机1  
    16500,//试剂注射泵电机2 
    26500,//清水注射泵电机1  
    26500,//清水注射泵电机2  
    5,//空              
    26500,//稀释液注射泵电机
};

/*!  电机上次的位置,默认为“不知道在什么位置”(实际的电机物理地址) */
//static uint8_t s_ucMotorType[0].ucMotorLastRealPos = REAL_POS_NONE_SAMPROBE_X_MOTOR;

motor_type_t           s_ucMotorType[MAX_MOTOR_NUM]  = {0};
motor_para_t           s_ucMotorOffsetPara           = {0};
static motor_check_liquid_t  s_stMotorCheckLiquid[MAX_MOTOR_NUM]  = {0};
static uint8_t         s_ucIsCheckLiguidFlag         = 1;
static uint8_t         s_ucaSampLiguiDepthType[SAMP_TYPE_MAX]         = {25, 20, 20, 50};
static uint8_t 		   s_ucReagLiguiDepth    = 50;
static uint8_t		   s_ucSampType          = 1;             //默认为血清，血清的步数是最小的


/*!  电机的无效位置 */
static  uint8_t s_ucaMotorMaxRealPos[MAX_MOTOR_NUM]  = 
{
    REAL_MAX_POS_SAMPROBE_X_MOTOR,
    REAL_MAX_POS_SAMPROBE_Y_MOTOR,	
    REAL_MAX_POS_REAGPROBE_X_MOTOR,
    REAL_MAX_POS_REAGPROBE_Y_MOTOR_L,
    REAL_MAX_POS_REAGPROBE_Y_MOTOR_R,
    REAL_MAX_POS_WASH_PROBE_MOTOR,
    REAL_MAX_POS_SCAN_MOTOR,
    REAL_MAX_POS_REACTION_MOTOR ,
    REAL_MAX_POS_REAGENT_MOTOR ,	
    REAL_MAX_POS_SAMPLE_SYRINGE_MOTOR ,	
    REAL_MAX_POS_REAGENT_SYRINGE1_MOTOR ,	
    REAL_MAX_POS_REAGENT_SYRINGE2_MOTOR ,	
    REAL_MAX_POS_WATER_SYRINGE1_MOTOR ,	
    REAL_MAX_POS_WATER_SYRINGE2_MOTOR ,	
    REAL_MAX_POS_DILUENT_SYRINGE_MOTOR ,
};

static  uint8_t s_ucaMotorNoneRealPos[MAX_MOTOR_NUM] = 
{
    REAL_POS_NONE_SAMPROBE_X_MOTOR, 
    REAL_POS_NONE_SAMPROBE_Y_MOTOR,	
    REAL_POS_NONE_REAGPROBE_X_MOTOR,
    REAL_POS_NONE_REAGPROBE_Y_MOTOR_L,	
    REAL_POS_NONE_REAGPROBE_Y_MOTOR_R,			
    REAL_POS_NONE_WASH_PROBE_MOTOR,		
    REAL_POS_NONE_SCAN_MOTOR,	
    REAL_POS_NONE_REACTION_MOTOR,		    
    REAL_POS_NONE_REAGENT_MOTOR,		
    REAL_POS_NONE_SAMPLE_SYRINGE_MOTOR,
    REAL_POS_NONE_REAGENT_SYRINGE1_MOTOR,
    REAL_POS_NONE_REAGENT_SYRINGE2_MOTOR,
    REAL_POS_NONE_WATER_SYRINGE1_MOTOR,
    REAL_POS_NONE_WATER_SYRINGE2_MOTOR,    
    REAL_POS_NONE_DILUENT_SYRINGE_MOTOR,
};

static  uint8_t s_ucaMotorInitRealPos[MAX_MOTOR_NUM] =
{
    REAL_POS_INIT_SAMPROBE_X_MOTOR,
    REAL_POS_INIT_SAMPROBE_Y_MOTOR,
    REAL_POS_INIT_REAGPROBE_X_MOTOR,
    REAL_POS_INIT_REAGPROBE_Y_MOTOR_L,
    REAL_POS_INIT_REAGPROBE_Y_MOTOR_R,
    REAL_POS_INIT_WASH_PROBE_MOTOR,
    REAL_POS_INIT_SCAN_MOTOR,
    REAL_POS_INIT_REACTION_MOTOR,
    REAL_POS_INIT_REAGENT_MOTOR,
    REAL_POS_INIT_SAMPLE_SYRINGE_MOTOR,
    REAL_POS_INIT_REAGENT_SYRINGE1_MOTOR,
    REAL_POS_INIT_REAGENT_SYRINGE2_MOTOR,
    REAL_POS_INIT_WATER_SYRINGE1_MOTOR,
    REAL_POS_INIT_WATER_SYRINGE2_MOTOR,
    REAL_POS_INIT_DILUENT_SYRINGE_MOTOR,
};


/**********************************************************************
* 函数名称：SetMotorSpeed
* 功能描述：设置电机速度
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ：用于初始化样本针电机的各个参数
**********************************************************************/
void SetMotorSpeed(uint8_t _ucMotorId, uint8_t _ucSpeedLevel)
{
	s_ucMotorType[_ucMotorId].ucMotorRunSpeed   = _ucSpeedLevel;
	
	//保存电机速度
	bsp_eeprom_wt8(EEPROM_MOTOR_SPEED_LEVEL_ADDER + _ucMotorId, _ucSpeedLevel);		  
}


/**********************************************************************
* 函数名称：SetSampLiguiDepthType
* 功能描述： 根据样本类型，设置样本针的插水深度
* 输入参数：
			_ucSampType  电机编号
			_ucDepthStep  是否检测液面
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ： 只有垂直电机，需要吸液体的电机有此功能
**********************************************************************/
void SetSampLiguiDepthType(uint8_t _ucSampType, uint8_t _ucDepthStep)
{
	if(_ucSampType >= SAMP_TYPE_MAX)
	{
		return;
	}

	s_ucaSampLiguiDepthType[_ucSampType] = _ucDepthStep;
	bsp_eeprom_wt8(EEPROM_SAMPROBE_WATER_DEEP_STEP_ADDER + _ucSampType, _ucDepthStep);		  
}



/**********************************************************************
* 函数名称：SetSampLiguiDepthType
* 功能描述： 根据样本类型，设置样本针的插水深度
* 输入参数：
			_ucSampType  电机编号
			_ucDepthStep  是否检测液面
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ： 只有垂直电机，需要吸液体的电机有此功能
**********************************************************************/
void SetReagLiguiDepth(uint8_t _ucDepthStep)
{
	s_ucReagLiguiDepth = _ucDepthStep;
	bsp_eeprom_wt8(EEPROM_REAGROBE_WATER_DEEP_STEP_ADDER, _ucDepthStep);		  
}

/**********************************************************************
* 函数名称： GetReag试剂针的插水深度
* 输入参数：无
* 输出参数：无
* 返 回 值：
			试剂针插水深度
* 其它说明：
*		  ： 只有垂直电机，需要吸液体的电机有此功能
**********************************************************************/
uint8_t GetReagLiguiDepth(void)
{
	return s_ucReagLiguiDepth;
}


/**********************************************************************
* 函数名称：SetMotorIsCheckLiquid
* 功能描述：设置电机是否检测液面
* 输入参数：
			_ucMotorId  电机编号
			_ucIsCheck  是否检测液面
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ： 只有垂直电机，需要吸液体的电机有此功能
**********************************************************************/
void SetMotorIsCheckLiquid(uint8_t _ucIsCheck)
{
	uint8_t   i = 0;
	
	/*! 液面检测开关 */
	for(i = 0; i < MOTOR_CHECK_LIQUID_NUM; i++)
	{
		s_stMotorCheckLiquid[i].ucMotorCheckLiquidFlag   = _ucIsCheck;
		s_stMotorCheckLiquid[i].ucMotorCheckLiquidStep   = 0;
		s_stMotorCheckLiquid[i].ucMotorCheckLiquidSignal = 0;
	}
	s_ucIsCheckLiguidFlag = _ucIsCheck;
}


/**********************************************************************
* 函数名称：GetMotorIsCheckLiquid
* 功能描述：获取电机是否检测液面
* 输入参数：
* 输出参数：
			是否检测液面
* 返 回 值：无
* 其它说明：
*		  ： 只有垂直电机，需要吸液体的电机有此功能
**********************************************************************/
uint8_t GetMotorIsCheckLiquid(void)
{
	return s_ucIsCheckLiguidFlag;
}

/**********************************************************************
* 函数名称：motor_init
* 功能描述：电机初始化
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ：用于初始化样本针电机的各个参数
**********************************************************************/
void motor_init(void)
{
	/*! 电机状态初始化 */
	sample_motor_state_init();
	
	/*! 电机状态的控制，初始位置的设置 */
	motor_none_real_pos_init();	
	
	/*! 电机液面检测功能的设置 */
	motor_check_liquid_init();
}


/**********************************************************************
* 函数名称：motor_none_real_pos_init
* 功能描述：每个电机初始位置，无效位置的初始化
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ：用于初始化样本针电机的各个参数
**********************************************************************/
void motor_none_real_pos_init(void)
{
	/*! 样本针X电机 */
	s_ucMotorType[SAMPLE_X_MOTOR].ucMotorType        = MOTOR_TYPE_CODE;
//	s_ucMotorType[SAMPLE_X_MOTOR].ucMotorRunSpeed    = SPEED_SAMPROBE_X_MOTOR;
	s_ucMotorType[SAMPLE_X_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_SAMPROBE_X_MOTOR;
	s_ucMotorType[SAMPLE_X_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[SAMPLE_X_MOTOR]            = REAL_POS_NONE_SAMPROBE_X_MOTOR;
	s_ucaMotorInitRealPos[SAMPLE_X_MOTOR]			 = REAL_POS_INIT_SAMPROBE_X_MOTOR;
	s_ucaMotorMaxRealPos[SAMPLE_X_MOTOR]             = REAL_MAX_POS_SAMPROBE_X_MOTOR;
	
	/*! 样本针Y电机 */
	s_ucMotorType[SAMPLE_Y_MOTOR].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[SAMPLE_Y_MOTOR].ucMotorRunSpeed    = SPEED_SAMPROBE_Y_MOTOR;
	s_ucMotorType[SAMPLE_Y_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_SAMPROBE_Y_MOTOR;
	s_ucMotorType[SAMPLE_Y_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[SAMPLE_Y_MOTOR]            = REAL_POS_NONE_SAMPROBE_Y_MOTOR;
	s_ucaMotorInitRealPos[SAMPLE_Y_MOTOR]			 = REAL_POS_INIT_SAMPROBE_Y_MOTOR;
	s_ucaMotorMaxRealPos[SAMPLE_Y_MOTOR]             = REAL_MAX_POS_SAMPROBE_Y_MOTOR;
	
	/*! 试剂针针X电机 */
	s_ucMotorType[REAGENT_X_MOTOR].ucMotorType        = MOTOR_TYPE_CODE;
//	s_ucMotorType[REAGENT_X_MOTOR].ucMotorRunSpeed    = SPEED_REAGPROBE_X_MOTOR;
	s_ucMotorType[REAGENT_X_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_REAGPROBE_X_MOTOR;
	s_ucMotorType[REAGENT_X_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[REAGENT_X_MOTOR]            = REAL_POS_NONE_REAGPROBE_X_MOTOR;
	s_ucaMotorInitRealPos[REAGENT_X_MOTOR]			  = REAL_POS_INIT_REAGPROBE_X_MOTOR;
	s_ucaMotorMaxRealPos[REAGENT_X_MOTOR]             = REAL_MAX_POS_REAGPROBE_X_MOTOR;
	
	/*! 试剂针Y电机L */
	s_ucMotorType[REAGENT_Y_MOTOR_L].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[REAGENT_Y_MOTOR_L].ucMotorRunSpeed    = SPEED_REAGPROBE_Y_MOTOR_L;
	s_ucMotorType[REAGENT_Y_MOTOR_L].ucMotorLastRealPos = REAL_POS_NONE_REAGPROBE_Y_MOTOR_L;
	s_ucMotorType[SAMPLE_X_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[REAGENT_Y_MOTOR_L]            = REAL_POS_NONE_REAGPROBE_Y_MOTOR_L;
	s_ucaMotorInitRealPos[REAGENT_Y_MOTOR_L]			 = REAL_POS_INIT_REAGPROBE_Y_MOTOR_L;
	s_ucaMotorMaxRealPos[REAGENT_Y_MOTOR_L]             = REAL_MAX_POS_REAGPROBE_Y_MOTOR_L;
	
	/*! 试剂针Y电机R  */
	s_ucMotorType[REAGENT_Y_MOTOR_R].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[REAGENT_Y_MOTOR_R].ucMotorRunSpeed    = SPEED_REAGPROBE_Y_MOTOR_R;
	s_ucMotorType[REAGENT_Y_MOTOR_R].ucMotorLastRealPos = REAL_POS_NONE_REAGPROBE_Y_MOTOR_R;
	s_ucMotorType[REAGENT_Y_MOTOR_R].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[REAGENT_Y_MOTOR_R]            = REAL_POS_NONE_REAGPROBE_Y_MOTOR_R;
	s_ucaMotorInitRealPos[REAGENT_Y_MOTOR_R]			= REAL_POS_INIT_REAGPROBE_Y_MOTOR_R;
	s_ucaMotorMaxRealPos[REAGENT_Y_MOTOR_R]             = REAL_MAX_POS_REAGPROBE_Y_MOTOR_R;

	/*! 清洗针垂直电机 */
	s_ucMotorType[WASH_Y_MOTOR].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[WASH_Y_MOTOR].ucMotorRunSpeed    = SPEED_WASH_PROBE_MOTOR;
	s_ucMotorType[WASH_Y_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_WASH_PROBE_MOTOR;
	s_ucMotorType[SAMPLE_X_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[WASH_Y_MOTOR]            = REAL_POS_NONE_WASH_PROBE_MOTOR;
	s_ucaMotorInitRealPos[WASH_Y_MOTOR]			   = REAL_POS_INIT_WASH_PROBE_MOTOR;
	s_ucaMotorMaxRealPos[WASH_Y_MOTOR]             = REAL_MAX_POS_WASH_PROBE_MOTOR;

	/*! 旋转扫码电机  */
	s_ucMotorType[SCAN_SPIN_MOTOR].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[SCAN_SPIN_MOTOR].ucMotorRunSpeed    = SPEED_SCAN_MOTOR;
	s_ucMotorType[SCAN_SPIN_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_SCAN_MOTOR;
	s_ucMotorType[SCAN_SPIN_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[SCAN_SPIN_MOTOR]            = REAL_POS_NONE_SCAN_MOTOR;
	s_ucaMotorInitRealPos[SCAN_SPIN_MOTOR]			  = REAL_POS_INIT_SCAN_MOTOR;
	s_ucaMotorMaxRealPos[SCAN_SPIN_MOTOR]             = REAL_MAX_POS_SCAN_MOTOR;
	
	/*! 反应盘电机 */
	s_ucMotorType[REACTION_SPIN_MOTOR].ucMotorType        = MOTOR_TYPE_CODE;
//	s_ucMotorType[REACTION_SPIN_MOTOR].ucMotorRunSpeed    = SPEED_REACTION_MOTOR;
	s_ucMotorType[REACTION_SPIN_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_REACTION_MOTOR;
	s_ucMotorType[REACTION_SPIN_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[REACTION_SPIN_MOTOR]            = REAL_POS_NONE_REACTION_MOTOR;
	s_ucaMotorInitRealPos[REACTION_SPIN_MOTOR]			 = REAL_POS_INIT_REACTION_MOTOR;
	s_ucaMotorMaxRealPos[REACTION_SPIN_MOTOR]             = REAL_MAX_POS_REACTION_MOTOR;

	/*! 试剂盘电机 */
	s_ucMotorType[REAGENT_SPIN_MOTOR].ucMotorType        = MOTOR_TYPE_CODE;
//	s_ucMotorType[REAGENT_SPIN_MOTOR].ucMotorRunSpeed    = SPEED_REAGENT_MOTOR;
	s_ucMotorType[REAGENT_SPIN_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_REAGENT_MOTOR;
	s_ucMotorType[REAGENT_SPIN_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[REAGENT_SPIN_MOTOR]            = REAL_POS_NONE_REAGENT_MOTOR;
	s_ucaMotorInitRealPos[REAGENT_SPIN_MOTOR]			 = REAL_POS_INIT_REAGENT_MOTOR;
	s_ucaMotorMaxRealPos[REAGENT_SPIN_MOTOR]             = REAL_MAX_POS_REAGENT_MOTOR;
    
    /*! 样本注射泵电机 */
	s_ucMotorType[SAMPLE_SYRINGE_MOTOR].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[SAMPLE_SYRINGE_MOTOR].ucMotorRunSpeed    = SPEED_SAMPLE_SYRINGE_MOTOR;
	s_ucMotorType[SAMPLE_SYRINGE_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_SAMPLE_SYRINGE_MOTOR;
	s_ucMotorType[SAMPLE_SYRINGE_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[SAMPLE_SYRINGE_MOTOR]            = REAL_POS_NONE_SAMPLE_SYRINGE_MOTOR;
	s_ucaMotorInitRealPos[SAMPLE_SYRINGE_MOTOR]			 = REAL_POS_INIT_SAMPLE_SYRINGE_MOTOR;
	s_ucaMotorMaxRealPos[SAMPLE_SYRINGE_MOTOR]             = REAL_MAX_POS_SAMPLE_SYRINGE_MOTOR;	
    
    /*! 试剂注射泵1电机 */
	s_ucMotorType[REAGENT_SYRINGE_MOTOR_1].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[REAGENT_SYRINGE_MOTOR_1].ucMotorRunSpeed    = SPEED_REAGENT_SYRINGE_MOTOR_1;
	s_ucMotorType[REAGENT_SYRINGE_MOTOR_1].ucMotorLastRealPos = REAL_POS_NONE_REAGENT_SYRINGE1_MOTOR;
	s_ucMotorType[REAGENT_SYRINGE_MOTOR_1].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[REAGENT_SYRINGE_MOTOR_1]            = REAL_POS_NONE_REAGENT_SYRINGE1_MOTOR;
	s_ucaMotorInitRealPos[REAGENT_SYRINGE_MOTOR_1]			 = REAL_POS_INIT_REAGENT_SYRINGE1_MOTOR;
	s_ucaMotorMaxRealPos[REAGENT_SYRINGE_MOTOR_1]             = REAL_MAX_POS_REAGENT_SYRINGE1_MOTOR;	
    
    /*! 试剂注射泵2电机 */
	s_ucMotorType[REAGENT_SYRINGE_MOTOR_2].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[REAGENT_SYRINGE_MOTOR_2].ucMotorRunSpeed    = SPEED_REAGENT_SYRINGE_MOTOR_2;
	s_ucMotorType[REAGENT_SYRINGE_MOTOR_2].ucMotorLastRealPos = REAL_POS_NONE_REAGENT_SYRINGE2_MOTOR;
	s_ucMotorType[REAGENT_SYRINGE_MOTOR_2].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[REAGENT_SYRINGE_MOTOR_2]            = REAL_POS_NONE_REAGENT_SYRINGE2_MOTOR;
	s_ucaMotorInitRealPos[REAGENT_SYRINGE_MOTOR_2]			 = REAL_POS_INIT_REAGENT_SYRINGE2_MOTOR;
	s_ucaMotorMaxRealPos[REAGENT_SYRINGE_MOTOR_2]             = REAL_MAX_POS_REAGENT_SYRINGE2_MOTOR;
    
    /*! 清水注射泵1电机 */
	s_ucMotorType[WATER_SYRINGE_MOTOR_1].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[WATER_SYRINGE_MOTOR_1].ucMotorRunSpeed    = SPEED_WATER_SYRINGE_MOTOR_1;
	s_ucMotorType[WATER_SYRINGE_MOTOR_1].ucMotorLastRealPos = REAL_POS_NONE_WATER_SYRINGE1_MOTOR;
	s_ucMotorType[WATER_SYRINGE_MOTOR_1].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[WATER_SYRINGE_MOTOR_1]            = REAL_POS_NONE_WATER_SYRINGE1_MOTOR;
	s_ucaMotorInitRealPos[WATER_SYRINGE_MOTOR_1]			 = REAL_POS_INIT_WATER_SYRINGE1_MOTOR;
	s_ucaMotorMaxRealPos[WATER_SYRINGE_MOTOR_1]             = REAL_MAX_POS_WATER_SYRINGE1_MOTOR;	
    
    /*! 清水注射泵2电机 */
	s_ucMotorType[WATER_SYRINGE_MOTOR_2].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[WATER_SYRINGE_MOTOR_2].ucMotorRunSpeed    = SPEED_WATER_SYRINGE_MOTOR_2;
	s_ucMotorType[WATER_SYRINGE_MOTOR_2].ucMotorLastRealPos = REAL_POS_NONE_WATER_SYRINGE2_MOTOR;
	s_ucMotorType[WATER_SYRINGE_MOTOR_2].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[WATER_SYRINGE_MOTOR_2]            = REAL_POS_NONE_WATER_SYRINGE2_MOTOR;
	s_ucaMotorInitRealPos[WATER_SYRINGE_MOTOR_2]			 = REAL_POS_INIT_WATER_SYRINGE2_MOTOR;
	s_ucaMotorMaxRealPos[WATER_SYRINGE_MOTOR_2]             = REAL_MAX_POS_WATER_SYRINGE2_MOTOR; 
    
    /*!稀释液注射泵电机 */
	s_ucMotorType[DILUENT_SYRINGE_MOTOR].ucMotorType        = MOTOR_TYPE_STEP;
//	s_ucMotorType[DILUENT_SYRINGE_MOTOR].ucMotorRunSpeed    = SPEED_DILUENT_SYRINGE_MOTOR;
	s_ucMotorType[DILUENT_SYRINGE_MOTOR].ucMotorLastRealPos = REAL_POS_NONE_DILUENT_SYRINGE_MOTOR;
	s_ucMotorType[DILUENT_SYRINGE_MOTOR].ucErrCode          = 0;
	s_ucaMotorNoneRealPos[DILUENT_SYRINGE_MOTOR]            = REAL_POS_NONE_DILUENT_SYRINGE_MOTOR;
	s_ucaMotorInitRealPos[DILUENT_SYRINGE_MOTOR]			 = REAL_POS_INIT_DILUENT_SYRINGE_MOTOR;
	s_ucaMotorMaxRealPos[DILUENT_SYRINGE_MOTOR]             = REAL_MAX_POS_DILUENT_SYRINGE_MOTOR;   
}



/**********************************************************************
* 函数名称：sample_motor_state_init
* 功能描述：电机状态初始化
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ：用于初始化样本针电机的各个参数
**********************************************************************/
void sample_motor_state_init(void)
{
	uint8_t ucIntex = 0;
	
	for (ucIntex = 0; ucIntex < MAX_MOTOR_NUM; ucIntex++)
	{
		memset(&s_astSampleMotor[ucIntex], 0 ,sizeof(motor_param_t));
		s_astSampleMotor[ucIntex].ucState = MOTOR_STATE_IDLE;
	}
}


/**********************************************************************
* motor_check_liquid_init
* 功能描述：电机状态初始化
* 输入参数：无
* 输出参数：无
* 返 回 值：无
* 其它说明：
*		  ：用于初始化样本针电机的各个参数
**********************************************************************/
void motor_check_liquid_init(void)
{
	uint8_t ucIndex = 0;
	
	/*! 电机液面检测功能的设置 */
	for(uint8_t i = 0; i < MOTOR_CHECK_LIQUID_NUM; i++)
	{
		s_stMotorCheckLiquid[i].ucMotorCheckLiquidFlag   = MOTOR_CHECK_LIQUID_DEFAULT_VALUE;
		s_stMotorCheckLiquid[i].ucMotorCheckLiquidStep   = 0;
		s_stMotorCheckLiquid[i].ucMotorCheckLiquidSignal = 0;
		s_stMotorCheckLiquid[i].ucMotorCheckHitSignal    = 0;
	}
	
	s_ucIsCheckLiguidFlag = 1;
	
	/*! 读取试剂针液面高度 */
	s_ucReagLiguiDepth = bsp_eeprom_rd8(EEPROM_REAGROBE_WATER_DEEP_STEP_ADDER);
	
	/*! 读取样本针各个类型液面高度 */
	for(ucIndex = 0; ucIndex < 4; ucIndex++)
	{
		s_ucaSampLiguiDepthType[ucIndex] = bsp_eeprom_rd8(EEPROM_SAMPROBE_WATER_DEEP_STEP_ADDER + ucIndex);
	}
	
}




void clear_liquid_hit_state(int32_t lMotorType)
{
	s_stMotorCheckLiquid[lMotorType].ucMotorCheckLiquidStep   = 0;
	s_stMotorCheckLiquid[lMotorType].ucMotorCheckLiquidSignal = 0;
	s_stMotorCheckLiquid[lMotorType].ucMotorCheckHitSignal    = 0;
}

/**********************************************************************
* 函数名称：get_sample_motor_buffer
* 功能描述：获取样本针电机数据存储buffer
* 输入参数：无	 
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其它说明：无
**********************************************************************/
motor_param_t* get_sample_motor_buffer( void )
{
	return  s_astSampleMotor;
}

/**********************************************************************
* 函数名称：position_change_to_motor_step
* 功能描述：将电机位置转换成具体的电机步数
* 输入参数：
*         ：lMotorType   ：电机类型		  
*         ：ucSrcPosition：起始位置		ucDesPosition：目标位置
* 输出参数：无
* 返 回 值：
*         ：电机步数
* 其它说明：
*         ：X轴电机的和杯号选择电机位置步数放置在FPGA中,所以下位机只需要发送具体位置即可;
*         ：Y轴电机的位置步数放置在下位机中,所以下位机要自己计算出具体的电
*         ：机行走步数,然后下发给FPGA
**********************************************************************/
static uint16_t position_change_to_motor_step( int32_t lMotorType,uint8_t ucSrcPosition,uint8_t ucDesPosition )
{
	uint16_t ulResult = 0;
	
	do
	{
		/*! 对于码盘电机,起始位置放在“运动步数低字节”中，结束位置放在“运动步数高字节”中 */
		if(MOTOR_TYPE_CODE == s_ucMotorType[lMotorType].ucMotorType)
		{
			/*! 目标位置 + 起始位置 */
			ulResult = (uint16_t)( (ucDesPosition << 8) & 0xff00 ) + (uint16_t)( ucSrcPosition & 0xff );
            if(ucDesPosition == ucSrcPosition)
            {
                ulResult = 0;
            }
		}
		/*! 对于步数电机,直接下发具体的步数即可 */
		else if(MOTOR_TYPE_STEP == s_ucMotorType[lMotorType].ucMotorType)
		{
			if(SAMPLE_Y_MOTOR == lMotorType)
			{
				/*! 电机步数 */
				ulResult = abs(s_ucMotorOffsetPara.unSampleYMotorPosStepTable[ucDesPosition] - s_ucMotorOffsetPara.unSampleYMotorPosStepTable[ucSrcPosition]);
			}
			else if(REAGENT_Y_MOTOR_L == lMotorType)
			{
				/*! 电机步数 */
				ulResult = abs(s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_L[ucDesPosition] - s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_L[ucSrcPosition]);
			}
			else if(REAGENT_Y_MOTOR_R == lMotorType)
			{
				/*! 电机步数 */
				ulResult = abs(s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_R[ucDesPosition] - s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_R[ucSrcPosition]);
			}
			else if(WASH_Y_MOTOR == lMotorType)
			{
				/*! 电机步数 */
				ulResult = abs(s_ucMotorOffsetPara.unWashMotorPosStepTable[ucDesPosition] - s_ucMotorOffsetPara.unWashMotorPosStepTable[ucSrcPosition]);
			}
			else if(SCAN_SPIN_MOTOR == lMotorType)
			{
				/*! 电机步数 */
				ulResult = abs(s_ucMotorOffsetPara.unScanMotorPosStepTable[ucDesPosition] - s_ucMotorOffsetPara.unScanMotorPosStepTable[ucSrcPosition]);
				s_astSampleMotor[lMotorType].ucRunMode = STEP_MODE_MOTOR;
			}
			else 
			{
				/*! 电机步数 */
				//注射器电机不调用这个函数
			}				
		}
		
	}while(0);
	
	return ulResult;
}

/**********************************************************************
* 函数名称：get_sample_motor_real_position
* 功能描述：获取样本针电机实际的位置
* 输入参数：
*         ：lMotorType：电机类型 
* 输出参数：无
* 返 回 值：
*         ：电机位置
* 其它说明：
*         ：Y轴的实际位置与时序位置一致
**********************************************************************/
uint8_t get_sample_motor_real_position(uint32_t lMotorType)
{
	return s_ucMotorType[lMotorType].ucMotorLastRealPos;
}


/**********************************************************************
* 函数名称：read_sample_motor_sensor
* 功能描述：读取电机传感器信号
* 输入参数：
*         ：lMotorType:电机类型		ucSenserType：传感器类型
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其它说明：无
**********************************************************************/
int32_t read_sample_motor_sensor( int32_t lMotorType,uint8_t ucSenserType)
{
    uint8_t ucSensor = 0;
    int32_t lRet = EXIT_FAILURE;

    do
    {
		/*! 如果步数码盘电机，就没有位置光耦 */
		if((s_ucMotorType[lMotorType].ucMotorType != MOTOR_TYPE_CODE) && (ucSenserType == POS_SENSER))
		{
			PRO_DEBUG(MOTOR_DEBUG,("read_sample_motor_sensor err %d \r\n", lMotorType));
		}
		
		/*! 从FPGA中读电机传感器信号 */
		if ( EXIT_FAILURE == bsp_fpga_read_single_data((uint8_t)FPGA_MOTOR_SENSER_BASE_ADDER, (uint8_t)(lMotorType + SAMPLE_OFFSET_INDEX), &ucSensor) )
		{
			break;		
		}
		
		/*! 传感器类型：复位传感器 */
		if(RESET_SENSER == ucSenserType)
		{
			if ( BIT0 == (ucSensor & BIT0) ) 
			{
				/*! 不在起始位 */
				s_astSampleMotor[lMotorType].ucCurrentSensor = SAMPLE_MOTOR_OUT_POSITION;
			}
			else
			{
				/*!在起始位 */
				s_astSampleMotor[lMotorType].ucCurrentSensor = SAMPLE_MOTOR_IN_POSITION;
			}
		}
		else if(POS_SENSER == ucSenserType)
		{
			if ( BIT1 == (ucSensor & BIT1) ) 
			{
				/*! 不在起始位 */
				s_astSampleMotor[lMotorType].ucCurrentSensor = SAMPLE_MOTOR_OUT_POSITION;
			}
			else
			{
				/*!在起始位 */
				s_astSampleMotor[lMotorType].ucCurrentSensor = SAMPLE_MOTOR_IN_POSITION;
			}		
		}
		
        lRet = EXIT_SUCCESS;

    }while(0);

    return lRet;
}


/**********************************************************************
* 函数名称：read_motor_hit_state_from_fpga
* 功能描述：从FPGA读电机撞针信息
* 输入参数：
*         ：lMotorType：电机类型	*ucHitSensor：撞针状态
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其他说明：无
**********************************************************************/
int32_t read_motor_hit_state_from_fpga( int32_t lMotorType, uint8_t *ucHitSensor )
{
    int32_t lRet     = EXIT_FAILURE;
    uint8_t ucSensor = 0;
	
	do
    {
        if ( INVALID_POINTER(ucHitSensor) )
		{
			break;
		}
		
		/*! 从FPGA中读电机传感器信号 */
		if ( EXIT_FAILURE == bsp_fpga_read_single_data((uint8_t)FPGA_MOTOR_SENSER_BASE_ADDER, (uint8_t)(lMotorType + SAMPLE_OFFSET_INDEX), &ucSensor) )
		{
			break;		
		}
		
		/*! 从FPGA中读电机传感器信号,判断是否撞针 */
		if((ucSensor & BIT2) == 0)
		{
			*ucHitSensor = 1;
		}
		else
		{
			*ucHitSensor = 0;
		}
		
	}while(0);

    return lRet;
}




/*******************************************************************************
* 名称: check_motor_hit_state
* 功能:  是否进行撞针检测
			 MtxId   电机编号
			 ucDir 	 电机方向
             还回值：  0电机执行正确， 非0就是电机错误代码
* 说明: 无
********************************************************************************/
int32_t check_motor_hit_state( int32_t lMotorType, uint8_t ucDir)
{
    int32_t lRet = EXIT_SUCCESS;

	/*! 垂直电机 */
	if(lMotorType == SAMPLE_Y_MOTOR ||lMotorType == REAGENT_Y_MOTOR_L)
	{
		/*! 方向向下 */
		if(ucDir == 0)
		{
			read_motor_hit_state_from_fpga(lMotorType, &s_stMotorCheckLiquid[lMotorType].ucMotorCheckHitSignal);
			
			/*! 撞针触发 */
			if(s_stMotorCheckLiquid[lMotorType].ucMotorCheckHitSignal == 1)
			{
				/*! 停止电机 */
				Mtx_Stop(lMotorType);
			}
		}
	}

	return lRet;
}



int32_t is_motor_check_liquid( int32_t lMotorType, uint8_t eDir)
{
    int32_t lRet = 0;
	
	/*! 只有开启液面检测， 垂直电机， 向下动作才执行液面检测*/
	if(lMotorType == SAMPLE_Y_MOTOR ||lMotorType == REAGENT_Y_MOTOR_L )
	{
		/*! 走步数模式而且是向下模式(向前模式) */
		if(s_astSampleMotor[lMotorType].ucState == MOTOR_STATE_ACT_START && eDir == 0)
		{
			/*! 而且电机水平位置在指定的位置 */
			if(lMotorType == SAMPLE_Y_MOTOR )
			{
				/*! 样本针在急诊位，样本位，试剂位 */
				if(s_ucMotorType[SAMPLE_X_MOTOR].ucMotorLastRealPos == REAL_POS_SAMPRAIL_SAMPROBE_X_MOTOR 
					|| s_ucMotorType[SAMPLE_X_MOTOR].ucMotorLastRealPos == REAL_POS_EMERGENCY_SAMPROBE_X_MOTOR
					|| s_ucMotorType[SAMPLE_X_MOTOR].ucMotorLastRealPos == REAL_POS_R3_SAMPROBE_X_MOTOR )
				{
					lRet = 1;
				}
			}
			else if(lMotorType == REAGENT_Y_MOTOR_L )
			{
				/*! 样本针在急诊位，样本位，试剂位 */
				if(s_ucMotorType[REAGENT_X_MOTOR].ucMotorLastRealPos == REAL_POS_L_REAG_R_WASH_REAGPROBE_X_MOTOR)
				{
					lRet = 1;
				}
			
			}
		}
	}
	
	return lRet;

}


/*******************************************************************************
* 名称: is_motor_check_hit
* 功能:  是否进行撞针检测
			 MtxId   电机编号
			 timeov 	 电机运动超时时间
             还回值：  0电机执行正确， 非0就是电机错误代码
* 说明: 无
********************************************************************************/
int32_t is_motor_check_hit( int32_t lMotorType, uint8_t eDir)
{
    int32_t lRet = 0;
	
		/*! 只有开启液面检测， 垂直电机， 向下动作才执行液面检测*/
	if(lMotorType == SAMPLE_Y_MOTOR ||lMotorType == REAGENT_Y_MOTOR_L )
	{
		/*! 向下模式(向前模式) */
		if(eDir == 0)
		{
			lRet = 1;
		}
	}
	
	return lRet;
}


/*******************************************************************************
* 名称: is_x_motor_check_crash
* 功能:  x电机运动时碰撞检测
			 MtxId   电机编号
             还回值：  0电机执行正确， 非0就是电机错误代码
* 说明: 无
********************************************************************************/
int32_t is_x_motor_check_crash( int32_t lMotorType)
{
    int32_t lRet = 0;
	
	/*! 只有水平电机需要检测*/
	if(lMotorType == SAMPLE_X_MOTOR ||lMotorType == REAGENT_X_MOTOR )
	{
		if(lMotorType == SAMPLE_X_MOTOR)
		{
			read_sample_motor_sensor(SAMPLE_Y_MOTOR, RESET_SENSER);
			if( ( SAMPLE_MOTOR_IN_POSITION != s_astSampleMotor[SAMPLE_Y_MOTOR].ucCurrentSensor ) )
			{
				lRet =1;
			}
		}
		else if(lMotorType == REAGENT_X_MOTOR )
		{
			read_sample_motor_sensor(REAGENT_Y_MOTOR_L, RESET_SENSER);
			if( ( SAMPLE_MOTOR_IN_POSITION != s_astSampleMotor[REAGENT_Y_MOTOR_L].ucCurrentSensor ) )
			{
				lRet =1;
			}
		}
	}
	
	return lRet;
}



/*******************************************************************************
* 名称: Mtx_Waiting
* 功能: 等待电机运动停止， 信号触发就还回，并判断电机是否执行完成
			 MtxId   电机编号
			 timeov 	 电机运动超时时间
             还回值：  0电机执行正确， 非0就是电机错误代码
* 说明: 无
********************************************************************************/
uint8_t Mtx_Waiting(uint8_t MtxId, uint32_t timeov)
{
	OS_ERR    err;
    
    while(MOTOR_STATE_IDLE != s_astSampleMotor[MtxId].ucState)
    {
        OSTimeDlyHMSM ( 0, 0, 0, 1, OS_OPT_TIME_DLY,  &err );
    } 
    return s_ucMotorType[MtxId].ucErrCode;	
}

/*******************************************************************************
* 名称: Mtx_Stop
* 功能: 停止电机
			 MtxId   电机编号
             还回值：  0电机执行正确， 非0就是电机错误代码
* 说明: 无
********************************************************************************/
uint8_t Mtx_Stop(uint8_t MtxId)
{
    int32_t  lRet = EXIT_FAILURE;
	fpga_data_t astFpgaData[2];
	uint8_t ucIntex = 0;
	
    do
    {
//        s_astSampleMotor[MtxId].ucMotorSpeedLevel = 0;
//		s_astSampleMotor[MtxId].unMoveStep = 0;
//		
//        if ( EXIT_FAILURE == write_motor_param_to_fpga(MtxId, &(s_astSampleMotor[MtxId]) ) )
//        {
//			break;
//		}
		
		/*! 电机运动步数低8位 */
		astFpgaData[ucIntex].ucBaseAddr = FPGA_MOTOR_STEPL_BASE_ADDER;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)MtxId;
		astFpgaData[ucIntex].ucData     = 0;

		/*! 电机运动步数高8位 */
		ucIntex++;
		astFpgaData[ucIntex].ucBaseAddr = FPGA_MOTOR_STEPRH_BASE_ADDER;
		astFpgaData[ucIntex].ucOffsAddr = (uint8_t)MtxId;
		astFpgaData[ucIntex].ucData     = 0;
		
		if ( EXIT_FAILURE == bsp_fpga_write(astFpgaData, ucIntex + 1 ) )
		{
			break;
		}
		
        lRet = EXIT_SUCCESS;
	}while(0);
	
    return lRet;
}


/*******************************************************************************
* 名称: Mtx_Move
* 功能: 电机向指定方向运动指定步数
* 形参: 
			 MtxId   电机编号
			 nSteps  电机运动步数
			 eDir	 电机运动方向
			timeov 	 电机运动超时时间
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t Mtx_Move(uint8_t MtxId,  uint32_t nSteps, uint8_t eDir, uint8_t mode, uint32_t timeov,uint8_t RunSpeed)
{
    int32_t  lRet = EXIT_FAILURE;

    do
    {
		s_ucMotorType[MtxId].ucErrCode = 0;

		/*! 电机不在空闲状态，报错 */
        if( MOTOR_STATE_IDLE != s_astSampleMotor[MtxId].ucState )
        {
			lRet = ERR_MOTOR_ACT_EXE;
            return lRet;
        }

        s_astSampleMotor[MtxId].ucMotorSpeedLevel = RunSpeed;
        s_astSampleMotor[MtxId].unMotorExecTime = timeov;
        s_astSampleMotor[MtxId].unMotorMovedTime = 0;
		s_astSampleMotor[MtxId].unActionDelay = 0;		
		s_astSampleMotor[MtxId].ucRunMode = STEP_MODE_MOTOR;
		s_astSampleMotor[MtxId].unMoveStep = nSteps;
        s_astSampleMotor[MtxId].ucMoveDirection = eDir;
        s_astSampleMotor[MtxId].ucState = MOTOR_STATE_ACT_START;
        s_astSampleMotor[MtxId].ucMotorOffsetStep = 0;
		
		/*! 是否是液面检测模式 */
		if(is_motor_check_liquid(MtxId, eDir) && (s_ucIsCheckLiguidFlag == 1))
		{
			s_astSampleMotor[MtxId].ucRunMode = LIQUID_MODE_MOTOR;
			if(MtxId == REAGENT_Y_MOTOR_L)
			{
				s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucReagLiguiDepth;
			}
			else
			{
				s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucaSampLiguiDepthType[s_ucSampType];
			}
		}
		
		/*! 清除撞针信息 */
		clear_liquid_hit_state(MtxId);
		
        if ( EXIT_FAILURE == write_motor_param_to_fpga(MtxId, &(s_astSampleMotor[MtxId]) ) )
        {
			break;
		}

		/*! 等待电机运行停止, mode = 0 就是同步模式，要等待电机执行完成才还回 */
		if(mode == 0)
		{
			return Mtx_Waiting(MtxId,0);		
		}
		
        lRet = EXIT_SUCCESS;

    }while(0);
    
    return lRet;
	
}


/*******************************************************************************
* 名称: Mtx_Move_To
* 功能: 电机运动到指定位置
* 形参: 
			 MtxId       电机编号
			 nSteps      电机运动到指定的位置
			 timeov 	 电机运动超时时间
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t Mtx_Move_To(uint8_t MtxId,  uint32_t nSteps, uint8_t mode, uint32_t timeov,uint8_t RunSpeed)
{
    int32_t  lRet = EXIT_FAILURE;
	uint8_t ucResult = 0;
    uint16_t adSteps;
    uint8_t eDir;
    
    if (s_astSampleMotor[MtxId].unPassedStep == nSteps)
	{
		lRet = EXIT_SUCCESS;
        return lRet;
	}
	
	/*! 根据已近走的步数，需要到位置，来确定运行方向和步数 */
    if (s_astSampleMotor[MtxId].unPassedStep > nSteps)
    {
        adSteps = s_astSampleMotor[MtxId].unPassedStep - nSteps;
        eDir = MOTOR_DIR_BACKWARD;
    }
    else
    {
        adSteps =  nSteps - s_astSampleMotor[MtxId].unPassedStep;
        eDir = MOTOR_DIR_FORWARD;
    }
	
	/*! 清除电机对应的液面和撞击信息 */
	clear_liquid_hit_state(MtxId);
	
	ucResult = Mtx_Move(MtxId ,adSteps ,eDir ,mode ,timeov,RunSpeed);
	
    return ucResult;
}


/*******************************************************************************
* 名称: set_motor_offset_step
* 功能: 电机运动到指定位置, 
* 形参: 
			 MtxId   电机编号
			 nPos    电机要运动到的位置
			timeov 	 电机运动超时时间
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t set_motor_offset_step(uint8_t MtxId,  uint32_t ulPos)
{
    int32_t  lRet = EXIT_FAILURE;
		
	if(MtxId == SAMPLE_X_MOTOR)
	{
		/*! 码盘电机是分方向补偿 */
		if (s_ucMotorType[MtxId].ucMotorLastRealPos > ulPos)
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unSampleXMotorBackOffsetStep[ulPos];
		}
		else
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unSampleXMotorForwardOffsetStep[ulPos];
		}
	}
	else if(MtxId == SAMPLE_Y_MOTOR)
	{
		//垂直电机，走顶长的电机，直接设置走液面补偿
		s_astSampleMotor[MtxId].ucMotorOffsetStep = 200;
	}
	else if(MtxId == REAGENT_X_MOTOR)// 试剂针水平电机
	{
		if (s_ucMotorType[MtxId].ucMotorLastRealPos  > ulPos)
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeXMotorBackOffsetStep[ulPos];
		}
		else
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeXMotorForwardOffsetStep[ulPos];
		}	
	}
	else if(MtxId == REAGENT_Y_MOTOR_L)// 试剂针垂直左电机
	{
		//垂直电机，走顶长的电机，直接设置走液面补偿
		s_astSampleMotor[MtxId].ucMotorOffsetStep = 200;
	}
	else if(MtxId == REAGENT_Y_MOTOR_R) // 试剂针垂直右电机
	{
		//垂直电机，走顶长的电机，直接设置走液面补偿
		s_astSampleMotor[MtxId].ucMotorOffsetStep = 200;
	}
	else if(MtxId == WASH_Y_MOTOR) // 清洗针垂直电机
	{
		//垂直电机，
		s_astSampleMotor[MtxId].ucMotorOffsetStep = 200;
	}	
	else if(MtxId == SCAN_SPIN_MOTOR)// 扫码旋转电机
	{
		//旋转电机直接走固定步数
		s_astSampleMotor[MtxId].ucMotorOffsetStep = 0;
	}	
	else if(MtxId == REACTION_SPIN_MOTOR) // 反应盘电机
	{
		if (s_ucMotorType[MtxId].ucMotorLastRealPos  > ulPos)
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReacTrayYMotorPosStepTable[ulPos];
		}
		else
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeXMotorForwardOffsetStep[ulPos];
		}	
	}	
	else if(MtxId == REAGENT_SPIN_MOTOR)// 试剂盘电机
	{
		if (s_ucMotorType[MtxId].ucMotorLastRealPos  > ulPos)
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagTrayMotorPosStepTable[ulPos];
		}
		else
		{
			s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagTrayMotorPosStepTable[ulPos];
		}	
	}	
	else//其他的默认补偿, 注射器电机是不要补偿的的
	{
		// 样本注射器电机
		// 试剂注射器电机1
		// 试剂注射器电机2
		// 清水注射器电机1
		// 清水注射器电机2
		// 稀释液注射器电机
		s_astSampleMotor[MtxId].ucMotorOffsetStep = 0;
	}

	return lRet;
}

/**********************************************************************
* 函数名称：set_sample_motor_position
* 功能描述：设置采样针电机位置(包括时序位置和实际的物理位置)
* 输入参数：
*         ：lMotorType：电机类型	ucMotorPosition：电机位置 	
* 输出参数：无
* 返 回 值：无
* 其它说明：无
**********************************************************************/
void set_sample_motor_position(uint32_t lMotorType, uint8_t ucMotorPosition)
{
	
	/*! 电机的位置 */
	if(ucMotorPosition > s_ucaMotorNoneRealPos[lMotorType])
	{
		return;		
	}
	
	s_ucMotorType[lMotorType].ucMotorLastRealPos = ucMotorPosition;
}



/**********************************************************************
* 函数名称：check_sample_motor_direction
* 功能描述：获取样本针电机方向
* 输入参数：
*         ：lMotorType：电机类型	ucDesPosition：目标位置  
* 输出参数：无
* 返 回 值：
*         ：电机方向
* 其它说明：无
**********************************************************************/
static uint8_t check_sample_motor_direction(uint32_t lMotorType,uint8_t ucDesPosition)
{
	uint8_t ucMotorDir = MOTOR_DIR_BACKWARD;
	
	do
	{
		{
			/*! 电机要走的位置超出了最大位置 */
			if ( ucDesPosition >= s_ucaMotorMaxRealPos[lMotorType] )
			{
				break;		
			}	
			
			/*! 上一次电机位置大于目标电机位置,则方向向后 */
			if ( s_ucMotorType[lMotorType].ucMotorLastRealPos > ucDesPosition )
			{
				ucMotorDir = MOTOR_DIR_BACKWARD;
			}
			/*! 上一次电机位置小于目标电机位置,则方向向前 */
			else if ( s_ucMotorType[lMotorType].ucMotorLastRealPos <= ucDesPosition )
			{
				ucMotorDir = MOTOR_DIR_FORWARD;
			}		
		}
		
		/*! 更新上一次电机位置 */
		set_sample_motor_position(lMotorType,ucDesPosition);
		
	}while(0);
	
	return (ucMotorDir);
}




/*******************************************************************************
* 名称: Mtx_Move_To_Pos
* 功能: 电机运动到指定位置, 
* 形参: 
			 MtxId   电机编号
			 nPos    电机要运动到的位置
			timeov 	 电机运动超时时间
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t Mtx_Move_To_Pos(uint8_t MtxId,  uint32_t ucMotorPosition, uint8_t mode , uint32_t timeov,uint8_t RunSpeed)
{
	uint16_t unMoveStep = 0;
    uint8_t  lRet = EXIT_FAILURE;
    
    do
    {
		s_ucMotorType[MtxId].ucErrCode = 0;
						
		/*! 电机不在空闲状态，报错 */
        if ( MOTOR_STATE_IDLE != s_astSampleMotor[MtxId].ucState )
        {		
			lRet = ERR_MOTOR_ACT_EXE;
            return lRet;
        }   

		/*! 水平电机运动时，垂直电机位置判断 */
		if(is_x_motor_check_crash(MtxId) == 1)
		{
			lRet =  ERR_MOTOR_X_RUN_MOTOR_Y_NO_RESET_POS;	
            return lRet;
		}
		
		/*! 电机在不可知位置，报错 *////////////////错误更改/////////////////////
        if ( s_ucMotorType[MtxId].ucMotorLastRealPos == s_ucaMotorNoneRealPos[MtxId] )//每个电机的位置都不一样
		{
			lRet = ERR_MOTOR_AT_INTANGIBLY_POS;	
            return lRet;
		}
		
		/*! 电机要走的位置超出了最大位置，报错 */
		if ( ucMotorPosition > s_ucaMotorNoneRealPos[MtxId] )
		{
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
			lRet = ERR_MOTOR_NOTHIS_POS;
            return lRet;
		}		

		/*! ***********************设置电机补偿*********************** */
		set_motor_offset_step(MtxId, ucMotorPosition);
		/*! *********************设置电机补偿 END ********************* */
		
		/*! 当电机在起始位置时，需要检测起始位置传感器 */
		if ( s_ucaMotorInitRealPos[MtxId] == s_ucMotorType[MtxId].ucMotorLastRealPos )
		{
			/*! 检测电机是否在起始位置 */
			if ( EXIT_FAILURE == read_sample_motor_sensor(MtxId, RESET_SENSER) )
			{
				break;
			}
				
			/*! 电机在起始位置，去取光耦状态，光耦状态不正确就报错 */
			if( ( SAMPLE_MOTOR_IN_POSITION != s_astSampleMotor[MtxId].ucCurrentSensor ) )
			{
				/*! 电机不在起始位置,报错 */
				s_astSampleMotor[MtxId].unPassedStep = 0;
				s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;	
				lRet = ERR_MOTOR_RESETSTART_STEPOUT;
				
				/*! 修改无Break的BUG 20190118 (该故障和其他BUG一起造成故障码为00) */
				break;
			}
			else
			{		
				/*! 电机在起始位置 */	
				s_astSampleMotor[MtxId].ucRunMode = POSITION_MODE_MOTOR;		
				s_astSampleMotor[MtxId].ucMotorSpeedLevel = RunSpeed;
				s_astSampleMotor[MtxId].ucState = MOTOR_STATE_POS_START; 	
				s_astSampleMotor[MtxId].unMotorExecTime = timeov;	
				s_astSampleMotor[MtxId].unMotorMovedTime = 0;	
				s_astSampleMotor[MtxId].unActionDelay = 0;
				
				/*! 将运动位置转换成运动步数格式 */
				unMoveStep = position_change_to_motor_step(MtxId,get_sample_motor_real_position(MtxId),ucMotorPosition);
				s_astSampleMotor[MtxId].unMoveStep = unMoveStep;			   
				
				/*! 计算电机运动方向 */
				s_astSampleMotor[MtxId].ucMoveDirection = check_sample_motor_direction(MtxId, ucMotorPosition);
				if ( EXIT_FAILURE == write_motor_param_to_fpga( MtxId,  &(s_astSampleMotor[MtxId]) ) )
				{
					break;
				}	
			}
		}
		else 
		{						                
            /*! 电机走到指定的位置 */
            s_astSampleMotor[MtxId].ucRunMode = POSITION_MODE_MOTOR;
            s_astSampleMotor[MtxId].ucState = MOTOR_STATE_POS_START; 
            s_astSampleMotor[MtxId].ucMotorSpeedLevel = RunSpeed;
            s_astSampleMotor[MtxId].unMotorExecTime = timeov;
            s_astSampleMotor[MtxId].unMotorMovedTime = 0;
            s_astSampleMotor[MtxId].unActionDelay = 0;		
            
            /*! 将运动位置转换成运动步数格式 */
            unMoveStep = position_change_to_motor_step(MtxId,get_sample_motor_real_position(MtxId),ucMotorPosition);
            s_astSampleMotor[MtxId].unMoveStep = unMoveStep;
            
            /*! 计算电机运动方向 */
            s_astSampleMotor[MtxId].ucMoveDirection = check_sample_motor_direction(MtxId,ucMotorPosition);
            
            if ( EXIT_FAILURE == write_motor_param_to_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
            {
                break;
            }				
		}
		
		/*! 等待电机运行停止, mode = 0 就是同步模式，要等待电机执行完成才还回 */
		if(mode == 0)
		{
			return Mtx_Waiting(MtxId,0);		
		}
		
		lRet = EXIT_SUCCESS;
    }while(0);

    return lRet;
}

/*******************************************************************************
* 名称: Mtx_Fast_Rst
* 功能: 电机快速复位
			 MtxId   电机编号
			 timeov 	 电机运动超时时间
* 返回:  0 正常， 非0表示错误代码
* 说明:  电机在复位位置就不动作，电机不在复位位置就回到复位位置
********************************************************************************/
uint8_t Mtx_Fast_Rst(uint8_t MtxId, uint8_t mode,uint32_t timeov, uint8_t RunSpeed)
{
	uint8_t ret = 0;

    do
    {
		s_ucMotorType[MtxId].ucErrCode = 0;
		
		/*! 电机不在空闲状态，报错 */
        if( MOTOR_STATE_IDLE != s_astSampleMotor[MtxId].ucState )
        {
			ret =  ERR_MOTOR_ACT_EXE;
            return ret;
        }  
		
       	/*! 读取光耦当前状态 */
		if ( EXIT_FAILURE == read_sample_motor_sensor(MtxId,RESET_SENSER) )
		{	
			ret =  ERR_FPGA_EXE;	
            return ret;
		}

        switch(MtxId)
        {
            case SAMPLE_X_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unSampleXMotorBackOffsetStep[0];
                break;
            
            case SAMPLE_Y_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unSampleYMotorPosStepTable[0];
                break;
            
            case REAGENT_X_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeXMotorForwardOffsetStep[0];
                break;
            
            case REAGENT_Y_MOTOR_L:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_L[0];
                break;
            
            case REAGENT_Y_MOTOR_R:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_R[0];
                break;
            
            case WASH_Y_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unWashMotorPosStepTable[0];
                break;
           
            case SCAN_SPIN_MOTOR:
//                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unScanMotorPosStepTable[0];
                s_astSampleMotor[MtxId].ucMotorOffsetStep = 100;
                break;
            
            case REACTION_SPIN_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReacTrayYMotorPosStepTable[0];
                break;
            
            case REAGENT_SPIN_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagTrayMotorPosStepTable[0];
                break;
            
            case SAMPLE_SYRINGE_MOTOR:			// 样本注射器电机
            case REAGENT_SYRINGE_MOTOR_1:		// 试剂注射器电机1 RS1
            case REAGENT_SYRINGE_MOTOR_2:		// 试剂注射器电机2 RS2    
            case WATER_SYRINGE_MOTOR_1:			// 清水注射器电机1 PS1
            case WATER_SYRINGE_MOTOR_2:			// 清水注射器电机2 PS2
            case DILUENT_SYRINGE_MOTOR:	        // 稀释液注射器电机
                s_astSampleMotor[MtxId].ucMotorOffsetStep = 180;
                break;
            
            default:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = 0;
                break;
        }        

	    if(SAMPLE_MOTOR_IN_POSITION == s_astSampleMotor[MtxId].ucCurrentSensor && (REAGENT_SPIN_MOTOR!= MtxId) && (REACTION_SPIN_MOTOR!= MtxId))
        {
			/*! 在复位位置不处理 */
		}
		else    
		{		
			/*! 不在起始位置,直接回起始位置 */
			PRO_DEBUG(MOTOR_DEBUG,("sample x motor out sensor \r\n"));
			s_astSampleMotor[MtxId].ucMotorSpeedLevel = RunSpeed;
			s_astSampleMotor[MtxId].unMotorExecTime = timeov;  
			s_astSampleMotor[MtxId].unMotorMovedTime = 0;	
			s_astSampleMotor[MtxId].unActionDelay = 0;
			s_astSampleMotor[MtxId].ucRunMode = RESET_MODE_MOTOR;
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_RESET_START;   
			s_astSampleMotor[MtxId].unMoveStep = s_aunSampleMaxStepTable[MtxId];// 步数为最大执行步数			
			s_astSampleMotor[MtxId].ucMoveDirection = MOTOR_DIR_BACKWARD;	    // 方向为逆时针	
			
			if ( EXIT_FAILURE == write_motor_param_to_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
			{	
				ret =  ERR_FPGA_EXE;	
				return ret;
			}
		}
    }while(0);
	
	return ret;

}

/*******************************************************************************
* 名称: Mtx_Rst
* 功能: 重队列中取出数据，分发到各个模块
			 MtxId   电机编号
			 timeov 	 电机运动超时时间
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t Mtx_Rst(uint8_t MtxId, uint8_t mode, uint32_t timeov, uint8_t RunSpeed)
{
	uint8_t ret = 0;

    do
    {
		s_ucMotorType[MtxId].ucErrCode = 0;
		
		/*! 电机不在空闲状态，报错 */
        if( MOTOR_STATE_IDLE != s_astSampleMotor[MtxId].ucState )
        {
			ret =  ERR_MOTOR_ACT_EXE;
            return ret;
        }  
		
		/*! 水平电机运动时，垂直电机位置判断 */
		if(is_x_motor_check_crash(MtxId) == 1)
		{
			ret =  ERR_MOTOR_X_RUN_MOTOR_Y_NO_RESET_POS;	
            return ret;
		}
		
       	/*! 读取光耦当前状态 */
		if ( EXIT_FAILURE == read_sample_motor_sensor(MtxId,RESET_SENSER) )
		{	
			ret =  ERR_FPGA_EXE;	
            return ret;
		}

        switch(MtxId)
        {
            case SAMPLE_X_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unSampleXMotorBackOffsetStep[0];
                break;
            
            case SAMPLE_Y_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unSampleYMotorPosStepTable[0];
                break;
            
            case REAGENT_X_MOTOR:
//                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeXMotorForwardOffsetStep[0];
				/*! 复位反弹 */
                s_astSampleMotor[MtxId].ucMotorOffsetStep = 30;
                break;
            
            case REAGENT_Y_MOTOR_L:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_L[0];
                break;
            
            case REAGENT_Y_MOTOR_R:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagProbeYMotorPosStepTable_R[0];
                break;
            
            case WASH_Y_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unWashMotorPosStepTable[0];
                break;
           
            case SCAN_SPIN_MOTOR:
//                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unScanMotorPosStepTable[0];
                s_astSampleMotor[MtxId].ucMotorOffsetStep = 10;
                break;
            
            case REACTION_SPIN_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReacTrayYMotorPosStepTable[0];
                break;
            
            case REAGENT_SPIN_MOTOR:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = s_ucMotorOffsetPara.unReagTrayMotorPosStepTable[0];
                break;
            
            case SAMPLE_SYRINGE_MOTOR:			// 样本注射器电机
            case REAGENT_SYRINGE_MOTOR_1:		// 试剂注射器电机1 RS1
            case REAGENT_SYRINGE_MOTOR_2:		// 试剂注射器电机2 RS2    
            case WATER_SYRINGE_MOTOR_1:			// 清水注射器电机1 PS1
            case WATER_SYRINGE_MOTOR_2:			// 清水注射器电机2 PS2
            case DILUENT_SYRINGE_MOTOR:	        // 稀释液注射器电机
                s_astSampleMotor[MtxId].ucMotorOffsetStep = 180;
                break;
            
            default:
                s_astSampleMotor[MtxId].ucMotorOffsetStep = 0;
                break;
        } 

       	/*! 电机的复位速度设置，复位速度过快容易撞击限位 */
		switch(MtxId)
		{
			case SCAN_SPIN_MOTOR:       // 扫码电机
				s_astSampleMotor[MtxId].ucMotorSpeedLevel = 1;
				break;
			case SAMPLE_X_MOTOR:       // 采样针水平X轴电机 
			case REAGENT_X_MOTOR:      // 采样针水平X轴电机 
				s_astSampleMotor[MtxId].ucMotorSpeedLevel = 5;
				break;

            case SAMPLE_SYRINGE_MOTOR:			// 样本注射器电机
            case REAGENT_SYRINGE_MOTOR_1:		// 试剂注射器电机1 RS1
            case REAGENT_SYRINGE_MOTOR_2:		// 试剂注射器电机2 RS2    
            case WATER_SYRINGE_MOTOR_1:			// 清水注射器电机1 PS1
            case WATER_SYRINGE_MOTOR_2:			// 清水注射器电机2 PS2
            case DILUENT_SYRINGE_MOTOR:	        // 稀释液注射器电机
				s_astSampleMotor[MtxId].ucMotorSpeedLevel = 12;
                break;
			default:
				s_astSampleMotor[MtxId].ucMotorSpeedLevel = 7;
				break;
        }

	    if(SAMPLE_MOTOR_IN_POSITION == s_astSampleMotor[MtxId].ucCurrentSensor && (REAGENT_SPIN_MOTOR!= MtxId) && (REACTION_SPIN_MOTOR!= MtxId))
        {
			PRO_DEBUG(MOTOR_DEBUG,("sample x motor in sensor \r\n"));
			
			/*! 设置电机位置,电机在初始化位置 */
			set_sample_motor_position(MtxId, s_ucaMotorInitRealPos[MtxId]);
		
			/*! 在起始位置,先离开起始位置MOTOR_STEP_LEAVE_SENSOR步数,再回到起始位置 */
			s_astSampleMotor[MtxId].unMotorExecTime   = timeov;
			s_astSampleMotor[MtxId].unMotorMovedTime = 0;
			s_astSampleMotor[MtxId].unActionDelay = 0; 
			s_astSampleMotor[MtxId].ucRunMode = STEP_MODE_MOTOR;
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_RESET_STEP1;
            
            switch(MtxId)
            {
                case SCAN_SPIN_MOTOR:       // 扫码电机
                    s_astSampleMotor[MtxId].unMoveStep = CODE_SCAN_MOTOR_STEP_LEAVE_SENSOR;
                    break;
                
                case REAGENT_Y_MOTOR_L:		// 试剂针垂直左电机
                case REAGENT_Y_MOTOR_R:		// 试剂针垂直右电机
                    s_astSampleMotor[MtxId].unMoveStep = CODE_REAG_PROBR_MOTOR_STEP_LEAVE_SENSOR;
                    break;
                
                case SAMPLE_Y_MOTOR:        //样本针垂直单元
                    s_astSampleMotor[MtxId].unMoveStep = CODE_SAMP_PROBR_MOTOR_STEP_LEAVE_SENSOR;
                    break;
                
                case REAGENT_X_MOTOR:       //试剂针水平
                    s_astSampleMotor[MtxId].unMoveStep = CODE_REAGENT_X__MOTOR_STEP_LEAVE_SENSOR;
                    break;
				
                case SAMPLE_X_MOTOR:        //样本针水平
                    s_astSampleMotor[MtxId].unMoveStep = CODE_SAMP_X_MOTOR_STEP_LEAVE_SENSOR;
                    break;
                    
                default:
                    s_astSampleMotor[MtxId].unMoveStep = MOTOR_STEP_LEAVE_SENSOR;
                    break;
            }
                       
			s_astSampleMotor[MtxId].ucMoveDirection = MOTOR_DIR_FORWARD;
			
			if ( EXIT_FAILURE == write_motor_param_to_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
			{	
				ret =  ERR_FPGA_EXE;	
				return ret;
			}
		}
		else    
		{		
			/*! 不在起始位置,直接回起始位置 */
			PRO_DEBUG(MOTOR_DEBUG,("sample x motor out sensor \r\n"));
			s_astSampleMotor[MtxId].ucMotorSpeedLevel = RunSpeed;
			s_astSampleMotor[MtxId].unMotorExecTime = timeov;  
			s_astSampleMotor[MtxId].unMotorMovedTime = 0;	
			s_astSampleMotor[MtxId].unActionDelay = 0;
			s_astSampleMotor[MtxId].ucRunMode = RESET_MODE_MOTOR;
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_RESET_START;   
			s_astSampleMotor[MtxId].unMoveStep = s_aunSampleMaxStepTable[MtxId];// 步数为最大执行步数			
			s_astSampleMotor[MtxId].ucMoveDirection = MOTOR_DIR_BACKWARD;	    // 方向为逆时针	
			
			if ( EXIT_FAILURE == write_motor_param_to_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
			{	
				ret =  ERR_FPGA_EXE;	
				return ret;
			}
		}
    }while(0);
	return ret;
}

/*******************************************************************************
* 名称: MT_Cup_Move
* 功能: 重队列中取出数据，分发到各个模块
			 MtxId      电机编号
			 eDir        反应杯和试剂杯是只会向一个方向运动
			 offset_pos  转动多少个杯
		 	 mode		 0 同步模式， 1 异步模式
			 timeov 	 电机运动超时时间
* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t MT_Cup_Move(uint8_t MtxId, uint8_t eDir, uint16_t offset_pos, uint8_t mode, uint16_t timeov,uint8_t ucOffsetStep,uint8_t RunSpeed)
{
    int32_t  lRet = EXIT_FAILURE;

    do
    {		
		s_ucMotorType[MtxId].ucErrCode = 0;
				
		/*! 电机不在空闲状态，报错 */
        if( MOTOR_STATE_IDLE != s_astSampleMotor[MtxId].ucState )
        {
			lRet = ERR_MOTOR_ACT_EXE;
            return lRet;
        }
		
		/*! 盘电机转动，判断试剂针电机和样本样电机是否在复位位置 */
        s_astSampleMotor[MtxId].ucMotorSpeedLevel = RunSpeed;
        s_astSampleMotor[MtxId].unMotorExecTime = timeov;
        s_astSampleMotor[MtxId].unMotorMovedTime = 0;
		s_astSampleMotor[MtxId].unActionDelay = 0;		
		s_astSampleMotor[MtxId].ucRunMode       = POSITION_MODE_MOTOR;
		s_astSampleMotor[MtxId].unMoveStep 		= offset_pos;
        s_astSampleMotor[MtxId].ucMoveDirection = eDir;
        s_astSampleMotor[MtxId].ucState = MOTOR_STATE_ACT_START;
        s_astSampleMotor[MtxId].ucMotorOffsetStep = ucOffsetStep;
		
        if ( EXIT_FAILURE == write_motor_param_to_fpga(MtxId, &(s_astSampleMotor[MtxId]) ) )
        {
			break;
		}

		/*! 等待电机运行停止, mode = 0 就是同步模式，要等待电机执行完成才还回 */
		if(mode == 0)
		{
			return Mtx_Waiting(MtxId,0);		
		}
		
        lRet = EXIT_SUCCESS;

    }while(0);

    return lRet;

}

/**********************************************************************
* 函数名称：motor_state_reset_started
* 功能描述： 电机复位走完设置最大步数
* 输入参数：无	 
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其它说明：无
**********************************************************************/
static int32_t motor_state_reset_started(uint8_t MtxId )
{
	int32_t lRet = EXIT_FAILURE;

	do
	{
		if ( MOTOR_STATE_RESET_START != s_astSampleMotor[MtxId].ucState )
		{
			break;
		}	
		
        /*! 读电机状态信息 */
        if ( EXIT_FAILURE == read_motor_param_from_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
        {
            break;
        }	
        
        if ( MOTOR_RESULT_OK != s_astSampleMotor[MtxId].ucRunResult )
        {
            /*! 电机停止失败 */
            s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE; 
            lRet =  ERR_MOTOR_ACT;			
            return lRet;
        }
        
        if((REAGENT_SPIN_MOTOR == MtxId) || (REACTION_SPIN_MOTOR == MtxId))
        {
            //反应盘、试剂盘
            /*! 设置电机位置,电机在初始化位置 */
            set_sample_motor_position(MtxId, s_ucaMotorInitRealPos[MtxId]);
                        
            /*! 清除电机状态 */
            s_astSampleMotor[MtxId].ucRunMode = STEP_MODE_MOTOR;
            s_astSampleMotor[MtxId].unPassedStep = 0;
            s_astSampleMotor[MtxId].ucMotorSpeedLevel = 0;
            s_astSampleMotor[MtxId].unMoveStep = 0;
            s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
            
            lRet = EXIT_SUCCESS;
        }
        else
        {
            if ( EXIT_FAILURE == read_sample_motor_sensor(MtxId,RESET_SENSER) )
            {
                break;
            }
            
            if ( SAMPLE_MOTOR_IN_POSITION == s_astSampleMotor[MtxId].ucCurrentSensor )
            {
                /*! 设置电机位置,电机在初始化位置 */
                set_sample_motor_position(MtxId, s_ucaMotorInitRealPos[MtxId]);
                            
                /*! 清除电机状态 */
                s_astSampleMotor[MtxId].ucRunMode = STEP_MODE_MOTOR;
                s_astSampleMotor[MtxId].unPassedStep = 0;
                s_astSampleMotor[MtxId].ucMotorSpeedLevel = 0;
                s_astSampleMotor[MtxId].unMoveStep = 0;
                s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
                
                lRet = EXIT_SUCCESS;
            }
            else
            {
                /*! 电机不在起始位 */
                PRO_DEBUG(MOTOR_DEBUG,("motor out sensor [ERR] \r\n"));
                
                /*! 清除电机状态 */
                s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
                
                /*! 报警 */
                lRet =  ERR_MOTOR_RESET_STEPOUT;

                /*! 设置电机位置,电机不知道在什么位置 */
                set_sample_motor_position(MtxId, s_ucaMotorNoneRealPos[MtxId]);
                
                break;
            }    
        }
	}while (0);

	return lRet;
}


/**********************************************************************
* 函数名称：motor_state_reset_step1ed
* 功能描述： 电机复位离开起始位走完设置步数
* 输入参数：无	 
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其它说明：无
**********************************************************************/
static int32_t motor_state_reset_step1ed( uint8_t MtxId )
{
	int32_t  lRet = EXIT_FAILURE;
    OS_ERR   err;
    
	PRO_DEBUG(MOTOR_DEBUG,("motor_state_reset_step1ed \r\n"));
	do
	{
		/*! 电机不在复位状态1，报错 */
		if ( MOTOR_STATE_RESET_STEP1 != s_astSampleMotor[MtxId].ucState )
		{
			break;
		}	
		
		if ( EXIT_FAILURE == read_sample_motor_sensor(MtxId,RESET_SENSER) )
		{
			break;
		}
		
		/*! 检测电机是否已经离开起始位 */
		if ( SAMPLE_MOTOR_IN_POSITION != s_astSampleMotor[MtxId].ucCurrentSensor )
		{	
			/*! 增加延时,减小电机急速换方向所产生的噪声 20181212 */
			OSTimeDlyHMSM ( 0, 0, 0,200, OS_OPT_TIME_DLY, &err );  //每20ms扫描一次
//			s_astSampleMotor[MtxId].ucMotorSpeedLevel = s_ucMotorType[MtxId].ucMotorRunSpeed;
			/*! 重新设置电机向反方向运行,回复位位置 */
			s_astSampleMotor[MtxId].ucRunMode = RESET_MODE_MOTOR;
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_RESET_START;
			s_astSampleMotor[MtxId].unMoveStep = s_aunSampleMaxStepTable[MtxId];		// 步数为最大执行步数	
			s_astSampleMotor[MtxId].ucMoveDirection = MOTOR_DIR_BACKWARD;	// 方向为逆时针
			if ( EXIT_FAILURE == write_motor_param_to_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
			{
				break;
			}           
		}
		else	
		{	
			/*! 没有离开起始位 */	
			PRO_DEBUG(MOTOR_DEBUG,("sample x motor in sensor [ERR] \r\n"));
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
			lRet =  ERR_MOTOR_RESETSTEP1_STEPOUT;
			return lRet;
		}
		
		lRet = EXIT_SUCCESS;

	}while (0);

	return lRet;
}


/**********************************************************************
* 函数名称：motor_state_move_steped
* 功能描述：样本针水平电机运动完指定步数
* 输入参数：无	 
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其它说明：无
**********************************************************************/
static int32_t motor_state_move_steped( uint8_t MtxId )
{
    int32_t  lRet = EXIT_FAILURE;

	PRO_DEBUG(MOTOR_DEBUG,("motor_state_move_steped \r\n"));
    do
    {
        if (( MOTOR_STATE_ACT_START != s_astSampleMotor[MtxId].ucState ))
        {
			break;
		}

		/*! 读电机状态信息 */
		if ( EXIT_FAILURE == read_motor_param_from_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
		{
			break;
		}	
		
		/*! 电机在液面模式下，是否检测到液面 */
		if( LIQUID_MODE_MOTOR == s_astSampleMotor[MtxId].ucRunMode)
		{
			/*! 电机没有检测到液面就报警 */
			if ( NEEDLE_OUT_LIQUID == (s_astSampleMotor[MtxId].ucRunResult & BIT1 ))
			{
				s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
				lRet =  ERR_MOTOR_ABSORB_CHECK_NO_LIQUID ;
				return lRet;
			}
		}

		/*! 电机下降时，检测是否撞针 */
		if(s_stMotorCheckLiquid[MtxId].ucMotorCheckHitSignal == 1)
		{
			/*! 清零撞针 */
			s_stMotorCheckLiquid[MtxId].ucMotorCheckHitSignal = 0;
			
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
			
			/*! 撞针之后，电机复位 */
			Mtx_Fast_Rst(MtxId,0, 0, 7);

			lRet =  ERR_MOTOR_HIT;
			return lRet;
		}
		
		/*! 电机运动动作执行错误,报警 */
		if ( MOTOR_RESULT_OK != (s_astSampleMotor[MtxId].ucRunResult & BIT0 ))
		{
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
			lRet =  ERR_MOTOR_ACT;
			return lRet;
		}


		/*! 设置电机位置,电机不知道在什么位置 */
		set_sample_motor_position(MtxId, s_ucaMotorNoneRealPos[MtxId]);
		
      	s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
		
        lRet = EXIT_SUCCESS;

    }while(0);

    return lRet;
}


/**********************************************************************
* 函数名称：motor_state_move_posed
* 功能描述：检测 电机是否移动到指定的位置
* 输入参数：无
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其它说明：无
**********************************************************************/
static int32_t motor_state_move_posed( uint8_t MtxId )
{
	int32_t lRet = EXIT_FAILURE;

	PRO_DEBUG(MOTOR_DEBUG,("motor_state_move_posed \r\n"));
	do
	{ 
		/*! 电机不在按指定位置运动状态，报错 */
		if ( MOTOR_STATE_POS_START != s_astSampleMotor[MtxId].ucState )
		{
			break;
		}

		/*! 读电机状态信息 */
		if ( EXIT_FAILURE == read_motor_param_from_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
		{
			break;
		}	
		
		if ( MOTOR_RESULT_OK != s_astSampleMotor[MtxId].ucRunResult )
		{
			/*! 电机停止失败 */
			s_astSampleMotor[MtxId].unActionDelay = 0;
			s_astSampleMotor[MtxId].unPassedStep = 0x00; 
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE; 
			lRet =  ERR_MOTOR_GOOVERSEPP_NOWIN_CUPPOS;			
			return lRet;
		}
		else
		{
			
			s_astSampleMotor[MtxId].unPassedStep = 0;
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE; 
			//set_frame_comm_state(COMM_FINISH, 0);
			lRet = EXIT_SUCCESS;	 
		}
					
		lRet = EXIT_SUCCESS;
		
    }while(0);	
	
	return lRet;
}


/**********************************************************************
* 函数名称：motor_state_stoped
* 功能描述： 电机停止检测
* 输入参数：无       	 
* 输出参数：无
* 返 回 值：
*         ：EXIT_SUCCESS：成功	EXIT_FAILURE：失败
* 其它说明：无
**********************************************************************/
static int32_t motor_state_stoped( uint8_t MtxId )
{
	int32_t lRet = EXIT_FAILURE;

	PRO_DEBUG(MOTOR_DEBUG,("motor_state_stoped \r\n"));
	do
	{
		if ( MOTOR_STATE_STOP_START != s_astSampleMotor[MtxId].ucState )
		{
			break;
		}
		
		/*! 读电机状态信息 */
		if ( EXIT_FAILURE == read_motor_param_from_fpga( MtxId, &(s_astSampleMotor[MtxId]) ) )
		{
			break;
		}	
		
		/*! 电机停止失败 */
		if ( MOTOR_RESULT_OK != s_astSampleMotor[MtxId].ucRunResult )
		{
			s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
			lRet =  ERR_MOTOR_STOP;
			return lRet;
		}
		
		/*! 设置电机位置,电机不知道在什么位置 */
		set_sample_motor_position(MtxId, s_ucaMotorNoneRealPos[MtxId]);
		
		s_astSampleMotor[MtxId].ucState = MOTOR_STATE_IDLE;
		//set_frame_comm_state(COMM_FINISH, 0);
		
		lRet = EXIT_SUCCESS;

	}while (0);

	return  lRet;
}



/*******************************************************************************
* 名称: Mtx_State_Main
* 功能: 10ms周期性的调用本函数，更新电机的状态

* 返回:  0 正常， 非0表示错误代码
* 说明: 无
********************************************************************************/
uint8_t Mtx_State_Main(void)
{
	uint8_t lRet        = 0;
	uint8_t index       = 0;
    uint8_t ucMoveState = 0;
	uint16_t unRemainStep = 0;
		
	/*! 动作开始 */
	for ( index = 0; index < MAX_MOTOR_NUM; index++ )
	{
		/*! 电机动作 */
		if ( ( MOTOR_STATE_IDLE != s_astSampleMotor[index].ucState ) && (0 == s_astSampleMotor[index].unActionDelay) )
		{
			/*! 当前电机撞击 */
			check_motor_hit_state(index, s_astSampleMotor[index].ucMoveDirection) ;
			
			/*! 读取当前电机剩余的步数 */
			if ( EXIT_FAILURE == read_motor_param_from_fpga(index, &(s_astSampleMotor[index]) ) )
			{
				lRet = ERR_FPGA_EXE;
				s_ucMotorType[index].ucErrCode = lRet;
			}
			
			/*! 更新电机已经走过的步数 */
			unRemainStep = s_astSampleMotor[index].unRemainStep;	
			
			/*! 计算更新离起始位置步数*/
			if ( s_astSampleMotor[index].ucMoveDirection == MOTOR_DIR_FORWARD )		//电机方向向前
			{
				s_astSampleMotor[index].unPassedStep += s_astSampleMotor[index].unMoveStep - unRemainStep;
			}
			else
			{
				s_astSampleMotor[index].unPassedStep -= s_astSampleMotor[index].unMoveStep - unRemainStep;
			}
			
//			PRO_DEBUG(INIT_DEBUG, ("unRemainStep=%d \r\n", unRemainStep));
			
			/*!更新还要走的步数*/
			s_astSampleMotor[index].unMoveStep = unRemainStep;

			/*! 读取当前电机的运行状态 */	
			ucMoveState = s_astSampleMotor[index].ucRunState;	
		
			/*! 电机执行完成,停止 */
			if ( MOTOR_STATE_STOP == ucMoveState )
			{
				switch( s_astSampleMotor[index].ucState )
				{  
					/*! 复位回起始位（走完了最大步数看其是否到达起始位） */
					case MOTOR_STATE_RESET_START:
					{
						lRet = motor_state_reset_started(index);
					}break;
								
					/*! 复位回起始位（离开起始位走完设置步数） */
					case MOTOR_STATE_RESET_STEP1:
					{
						lRet = motor_state_reset_step1ed(index);
					}break;
					
					/*! 电机运动 （走完步数） */
					case MOTOR_STATE_ACT_START:
					{
						lRet = motor_state_move_steped(index);
					}break;

					/*! 电机走到指定的位置检测 */
					case MOTOR_STATE_POS_START:
					{
						lRet = motor_state_move_posed(index);
					}break; 
				
					/*! 电机停止检测 */
					case MOTOR_STATE_STOP_START:
					{
						lRet = motor_state_stoped(index);
					}break;
					
					/*! 电机空闲状态 */
					case MOTOR_STATE_IDLE:
					{
						lRet = EXIT_SUCCESS;                        
					}break;
					
					default:break;
				}
				
				/*! 电机进行错误检测 */
				if(lRet != EXIT_SUCCESS)
				{
					s_ucMotorType[index].ucErrCode = lRet;
				}	

//				PRO_DEBUG(INIT_DEBUG, ("unPassedStep=%d \r\n", s_astSampleMotor[index].unPassedStep));
			}
		}				
	}//for
		
	return lRet;
}

/*******************************************************************************
* 名称: Mtx_Waiting_To_Stop
* 功能: 睡眠，等待电机运行停止
			MtxId   电机编号
			timeov  等待时间
* 返回:    lRet    0 电机状态停止， 非0 电机状态没有停止或报错
* 说明: 无
********************************************************************************/
uint8_t Mtx_Waiting_To_Stop(uint8_t MtxId, uint32_t timeov)
{
	OS_ERR     err;
	uint8_t    lRet         = EXIT_FAILURE;
	uint16_t   ucTime       = 0;
	
	do
	{
		ucTime++;
		
		/*! 当电机运行停止就直接还回 */
		if(s_astSampleMotor[MtxId].ucRunState == MOTOR_STATE_STOP)
		{
			lRet = EXIT_SUCCESS;
			break;
		}
		/*! 每10ms检查一次电机的运行状态  */
		OSTimeDlyHMSM ( 0, 0, 0, 20, OS_OPT_TIME_DLY,  & err );
	
	}while(ucTime <= timeov);
	
	/*! 进行电机运行状态的错误检测 */
	if(s_ucMotorType[MtxId].ucErrCode != 0)
	{
		return s_ucMotorType[MtxId].ucErrCode;
	}
	else
	{
		return lRet;
	}
}

/*******************************************************************************
* 名称: Mtx_Get_State
* 功能: 获取电机运行运行状态
	    MtxId   电机编号
* 返回: lRet    电机的状态
* 说明: 无
********************************************************************************/
uint8_t Mtx_Get_State(uint8_t MtxId)
{
	return s_astSampleMotor[MtxId].ucState;
}



/*******************************************************************************
* 名称: Mtx_Waiting_To_Idle
* 功能: 等待电机运行空闲
	    MtxId   电机编号
* 返回: lRet    0 电机状态停止， 非0 电机状态没有停止或报错
* 说明: 无
********************************************************************************/
uint8_t Mtx_Waiting_To_Idle(uint8_t MtxId)
{
    OS_ERR     err;

    while(MOTOR_STATE_IDLE != s_astSampleMotor[MtxId].ucState)
    {
        OSTimeDlyHMSM ( 0, 0, 0, 1, OS_OPT_TIME_DLY,  &err );
    }
	
    if(s_ucMotorType[MtxId].ucErrCode)
    {
        return s_ucMotorType[MtxId].ucErrCode;
    }
    return EXIT_SUCCESS;
	
}














