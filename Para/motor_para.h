
#ifndef __MOTOR_PARA_h__
#define __MOTOR_PARA_h__
#include <includes.h>

// 针参数
__packed typedef struct
{
    uint16_t unIncubate_HORZMotorPosStep[3];        	 // 孵育盒水平电机 每个位置步数， 每个补偿2个字节
    uint16_t unIncubate_FLIPMotorPosStep[7];             // 孵育盒卡条翻转电机 每个位置步数， 每个补偿2个字节
	uint16_t unIncubate_LIFTMotorPosStep[3];			 // 孵育盒升降电机 每个位置步数，每个运动步数2个字节
    uint16_t unIncubate_UNLOADMotorPosStep[3];    		 // 孵育盒卡条卸载电机 每个位置步数， 每个补偿2个字节
    uint16_t unMicro_BARMotorPosStep[3];    		     // 显微镜计数卡推杆电机5 每个位置步数， 每个补偿2个字节
	
    uint16_t unload_p_horzMotorPosStep[10];        	 		 // 载体水平电机      每个位置步数， 每个补偿2个字节
    uint16_t unload_p_pushMotorPosStep[7];            		 // 载体卡条推杆电机  每个位置步数， 每个补偿2个字节
	uint16_t unload_p_flipMotorPosStep[7];			 		 // 载体卡条门翻转电机 每个位置步数，每个运动步数2个字节
    uint16_t untip_head_boxMotorPosStep[7];    				 // TIP头仓盒电机   每个位置步数， 每个补偿2个字节
    uint16_t uncard_slice_boxMotorPosStep[7];    		     // 卡条盒进卡电机  每个位置步数， 每个补偿2个字节
	
}motor_para_t;


void SetAllMotorPara(frame_info_t *pstFrameInfo);
void GetAllMotorPara(frame_info_t *pstFrameInfo);

void SetSampleProbeSpeed(uint16_t ucHorzSpeed, uint16_t ucLoadSpeed, uint16_t ucSyriSpeed, uint16_t ucPickSpeed);

void Setload_p_horzMotorPosStep(uint8_t ucPos, uint16_t ucPosStep);
void Setload_p_pushMotorPosStep(uint8_t ucPos, uint16_t ucPosStep);
void Setload_p_flipMotorPosStep(uint8_t ucPos, uint16_t ucPosStep);
void Settip_head_boxMotorPosStep(uint8_t ucPos, uint16_t ucPosStep);
void Setcard_slice_boxMMotorPosStep(uint8_t ucPos, uint16_t ucPosStep);

uint16_t Getload_p_horzMotorPosStep(uint8_t ucPos);
uint16_t Getload_p_pushMotorPosStep(uint8_t ucPos);
uint16_t Getload_p_flipMotorPosStep(uint8_t ucPos);
uint16_t Gettip_head_boxMotorPosStep(uint8_t ucPos);
uint16_t Getcard_slice_boxMMotorPosStep(uint8_t ucPos);


void InitReadAllMotorPara(void);
uint16_t GetMotorPosStep(uint8_t ucMotorId, uint8_t ucPos);

void Set_temp_para(uint8_t ucPos, uint16_t unTemp);
uint16_t get_temp_para(uint8_t ucPos);


#endif
