#include "EXFunctionIO.h"
#include "IO_Def.h"
#include "includes.h"
#include "InputIOMain.h"


#define  IO_TIM			     TIM8
#define  IO_TIM_RCC			 RCC_APB2Periph_TIM8
#define  IO_TIM_IRQ			 TIM8_UP_IRQn

#define  X_DIY	         2 //滤波时间
#define  YMOTOR_DIY      5
#define  SMOTOR_DIY      10
#define  COUNTER_DIY     30

unsigned char X_FilterDiy[32]  = {0};
unsigned int InPutX            =  0;
unsigned int OutPutY           =  0;

BUTTONSTATE ButtonState[32]    = {BUTTON_OFF};//按钮状态
BUTTONSTATE OldButtonState[32] = {BUTTON_OFF};//按钮状态
BUTTONSTATE NewButtonState[32] = {BUTTON_OFF};//按钮状态

unsigned char CounterLeftFlag  = 0;
unsigned char CounterRightFlag = 0;
/* 左右计数器单独控制 */
static unsigned char s_ucLeftCountFlag  = 0;
static unsigned char s_ucRightCountFlag = 0;

extern unsigned int g_DelayCounter;
extern unsigned int g_uiCounter_Left ;
extern unsigned int g_uiCounter_Right ;


//外部IO扫描初始化  1ms
void ExFunctionIOScanInit(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB2PeriphClockCmd(IO_TIM_RCC, ENABLE);

	TIM_TimeBaseStructure.TIM_Period		 = 100;	  		 //计数值。  计时时间=计数值/TIM5 counter clock
	TIM_TimeBaseStructure.TIM_Prescaler	 	 = 84;	   //与分频
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;		 //定时器时钟（CK_INT）频率与数字滤波器（ETR,TIx）使用的采样频率之间的分频比例。 
													 //TIM_CKD_DIV1 <->TDTS = Tck_tim    	TIM_CKD_DIV1=0x0000												 //TIM_CKD_DIV1 <->TDTS = Tck_tim
													 //TIM_CKD_DIV2 <->TDTS = 2Tck_tim 		TIM_CKD_DIV2=0x0100												 //TIM_CKD_DIV1 <->TDTS = Tck_tim
													 //TIM_CKD_DIV4 <->TDTS = 4Tck_tim		TIM_CKD_DIV4=0x0200	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;	//向上计算是从0计数到自动加载值，向下计算是从自动加载值计数到0
											 //中央对齐模式是从0计数到自动加载值（溢出），再从自动加载值计数到1（溢出）在从0开始计数。。。
											 //所以不管选用哪种模式，溢出的时间是不变的
	TIM_TimeBaseInit(IO_TIM, &TIM_TimeBaseStructure);
	TIM_ARRPreloadConfig(IO_TIM,ENABLE);			//预装载使能
	TIM_ITConfig(IO_TIM,TIM_FLAG_Update,ENABLE);   //TIM更新中断源
	TIM_Cmd(IO_TIM, ENABLE);						//开IO_TIM定时器 

	/* Enable the IO_TIM gloabal Interrupt */
//	NVIC_InitStructure.NVIC_IRQChannel = IO_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

//打开计数器的计数
void SetCounterSwitchFlag(void)
{
	s_ucLeftCountFlag  = 0;
	s_ucRightCountFlag = 0;
}

//关闭计数器的计数
void ResetCounterSwitchFlag(void)
{
	
}

//初始化IO扫描程序
void InitExFuntionIO(void)
{
	InPutX             = 0;
	OutPutY            = 0;
	s_ucLeftCountFlag  = 0;
	s_ucRightCountFlag = 0;
	
	// ExFunctionIOScanInit();
}


//X点输入
void INPUT_X_Filter(void)
{
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X00)==Bit_RESET){if(X_FilterDiy[0]<X_DIY){X_FilterDiy[0]++;}else{InPutX|=0x00000001;}}//X00
	else{X_FilterDiy[0]=0;InPutX&=0xFFFFFFFE;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X01)==Bit_RESET){if(X_FilterDiy[1]<X_DIY){X_FilterDiy[1]++;}else{InPutX|=0x00000002;}}//X01
	else{X_FilterDiy[1]=0;InPutX&=0xFFFFFFFD;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X02)==Bit_RESET){if(X_FilterDiy[2]<X_DIY){X_FilterDiy[2]++;}else{InPutX|=0x00000004;}}//X02
	else{X_FilterDiy[2]=0;InPutX&=0xFFFFFFFB;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X03)==Bit_RESET){if(X_FilterDiy[3]<X_DIY){X_FilterDiy[3]++;}else{InPutX|=0x00000008;}}//X03
	else{X_FilterDiy[3]=0;InPutX&=0xFFFFFFF7;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X04)==Bit_RESET){if(X_FilterDiy[4]<X_DIY){X_FilterDiy[4]++;}else{InPutX|=0x00000010;}}//X04
	else{X_FilterDiy[4]=0;InPutX&=0xFFFFFFEF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X05)==Bit_RESET){if(X_FilterDiy[5]<X_DIY){X_FilterDiy[5]++;}else{InPutX|=0x00000020;}}//X05
	else{X_FilterDiy[5]=0;InPutX&=0xFFFFFFDF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X06)==Bit_RESET){if(X_FilterDiy[6]<X_DIY){X_FilterDiy[6]++;}else{InPutX|=0x00000040;}}//X06
	else{X_FilterDiy[6]=0;InPutX&=0xFFFFFFBF;}


	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X07)==Bit_RESET){if(X_FilterDiy[7]<X_DIY){X_FilterDiy[7]++;}else{InPutX|=0x00000080;}}//X07
	else{X_FilterDiy[7]=0;InPutX&=0xFFFFFF7F;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X08)==Bit_RESET){if(X_FilterDiy[8]<X_DIY){X_FilterDiy[8]++;}else{InPutX|=0x00000100;}}//X08
	else{X_FilterDiy[8]=0;InPutX&=0xFFFFFEFF;}

	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X09)==Bit_RESET){if(X_FilterDiy[9]<YMOTOR_DIY){X_FilterDiy[9]++;}else{InPutX|=0x00000200;}}//X09
	else{X_FilterDiy[9]=0;InPutX&=0xFFFFFDFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X10)==Bit_RESET){if(X_FilterDiy[10]<X_DIY){X_FilterDiy[10]++;}else{InPutX|=0x00000400;}}//X10
	else{X_FilterDiy[10]=0;InPutX&=0xFFFFFBFF;}

	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X11)==Bit_RESET){if(X_FilterDiy[11]<X_DIY){X_FilterDiy[11]++;}else{InPutX|=0x00000800;}}//X11
	else{X_FilterDiy[11]=0;InPutX&=0xFFFFF7FF;}

	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X12)==Bit_SET){if(X_FilterDiy[12]<SMOTOR_DIY){X_FilterDiy[12]++;}else{InPutX|=0x00001000;}}//X12
	else{X_FilterDiy[12]=0;InPutX&=0xFFFFEFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X13)==Bit_SET){if(X_FilterDiy[13]<X_DIY){X_FilterDiy[13]++;}else{InPutX|=0x00002000;}}//X13
	else{X_FilterDiy[13]=0;InPutX&=0xFFFFDFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X14)==Bit_SET){if(X_FilterDiy[14]<X_DIY){X_FilterDiy[14]++;}else{InPutX|=0x00004000;}}//X14
	else{X_FilterDiy[14]=0;InPutX&=0xFFFFBFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X15)==Bit_SET){if(X_FilterDiy[15]<X_DIY){X_FilterDiy[15]++;}else{InPutX|=0x00008000;}}//X15
	else{X_FilterDiy[15]=0;InPutX&=0xFFFF7FFF;}

	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X16)==Bit_SET){if(X_FilterDiy[16]<X_DIY){X_FilterDiy[16]++;}else{InPutX|=0x00010000;}}//X16
	else{X_FilterDiy[16]=0;InPutX&=0xFFFEFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X17)==Bit_SET){if(X_FilterDiy[17]<X_DIY){X_FilterDiy[17]++;}else{InPutX|=0x00020000;}}//X17
	else{X_FilterDiy[17]=0;InPutX&=0xFFFDFFFF;}

	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X18)==Bit_SET){if(X_FilterDiy[18]<X_DIY){X_FilterDiy[18]++;}else{InPutX|=0x00040000;}}//X18  
	else{X_FilterDiy[18]=0;InPutX&=0xFFFBFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X19)==Bit_RESET){if(X_FilterDiy[19]<X_DIY){X_FilterDiy[19]++;}else{InPutX|=0x00080000;}}//X19 
	else{X_FilterDiy[19]=0;InPutX&=0xFFF7FFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X20)==Bit_RESET){if(X_FilterDiy[20]<X_DIY){X_FilterDiy[20]++;}else{InPutX|=0x00100000;}}//X20
	else{X_FilterDiy[20]=0;InPutX&=0xFFEFFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X21)==Bit_SET){if(X_FilterDiy[21]<X_DIY){X_FilterDiy[21]++;}else{InPutX|=0x00200000;}}//X21
	else{X_FilterDiy[21]=0;InPutX&=0xFFDFFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X22)==Bit_SET){if(X_FilterDiy[22]<X_DIY){X_FilterDiy[22]++;}else{InPutX|=0x00400000;}}//X22
	else{X_FilterDiy[22]=0;InPutX&=0xFFBFFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X23)==Bit_SET){if(X_FilterDiy[23]<COUNTER_DIY){X_FilterDiy[23]++;}else{InPutX|=0x00800000;}}//X23
	else{X_FilterDiy[23]=0;InPutX&=0xFF7FFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X24)==Bit_RESET){if(X_FilterDiy[24]<COUNTER_DIY){X_FilterDiy[24]++;}else{InPutX|=0x01000000;}}//X24
	else{X_FilterDiy[24]=0;InPutX&=0xFEFFFFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X25)==Bit_RESET){if(X_FilterDiy[25]<COUNTER_DIY){X_FilterDiy[25]++;}else{InPutX|=0x02000000;}}//X25
	else{X_FilterDiy[25]=0;InPutX&=0xFDFFFFFF;}
	if(GPIO_ReadRegisterBit(SENSOR_INPUT_X26)==Bit_SET){if(X_FilterDiy[26]<X_DIY){X_FilterDiy[26]++;}else{InPutX|=0x04000000;}}//X26  
	else{X_FilterDiy[26]=0;InPutX&=0xFBFFFFFF;}
 
}

/*******************************************************************************
* 名称: OUTPUT_Y_Set
* 功能: 设置Y点输出
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void OUTPUT_Y_Set(void)//Y点输出  
{
	if((OutPutY&0x00000001)!=0){GPIO_SetBits(IO_Y00);}   //Y00
	else{GPIO_ResetBits(IO_Y00);}
	if((OutPutY&0x00000002)!=0){GPIO_SetBits(IO_Y01);}   //Y01
	else{GPIO_ResetBits(IO_Y01);}
	if((OutPutY&0x00000004)!=0){GPIO_SetBits(IO_Y02);}   //Y02
	else{GPIO_ResetBits(IO_Y02);}
	if((OutPutY&0x00000008)!=0){GPIO_SetBits(IO_Y03);}   //Y03
	else{GPIO_ResetBits(IO_Y03);}
	if((OutPutY&0x00000010)!=0){GPIO_SetBits(IO_Y04);}   //Y04
	else{GPIO_ResetBits(IO_Y04);}
	if((OutPutY&0x00000020)!=0){GPIO_SetBits(IO_Y05);}   //Y05
	else{GPIO_ResetBits(IO_Y05);}
	if((OutPutY&0x00000040)!=0){GPIO_SetBits(IO_Y06);}   //Y06
	else{GPIO_ResetBits(IO_Y06);}
	if((OutPutY&0x00000080)!=0){GPIO_SetBits(IO_Y07);}   //Y07
	else{GPIO_ResetBits(IO_Y07);}
}

/*******************************************************************************
* 名称: SetScanDCMotorIO
* 功能: 设置扫码直流电机的状态
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SetScanDCMotorIO(unsigned char _state)
{
	if(_state == DC_MOTOR_ON)
	{
		GPIO_ResetBits(IO_DC_MOTOR);
	}
	else
	{
		GPIO_SetBits(IO_DC_MOTOR);
	}
}

/*******************************************************************************
* 名称: GetLeftCountFlag
* 功能: 左计数器有效位
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
unsigned char GetLeftCountFlag(void)
{
  return s_ucLeftCountFlag;
}

/*******************************************************************************
* 名称: GetRightCountFlag
* 功能: 右计数器计数
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
unsigned char GetRightCountFlag(void)
{
  return s_ucRightCountFlag;
}

/*******************************************************************************
* 名称: SetLeftCountFlag
* 功能: 左计数器计数
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SetLeftCountFlag(unsigned char ucTemp)
{
   s_ucLeftCountFlag = ucTemp ;
}

/*******************************************************************************
* 名称: GetRightCountFlag
* 功能: 右计数器计数
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void SetRightCountFlag(unsigned char ucTemp)
{
  s_ucRightCountFlag = ucTemp;
}

/*
功能：获取扩展板的输入点状态
输入:获取输入点的位置
输出：输入点的状态
*/
void KeyOnOffStateScan(void)
{
	unsigned char i = 0;
	unsigned int  m = 0;
	
	for(i=0; i<32; i++)
	{
		m=1<<i;
		if((InPutX&m)!=0)
			NewButtonState[i] = BUTTON_ON;
		else
			NewButtonState[i] = BUTTON_OFF;
	}
	
	/*! 打开计数 */	
	for(i=0;i<32;i++)
	{
		if(i == Y_CNT_LEFT || i == Y_CNT_RIGHT )
		{
			if(OldButtonState[i] != NewButtonState[i])
			{
				if(OldButtonState[i]==BUTTON_OFF&&NewButtonState[i]==BUTTON_ON)
				{
					ButtonState[i]=BUTTON_OFF_TO_ON;
					if(i == Y_CNT_LEFT)
					{
						s_ucLeftCountFlag ++;
					}
					if(i == Y_CNT_RIGHT )
					{
						s_ucRightCountFlag ++;
					}
				}
				else if(OldButtonState[i]==BUTTON_ON&&NewButtonState[i]==BUTTON_OFF)
				{
					ButtonState[i]=BUTTON_ON_TO_OFF;
					if(i == Y_CNT_LEFT )//只有先ON_TO_OFF， 再到OFF_TO_ON才判定为有效计数
					{
						s_ucLeftCountFlag ++;
					}
					if(i == Y_CNT_RIGHT )
					{
						s_ucRightCountFlag ++;
					}
				  //DEBUG(" EXF Right:%d,  RightFlag:%d   EXF Left:%d,  LeftFlag:%d\r\n", g_uiCounter_Right, CounterRightFlag, g_uiCounter_Left, CounterLeftFlag);	
				}
				else
				{
					ButtonState[i]=NewButtonState[i];
				}
			}
		}//if
	}	
		
	/*! 更新状态 */
	OldButtonState[Y_CNT_LEFT]=NewButtonState[Y_CNT_LEFT];
	OldButtonState[Y_CNT_RIGHT]=NewButtonState[Y_CNT_RIGHT];
	
}

/*******************************************************************************
* 名称: GetButtonState
* 功能: 获取当前按键信息
* 形参: buttonPos 按键位置
* 返回: 无
* 说明: 无
********************************************************************************/
BUTTONSTATE GetButtonState(unsigned char buttonPos)
{
	unsigned char i;
	i=buttonPos;
	
	return ButtonState[i];
}


/*******************************************************************************
* 名称: GetButtonState
* 功能: 获取当前按键信息
* 形参: buttonPos 按键位置
* 返回: 无
* 说明: 无
********************************************************************************/
void GetInputXData(unsigned int *puInputData)
{
	*puInputData = InPutX;
}


/*
功能：获取扩展板的输入点状态
输入:获取输入点的位置
输出：输入点的状态
*/
KEYSTATE GetInputSensor(XPOS Pos)
{
	KEYSTATE state=KEY_OFF;
	switch(Pos)
	{
		/*! 第1位 */
		case SYRINGE_INSTALL_RF:
			if((InPutX&0x00000001)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case SALINE_WATER_SW:
			if((InPutX&0x00000002)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case NEEDLE_OUT_SW:
			if((InPutX&0x00000004)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case NEEDLE_IN_SW:
			if((InPutX&0x00000008)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		/*! 第2位 */
		case STREAM_GENERATE_SW:
			if((InPutX&0x00000010)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case SLICE_BOX_RF2:
			if((InPutX&0x00000020)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case CARD_BOX_RF1:
			if((InPutX&0x00000040)!=0)
			 {
				  state=KEY_ON;
			 }
			break;			 
		case CARD_BOX_RF2:
			if((InPutX&0x00000080)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		/*! 第3位 */
		case OUT_SMAPLE_SW:
			if((InPutX&0x00000100)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case SW_NO_USE1:
			if((InPutX&0x00000200)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case SW_NO_USE2:
			if((InPutX&0x00000400)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case Y_CNT_LEFT:
			if((InPutX&0x00000800)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		/*! 第4位 */
		case Y_CNT_RIGHT:
			if((InPutX&0x00001000)!=0)
			 {
				  state=KEY_ON;
			 }
			 break;
		case EMERG_RESET:
			if((InPutX&0x00002000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case EMERG_POS:
			if((InPutX&0x00004000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case IN_SAMPLE_OPTCL:
			if((InPutX&0x00008000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		/*! 第5位 */
		case SENSOR17:
			if((InPutX&0x00010000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case SENSOR18:
			if((InPutX&0x00020000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case TUBE_EXIST1:
			if((InPutX&0x00040000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case TUBE_EXIST2:
			if((InPutX&0x00080000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;	
		/*! 第6位 */
		case EMERG_BTN:
			if((InPutX&0x00100000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case Z_SWITCH1_FULL:
			if((InPutX&0x00200000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case X_SWITCH2_REACH:
			if((InPutX&0x00400000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case FACESHELL_STATE:
			if((InPutX&0x00800000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		/*! 第7位 */
		case INPUT_AS_OUT1:
			if((InPutX&0x01000000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		case INPUT_AS_OUT2:
			if((InPutX&0x02000000)!=0)
			 {
				  state=KEY_ON;
			 }
			break;
		/*! 第8位 */
			 
		default:
			break;
	}
	return state;
}



/*
功能：设置Y点输出点状态
输入:Pos 获取输入点的位置 state 输出点状态
输出： 无
*/
void SetOutputY(YPOS Pos,KEYSTATE state)
{
	unsigned int i = 0, j;
	
	i = 0x1<<(Pos-1);
	if(state==KEY_ON)
	{
		OutPutY|=i;
	}
	else if(state==KEY_OFF)
	{
		i=~i;
		OutPutY&=i;
	}
	else if(state==KEY_REVER)
	{
		j=(OutPutY)&i;
		if(j!=0)
		{
			i=~i;
			OutPutY&=i;
		}
		else
		{
			OutPutY|=i;
		}
	}
}


//IO扫描基本主程序
void EXFuntionIOScan(void)
{
	INPUT_X_Filter();
	KeyOnOffStateScan();
}


//定时器7 1ms 中断
void TIM8_IRQHandler(void)
{
	if(TIM_GetITStatus(IO_TIM,TIM_IT_Update)!=RESET)
	{
		TIM_ClearITPendingBit(IO_TIM,TIM_IT_Update);
		EXFuntionIOScan();

	} 
} 



