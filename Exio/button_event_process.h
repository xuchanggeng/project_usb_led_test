/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 


#ifndef __BUTTON_EVENT_PROCESS_H
#define __BUTTON_EVENT_PROCESS_H
#include "includes.h"

/*! 定时事件控制结构体 */
//typedef struct _tag_timer_io_event_t
//{
//	uint8_t ucTimerEventFlag;
//	uint16_t unTimerEventTick;
//	uint16_t unTimerEventInterval;
//}timer_io_event_t;

/*! 按钮时间处理结构体 */
typedef struct _tag_button_event_t
{
	uint8_t  ucButtonNum;				// 序号
	uint8_t  ucButtonState;			    // 使能状态
    uint8_t  unButtonTriggerType;		// 触发类型
    void (*pbutton_event_handle_fun) (uint8_t ucIndex);		//处理函数	
} button_event_t;



/*! 按钮名称常量定义  */
typedef enum
{
	BUTTON_NAME_NUM0,
	BUTTON_NAME_NUM1,
	BUTTON_NAME_NUM2,
	BUTTON_NAME_NUM3,
	BUTTON_NAME_NUM4,
	BUTTON_NAME_NUM5,
} e_button_name_t;

void button_event_init(void);
void button_event_loop(void);
void set_one_button_event_state(uint8_t ucButtonNo, uint8_t ucState);
void add_one_button_event(button_event_t *stBtnEvent);


#endif  /*__BUTTON_EVENT_PROCESS_H*/

