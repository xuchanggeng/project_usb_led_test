#ifndef _EXFunctionIO_H__
#define _EXFunctionIO_H__
#include "includes.h"


typedef enum
{
	KEY_OFF       = 0,    //按键弹起
	KEY_ON        = 1,    //按键按下
	KEY_REVER     = 2,     //反转按键状态
}KEYSTATE ;


typedef enum _BUTTONSTATE
{
	BUTTON_OFF       = 0, //按钮松开中
	BUTTON_OFF_TO_ON = 1, //按钮按下边缘
	BUTTON_ON        = 2, //按钮按下中
	BUTTON_ON_TO_OFF = 3 , //按钮松开边缘
}BUTTONSTATE;

typedef enum
{
	/*! 反射光耦输入 */
	SYRINGE_INSTALL_RF	    = 0,         //SENSOR00
	SALINE_WATER_SW         = 1,         //SENSOR00
	NEEDLE_OUT_SW           = 2,          //SENSOR01
	NEEDLE_IN_SW            = 3,          //SENSOR02
	STREAM_GENERATE_SW      = 4,          //SENSOR03
	SLICE_BOX_RF2           = 5,          //SENSOR05
	CARD_BOX_RF1            = 6,          //SENSOR06
	CARD_BOX_RF2            = 7,          //SENSOR07
	
	/*! 开关输入 */
	OUT_SMAPLE_SW           = 8,         //SENSOR08
	SW_NO_USE1              = 9,         //SENSOR09  
	SW_NO_USE2              = 10,        //SENSOR10	 

	/*! 左右计数器输入 */
	Y_CNT_LEFT              = 11,         //SENSOR11  
	Y_CNT_RIGHT             = 12,         //SENSOR12  

	/*! IO没有用到 */
	EMERG_RESET        = 13,             //SENSOR14
	EMERG_POS    	   = 14,			 //SENSOR15
	IN_SAMPLE_OPTCL    = 15,			 //SENSOR16
	SENSOR17    	   = 16,			 //SENSOR17
	SENSOR18		   = 17,			 //SENSOR18
	TUBE_EXIST1       = 18,     		 //SENSOR19
	TUBE_EXIST2       = 19,     		 //SENSOR20
	EMERG_BTN 		  = 20,         	//SENSOR21
	Z_SWITCH1_FULL    = 21,         	//SENSOR22
	X_SWITCH2_REACH   = 22,         	//SENSOR23
	FACESHELL_STATE   = 23,         	//SENSOR24
	MICROSWITCH3_1    = 24,         	//SENSOR24
	INPUT_AS_OUT1     = 25,         	//SENSOR23
	INPUT_AS_OUT2     = 26,        		//SENSOR24

	/*! 兼容老信号的定义 */
	MIX_RESET                 = 0,         //SENSOR01
	HORI_PRE                  = 1,         //SENSOR02
	HORI_BACK                 = 2,         //SENSOR03
	VECTICAL_UP               = 7,         //SENSOR04
	VECTICAL_DOWN             = 8,         //SENSOR05

}XPOS;


typedef enum
{
	Y_ALL             = 0,
	Y00               = 1,  //Y00
	Y01               = 2,  //Y01
	Y02               = 3,  //Y02
	Y03               = 4,  //Y03
	Y04               = 5,  //Y04
	Y05               = 6,  //Y05
	Y06               = 7,  //Y06
	Y07               = 8,  //Y07
}YPOS;

/*******************************************************************************
* Function Name  : GPIO_ReadRegisterBit
* Description    : 读取IO函数为内联函数，加快速度
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
static __inline  uint8_t GPIO_ReadRegisterBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
  return (READ_BIT(GPIOx->IDR, GPIO_Pin) == (GPIO_Pin));
}

//初始化IO扫描程序
void InitExFuntionIO(void);

//输入检测
void INPUT_X_Filter(void);

void KeyOnOffStateScan(void);

BUTTONSTATE GetButtonState(unsigned char buttonPos);

void GetInputXData(unsigned int *puInputData);

//扫描输出
void OUTPUT_Y_Set(void);

KEYSTATE GetInputSensor(XPOS Pos);

void SetOutputY(YPOS Pos,KEYSTATE state);

//IO扫描基本主程序
void EXFuntionIOScan(void);

//打开计数器的计数
void SetCounterSwitchFlag(void);

//关闭计数器的计数
void ResetCounterSwitchFlag(void);

void SetRightCountFlag(unsigned char ucTemp);

void SetLeftCountFlag(unsigned char ucTemp);

unsigned char GetRightCountFlag(void);

unsigned char GetLeftCountFlag(void);

void SetScanDCMotorIO(unsigned char _state);

#endif

