#include "EXFunctionIO.h"
#include "InputIOMain.h"
#include "includes.h"
#include "IO_Def.h"
#include "bsp_eeprom.h"
#include "MaintainMain.h"
#include "Com_FrameMake.h"
#include "bsp_pump_valve.h"
#include "bsp_motor_cmd.h"
#include "cmd_send_frame.h"

#define        SAMPLE_REAL_TIME_INTERVAL               				30
#define        IO_STATE_INTERVAL        			 			    50

static void DebugPrintIO_Info(void);

/*! 进样仓自动触发，急诊仓触发状态上传 状态标志 */	
//static timer_io_event_t s_stSampleRealTimeState = {1, 0, SAMPLE_REAL_TIME_INTERVAL};

/*! IO 状态信息上传 状态标志 */	
//static timer_io_event_t s_stIOState = {0, 0, IO_STATE_INTERVAL};

/*! 上位机状态标志 */	
static unsigned char s_ucMasterMachine      = 0;    



/*******************************************************************************
* Function Name  : Set_SendIOStateFlag
* Description    : 设置是否发送IO状态给PC
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Set_SendIOStateFlag(unsigned char ucTemp)
{


}

/*******************************************************************************
* 名称: SetMasterMachineState
* 功能: 
		设置上位机的状态
* 形参: 
* 返回: 无
* 说明: 无
********************************************************************************/
void SetMasterMachineState(unsigned char ucTemp)
{
	s_ucMasterMachine = ucTemp;
}


/*******************************************************************************
* 名称: MaintainMainInit
* 功能: 
		维护任务函数的初始化
* 形参: 
* 返回: 无
* 说明: 无
********************************************************************************/
void MaintainMainInit(void)
{
	(void)s_ucMasterMachine;
	s_ucMasterMachine      = 0;
}


/*******************************************************************************
* 名称: is_real_time_state_change
* 功能: 
		发送实时状态给上位机
* 形参: 
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t is_real_time_state_change(void)
{
	uint8_t ucRet = 0;
	
	/*! 进样仓进样触发 */
	if(GetInputSensor(Z_SWITCH1_FULL) == KEY_ON)
	{
		ucRet = 1;
	}
	/*! 急诊仓进样触发 */
	if(GetInputSensor(Z_SWITCH1_FULL) == KEY_ON)
	{
		ucRet = 1;
	}
	
	return ucRet;
}




/*******************************************************************************
* 名称: MaintainMain
* 功能: 
		 实时状态处理
* 形参: 
* 返回: 无
* 说明: 无
********************************************************************************/
void MaintainMain(void)
{
	static uint16_t ucCounter     = 0;
	uint8_t ucaData[8] = {0};
	
	/*! IO状态调试串口输出 */
	if(ucCounter++ > 3000)
	{
		ucCounter = 0;
		DebugPrintIO_Info();
	}

	/*! 0. 无菌水注射器是否放入检测， */
	if(GetButtonState(SALINE_WATER_SW) == BUTTON_OFF_TO_ON)
	{
		/*! 检测到无菌水注射器, 发送信息给PC */
		ucaData[0] = CMD_TYPE_WATER_SYRING_INSTALL_STATUS;
		ucaData[1] = 0x01;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
	}
	else if(GetButtonState(SALINE_WATER_SW) == BUTTON_ON_TO_OFF)
	{
		/*! 没有，检测到无菌水注射器, 发送信息给PC */
		ucaData[0] = CMD_TYPE_WATER_SYRING_INSTALL_STATUS;
		ucaData[1] = 0x00;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
	}

	/*! 1. 灌注按住，慢速灌注*/
	if(GetButtonState(SALINE_WATER_SW) == BUTTON_OFF_TO_ON)
	{
		/*! 慢速灌注 */
		bsp_motor_cmd_move_speed(SALINE_PERFUSION_MOTOR, SALINE_PERFUSION_MOTOR_SPEES_LEVEL1);

		ucaData[0] = CMD_TYPE_SALINE_MOTOR_PERFUSION_STATUS;
		ucaData[1] = 0x01;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
	}
	else if(GetButtonState(SALINE_WATER_SW) == BUTTON_ON_TO_OFF)
	{
		/*! 停止灌注 */
		bsp_motor_cmd_stop(SALINE_PERFUSION_MOTOR);

		ucaData[0] = CMD_TYPE_SALINE_MOTOR_PERFUSION_STATUS;
		ucaData[1] = 0x00;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
	}

	/*! 2. 灌注双击，快速灌注 */
	if( (GetButtonState(SALINE_WATER_SW) == BUTTON_OFF_TO_ON) && GetInputSensor(SALINE_WATER_SW) == KEY_ON)
	{
		/*! 快速灌注 */
		bsp_motor_cmd_move_speed(SALINE_PERFUSION_MOTOR, SALINE_PERFUSION_MOTOR_SPEES_LEVEL2);

		ucaData[0] = CMD_TYPE_SALINE_MOTOR_PERFUSION_STATUS;
		ucaData[1] = 0x02;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
	}

	/*! 3. 灌注按住，出针按住，出针 */
	if((GetButtonState(NEEDLE_OUT_SW) == BUTTON_OFF_TO_ON) && (GetInputSensor(SALINE_WATER_SW) == KEY_ON) )
	// if( GetInputSensor(SYRINGE_INSTALL_RF) == KEY_ON )
	{
		/*! 出针 */
			/*! 先全部关闭 */
			bsp_pv_close(E_PV_1);
			bsp_pv_close(E_PV_2);

			/*!   1 出针*/
			bsp_pv_timeout_check(E_PV_1, 1090);

		ucaData[0] = CMD_TYPE_NEEDLE_STATUS;
		ucaData[1] = 0x01;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
		
	}

	/*! 4. 枪头按钮按下，进针 ==>收针条件很宽松*/
	if(GetButtonState(NEEDLE_IN_SW) == BUTTON_OFF_TO_ON )
	{
		/*! 收针 */
		/*! 出针 */
			/*! 先全部关闭 */
			bsp_pv_close(E_PV_1);
			bsp_pv_close(E_PV_2);

			/*!   1 出针*/
			bsp_pv_timeout_check(E_PV_2, 100);

		ucaData[0] = CMD_TYPE_NEEDLE_STATUS;
		ucaData[1] = 0x00;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
	}

	/*! 5.  灌注按住，出针按钮，蒸汽按住，出蒸汽 */
	if( (GetButtonState(STREAM_GENERATE_SW) == BUTTON_OFF_TO_ON) && (GetInputSensor(SALINE_WATER_SW) == KEY_ON)  && (GetInputSensor(NEEDLE_IN_SW) == KEY_ON) ) 
	{
		/*! 出蒸气 */
		//推动无菌水电机
		bsp_motor_cmd_move_speed(WATER_PERFUSION_MOTOR, SALINE_PERFUSION_MOTOR_SPEES_LEVEL2);

		//打开蒸汽开关

		ucaData[0] = CMD_TYPE_WATER_BTN_STATUS;
		ucaData[1] = 0x01;
		send_cmd_data_frame(PC_PART_UNIT_CAN_ID, CMD_PROCESS_DATA_READ, ucaData, 2);
	}


	
}


/*******************************************************************************
* Function Name  : ReportIOMsgToPC
* Description    : 上报IO信息给上位机
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void GetIOStatusData(unsigned char *ucaDataBuffer, unsigned char ucDataLen)
{
	

	
}


/*******************************************************************************
* 名称: CheckSampleRailSigleAction
* 功能: 
		 命令帧执行结果检测和处理

* 形参: 
			ucRet 命令执行结果
* 返回: 无
* 说明: 无
********************************************************************************/
static void DebugPrintIO_Info(void)
{
	PRO_DEBUG(INFO_DEBUG, ("SYRINGE_INSTALL_RF is:%d\r\n", GetInputSensor(SYRINGE_INSTALL_RF)) );
	PRO_DEBUG(INFO_DEBUG, ("SALINE_WATER_SW is:%d\r\n",  GetInputSensor(SALINE_WATER_SW)) );
	PRO_DEBUG(INFO_DEBUG, ("NEEDLE_OUT_SW is:%d\r\n", GetInputSensor(NEEDLE_OUT_SW)) );
//	PRO_DEBUG(INFO_DEBUG, ("Z_RESET is:%d\r\n", GetInputSensor(Z_RESET)) );
//	PRO_DEBUG(INFO_DEBUG, ("SLICE_BOX_RF1 is:%d\r\n",    GetInputSensor(SLICE_BOX_RF1)) );
	PRO_DEBUG(INFO_DEBUG, ("SLICE_BOX_RF2 is:%d\r\n",    GetInputSensor(SLICE_BOX_RF2)) );
	PRO_DEBUG(INFO_DEBUG, ("CARD_BOX_RF1  is:%d\r\n",    GetInputSensor(CARD_BOX_RF1)) );
	PRO_DEBUG(INFO_DEBUG, ("CARD_BOX_RF2 is:%d\r\n",     GetInputSensor(CARD_BOX_RF2)) );
	PRO_DEBUG(INFO_DEBUG, ("\r\n") );
}



