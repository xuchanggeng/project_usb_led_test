/******************** (C) COPYRIGHT 2018 XuchangGeng ******************************
* File Name          : BarCodeDriver.c
* Author             : XuchangGeng
* Version            : V1.0
* Date               : 28/06/2018
* Description        : use firmware lib version: V3.5.1 06/13/2018
********************************************************************************
* 
*******************************************************************************/

#ifndef _MAINTAINMAIN_H__
#define _MAINTAINMAIN_H__

/*! 定时事件控制结构体 */
typedef struct _tag_timer_io_event_t
{
	uint8_t ucTimerEventFlag;
	uint16_t unTimerEventTick;
	uint16_t unTimerEventInterval;
}timer_io_event_t;

	
	
	

void GetIOStatusData(unsigned char *ucaDataBuffer, unsigned char ucDataLen);

void Set_SendIOStateFlag(unsigned char ucTemp);

void MaintainMain(void);

void MaintainMainInit(void);

void SetMasterMachineState(unsigned char ucTemp);
 
void SetFaceReagentStateReportFlag(unsigned char ucTemp);

#endif  /*_MAINTAINMAIN_H__*/

