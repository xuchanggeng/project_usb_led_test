#include "EXFunctionIO.h"
#include "InputIOMain.h"
#include "includes.h"
#include "IO_Def.h"
#include "bsp_eeprom.h"

unsigned int  g_uiCounter_Left        = 0;
unsigned int  g_uiCounter_Right       = 0;
extern unsigned char CounterLeftFlag;
extern unsigned char CounterRightFlag;


/*******************************************************************************
* Function Name  : InputIOMainInit
* Description    : 输入事件处理的初始化
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void InputIOMainInit(void)
{
	g_uiCounter_Left  = 0;
	g_uiCounter_Right = 0;
	CounterLeftFlag  = 0;
	CounterRightFlag = 0;
}

/*******************************************************************************
* Function Name  : InputIOMain
* Description    : 输入事件的处理
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void InputIOMain(void)
{
	
}


/*******************************************************************************
* Function Name  : GetCounterLeft
* Description    : 得到左计数
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
unsigned int GetCounterLeft(void)
{
	return g_uiCounter_Left;
}

/*******************************************************************************
* Function Name  : InputIOMain
* Description    : 得到右计数
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
unsigned int GetCounterRight(void)
{
	return g_uiCounter_Right;
}


/*******************************************************************************
* Function Name  : GetCounterLeftFlag
* Description    : 得到左计数器标志
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
unsigned char GetCounterLeftFlag(void)
{
	return CounterLeftFlag;
}

/*******************************************************************************
* Function Name  : GetCounterRightFlag
* Description    : 得到右计数标志
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
unsigned char GetCounterRightFlag(void)
{
  return CounterRightFlag;
}


