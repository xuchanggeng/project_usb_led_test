#ifndef _INPUTIOMAIN_H__
#define _INPUTIOMAIN_H__


void InputIOMainInit(void);
void InputIOMain(void);
void InputCounterMain(void);
void INCCounterLeft(void);
void INCCounterRight(void);

unsigned int GetCounterLeft(void);

unsigned int GetCounterRight(void);
	
//void OpticalSensorPrint(void);
unsigned char GetCounterLeftFlag(void);

unsigned char GetCounterRightFlag(void);

#endif  /*_INPUTIOMAIN_H__*/

