#ifndef _IO_Def_H__
#define _IO_Def_H__


///////////////////////SENSOR输入IO//////////////////////////////

/*! 自动进样传感器输入光耦 */
/*! 按键输入 */
#define SENSOR_INPUT_X00    GPIOB,GPIO_Pin_4  //X00
#define SENSOR_INPUT_X01    GPIOB,GPIO_Pin_5   //X01
#define SENSOR_INPUT_X02    GPIOB,GPIO_Pin_6   //X02
#define SENSOR_INPUT_X03    GPIOB,GPIO_Pin_7   //X03
#define SENSOR_INPUT_X04    GPIOB,GPIO_Pin_8   //X04
#define SENSOR_INPUT_X05    GPIOB,GPIO_Pin_9   //X05

/*! IO传感器输入 */
#define SENSOR_INPUT_X06    GPIOA,GPIO_Pin_5   //X06
#define SENSOR_INPUT_X07    GPIOA,GPIO_Pin_6   //X07
#define SENSOR_INPUT_X08    GPIOA,GPIO_Pin_7   //X08
#define SENSOR_INPUT_X09    GPIOB,GPIO_Pin_0   //X09
#define SENSOR_INPUT_X10    GPIOB,GPIO_Pin_1   //X10
#define SENSOR_INPUT_X11    GPIOB,GPIO_Pin_2   //X11

/*! 未用到的信号 */
#define SENSOR_INPUT_X12    GPIOB,GPIO_Pin_5   //X12
#define SENSOR_INPUT_X13    GPIOE,GPIO_Pin_0   //X13
#define SENSOR_INPUT_X14    GPIOE,GPIO_Pin_1   //X14
#define SENSOR_INPUT_X15    GPIOE,GPIO_Pin_2   //X15
#define SENSOR_INPUT_X16    GPIOE,GPIO_Pin_3   //X16
#define SENSOR_INPUT_X17    GPIOE,GPIO_Pin_4   //X17
#define SENSOR_INPUT_X18    GPIOE,GPIO_Pin_5   //X18
#define SENSOR_INPUT_X19    GPIOE,GPIO_Pin_6   //X19
#define SENSOR_INPUT_X20    GPIOG,GPIO_Pin_7   //X20
#define SENSOR_INPUT_X21    GPIOG,GPIO_Pin_8   //X21
#define SENSOR_INPUT_X22    GPIOG,GPIO_Pin_7   //X22
#define SENSOR_INPUT_X23    GPIOG,GPIO_Pin_8   //X23
#define SENSOR_INPUT_X24    GPIOG,GPIO_Pin_7   //X24
#define SENSOR_INPUT_X25    GPIOB,GPIO_Pin_8   //X25
#define SENSOR_INPUT_X26    GPIOB,GPIO_Pin_8   //X26


#define IO_X00    GPIOD,GPIO_Pin_0   //X00
#define IO_X01    GPIOD,GPIO_Pin_1   //X01
#define IO_X02    GPIOD,GPIO_Pin_2   //X02
#define IO_X03    GPIOD,GPIO_Pin_3   //X03
#define IO_X04    GPIOD,GPIO_Pin_4   //X04
#define IO_X05    GPIOD,GPIO_Pin_5   //X05
#define IO_X06    GPIOD,GPIO_Pin_6   //X06
#define IO_X07    GPIOD,GPIO_Pin_7   //X07
#define IO_X10    GPIOD,GPIO_Pin_8    //X10
#define IO_X11    GPIOD,GPIO_Pin_9    //X11
#define IO_X12    GPIOD,GPIO_Pin_10   //X12
#define IO_X13    GPIOD,GPIO_Pin_11   //X13
#define IO_X14    GPIOD,GPIO_Pin_12   //X06
#define IO_X15    GPIOD,GPIO_Pin_13   //X07
#define IO_X16    GPIOD,GPIO_Pin_14   //X10
#define IO_X17    GPIOD,GPIO_Pin_15   //X11
#define IO_X18    GPIOD,GPIO_Pin_16   //X12
#define IO_X19    GPIOD,GPIO_Pin_17   //X13
#define IO_X20    GPIOD,GPIO_Pin_18   //X11
#define IO_X21    GPIOD,GPIO_Pin_8    //X12
#define IO_X22    GPIOD,GPIO_Pin_7    //X13


///////////////////////Y输出IO///////////////////////////////
#define IO_Y00    GPIOA,GPIO_Pin_4   //Y00 
#define IO_Y01    GPIOA,GPIO_Pin_5   //Y01 
#define IO_Y02    GPIOA,GPIO_Pin_6   //Y02 
#define IO_Y03    GPIOA,GPIO_Pin_7   //Y03 
#define IO_Y04    GPIOC,GPIO_Pin_5   //Y04 
#define IO_Y05    GPIOB,GPIO_Pin_0   //Y05 
#define IO_Y06    GPIOB,GPIO_Pin_1   //Y06 
#define IO_Y07    GPIOF,GPIO_Pin_11  //Y07  
#define IO_DC_MOTOR   GPIOE,GPIO_Pin_14



//IO初始化
void IO_Configuration(void);

#endif /* _IO_Def_H__ */

