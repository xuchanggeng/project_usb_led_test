/**
  ******************************************************************************
  * @文件   Com_Prase.c
  * @作者   Xcg
  * @版本   V0.00.1
  * @日期   7-16-2020
  * @简介   XXX module.    
  ******************************************************************************
  * @注意
  *		
  *		Copyright(c) 2020-2030 www.geniusmedica.com
  ******************************************************************************  
  */ 

#include "EXFunctionIO.h"
#include "InputIOMain.h"
#include "IO_Def.h"
#include "bsp_eeprom.h"
#include "MaintainMain.h"
#include "Com_FrameMake.h"
#include "button_event_process.h"


#define SAMPLE_REAL_TIME_INTERVAL               				30				//定时器时间间隔
#define BUTTON_EVENT_NUMBERS                 					10				//按钮处理数量


/*! 按键处理定时器 */	
static timer_io_event_t s_stSampleRealTimeState = {1, 0, SAMPLE_REAL_TIME_INTERVAL};


/*! 按钮处理函数的定义 */
static void button_event_num0_handle(uint8_t);
static void button_event_num1_handle(uint8_t);
static void button_event_num2_handle(uint8_t);
//static void button_event_num3_handle(uint8_t);
//static void button_event_num4_handle(uint8_t);
//static void button_event_num5_handle(uint8_t);


/*! 按钮处理事件函数（数组模式） */
static button_event_t s_stButtonEventProcess[BUTTON_EVENT_NUMBERS] = 
{
	/*! 序号，使能状态，触发状态，处理函数 */
	{0, ENABLED, BUTTON_OFF_TO_ON, button_event_num0_handle},
	{1, ENABLED, BUTTON_OFF_TO_ON, button_event_num1_handle},
	{2, ENABLED, BUTTON_OFF_TO_ON, button_event_num2_handle},
	{3, ENABLED, BUTTON_OFF_TO_ON, 0},
	{4, ENABLED, BUTTON_OFF_TO_ON, 0},
	{5, ENABLED, BUTTON_OFF_TO_ON, 0},
	{0, 0, 0, 0},
};



/*******************************************************************************
* 名称: button_event_num0_handle
* 功能: 
		 按钮事件初始化
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void button_event_num0_handle(uint8_t ucBtnNum)
{
	(void)ucBtnNum;
}

/*******************************************************************************
* 名称: button_event_num0_handle
* 功能: 
		 按钮事件初始化
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void button_event_num1_handle(uint8_t ucBtnNum)
{
	(void)ucBtnNum;
}

/*******************************************************************************
* 名称: button_event_num0_handle
* 功能: 
		 按钮事件初始化
* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void button_event_num2_handle(uint8_t ucBtnNum)
{
	(void)ucBtnNum;
}





/*******************************************************************************
* 名称: button_event_init
* 功能: 
		 按钮事件初始化

* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void button_event_init(void)
{


}

/*******************************************************************************
* 名称: button_event_loop
* 功能: 
		 按钮事件循环

* 形参: 无
* 返回: 无
* 说明: 无
********************************************************************************/
void button_event_loop(void)
{
	uint16_t unIndex = 0;
	uint16_t unBtnState = 0;

	for(unIndex = 0; unIndex < BUTTON_EVENT_NUMBERS; unIndex++)
	{
		/*! 按键没有使用，就直接检测下一个按钮 */
		if(s_stButtonEventProcess[unIndex].ucButtonState == DISENABLE)
		{
			continue;
		}

        /*! 按钮使能 */
        if(s_stButtonEventProcess[unIndex].ucButtonState == ENABLED)
        {
			unBtnState = GetInputSensor(s_stButtonEventProcess[unIndex].ucButtonNum);

            /*! 按钮状态类型匹配，事件处理函数不为NULL */
            if(s_stButtonEventProcess[unIndex].unButtonTriggerType == unBtnState && s_stButtonEventProcess[unIndex].pbutton_event_handle_fun != NULL)
            {
				/*! 执行事件处理函数 */
                s_stButtonEventProcess[unIndex].pbutton_event_handle_fun( unIndex );
            }

        }
	}

}

/*******************************************************************************
* 名称: set_one_button_event_state
* 功能: 
		 设置按钮事件处理状态

* 形参: 
			ucButtonNo 按钮序号
			ucState 按钮状态
* 返回: 无
* 说明: 无
********************************************************************************/
void set_one_button_event_state(uint8_t ucButtonNo, uint8_t ucState)
{
	s_stButtonEventProcess[ucButtonNo].ucButtonState = ucState;
}

/*******************************************************************************
* 名称: is_button_event_empty
* 功能: 
		 判断一个是否是空位

* 形参: 
			stBtnEvent 命令执行结unIndex果
* 返回: 无
* 说明: 无
********************************************************************************/
uint8_t is_button_event_empty(uint16_t ucButtonNo)
{
	uint8_t ucRet = 0;

	if( s_stButtonEventProcess[ucButtonNo].ucButtonState == DISENABLE && s_stButtonEventProcess[ucButtonNo].pbutton_event_handle_fun == NULL)
	{
		ucRet = 1;
	}

	return ucRet;
}

/*******************************************************************************
* 名称: add_one_button_event
* 功能: 
		 添加一个按钮事件处理

* 形参: 
			stBtnEvent 命令执行结果
* 返回: 无
* 说明: 无
********************************************************************************/
void add_one_button_event(button_event_t *stBtnEvent)
{
	uint16_t unIndex = 0;

    /*! 查找出第一个空闲的位置 */
	for(unIndex = 0; unIndex < BUTTON_EVENT_NUMBERS; unIndex++)
	{
		if( is_button_event_empty(unIndex) )
            {
                break;
            }
	}

    /* 添加该按钮处理函数 */
    s_stButtonEventProcess[unIndex].ucButtonNum              = stBtnEvent->ucButtonNum;
    s_stButtonEventProcess[unIndex].ucButtonState            = stBtnEvent->ucButtonState;
    s_stButtonEventProcess[unIndex].unButtonTriggerType      = stBtnEvent->unButtonTriggerType;
    s_stButtonEventProcess[unIndex].pbutton_event_handle_fun = stBtnEvent->pbutton_event_handle_fun;
	
}



























