/*
************************************************************************************************
主要的包含文件

文 件: INCLUDES.C ucos包含文件
作 者: Jean J. Labrosse
************************************************************************************************
*/

#ifndef __INCLUDES_H__
#define __INCLUDES_H__

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>

#include "ucos_ii.h"
#include "os_cpu.h"
#include "os_cfg.h"

#include <stm32f10x.h>	
#include "userdef.h"
#include "pro_type.h"
#include "pro_info.h"
#include "frame_info.h"
#include "action_info.h"
#include "confdef.h"
#include "Com_FrameMake.h"
#include "motor_pos_def.h"
#include "define.h"

#endif
